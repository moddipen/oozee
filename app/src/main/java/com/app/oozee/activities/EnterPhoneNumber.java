package com.app.oozee.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.database.CountryNameByDatabase;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.CountryModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.database.CountryNameByDatabase.getCountryId;
import static com.app.oozee.database.CountryNameByPrefix.getCountryFullName;
import static com.app.oozee.database.CountryNameByPrefix.insertAllCountries;
import static com.app.oozee.utils.Utility.COUNTRIESLOADED;
import static com.app.oozee.utils.Utility.COUNTRYID;
import static com.app.oozee.utils.Utility.getTypeFace;

public class EnterPhoneNumber extends BaseActivity {

    private EditText phoneNumberEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_enter_phone_number);

        TextView termsTextView = findViewById(R.id.termsTextView);
        TextView enterTextView = findViewById(R.id.enterTextView);
        Button proceedButton = findViewById(R.id.proceedButton);
        phoneNumberEditText = findViewById(R.id.phoneNumberEditText);

        Typeface typeface = getTypeFace(this);
        termsTextView.setTypeface(typeface);
        enterTextView.setTypeface(typeface);
        proceedButton.setTypeface(typeface);
        phoneNumberEditText.setTypeface(typeface);

        /*String sourceString = "By clicking on proceed you are agreeing with our " +
                "<b>" + "terms and conditions." + "</b>";*/

        //termsTextView.setText(Html.fromHtml(sourceString));

        termsTextView.setMovementMethod(LinkMovementMethod.getInstance());
        /*Spanned text = Html.fromHtml(""
                + String.format("<a href=\"%s\">Terms and Conditions</a> ", showDialogOfTerms()));*/

        String sourceString = "By clicking on proceed you are agreeing with our terms and conditions.";
        termsTextView.setText(sourceString);
        setClick(termsTextView, termsTextView.getText().toString());

        String mPhoneNumber = "";
        /*if (ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            try {
                TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                mPhoneNumber = tMgr.getLine1Number();

                if (mPhoneNumber != null) {
                    if (mPhoneNumber.contains("+91")) {
                        mPhoneNumber = mPhoneNumber.replace("+91", "");
                    }
                }
                Log.d("mPhoneNumber", mPhoneNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        phoneNumberEditText.setText("+91 | " + mPhoneNumber);
        Selection.setSelection(phoneNumberEditText.getText(), phoneNumberEditText.getText().length());

        phoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("+91 | ")) {
                    phoneNumberEditText.setText("+91 | ");
                    Selection.setSelection(phoneNumberEditText.getText(), phoneNumberEditText.getText().length());
                }
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = phoneNumberEditText.getText().toString().trim();
                number = number.replace("+91 | ", "");

                if (number.equalsIgnoreCase("")) {
                    showToast("Please enter number");
                    return;
                }

                if (number.length() < 9) {
                    showToast("Please enter valid number");
                    return;
                }

                Intent intent = new Intent(EnterPhoneNumber.this, ConfirmDoneNumber.class);
                intent.putExtra("phoneNumber", number);
                startActivity(intent);
                finish();
            }
        });

        boolean countriesLoaded = OozeePreferences.getInstance().getPref().getBoolean(COUNTRIESLOADED, false);
        if (!countriesLoaded) {
            getCountries(this, "countries");
        }
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("countries")) {
            ArrayList<CountryModel> countryModels = CountryModel.getCountryList(busModel.jsonElement);
            CountryNameByDatabase.insertAllCountriesDb(new DatabaseHelper(EnterPhoneNumber.this), countryModels);
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();
            insertAllCountries(new DatabaseHelper(this));
            String countryName = getCountryFullName(new DatabaseHelper(this), countryCodeValue);
            int id = getCountryId(new DatabaseHelper(EnterPhoneNumber.this), countryName.toLowerCase());

            OozeePreferences.getInstance().getEditor().putBoolean(COUNTRIESLOADED, true).apply();
            OozeePreferences.getInstance().getEditor().putInt(COUNTRYID, id).apply();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    public void showDialogOfTerms() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permissions);

        TextView continueTextView = dialog.findViewById(R.id.continueTextView);
        TextView textView = dialog.findViewById(R.id.textView);
        RelativeLayout relativeLayout = dialog.findViewById(R.id.relativeLayout);
        ImageView imageView = dialog.findViewById(R.id.imageView);

        imageView.setVisibility(View.GONE);
        relativeLayout.setBackground(getResources().getDrawable(R.drawable.rounded_corners_less));
        textView.setPadding(0, 25, 0, 0);

        Typeface typeface = getTypeFace(this);
        continueTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        textView.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

        continueTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isDestroyed()) {
                    dialog.dismiss();
                }
            }
        });

        if (!isDestroyed() && dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.show();
        }
    }

    private void setClick(TextView textView, String s) {
        SpannableString ss = new SpannableString(s);
        ClickableSpan clickableSpan = new ClickableSpan() {

            @Override
            public void onClick(@NonNull View textView) {
                showDialogOfTerms();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        ss.setSpan(bss, s.indexOf("terms"), s.length(), 0);

        ss.setSpan(clickableSpan, s.indexOf("terms"), s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF"))
                , s.indexOf("terms"), s.length(), 0);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }
}