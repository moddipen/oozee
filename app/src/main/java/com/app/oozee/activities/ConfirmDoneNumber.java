package com.app.oozee.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.sinch.verification.Config;
import com.sinch.verification.InitiationResult;
import com.sinch.verification.PhoneNumberUtils;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;

import static com.app.oozee.utils.Utility.getTypeFace;

public class ConfirmDoneNumber extends BaseActivity {

    private RelativeLayout relativeLayout;
    private TextView textView;
    private ImageView imageView;
    private YoYo.YoYoString yoYo;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_done_number);

        textView = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);
        relativeLayout = findViewById(R.id.relativeLayout);

        Typeface typeface = getTypeFace(this);
        textView.setTypeface(typeface);

        yoYo = YoYo.with(Techniques.Shake).duration(600).repeat(100).playOn(imageView);

        phoneNumber = getIntent().getStringExtra("phoneNumber");

        Config config = SinchVerification.config().applicationKey("1c8e845a-de9e-4e51-aca5-3b040b960189")
                .context(getApplicationContext()).build();
        String defaultRegion = PhoneNumberUtils.getDefaultCountryIso(this);
        String phoneNumberInE164 = PhoneNumberUtils.formatNumberToE164(phoneNumber, defaultRegion);
        Verification verification = SinchVerification.createFlashCallVerification(config, phoneNumberInE164, listener);
        verification.initiate();
    }

    VerificationListener listener = new VerificationListener() {
        @Override
        public void onInitiated(InitiationResult result) {
        }

        @Override
        public void onInitiationFailed(Exception e) {
            Log.d("failedfailedfailed", "ueahhhhhh");
        }

        @Override
        public void onVerified() {
            if (yoYo != null && yoYo.isRunning()) {
                yoYo.stop();
            }
            textView.setTextColor(getResources().getColor(R.color.blackColor));
            textView.setText("Done");
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.done));
            relativeLayout.setBackgroundColor(getResources().getColor(R.color.grayColor));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(ConfirmDoneNumber.this, SignUpScreen.class);
                    intent.putExtra("phoneNumber", phoneNumber);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        }

        @Override
        public void onVerificationFailed(Exception e) {
            showToast("Failed to verify phone number");
            finish();
        }

        @Override
        public void onVerificationFallback() {
            showToast("Failed to verify phone number");
            finish();
        }
    };
}
