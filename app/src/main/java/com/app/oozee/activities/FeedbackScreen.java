package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.utils.Utility;

public class FeedbackScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_screen);

        Button submitButton = findViewById(R.id.submitButton);
        EditText descriptionEditText = findViewById(R.id.descriptionEditText);
        EditText titleEdittext = findViewById(R.id.titleEdittext);
        ImageView backImageView = findViewById(R.id.backImageView);

        Typeface typeface = Utility.getTypeFace(this);

        submitButton.setTypeface(typeface);
        titleEdittext.setTypeface(typeface);
        descriptionEditText.setTypeface(typeface);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showToast("Feedback Sent");
                finish();
            }
        });

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
