package com.app.oozee.activities;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.SearchAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.database.ContactsModel;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.SearchContactsDb;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.SearchContactModel;
import com.app.oozee.models.Sms;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.MyProgressDialog;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TooManyListenersException;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.database.ContactsModel.checkIfExists;
import static com.app.oozee.database.ContactsModel.getAllOozeeContacts;
import static com.app.oozee.database.SearchContactsDb.deleteSearchHistory;
import static com.app.oozee.database.SearchContactsDb.getAllSearchContacts;
import static com.app.oozee.models.SearchContactModel.getList;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.getTypeFace1;

public class SearchScreenActivity extends BaseActivity {

    private ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
    private ArrayList<com.app.oozee.database.ContactsModel> oozeeContacts = new ArrayList<>();
    private ArrayList<Sms> messages = new ArrayList<>();
    private RecyclerView contactsRecyclerView;
    private RecyclerView oozeeRecyclerView;
    private RecyclerView messagesRecyclerView;
    private TextView nocontactsTextView;
    private TextView nooozzeeTextView;
    private TextView noMessagesTextView;
    private ACProgressFlower mProgressDialog;
    private DatabaseHelper databaseHelper;
    private EditText searchView;
    private ImageView closeImageView;
    private TextView historyTextView;
    private TextView clearHistoryTextView;
    private RecyclerView historyRecyclerView;
    private ArrayList<SearchContactModel> searchContactsModels = new ArrayList<>();
    private ArrayList<SearchContactModel> searchOozeeModels = new ArrayList<>();
    private ArrayList<SearchContactModel> searchMessagesModels = new ArrayList<>();
    private ArrayList<SearchContactsDb> searchContactsDbs = new ArrayList<>();
    private ArrayList<SearchContactModel> historyModels = new ArrayList<>();
    private SearchAdapter contactsSearchAdapter;
    private SearchAdapter oozeeSearchAdapter;
    private SearchAdapter messagesSearchAdapter;
    private SearchAdapter historyAdapter;
    private ProgressBar progressBar;
    TextView oozeeTextView, contactsTextView, messagesTextView;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_screen);

        databaseHelper = new DatabaseHelper(getApplicationContext());

        progressBar = findViewById(R.id.progressBar);
        contactsTextView = findViewById(R.id.contactsTextView);
        oozeeTextView = findViewById(R.id.oozeeTextView);
        messagesTextView = findViewById(R.id.messagesTextView);
        ImageView backImageView = findViewById(R.id.backImageView);
        searchView = findViewById(R.id.searchView);
        closeImageView = findViewById(R.id.closeImageView);
        historyTextView = findViewById(R.id.historyTextView);
        historyRecyclerView = findViewById(R.id.historyRecyclerView);
        clearHistoryTextView = findViewById(R.id.clearHistoryTextView);

        contactsRecyclerView = findViewById(R.id.contactsRecyclerView);
        nocontactsTextView = findViewById(R.id.nocontactsTextView);

        oozeeRecyclerView = findViewById(R.id.oozeeRecyclerView);
        nooozzeeTextView = findViewById(R.id.nooozzeeTextView);

        messagesRecyclerView = findViewById(R.id.messagesRecyclerView);
        noMessagesTextView = findViewById(R.id.noMessagesTextView);

        Typeface typeface = getTypeFace(this);
        nocontactsTextView.setTypeface(typeface);
        nooozzeeTextView.setTypeface(typeface);
        noMessagesTextView.setTypeface(typeface);

        Typeface typeface1 = getTypeFace1(this);
        historyTextView.setTypeface(typeface1);
        contactsTextView.setTypeface(typeface1);
        oozeeTextView.setTypeface(typeface1);
        messagesTextView.setTypeface(typeface1);
        clearHistoryTextView.setTypeface(typeface1);

        contactsTextView.setVisibility(View.GONE);
        messagesTextView.setVisibility(View.GONE);
        oozeeTextView.setVisibility(View.GONE);

        contactsSearchAdapter = new SearchAdapter(this, searchContactsModels, false, false);
        contactsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        contactsRecyclerView.setAdapter(contactsSearchAdapter);

        oozeeSearchAdapter = new SearchAdapter(this, searchOozeeModels, true, false);
        oozeeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        oozeeRecyclerView.setAdapter(oozeeSearchAdapter);

        messagesSearchAdapter = new SearchAdapter(this, searchMessagesModels, false, true);
        messagesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        messagesRecyclerView.setAdapter(messagesSearchAdapter);

        historyAdapter = new SearchAdapter(this, historyModels, false, false);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        historyRecyclerView.setAdapter(historyAdapter);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        progressBar.setVisibility(View.GONE);

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setText("");
            }
        });

        clearHistoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSearchHistory(databaseHelper);
                searchContactsDbs.clear();
                checkAndSetVisibility(historyTextView, 0);
                checkAndSetVisibility(clearHistoryTextView, 0);
                checkAndSetVisibility(historyRecyclerView, 0);
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    closeImageView.setVisibility(View.GONE);
                } else {
                    closeImageView.setVisibility(View.VISIBLE);

                    checkAndSetVisibility(historyTextView, 0);
                    checkAndSetVisibility(clearHistoryTextView, 0);
                    checkAndSetVisibility(historyRecyclerView, 0);
                }

                if (s.length() == 0 || s.length() < 4) {
                    checkAndSetVisibility(oozeeRecyclerView, 0);
                    checkAndSetVisibility(contactsRecyclerView, 0);
                    checkAndSetVisibility(messagesRecyclerView, 0);

                    checkAndSetVisibility(contactsTextView, 0);
                    checkAndSetVisibility(messagesTextView, 0);
                    checkAndSetVisibility(oozeeTextView, 0);
                } else /*if (s.length() >= 10)*/ {
                    checkAndSetVisibility(oozeeRecyclerView, 1);
                    checkAndSetVisibility(contactsRecyclerView, 1);
                    checkAndSetVisibility(messagesRecyclerView, 1);

                    checkAndSetVisibility(contactsTextView, 1);
                    checkAndSetVisibility(messagesTextView, 1);
                    checkAndSetVisibility(oozeeTextView, 1);

                    setContactsAdapter(s.toString());
                    setMessagesAdapter(s.toString());
                    setOozeeAdapter(s.toString());

                    boolean atleastOneAlpha = s.toString().matches(".*[a-zA-Z]+.*");

                    if (s.length() >= 10 && !atleastOneAlpha) {
                        progressBar.setVisibility(View.VISIBLE);
                        nooozzeeTextView.setVisibility(View.GONE);
                        SearchContactModel searchContactModel = new SearchContactModel(19, 99, s.toString());
                        searchCall(SearchScreenActivity.this, searchContactModel, "searchCall");
                    }
                }
                /*else {
                   // checkAndSetVisibility(oozeeRecyclerView, 1);
                    checkAndSetVisibility(contactsRecyclerView, 1);
                    checkAndSetVisibility(messagesRecyclerView, 1);

                    checkAndSetVisibility(contactsTextView, 1);
                    checkAndSetVisibility(messagesTextView, 1);
                    //checkAndSetVisibility(oozeeTextView, 1);

                    setContactsAdapter(s.toString());
                    setMessagesAdapter(s.toString());
                    //setOozeeAdapter(s.toString());

                    boolean atleastOneAlpha = s.toString().matches(".*[a-zA-Z]+.*");

                    if (s.length() >= 10 && !atleastOneAlpha) {
                        progressBar.setVisibility(View.VISIBLE);
                        nooozzeeTextView.setVisibility(View.GONE);
                        SearchContactModel searchContactModel = new SearchContactModel(19, 99, s.toString());
                        searchCall(SearchScreenActivity.this, searchContactModel, "searchCall");
                    }
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        ArrayList<LocalContactsModel> contactsModelss = getAllContacs(databaseHelper);

        Collections.sort(contactsModelss, new Comparator<LocalContactsModel>() {
            @Override
            public int compare(LocalContactsModel o1, LocalContactsModel o2) {
                return o1.name.compareToIgnoreCase(o2.name);
            }
        });

        String initial = "";
        for (int i = 0; i < contactsModelss.size(); i++) {
            String name = contactsModelss.get(i).name;
            String newInitial = getStringInitial(name);
            if (!initial.equalsIgnoreCase(newInitial)) {
                initial = newInitial;
            }
            contactsModels.add(new LocalContactsModel(1, name, newInitial, contactsModelss.get(i).number,
                    contactsModelss.get(i).dir));
        }

        new getSms().execute();
    }

    private void setContactsAdapter(String match) {
        searchContactsModels.clear();
        for (int i = 0; i < contactsModels.size(); i++) {
            if (contactsModels.get(i).name.toLowerCase().contains(match.toLowerCase()) ||
                    contactsModels.get(i).number.toLowerCase().contains(match.toLowerCase())) {
                searchContactsModels.add(new SearchContactModel(contactsModels.get(i).name,
                        contactsModels.get(i).number, contactsModels.get(i).image));
            }
        }

        if (searchContactsModels.size() > 0) {
            contactsTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
            checkAndSetVisibility(contactsRecyclerView, 1);
            checkAndSetVisibility(nocontactsTextView, 0);
        } else {
            checkAndSetVisibility(contactsRecyclerView, 0);
            checkAndSetVisibility(nocontactsTextView, 1);
        }

        contactsSearchAdapter.notifyDataSetChanged();
    }

    private void getHistory() {
        historyModels.clear();
        for (int i = 0; i < searchContactsDbs.size(); i++) {
            historyModels.add(new SearchContactModel(searchContactsDbs.get(i).name,
                    searchContactsDbs.get(i).number, searchContactsDbs.get(i).image));
        }

        Collections.reverse(historyModels);
        if (historyModels.size() == 0) {
            checkAndSetVisibility(historyTextView, 0);
            checkAndSetVisibility(clearHistoryTextView, 0);
            checkAndSetVisibility(historyRecyclerView, 0);
        }
    }

    private void setOozeeAdapter(String match) {

        searchOozeeModels.clear();
        for (int i = 0; i < oozeeContacts.size(); i++) {

            String number = oozeeContacts.get(i).number;
            if (number == null) {
                number = "";
            }
            number = number.toLowerCase();
            if (/*name.contains(match.toLowerCase()) ||*/ number.contains(match.toLowerCase())) {
                searchOozeeModels.add(new SearchContactModel(oozeeContacts.get(i).name,
                        oozeeContacts.get(i).number));
            }
        }

        if (searchOozeeModels.size() > 0) {
            oozeeTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
            checkAndSetVisibility(oozeeRecyclerView, 1);
            checkAndSetVisibility(nooozzeeTextView, 0);
        } else {
            checkAndSetVisibility(oozeeRecyclerView, 0);
            checkAndSetVisibility(nooozzeeTextView, 1);
        }

        oozeeSearchAdapter.notifyDataSetChanged();
    }

    private void setMessagesAdapter(String match) {
        searchMessagesModels.clear();
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i) != null && messages.get(i).getAddress() != null) {
                if (messages.get(i).getAddress().toLowerCase().contains(match.toLowerCase())/* ||
                    messages.get(i).getMsg().toLowerCase().contains(match.toLowerCase())*/) {
                    searchMessagesModels.add(new SearchContactModel(messages.get(i).getAddress(),
                            messages.get(i).getMsg()));
                }
            }
        }

        if (searchMessagesModels.size() > 0) {
            messagesTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
            checkAndSetVisibility(messagesRecyclerView, 1);
            checkAndSetVisibility(noMessagesTextView, 0);
        } else {
            checkAndSetVisibility(messagesRecyclerView, 0);
            checkAndSetVisibility(noMessagesTextView, 1);
        }

        messagesSearchAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("searchCall")) {
            progressBar.setVisibility(View.GONE);
            nooozzeeTextView.setVisibility(View.GONE);
            ArrayList<SearchContactModel> searchContactModels = getList(busModel.jsonElement);

            if (searchContactModels.size() > 0) {
                addToContactsRefresh(searchContactModels);
                checkIfExists(databaseHelper, searchContactModels);

                checkAndSetVisibility(oozeeRecyclerView, 1);
                checkAndSetVisibility(nooozzeeTextView, 0);
            } else {
                checkAndSetVisibility(oozeeRecyclerView, 0);
                checkAndSetVisibility(nooozzeeTextView, 1);
            }

            searchOozeeModels.clear();
            searchOozeeModels.addAll(searchContactModels);
            oozeeSearchAdapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getSms extends AsyncTask<Void, Void, Void> {
        ArrayList<Sms> lstSms = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = MyProgressDialog.show(SearchScreenActivity.this, "", "");
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Uri message = Uri.parse("content://sms/");

            ContentResolver cr = getContentResolver();

            Cursor c = cr.query(message, null, null, null, null);
            int totalSMS;
            if (c != null) {
                totalSMS = c.getCount();
                if (c.moveToFirst()) {
                    for (int i = 0; i < totalSMS; i++) {
                        Sms objSms = new Sms();
                        String name = c.getString(c.getColumnIndexOrThrow("address"));
                        objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                        objSms.setAddress(name);
                        objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                        objSms.setReadState(c.getString(c.getColumnIndex("read")));
                        objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                        if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                            objSms.setFolderName("Inbox");
                        } else {
                            objSms.setFolderName("Sent");
                        }

                        if (!strings.contains(name)) {
                            strings.add(name);
                            lstSms.add(objSms);
                        }
                        c.moveToNext();
                    }
                }
                c.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            messages = lstSms;
            new getoozeeContacts().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getoozeeContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            oozeeContacts = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            searchContactsDbs = getAllSearchContacts(databaseHelper);
            oozeeContacts = getAllOozeeContacts(databaseHelper);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dismissProgressDialog(mProgressDialog);

            searchView.requestFocus();
            showKeyboard(SearchScreenActivity.this);

            getHistory();
            if (historyAdapter != null) {
                historyAdapter.notifyDataSetChanged();
            }

            getTextFromClipBoard();
        }
    }

    private void addToContactsRefresh(ArrayList<SearchContactModel> searchContactModels) {
        for (int i = 0; i < searchContactModels.size(); i++) {
            for (int k = 0; k < oozeeContacts.size(); k++) {

                String number = searchContactModels.get(i).phone_number;
                if (number == null) {
                    number = "";
                }

                if (!oozeeContacts.get(k).number.equalsIgnoreCase(number)) {
                    oozeeContacts.add(new ContactsModel(0, searchContactModels.get(i).first_name,
                            searchContactModels.get(i).phone_number));
                }
            }
        }
    }

    private void getTextFromClipBoard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (clipboard != null && clipboard.getPrimaryClip() != null &&
                clipboard.getPrimaryClip().getItemCount() > 0) {
            CharSequence text = clipboard.getPrimaryClip().getItemAt(0).getText();

            if (text != null) {
                boolean atleastOneAlpha = text.toString().matches(".*[a-zA-Z]+.*");
                if (!atleastOneAlpha) {
                    showDialogForNumber(text.toString());
                }
            }
        }
    }

    private void showDialogForNumber(final String number) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Copy number to search?");
        builder1.setCancelable(true);

        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                searchView.setText(number);
                searchView.setSelection(searchView.getText().length());
                hideKeyboard(SearchScreenActivity.this);
                dialog.cancel();
            }
        });

        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}