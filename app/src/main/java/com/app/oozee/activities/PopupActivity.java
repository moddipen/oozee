package com.app.oozee.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.ChipsAdapter;
import com.app.oozee.adapters.TagAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.TagModels;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.skyfishjy.library.RippleBackground;
import com.squareup.otto.Subscribe;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.AllContactsModel.getNameFromNumber;
import static com.app.oozee.database.TagsDb.getAllTags;
import static com.app.oozee.database.TagsDb.insertAllTags;
import static com.app.oozee.models.TagModels.getSubTagList;
import static com.app.oozee.models.TagModels.getTagList;
import static com.app.oozee.receivers.MyPhoneReceiver.Tag;
import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.Config.openWhatsapp;
import static com.app.oozee.utils.EphocTime.getTimeAgoFeed;

public class PopupActivity extends BaseActivity implements ChipsAdapter.TagSubTag,
        TagAdapter.AddTagInterface {

    private ArrayList<TagModels> tagModels = new ArrayList<>();
    private BottomSheetBehavior bottomSheetBehavior;
    private String name;
    private int tagId = 0;
    private TextView genderTextView;
    private RecyclerView chipsRecyclerView;
    private TextView textView;
    private DatabaseHelper databaseHelper;
    private LinearLayout linearLayout;
    private RelativeLayout relativeLayout;
    private ImageView one;
    private ImageView two;
    private ImageView three;
    private ImageView four;
    private ImageView five;
    private ImageView simImageView;
    private ImageView closeImageView;
    private ImageView imageoozeelogo;
    private TextView callLogTextView;
    private TextView websiteTextView;
    private TextView workTextView;
    private TextView carrierTextView;
    private TextView nameTextView;
    private TextView locationTextView;
    private TextView numberTextView;
    private ProfileModel profileModel;
    private RippleBackground rippleBackground;
    private RippleBackground rippleBackground2;
    private RippleBackground rippleBackground3;
    private RelativeLayout popUpRelativeLayout;
    private RelativeLayout rvParentView;
    private RecyclerView tagLinear;
    private ArrayList<TagModels> profileTagsModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.activity_popup);

        startAds();

        nameTextView = findViewById(R.id.nameTextView);
        locationTextView = findViewById(R.id.locationTextView);
        numberTextView = findViewById(R.id.numberTextView);
        imageoozeelogo = findViewById(R.id.imageoozeelogo);
        callLogTextView = findViewById(R.id.callLogTextView);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        genderTextView = findViewById(R.id.genderTextView);
        relativeLayout = findViewById(R.id.relativeLayout);
        closeImageView = findViewById(R.id.closeImageView);
        websiteTextView = findViewById(R.id.websiteTextView);
        workTextView = findViewById(R.id.workTextView);
        carrierTextView = findViewById(R.id.carrierTextView);
        simImageView = findViewById(R.id.simImageView);
        tagLinear = findViewById(R.id.tagLinear);
        popUpRelativeLayout = findViewById(R.id.popUpRelativeLayout);
        rvParentView = findViewById(R.id.rvParentView);
        CircleImageView imageView1 = findViewById(R.id.imageViewCircle1);
        CircleImageView imageView2 = findViewById(R.id.imageViewCircle2);
        CircleImageView imageView3 = findViewById(R.id.imageViewCircle3);

        RelativeLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        textView = findViewById(R.id.textView);
        chipsRecyclerView = findViewById(R.id.chipsRecyclerView);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        profileModel = getIntent().getParcelableExtra("profileModel");

        name = profileModel.first_name + " " + profileModel.last_name;
        String photo = profileModel.photo;

        boolean callEnded = getIntent().getBooleanExtra("callEnded", false);
        rippleBackground = findViewById(R.id.imageView);
        rippleBackground2 = findViewById(R.id.imageView2);
        rippleBackground3 = findViewById(R.id.imageView3);


        Log.d(Tag, callEnded + " callEnded");
        linearLayout = findViewById(R.id.linearLayout);

        if (callEnded) {
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            linearLayout.setVisibility(View.GONE);
        }

        one.setOnClickListener(v -> callPhone(this, profileModel.number));

        two.setOnClickListener(v -> {
            Intent intent = new Intent(this, MessagesScreen.class);
            intent.putExtra("address", profileModel.number);
            intent.putExtra("smsid", "0");
            startActivity(intent);
        });

        three.setOnClickListener(v -> openWhatsapp(this, profileModel.number));

        four.setOnClickListener(v -> {
            Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
            contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, "")
                    .putExtra(ContactsContract.Intents.Insert.PHONE, profileModel.number);
            startActivityForResult(contactIntent, 1);
        });

        five.setOnClickListener(v -> {
            Intent intent = new Intent(this, ProfileScreen.class);
            intent.putExtra("contactsModel", new LocalContactsModel(name, profileModel.number));
            startActivity(intent);
        });

        runOnUiThread(() -> {
            String s = "Call received " + getTimeAgoFeed(Long.parseLong(getCallLogDetail()));
            callLogTextView.setText(s);
        });

        databaseHelper = new DatabaseHelper(this);

        setTagAdapter(profileModel.tag);

        numberTextView.setText(profileModel.number);

        if (profileModel.service_provider != null && !profileModel.service_provider.equalsIgnoreCase("")) {
            String carrier = "Mobile - " + profileModel.service_provider;
            carrierTextView.setText(carrier);
        } else {
            carrierTextView.setVisibility(View.GONE);
        }

        if (profileModel.spam == 1) {
            startAnimation(1);
            websiteTextView.setVisibility(View.GONE);
            workTextView.setText("3526 Reported Spam");
            changeColor(5);
        } else if (profileModel.contact_user_id == 0) {
            if (profileModel.website == null || profileModel.website.equalsIgnoreCase("")) {
                websiteTextView.setVisibility(View.GONE);
            } else {
                websiteTextView.setText(profileModel.website);
            }

            if (profileModel.business == null || profileModel.business.equalsIgnoreCase("")) {
                workTextView.setVisibility(View.GONE);
            } else {
                workTextView.setText(profileModel.business);
            }
            startAnimation(2);
            changeColor(0);
        } else {
            if (profileModel.website == null || profileModel.website.equalsIgnoreCase("")) {
                websiteTextView.setVisibility(View.GONE);
            } else {
                websiteTextView.setText(profileModel.website);
            }

            if (profileModel.business == null || profileModel.business.equalsIgnoreCase("")) {
                workTextView.setVisibility(View.GONE);
            } else {
                workTextView.setText(profileModel.business);
            }
            if (!profileModel.premium) {
                if (profileModel.gender.equalsIgnoreCase("Male")) {
                    startAnimation(2);
                    changeColor(0);
                } else {
                    startAnimation(3);
                    changeColor(1);
                }
            } else {
                startAnimation(1);
            }
        }

        if (profileModel.gender.equalsIgnoreCase("Male")) {
            genderTextView.setText(getStringInitial(profileModel.gender));
            maleFemale(0);

            if (profileModel.spam == 1) {
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView1);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView2);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView3);
            } else {
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView1);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView2);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView3);
            }
        } else {
            genderTextView.setText(getStringInitial(profileModel.gender));
            maleFemale(1);
            if (profileModel.spam == 1) {
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView1);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView2);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.spamnotavailable)).into(imageView3);
            } else {
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.femalenotavailable)).into(imageView1);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.femalenotavailable)).into(imageView2);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.femalenotavailable)).into(imageView3);
            }

            if (genderTextView.getText().toString().trim().equalsIgnoreCase("")) {
                genderTextView.setVisibility(View.GONE);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView1);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView2);
                Glide.with(this).load(photo).placeholder(getResources().getDrawable(R.drawable.malenotavailable)).into(imageView3);
            }
        }

        if (name == null || name.equalsIgnoreCase("")) {
            name = "";
            nameTextView.setVisibility(View.GONE);
        } else {
            String namestr = getNameFromNumber(new DatabaseHelper(this), profileModel.number);
            if (namestr != null && !namestr.equalsIgnoreCase("")) {
                int i = namestr.length();
                nameTextView.setText(namestr);
            } else {
                int i = name.length();
                nameTextView.setText(name);
            }
        }

        String address = profileModel.address;
        if (address == null || address.equalsIgnoreCase("") ||
                address.equalsIgnoreCase("-")) {
            locationTextView.setVisibility(View.GONE);
        } else {
            locationTextView.setText(address);
        }

        closeImageView.setOnClickListener(v -> onBackPressed());

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        if (!callEnded) {
            rvParentView.setOnTouchListener(onTouchListener);
        }
        //popUpRelativeLayout.setOnTouchListener(onTouchListener);
    }

    @SuppressLint("ClickableViewAccessibility")
    View.OnTouchListener onTouchListener = (view, motionEvent) -> {
        view.setOnTouchListener(new View.OnTouchListener() {
            private float initCenterX;
            private float lastEventY = 0.0f;
            private float lastEventX = 0.0f;
            private float initY;
            private float initX;
            boolean isFirstTime = true;
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN: {
                        initX = view.getX();
                        initY = view.getY();
                        lastEventX = motionEvent.getRawX();
                        lastEventY = motionEvent.getRawY();
                        initCenterX = initX + view.getWidth() / 2;
                        break;
                    }

                    case MotionEvent.ACTION_MOVE: {
                        float eventX = motionEvent.getRawX();
                        float eventY = motionEvent.getRawY();
                        float eventDx = eventX - lastEventX;
                        float eventDy = eventY - lastEventY;
                        float centerX = view.getX() + eventDx + view.getWidth() / 2;
                        float centerDx = centerX - initCenterX;
                        //   view.setX(view.getX() + eventDx);
                        if(isFirstTime){
                            view.setY(view.getY());
                            view.invalidate();
                            lastEventX = eventX;
                            lastEventY = motionEvent.getY();
                            isFirstTime = false;
                        }else{
                            view.setY(view.getY() + eventDy);
                            view.invalidate();
                            lastEventX = eventX;
                            lastEventY = eventY;
                        }

                        //  float rotationAngle = centerDx * view.horizontalOscillation / initCenterX;
                        //view.setRotation(rotationAngle);

                        break;
                    }

                    case MotionEvent.ACTION_UP: {
                        /*PropertyValuesHolder horizontalAnimation =
                                PropertyValuesHolder.ofFloat("x", initX);
                        PropertyValuesHolder verticalAnimation =
                                PropertyValuesHolder.ofFloat("y", initY);
                        PropertyValuesHolder rotateAnimation =
                                PropertyValuesHolder.ofFloat("rotation", 0f);
                        ObjectAnimator originBackAnimation =
                                ObjectAnimator.ofPropertyValuesHolder(view, horizontalAnimation,
                                        verticalAnimation, rotateAnimation);
                        originBackAnimation.setInterpolator(
                                new AccelerateDecelerateInterpolator());
                        originBackAnimation.setDuration(300);
                        originBackAnimation.start();*/
                        break;
                    }
                }
                return true;
            }
        });
        /*if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            popUpRelativeLayout.setTranslationY(motionEvent.getRawY());
            popUpRelativeLayout.invalidate();
        }*/
        return true;
    };

    private void chipView() {
        textView.setText("Select Category");

        ChipsAdapter chipsAdapter = new ChipsAdapter(tagModels, this, false);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.removeItemPerLineLimit();
        chipsRecyclerView.setLayoutManager(flowLayoutManager);
        chipsRecyclerView.setAdapter(chipsAdapter);
    }

    private void subChipView() {
        textView.setText("Select Sub Category");

        ChipsAdapter chipsAdapter = new ChipsAdapter(tagModels, this, true);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.removeItemPerLineLimit();
        chipsRecyclerView.setLayoutManager(flowLayoutManager);
        chipsRecyclerView.setAdapter(chipsAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (rippleBackground != null && rippleBackground.isRippleAnimationRunning()) {
            rippleBackground.stopRippleAnimation();
        }
        if (rippleBackground2 != null && rippleBackground2.isRippleAnimationRunning()) {
            rippleBackground2.stopRippleAnimation();
        }
        if (rippleBackground3 != null && rippleBackground3.isRippleAnimationRunning()) {
            rippleBackground3.stopRippleAnimation();
        }
        Log.d(Tag, "Back Finished");
        finishAffinity();
        System.exit(0);
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("tagCallPopUp")) {
            tagModels.clear();
            tagModels = getTagList(busModel.jsonElement);
            insertAllTags(databaseHelper, tagModels);
            chipView();
        } else if (busModel.serviceName.equalsIgnoreCase("subtagCallPopUp")) {
            tagModels.clear();
            tagModels = getSubTagList(busModel.jsonElement);
            subChipView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    public void tagSubtag(boolean isSubtag, int id, String name) {
        if (isSubtag) {

            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            TagModels tagModel = new TagModels(userId, profileModel.number, 99, tagId, id);
            addTag(PopupActivity.this, tagModel, "addTagPopUp");

            tagModel.name = name;
            profileTagsModel.add(tagModel);
            setTagAdapter(profileTagsModel);

            tagId = 0;
            tagModels.clear();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            tagId = id;
            getSubTags(PopupActivity.this, id, "subtagCallPopUp");
        }
    }

    private String getCallLogDetail() {
        String string = "0";
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        int number;
        if (managedCursor != null) {

            number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);

            while (managedCursor.moveToNext()) {
                String phNumber = managedCursor.getString(number);
                String callDate = managedCursor.getString(date);

                if (phNumber.trim().contains(profileModel.number.trim())) {
                    string = callDate;
                }
            }
        }
        if (managedCursor != null) {
            managedCursor.close();
        }

        if (string == null || string.equalsIgnoreCase("")){
            return "0";
        }
        return string;
    }

    private void maleFemale(int i) {
        if (i == 0) {
            genderTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
        } else {
            genderTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
        }
    }

    private void changeColor(int i) {
        if (i == 0) {
            linearLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupbg)));
            one.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            two.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            three.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            four.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            five.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            closeImageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.blackColor)));
            genderTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
            //tagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
            //addTagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
            //addTagTextView.setTextColor(getResources().getColor(R.color.whiteColor));
        } else if (i == 1) {
            linearLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupbg)));
            one.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            two.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            three.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            four.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            five.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            closeImageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.blackColor)));
            genderTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
            //tagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
            //addTagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
            //addTagTextView.setTextColor(getResources().getColor(R.color.whiteColor));
        } else if (i == 5) {
            one.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            two.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            three.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            four.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            five.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            linearLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupbg)));
            relativeLayout.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.spamcolor)));
            //tagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            //addTagTextView.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.whiteColor)));
            //tagTextView.setTextColor(getResources().getColor(R.color.spamcolor));
            //addTagTextView.setTextColor(getResources().getColor(R.color.spamcolor));

            genderTextView.setVisibility(View.GONE);
            nameTextView.setTextColor(getResources().getColor(R.color.whiteColor));
            locationTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            workTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            websiteTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            numberTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            carrierTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            callLogTextView.setTextColor(getResources().getColor(R.color.whitishColor));
            simImageView.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.whitishColor)));
        }
    }

    private void startAds() {
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    private void startAnimation(int i) {
        if (i == 1) {
            rippleBackground.startRippleAnimation();
            rippleBackground.setVisibility(View.VISIBLE);
            rippleBackground2.setVisibility(View.INVISIBLE);
            rippleBackground3.setVisibility(View.INVISIBLE);
        } else if (i == 2) {
            rippleBackground2.startRippleAnimation();
            rippleBackground.setVisibility(View.INVISIBLE);
            rippleBackground2.setVisibility(View.VISIBLE);
            rippleBackground3.setVisibility(View.INVISIBLE);
        } else {
            rippleBackground3.startRippleAnimation();
            rippleBackground.setVisibility(View.INVISIBLE);
            rippleBackground2.setVisibility(View.INVISIBLE);
            rippleBackground3.setVisibility(View.VISIBLE);
        }
    }

    private void setTagAdapter(ArrayList<TagModels> tagModels) {
        for (int i = 0; i < tagModels.size(); i++) {
            if (tagModels.get(i).name != null &&
                    tagModels.get(i).name.equalsIgnoreCase("addtag")) {
                tagModels.remove(i);
                break;
            }
        }

        TagModels tagModel = new TagModels(0, "", 99, 0, 0);
        tagModel.name = "addtag";
        tagModels.add(tagModel);
        profileTagsModel = tagModels;
        TagAdapter tagAdapter = new TagAdapter(tagModels, this);
        tagLinear.setLayoutManager(new GridLayoutManager(this, 3));
        tagLinear.setAdapter(tagAdapter);
    }

    @Override
    public void addTag() {
        tagModels = getAllTags(databaseHelper);
        if (tagModels.size() == 0) {
            getTags(PopupActivity.this, "tagCallPopUp");
        } else {
            chipView();
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}