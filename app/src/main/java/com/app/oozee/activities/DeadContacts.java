package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.BlockAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Utility;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.models.BlockedContactModel.getBlockedList;
import static com.app.oozee.models.BlockedContactModel.getDeadList;

public class DeadContacts extends BaseActivity implements BlockAdapter.BlockContactInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_contacts);

        ImageView imageView = findViewById(R.id.backImageView);
        TextView titleTextView = findViewById(R.id.titleTextView);

        titleTextView.setText("Blocked Contacts");

        Typeface typeface = Utility.getTypeFace(this);
        titleTextView.setTypeface(typeface);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getContactsByType(this, blocked.ordinal(), "blockedContacts");
    }

    @Subscribe
    public void GetContacts(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("blockedContacts")) {
            ArrayList<BlockedContactModel> blockedContactModels = getDeadList(busModel.jsonElement);
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            BlockAdapter blockAdapter = new BlockAdapter(blockedContactModels, this,
                    false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(blockAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    public void blockContact(BlockedContactModel blockedContactModel) {
        deleteContactsByType(DeadContacts.this, blockedContactModel.contact_id,
                blocked.ordinal(), "removeBlockedContact");
    }
}
