package com.app.oozee.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.app.oozee.BuildConfig;
import com.app.oozee.R;
import com.app.oozee.utils.OozeePreferences;
import com.suke.widget.SwitchButton;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AppSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        TextView securityTextView = findViewById(R.id.securityTextView);
        TextView backUpTextview = findViewById(R.id.backUpTextview);
        TextView changeContactTextView = findViewById(R.id.changeContactTextView);
        TextView appVersionTextView = findViewById(R.id.appVersionTextView);
        SwitchButton dialSwitch = findViewById(R.id.dialSwitch);
        SwitchButton callPopupSwitch = findViewById(R.id.callPopupSwitch);
        ImageView backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        appVersionTextView.setText(BuildConfig.VERSION_NAME);

        boolean dialTone = OozeePreferences.getInstance().getPref().getBoolean("dialTone", false);
        boolean callPopUp = OozeePreferences.getInstance().getPref().getBoolean("callPopUp", false);

        dialSwitch.setChecked(dialTone);
        callPopupSwitch.setChecked(callPopUp);

        dialSwitch.setOnCheckedChangeListener((buttonView, isChecked) ->
                OozeePreferences.getInstance().getEditor().putBoolean("dialTone", isChecked).apply());

        callPopupSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> OozeePreferences.getInstance().getEditor().putBoolean("callPopUp", isChecked).apply());

        backUpTextview.setOnClickListener(v -> {
            Intent intent = new Intent(this, BackupActivity.class);
            startActivity(intent);
        });

        securityTextView.setOnClickListener(v -> {
            Intent intent = new Intent(this, SecurityActivity.class);
            startActivity(intent);
        });

        changeContactTextView.setOnClickListener(v -> {
            Intent intent = new Intent(this, EnterPhoneNumber.class);
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        });
    }
}
