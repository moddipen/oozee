package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.BlockAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.blockednumber.BlockedNumber;
import com.app.oozee.blockednumber.BlockedNumberDao;
import com.app.oozee.blockednumber.BlockedNumberDatabase;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.utils.AsyncExecutorUtil;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Utility;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.dead;
import static com.app.oozee.database.AllContactsModel.getNameandImageFromNumber;
import static com.app.oozee.models.BlockedContactModel.getBlockedList;
import static com.app.oozee.models.BlockedContactModel.getDeadList;

public class BlockedContacts extends BaseActivity implements BlockAdapter.BlockContactInterface {

    private TextView noDataTextView;
    private DatabaseHelper databaseHelper;
    private RecyclerView recyclerView;
    private BlockedNumberDao blockedNumberDao;
    private List<BlockedNumber> blockedNumberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_contacts);

        ImageView imageView = findViewById(R.id.backImageView);
        TextView titleTextView = findViewById(R.id.titleTextView);
        noDataTextView = findViewById(R.id.noDataTextView);
        recyclerView = findViewById(R.id.recyclerView);

        databaseHelper = new DatabaseHelper(this);

        blockedNumberDao = BlockedNumberDatabase.getInstance(this).blockedNumberDao();
        AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberList = blockedNumberDao.getAll());

        boolean isDead = getIntent().getBooleanExtra("isDead", false);
        if (isDead) {
            titleTextView.setText("Dead Contacts");
        } else {
            titleTextView.setText("Blocked Contacts");
        }

        Typeface typeface = Utility.getTypeFace(this);
        titleTextView.setTypeface(typeface);
        noDataTextView.setTypeface(typeface);

        imageView.setOnClickListener(v -> finish());

        if (isDead) {
            getContactsByType(this, dead.ordinal(), "deadContacts");
        } else {
            getContactsByType(this, blocked.ordinal(), "blockedContacts");
        }
    }

    @Subscribe
    public void GetContacts(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("blockedContacts")) {
            ArrayList<BlockedContactModel> blockedContactModels = getBlockedList(busModel.jsonElement);
            for (int i = 0; i < blockedContactModels.size(); i++) {
                String[] values = getNameandImageFromNumber(databaseHelper, blockedContactModels.get(i).phone_number);
                blockedContactModels.get(i).first_name = values[0];
                blockedContactModels.get(i).image = values[1];
            }

            BlockAdapter blockAdapter = new BlockAdapter(blockedContactModels, this, true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(blockAdapter);

            if (blockedContactModels.size() == 0) {
                noDataTextView.setVisibility(View.VISIBLE);
            } else {
                noDataTextView.setVisibility(View.GONE);
            }
        } else if (busModel.serviceName.equalsIgnoreCase("deadContacts")) {

            ArrayList<BlockedContactModel> blockedContactModels = getDeadList(busModel.jsonElement);

            for (int i = 0; i < blockedContactModels.size(); i++) {
                String[] values = getNameandImageFromNumber(databaseHelper, blockedContactModels.get(i).phone_number);
                blockedContactModels.get(i).first_name = values[0];
                blockedContactModels.get(i).image = values[1];
            }

            BlockAdapter blockAdapter = new BlockAdapter(blockedContactModels, this, false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(blockAdapter);

            if (blockedContactModels.size() == 0) {
                noDataTextView.setVisibility(View.VISIBLE);
            } else {
                noDataTextView.setVisibility(View.GONE);
            }
        } else if (busModel.serviceName.equalsIgnoreCase("removeBlockedContact")) {
            showToast("Contact removed from block list");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    public void blockContact(BlockedContactModel blockedContactModel) {
        String number = blockedContactModel.phone_number;

        number = number.replaceAll("-", "");

        if (number.startsWith("+91")) {
            number = number.replaceAll("\\+91", "");
        }
        if (number.startsWith("91") && number.length() > 10) {
            number = number.replaceFirst("91", "");
        }

        final BlockedNumber blockedNumber;
        for (int i = 0; i < blockedNumberList.size(); i++) {
            if (blockedNumberList.get(i).getPhoneNumber().equalsIgnoreCase(number)) {
                blockedNumber = blockedNumberList.get(i);
                AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberDao.delete(blockedNumber));
                break;
            }
        }

        deleteContactsByType(BlockedContacts.this, blockedContactModel.id,
                blocked.ordinal(), "removeBlockedContact");
    }
}
