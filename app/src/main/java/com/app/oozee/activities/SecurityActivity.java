package com.app.oozee.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.utils.OozeePreferences;
import com.suke.widget.SwitchButton;

public class SecurityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        TextView titleTextView = findViewById(R.id.titleTextView);
        ImageView backImageView = findViewById(R.id.backImageView);

        titleTextView.setText("Security Settings");

        backImageView.setOnClickListener(v -> finish());

        SwitchButton mutualSwitch = findViewById(R.id.mutualSwitch);
        SwitchButton statusSwitch = findViewById(R.id.statusSwitch);

        boolean isMutual = OozeePreferences.getInstance().getPref().getBoolean("isMutual", false);
        boolean isStatus = OozeePreferences.getInstance().getPref().getBoolean("isStatus", false);

        if (isMutual) {
            mutualSwitch.setChecked(true);
        } else {
            mutualSwitch.setChecked(false);
        }

        if (isStatus) {
            statusSwitch.setChecked(true);
        } else {
            statusSwitch.setChecked(false);
        }

        mutualSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> OozeePreferences.getInstance().getEditor().putBoolean("isMutual", isChecked).apply());
        statusSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> OozeePreferences.getInstance().getEditor().putBoolean("isStatus", isChecked).apply());
    }
}