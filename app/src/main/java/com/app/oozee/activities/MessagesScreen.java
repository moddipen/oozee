package com.app.oozee.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.adapters.SmsAdapter;
import com.app.oozee.application.OozeeApplication;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.blockednumber.BlockedNumber;
import com.app.oozee.blockednumber.BlockedNumberDao;
import com.app.oozee.blockednumber.BlockedNumberDatabase;
import com.app.oozee.database.AllContactsModel;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.interfaces.OozeeInterface;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.MediaModel;
import com.app.oozee.models.MessageModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.Sms;
import com.app.oozee.utils.ApiClient;
import com.app.oozee.utils.AsyncExecutorUtil;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;
import com.google.gson.JsonElement;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.blockednumber.BlockedNumberType.EXACT_MATCH;
import static com.app.oozee.database.MessagesDb.deleteSMS;
import static com.app.oozee.models.MediaModel.getMediaModel;
import static com.app.oozee.models.MessageModel.getMessagesModel;
import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.Config.getChatDateString;
import static com.app.oozee.utils.Config.voiceCall;
import static com.app.oozee.utils.Utility.COUNTRYID;
import static com.app.oozee.utils.Utility.getTypeFace;

public class MessagesScreen extends BaseActivity {

    public static Socket mSocket;
    private ArrayList<MessageModel> messageModels = new ArrayList<>();
    private SmsAdapter smsAdapter;
    private EditText sendEditText;
    private TextView onlynameTextView;
    private LinearLayout chatLinearLayout;
    private RelativeLayout sendMessageRelative;
    private String number;
    private BlockedNumberDao blockedNumberDao;
    private DatabaseHelper databaseHelper;
    private EditText editMessage;
    private OozeeApplication app;
    private boolean mTyping;
    private String mUsername;
    private int receiverId;
    private int senderId;
    private TextView typingView;
    private TextView userName;
    CircleImageView chat_image;
    private boolean isConnected;
    private String TAG = "-->>";
    private RecyclerView recyclerView;
    private static final int TIMER = 500;
    private Handler typingHandler = new Handler();
    private MediaRecorder recorder = null;
    private String fileName;
    private boolean isSMS;
    private static final int PHOTO_EDITOR_REQUEST_CODE = 122;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_screen);

        recyclerView = findViewById(R.id.recyclerView);
        sendEditText = findViewById(R.id.sendEditText);
        onlynameTextView = findViewById(R.id.onlynameTextView);
        chatLinearLayout = findViewById(R.id.linearLayout);
        sendMessageRelative = findViewById(R.id.sendMessageRelative);
        chat_image = findViewById(R.id.chat_image);

        ImageView optionsImageView = findViewById(R.id.optionsImageView);
        ImageView backImageView = findViewById(R.id.backImageView);
        ImageView sendImageView = findViewById(R.id.sendImageView);
        TextView sendMessageTextView = findViewById(R.id.sendMessageTextView);
        ImageView callImageView = findViewById(R.id.callImageView);
        ImageView videocallImageView = findViewById(R.id.videocallImageView);

        backImageView.setOnClickListener(v -> onBackPressed());

        app = new OozeeApplication();

        messageModels = new ArrayList<>();

        isSMS = OozeePreferences.getInstance().getPref().getBoolean("ismss", false);

        int whichOption = getIntent().getIntExtra("whichOption", 1);

        if (whichOption != 1) {
            sendImageView.setVisibility(View.GONE);
            sendEditText.setVisibility(View.GONE);
            sendMessageTextView.setVisibility(View.VISIBLE);
            callImageView.setVisibility(View.INVISIBLE);
            videocallImageView.setVisibility(View.INVISIBLE);
            optionsImageView.setVisibility(View.INVISIBLE);
        } else {
            sendImageView.setVisibility(View.VISIBLE);
            sendEditText.setVisibility(View.VISIBLE);
            sendMessageTextView.setVisibility(View.GONE);
            callImageView.setVisibility(View.VISIBLE);
            videocallImageView.setVisibility(View.VISIBLE);
            optionsImageView.setVisibility(View.VISIBLE);
        }
        number = getIntent().getStringExtra("address");
        String smsid = getIntent().getStringExtra("smsid");
        String image = getIntent().getStringExtra("image");

        if (image != null && !image.isEmpty()&& !image.equals("")){
            chat_image.setVisibility(View.VISIBLE);
            Glide.with(this).load(image).fitCenter().into(chat_image);
        }

        markMessageRead(this, smsid);
        List<Sms> sms = getAllSms(this, number);

        for (int i = 0; i < sms.size(); i++) {
            int viewType = 0;
            if (sms.get(i).getFolderName().equalsIgnoreCase("Inbox")) {
                viewType = 1;
            }

            String time = sms.get(i).getTime();
            long timeStamp = Long.parseLong(time);
            time = Config.getTimeFromTimeStamp(Long.parseLong(time));
            messageModels.add(new MessageModel(sms.get(i).getId(),
                    viewType, sms.get(i).getMsg(), time, true, timeStamp));
        }

        Typeface typeface = getTypeFace(this);
        sendEditText.setTypeface(typeface);
        onlynameTextView.setTypeface(typeface);

        databaseHelper = new DatabaseHelper(this);
        String name = AllContactsModel.getNameFromNumber(new DatabaseHelper(this), number);
        if (name.equalsIgnoreCase("")) {
            onlynameTextView.setText(number);
        } else {
            onlynameTextView.setText(name);
        }

        sendImageView.setOnClickListener(v -> showPopUpWindow());

        sendImageView.setOnLongClickListener(v -> {
            //textViewLong.setVisibility(View.VISIBLE);
            switchPopUp();
            return true;
        });

        callImageView.setOnClickListener(v -> callPhone(MessagesScreen.this, number));

        videocallImageView.setOnClickListener(v -> voiceCall(number, MessagesScreen.this, true));

        optionsImageView.setOnClickListener(this::showPopUpWindowOptions);

        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);

        number = number.replaceAll("-", "");

        if (number.startsWith("+91")) {
            number = number.replaceAll("\\+91", "");
        }

        if (number.startsWith("91") && number.length() > 10) {
            number = number.replaceFirst("91", "");
        }

        if (whichOption == 1) {
            BlockedContactModel blockedContacts = new BlockedContactModel(userId, number, countryid);
            getcontactDetails(this, blockedContacts, "contactDetails");
        } else {
            smsAdapter = new SmsAdapter(messageModels, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
            recyclerView.setAdapter(smsAdapter);
        }
    }

    private List<Sms> getAllSms(@NonNull Activity mActivity, String address) {
        Sms objSms;
        List<Sms> lstSms = new ArrayList<>();
        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = mActivity.getContentResolver();
        Cursor c = cr.query(message, null, null, null, null);
        int totalSMS;
        if (c != null) {
            totalSMS = c.getCount();
            if (c.moveToFirst()) {
                for (int i = 0; i < totalSMS; i++) {
                    objSms = new Sms();
                    objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                    objSms.setAddress(c.getString(c.getColumnIndexOrThrow("address")));
                    objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                    objSms.setReadState(c.getString(c.getColumnIndex("read")));
                    objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                    if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                        objSms.setFolderName("Inbox");
                    } else {
                        objSms.setFolderName("Sent");
                    }
                    if(objSms.getAddress()!=null) {
                        if (address.contains(objSms.getAddress())) {
                            lstSms.add(objSms);
                        } else if (objSms.getAddress().contains(address)) {
                            lstSms.add(objSms);
                        }
                    }
                    c.moveToNext();
                }
            }
            c.close();
        }
        return lstSms;
    }

    public static void markMessageRead(Context context, String smsMessageId) {
        ContentValues values = new ContentValues();
        values.put(Telephony.Sms.READ, 1);
        context.getContentResolver().update(Telephony.Sms.Inbox.CONTENT_URI, values, "_id = ?", new String[]{smsMessageId});
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            OozeePreferences.getInstance().getEditor().putBoolean("isSMSSent", true).apply();
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            showToast("Message Sent");
        } catch (Exception ex) {
            showToast(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showPopUpWindow() {
        sendmessge();
        /*final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        imageView.setText("Default SMS App");
        textView.setText("Set Oozee as default sms application?");
        cancelTextView.setText("No");
        performTextView.setText("Yes");

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(v -> {
            dialog.dismiss();
            sendmessge();
        });

        cancelTextView.setOnClickListener(v -> {
            dialog.dismiss();
            sendmessge();
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }*/
    }

    private void sendmessge() {
        String sms = sendEditText.getText().toString().trim();

        if (sms.equalsIgnoreCase("")) {
            showToast("Please enter message");
            return;
        }
        sendSMS(number, sms);
        sendEditText.setText("");
        long millis = Calendar.getInstance().getTimeInMillis();
        String time = Config.getTimeFromTimeStamp(millis);

        messageModels.add(0, new MessageModel("0", 0, sms,
                time, true, millis));
        smsAdapter.notifyDataSetChanged();

        hideKeyboard(MessagesScreen.this);
    }

    @Override
    public void onBackPressed() {
        String sms = sendEditText.getText().toString().trim();
        if (!sms.equalsIgnoreCase("")) {
            super.onBackPressed();
            showPopUpWindowALert();
        } else {
            finish();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showPopUpWindowALert() {
        Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        imageView.setText("Message");
        textView.setText("You have not sent message yet. If you do not send it, it would not be visible further. Do you want to continue?");
        cancelTextView.setText("Cancel");
        performTextView.setText("Yes");

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(v -> {
            dialog.dismiss();
            onBackPressed();
        });

        cancelTextView.setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showPopUpWindowOptions(View view) {
        Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_contacts_details);
        dialog.setCancelable(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView blockTextView = dialog.findViewById(R.id.blockTextView);
        TextView addToQuickList = dialog.findViewById(R.id.addToQuickList);
        TextView recordTextView = dialog.findViewById(R.id.recordTextView);
        TextView blockNumber = dialog.findViewById(R.id.blockNumber);
        TextView addNotes = dialog.findViewById(R.id.addNotes);
        TextView chatTextView = dialog.findViewById(R.id.chatTextView);
        TextView markSpam = dialog.findViewById(R.id.markSpam);

        Typeface typeface = getTypeFace(this);
        blockTextView.setTypeface(typeface);
        recordTextView.setTypeface(typeface);
        blockNumber.setTypeface(typeface);
        addToQuickList.setTypeface(typeface);
        addNotes.setTypeface(typeface);
        chatTextView.setTypeface(typeface);
        markSpam.setTypeface(typeface);

        recordTextView.setText("View Contact");
        blockTextView.setText("Block Contact");

        isSMS = OozeePreferences.getInstance().getPref().getBoolean("ismss", false);

        if (isSMS) {
            blockNumber.setText("Switch to oozee Chat");
        } else {
            blockNumber.setText("Switch to SMS");
        }

        addToQuickList.setText("Delete Conversation");
        addNotes.setVisibility(View.GONE);
        chatTextView.setVisibility(View.GONE);
        markSpam.setVisibility(View.GONE);

        if (senderId == 0) {
            blockNumber.setVisibility(View.GONE);
        }

        recordTextView.setOnClickListener(v -> {
            dialog.dismiss();
            LocalContactsModel localContactsModel = new LocalContactsModel(0, onlynameTextView.getText().toString().trim(),
                    getStringInitial(onlynameTextView.getText().toString().trim()),
                    number, "");

            Intent intent = new Intent(MessagesScreen.this, ProfileScreen.class);
            intent.putExtra("contactsModel", localContactsModel);
            startActivity(intent);
        });

        blockTextView.setOnClickListener(v -> {
            dialog.dismiss();
            showPopUpWindowBlock(true);
        });

        blockNumber.setOnClickListener(v -> {
            dialog.dismiss();
            editMessage.setText("");
            sendEditText.setText("");
            if (blockNumber.getText().toString().trim().toLowerCase().contains("Chat".toLowerCase())) {
                isSMS = false;
                OozeePreferences.getInstance().getEditor().putBoolean("ismss", false).apply();
                sendMessageRelative.setVisibility(View.INVISIBLE);
                chatLinearLayout.setVisibility(View.VISIBLE);
            } else {
                isSMS = true;
                OozeePreferences.getInstance().getEditor().putBoolean("ismss", true).apply();
                sendMessageRelative.setVisibility(View.VISIBLE);
                chatLinearLayout.setVisibility(View.INVISIBLE);
            }
        });

        addToQuickList.setOnClickListener(v -> showPopUpWindowBlock(false));

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.TOP | Gravity.END);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    @Subscribe
    public void Response(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("blockContact")) {
            BlockedNumber blockedNumber = new BlockedNumber(EXACT_MATCH, "+91",
                    number);
            addNumber(blockedNumber);

            showToast("Contact added to block list");
        } else if (busModel.serviceName.equalsIgnoreCase("contactDetails")) {
            ProfileModel profileModel = ProfileModel.getProfileModel(busModel.jsonElement);
            receiverId = profileModel.contact_user_id;
            mUsername = profileModel.first_name + " " + profileModel.last_name;

            if (isSMS) {
                sendMessageRelative.setVisibility(View.VISIBLE);
                chatLinearLayout.setVisibility(View.INVISIBLE);
            } else {
                sendMessageRelative.setVisibility(View.INVISIBLE);
                chatLinearLayout.setVisibility(View.VISIBLE);
            }

            initializeSocket();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showPopUpWindowBlock(final boolean isBlock) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        if (!isBlock) {
            imageView.setText("Delete Conversation?");
            textView.setText("Are you sure you want to delete all messages?");
            performTextView.setText("Delete");
        }

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(v -> {
            dialog.dismiss();
            if (isBlock) {
                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
                BlockedContactModel blockedContacts = new BlockedContactModel(userId,
                        number, countryid);
                addContactsByType(MessagesScreen.this, blockedContacts, blocked.ordinal(),
                        "blockContact");
            } else {
                for (int i = 0; i < messageModels.size(); i++) {
                    deleteSMS(databaseHelper, messageModels.get(i).id);
                    deleteSms(messageModels.get(i).id);
                }
            }
        });

        cancelTextView.setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    public void addNumber(final BlockedNumber number) {
        blockedNumberDao = BlockedNumberDatabase.getInstance(this).blockedNumberDao();
        AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberDao.insert(number));
    }

    private void deleteSms(String smsId) {
        try {
            getContentResolver().delete(Uri.parse("content://sms/" + smsId), null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isConnected) {
            mSocket.disconnect();
            isConnected = false;
        }

        if (mSocket == null) {
            return;
        }
        mSocket.off("login", onLogin);
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("new message", onNewMessage);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
        mSocket.off("room", rooms);
        mSocket.off("message read", messageread);
        mTyping = false;
    }

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(() -> {
                isConnected = false;
                Log.w(TAG, "onDisconnect");
            });
        }
    };

    private Emitter.Listener onConnectError = args -> runOnUiThread(() -> Log.e(TAG, "error connecting"));

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.w(TAG, "onNewMesage");
            runOnUiThread(() -> {
                JSONObject data = (JSONObject) args[0];
                String username = "";
                String message = "";
                try {
                    username = data.getString("username");
                    message = data.getString("message");
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                    e.printStackTrace();
                }

                if (username != null && !username.equalsIgnoreCase(mUsername)) {
                    addMessage(username, message, 1);
                }
            });
        }
    };

    private Emitter.Listener onUserJoined;{
        onUserJoined = args -> {
            Log.w(TAG, "onUserJoined");
            runOnUiThread(() -> {
                JSONObject data = (JSONObject) args[0];
                String username;
                int numUsers;
                try {
                    username = data.getString("username");
                    numUsers = data.getInt("numUsers");
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                    return;
                }

                addParticipantsLog();
            });
        };
    }

    private Emitter.Listener onUserLeft = args -> {
        Log.w(TAG, "onUserLeft");
        runOnUiThread(() -> {
            JSONObject data = (JSONObject) args[0];
            String username;
            int numUsers;
            try {
                username = data.getString("username");
                numUsers = data.getInt("numUsers");
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return;
            }

            addParticipantsLog();
            removeTyping();
        });
    };

    private Emitter.Listener onTyping = args -> {
        Log.w(TAG, "onTyping");
        runOnUiThread(() -> {
            JSONObject data = (JSONObject) args[0];
            String username;
            try {
                username = data.getString("username");
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return;
            }
            addTyping(username);
        });
    };

    private Emitter.Listener onStopTyping = args -> {
        Log.w(TAG, "onStopTyping");
        runOnUiThread(this::removeTyping);
    };

    private Emitter.Listener rooms = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.w(TAG, "Rooms");
            runOnUiThread(() -> {
                MessageModel message = getMessagesModel((JSONObject) args[0], senderId, messageModels);

                for (int i = 0; i < message.messages.size(); i++) {
                    if (Integer.parseInt(message.messages.get(i).id) != senderId) {
                        JSONObject jsonObject = messageReadEvent(message.room,
                                Integer.parseInt(message.messages.get(i).id));
                        mSocket.emit("message read", jsonObject.toString());
                        break;
                    }
                }

                //Collections.reverse(message.messages);
                /*if (message.messages.size() == 0) {
                    return;
                }*/
                addAllMessage(message.messages);
            });
        }
    };

    private Emitter.Listener messageread = args -> {
        Log.d(TAG, Arrays.toString(args));
        runOnUiThread(() -> {

        });
    };

    private void addMessage(String username, String message, int messageType) {
        messageModels.add(0, new MessageModel(String.valueOf(senderId), messageType, message,
                "now", false, Calendar.getInstance().getTimeInMillis()));
        smsAdapter.notifyDataSetChanged();
        //scrollUp();
    }

    private void addAllMessage(ArrayList<MessageModel> messages) {
        messageModels.addAll(messages);

        Collections.sort(messageModels, new Comparator<MessageModel>() {
            public int compare(MessageModel o1, MessageModel o2) {
                return new Date(o1.timeStamp).compareTo(new Date(o2.timeStamp));
            }
        });

        Collections.reverse(messageModels);

        smsAdapter = new SmsAdapter(messageModels, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        recyclerView.setAdapter(smsAdapter);
    }

    private void addParticipantsLog() {

    }

    private void addTyping(String username) {
        if (!username.equalsIgnoreCase(mUsername)) {
            visibleorNot(false);
            typingView.setText(/*username.trim() + " is */"typing");
        }
    }

    private void removeTyping() {
        visibleorNot(true);
        typingView.setText("");
    }

    private void attemptSend() {
        if (mUsername == null) return;
        if (!mSocket.connected()) return;
        if (mTyping) {
            mTyping = false;
            JSONObject jsonObject = getJsonObject();
            mSocket.emit("stop typing", jsonObject.toString());
        }
        String message = editMessage.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            editMessage.requestFocus();
            return;
        }

        editMessage.setText("");
        addMessage(mUsername, message, 0);

        JSONObject jsonObject = getJsonObjectmessage(message);
        Log.d(TAG, jsonObject.toString());
        mSocket.emit("new message", jsonObject.toString());
    }

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping)
                return;

            mTyping = false;
            JSONObject jsonObject = getJsonObject();
            mSocket.emit("stop typing", jsonObject.toString());
        }
    };

    private void scrollUp() {
        if (messageModels.size() > 0) {
            runOnUiThread(() -> recyclerView.scrollToPosition(smsAdapter.getItemCount() - 1));
        }
    }

    private JSONObject messageReadEvent(String room, int messageid) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", senderId);
            jsonObject.put("message_id", messageid);
            jsonObject.put("room", room);
            jsonObject.put("read_at", getChatDateString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sender", senderId);
            jsonObject.put("receiver", receiverId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private JSONObject getJsonObjectmessage(String message) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sender", senderId);
            jsonObject.put("receiver", receiverId);
            jsonObject.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!isConnected) {
                isConnected = true;
                mSocket.emit("add user", mUsername);
            } else {
                Log.w("-->>", "onConnect Failure");
            }
        }
    };

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            int numUsers = 0;
            try {
                numUsers = data.getInt("numUsers");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            isConnected = true;
            addParticipantsLog();
            JSONObject jsonObject = getJsonObject();
            mSocket.emit("room", jsonObject.toString());
        }
    };

    private void visibleorNot(boolean truefalse) {
        if (truefalse) {
            onlynameTextView.setVisibility(View.VISIBLE);
            typingView.setVisibility(View.GONE);
            userName.setVisibility(View.GONE);
        } else {
            onlynameTextView.setVisibility(View.GONE);
            typingView.setVisibility(View.VISIBLE);
            userName.setVisibility(View.VISIBLE);
        }
    }

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }

        recorder.start();
    }

    private void stopRecording() {

        if (recorder == null) {
            return;
        }

        try {
            recorder.stop();
            recorder.release();
            recorder = null;

            MultipartBody.Part body = prepareFilePart(fileName);
            RequestBody type = RequestBody.create(MultipartBody.FORM, "voice");

            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addMedia(body, type);
            try {
                JsonElement loginResponse = call.execute().body();
                MediaModel mediaModel = getMediaModel(loginResponse, true);

                if (mediaModel != null) {
                    editMessage.setText(mediaModel.idImage);
                    attemptSend();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            recorder = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (recorder != null) {
            recorder.release();
            recorder = null;
        }
    }

    @NonNull
    public MultipartBody.Part prepareFilePart(String fileUri) {
        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);
        return MultipartBody.Part.createFormData("media", file.getName(), requestFile);
    }

    private void strictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
                editImage(filePath);
            }
        } else if (requestCode == PHOTO_EDITOR_REQUEST_CODE) { // same code you used while starting
            if (data != null) {
                String newFilePath = data.getStringExtra("output_path");
                boolean isImageEdit = data.getBooleanExtra("is_image_edited", false);

                if (isImageEdit) {
                    Toast.makeText(this, "ImageEditted", Toast.LENGTH_SHORT).show();
                }

                MultipartBody.Part body = prepareFilePart(newFilePath);
                RequestBody type = RequestBody.create(MultipartBody.FORM, "profile");
                OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
                Call<JsonElement> call = apiService.addMedia(body, type);
                try {
                    JsonElement loginResponse = call.execute().body();
                    MediaModel mediaModel = getMediaModel(loginResponse, true);

                    if (mediaModel != null) {
                        editMessage.setText(mediaModel.idImage);
                        attemptSend();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void editImage(String sourcePath) {
        try {
            String outputFilePath = getFileDestinationPath();

            Intent intent = new ImageEditorIntentBuilder(this, sourcePath, outputFilePath)
                    .withAddText() // Add the features you need
                    .withPaintFeature()
                    .withFilterFeature()
                    .withRotateFeature()
                    .withCropFeature()
                    //.withBrighnessFeature()
                    .withSaturationFeature()
                    .withBeautyFeature()
                    .withStickerFeature()
                    .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
                    .build();

            EditImageActivity.start(this, intent, PHOTO_EDITOR_REQUEST_CODE);
        } catch (Exception e) {
            Log.e("Demo App", e.getMessage()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
        }
    }

    public String getFileDestinationPath() {
        String filePathEnvironment =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File directoryFolder = new File(filePathEnvironment + "/images/");
        if (!directoryFolder.exists()) {
            directoryFolder.mkdir();
        }
        return filePathEnvironment + "/images/" + Calendar.getInstance().getTimeInMillis() + ".png";
    }

    private void selectProfilePic() {
        Intent intent = new Intent(this, ImageSelectActivity.class);
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
        startActivityForResult(intent, 1213);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setUpUI() {
        ImageView voiceMessage = findViewById(R.id.voiceMessage);
        ImageView attachments = findViewById(R.id.attachments);

        /*mAdapter = new ChatAdapter(messageList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/

        editMessage = findViewById(R.id.editMessage);
        typingView = findViewById(R.id.typing);
        userName = findViewById(R.id.nameTextView);
        ImageView sendImageView = findViewById(R.id.chatsendImageView);
        ImageView backImageView = findViewById(R.id.backImageView);

        userName.setText(mUsername);

        backImageView.setOnClickListener(v -> finish());

        attachments.setOnClickListener(v -> selectProfilePic());

        sendImageView.setOnLongClickListener(v -> {
//            textViewLong.setVisibility(View.VISIBLE);
            switchPopUp();
            return true;
        });

        editMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mUsername == null)
                    return;
                if (!mSocket.connected())
                    return;
                if (TextUtils.isEmpty(editMessage.getText()))
                    return;
                if (!mTyping) {
                    mTyping = true;
                    JSONObject jsonObject = getJsonObject();
                    mSocket.emit("typing", jsonObject.toString());
                }
                typingHandler.removeCallbacks(onTypingTimeout);
                typingHandler.postDelayed(onTypingTimeout, TIMER);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        voiceMessage.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                showToast("Started recording");
                onRecord(true);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                showToast("Stopped recording");
                onRecord(false);
            }
            return true;
        });

        sendImageView.setOnClickListener(view -> attemptSend());
    }

    public void initializeSocket() {

        //mUsername = getIntent().getStringExtra("username");
        //receiverId = getIntent().getIntExtra("contactUserId", 0);
        senderId = OozeePreferences.getInstance().getPref().getInt("userId", 0);

        fileName = Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath();
        fileName += "/oozeeRecording.3gp";

        isConnected = false;
        mTyping = false;

        setUpUI();
        strictMode();

        mSocket = app.getSocket();

        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("message read", messageread);
        mSocket.on("new message", onNewMessage);
        mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.on("room", rooms);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("login", onLogin);
        mSocket.connect();
    }

    private void switchPopUp() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.switchui);
        dialog.setCancelable(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView switchTextView = dialog.findViewById(R.id.switchTextView);
        ImageView sendImageView = dialog.findViewById(R.id.sendImageView);

        if (isSMS) {
            sendImageView.setImageDrawable(getResources().getDrawable(R.mipmap.oozeesendbutton));
            switchTextView.setText("Switch To Chat");
        } else {
            sendImageView.setImageDrawable(getResources().getDrawable(R.mipmap.smssendbutton));
            switchTextView.setText("Switch To SMS");
        }

        switchTextView.setOnClickListener(v -> {
            dialog.dismiss();
            if (!isSMS) {
                isSMS = true;
                OozeePreferences.getInstance().getEditor().putBoolean("ismss", true).apply();
                sendMessageRelative.setVisibility(View.VISIBLE);
                chatLinearLayout.setVisibility(View.INVISIBLE);
            } else {
                isSMS = false;
                OozeePreferences.getInstance().getEditor().putBoolean("ismss", false).apply();
                sendMessageRelative.setVisibility(View.INVISIBLE);
                chatLinearLayout.setVisibility(View.VISIBLE);
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM | Gravity.END);

        if (!isDestroyed()) {
            dialog.show();
        }
    }
}
