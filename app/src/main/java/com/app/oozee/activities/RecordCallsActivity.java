package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.SelectContactsAdapter;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.utils.Utility.getTypeFace;

public class RecordCallsActivity extends AppCompatActivity {

    private ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
    private ArrayList<LocalContactsModel> tempContactsModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_calls);

        Typeface typeface = getTypeFace(this);

        TextView autoCallTextView = findViewById(R.id.autoCallTextView);
        TextView selectedTextView = findViewById(R.id.selectedTextView);
        RadioButton selected = findViewById(R.id.selected);
        RadioButton unsaved = findViewById(R.id.unsaved);
        RadioButton all = findViewById(R.id.all);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        autoCallTextView.setTypeface(typeface);
        selected.setTypeface(typeface);
        selectedTextView.setTypeface(typeface);
        unsaved.setTypeface(typeface);
        all.setTypeface(typeface);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        ArrayList<LocalContactsModel> contactsModelss = getAllContacs(databaseHelper);

        Collections.sort(contactsModelss, new Comparator<LocalContactsModel>() {
            @Override
            public int compare(LocalContactsModel o1, LocalContactsModel o2) {
                return o1.name.compareToIgnoreCase(o2.name);
            }
        });

        String initial = "";
        for (int i = 0; i < contactsModelss.size(); i++) {
            String name = contactsModelss.get(i).name;
            String newInitial = getStringInitial(name);
            if (!initial.equalsIgnoreCase(newInitial)) {
                initial = newInitial;
            }
            contactsModels.add(new LocalContactsModel(name, contactsModelss.get(i).date,
                    contactsModelss.get(i).dir, contactsModelss.get(i).number));
        }

        SelectContactsAdapter selectContactsAdapter = new SelectContactsAdapter(contactsModels,
                RecordCallsActivity.this, true);
        recyclerView.setLayoutManager(new LinearLayoutManager(RecordCallsActivity.this));
        recyclerView.setAdapter(selectContactsAdapter);

    }
}
