package com.app.oozee.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.CheckSignup;
import com.app.oozee.models.LoginModel;
import com.app.oozee.models.MediaModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.app.oozee.utils.Utility;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.otto.Subscribe;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.oozee.utils.Config.getExpireMonth;
import static com.app.oozee.utils.Utility.COUNTRYID;
import static com.app.oozee.utils.Utility.DeviceType;
import static com.app.oozee.utils.Utility.PHONENUMBER;
import static com.app.oozee.utils.Utility.USERNAME;
import static com.app.oozee.utils.Utility.USERSUBSCRIPTION;

public class SignUpScreen extends BaseActivity {

    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String phoneNumber;
    private String firstNameString;
    private String lastNameString;
    private double latitude = 0;
    private double longitude = 0;
    private EditText dobEditText;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int REQUEST_CODE_GOOGLE = 1;
    private String user;
    private CallbackManager mCallbackManager;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private RadioButton maleRadio;
    private RadioButton femaleRadio;
    private TextView areyou , feelinglazy;
    private String photoStr;
    TextView uploadtext;
    private CircleImageView imageView;
    private String bydefaultimageurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up_screen);

        firstNameEditText = findViewById(R.id.firstNameEditText);
        lastNameEditText = findViewById(R.id.lastNameEditText);
        emailEditText = findViewById(R.id.emailEditText);
        dobEditText = findViewById(R.id.dobEditText);
        maleRadio = findViewById(R.id.maleRadio);
        femaleRadio = findViewById(R.id.femaleRadio);
        areyou = findViewById(R.id.areyou);
        uploadtext = findViewById(R.id.uploadtext);
        imageView = findViewById(R.id.imageView);
        feelinglazy = findViewById(R.id.feelinglazy);
        final RadioGroup genderRadioGroup = findViewById(R.id.genderRadioGroup);
        Button proceedButton = findViewById(R.id.proceedButton);

        Typeface typeface = Utility.getTypeFace(this);
        Typeface typeface1 = Utility.getTypeFace1(this);
        firstNameEditText.setTypeface(typeface);
        lastNameEditText.setTypeface(typeface);
        emailEditText.setTypeface(typeface);
        proceedButton.setTypeface(typeface);
        dobEditText.setTypeface(typeface);
        areyou.setTypeface(typeface);
        uploadtext.setTypeface(typeface);
        /*feelinglazy.setTypeface(typeface1);*/

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProfilePic();
            }
        });

        dobEditText.setOnClickListener(v -> {
            hideKeyboard(SignUpScreen.this);
            selectDate();
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        ImageView googleImageView = findViewById(R.id.googleImageView);
        ImageView facebookImageView = findViewById(R.id.facebookImageView);

        googleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        mCallbackManager = CallbackManager.Factory.create();
        final LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });

        facebookImageView.setOnClickListener(v -> loginButton.performClick());

        phoneNumber = getIntent().getStringExtra("phoneNumber");
        //phoneNumber = "9909402793";

        getLocation();
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);

        proceedButton.setOnClickListener(v -> {

            firstNameString = firstNameEditText.getText().toString().trim();
            lastNameString = lastNameEditText.getText().toString().trim();
            String emailNameString = emailEditText.getText().toString().trim();
            String dobEditTextString = dobEditText.getText().toString().trim();

            if (firstNameString.equalsIgnoreCase("")) {
                showToast("Please enter first name");
                return;
            }
            if (lastNameString.equalsIgnoreCase("")) {
                showToast("Please enter last name");
                return;
            }
            if (emailNameString.equalsIgnoreCase("")) {
                showToast("Please enter email address");
                return;
            }
            if (!emailNameString.matches(emailPattern)) {
                showToast("Please enter valid email address");
                return;
            }
            if (dobEditTextString.equalsIgnoreCase("")) {
                showToast("Please select date of birth");
                return;
            }

            int selectedId = genderRadioGroup.getCheckedRadioButtonId();
            RadioButton radioSexButton = findViewById(selectedId);
            String type = "Male";
            if (radioSexButton != null && radioSexButton.getText() != null) {
                if (radioSexButton.getText().toString().equalsIgnoreCase("Female")) {
                    type = "Female";
                }
            } else {
                showToast("Please select gender");
                return;
            }
            String deviceId = "";

            try {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(SignUpScreen.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    deviceId = telephonyManager.getDeviceId();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String token = FirebaseInstanceId.getInstance().getToken();

            if (photoStr == null || photoStr.equalsIgnoreCase("")){
                LoginModel loginModel = new LoginModel(phoneNumber, firstNameString, lastNameString, emailNameString,
                        countryid, type, "Manual", latitude, longitude, deviceId, token,
                        DeviceType, dobEditTextString,bydefaultimageurl);
                loginCall(SignUpScreen.this, loginModel, "loginCall");
            }
            else {
                LoginModel loginModel = new LoginModel(phoneNumber, firstNameString, lastNameString, emailNameString,
                        countryid, type, "Manual", latitude, longitude, deviceId, token,
                        DeviceType, dobEditTextString,photoStr);
                loginCall(SignUpScreen.this, loginModel, "loginCall");
            }
        });

        CheckSignup checkSignup = new CheckSignup(phoneNumber, countryid);
        checkSignUp(this, checkSignup, "checkSignup");
    }

    @Subscribe
    public void Login(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("loginCall")) {
            LoginModel loginModel = LoginModel.getLogin(busModel.jsonElement);
            if (loginModel != null) {
                OozeePreferences.getInstance().getEditor().putString("bearertoken", "Bearer " + loginModel.access_token).commit();
                OozeePreferences.getInstance().getEditor().putString("refreshtoken", "Bearer " + loginModel.refresh_token).commit();
                OozeePreferences.getInstance().getEditor().putInt("userId", loginModel.user_id).commit();
                OozeePreferences.getInstance().getEditor().putString(PHONENUMBER, phoneNumber).commit();
                OozeePreferences.getInstance().getEditor().putString(USERNAME, firstNameString +
                        " " + lastNameString).commit();
                if (loginModel.planModel != null) {
                    OozeePreferences.getInstance().getEditor().putString(USERSUBSCRIPTION, loginModel.planModel.name).apply();
                }

                OozeePreferences.getInstance().getEditor().putBoolean("isMutual", true).apply();
                OozeePreferences.getInstance().getEditor().putBoolean("isStatus", true).apply();
                OozeePreferences.getInstance().getEditor().putBoolean("dialTone", true).apply();
                OozeePreferences.getInstance().getEditor().putBoolean("callPopUp", true).apply();

                Intent intent = new Intent(SignUpScreen.this, IntroScreens.class);
                startActivity(intent);
                finish();
            }
        } else if (busModel.serviceName.equalsIgnoreCase("checkSignup")) {
            CheckSignup checkSignup = CheckSignup.getLogin(busModel.jsonElement);
            if (checkSignup != null) {
                if (checkSignup.user_id != 0) {
                    firstNameEditText.setText(checkSignup.first_name);
                    lastNameEditText.setText(checkSignup.last_name);
                    emailEditText.setText(checkSignup.email);
                    dobEditText.setText(checkSignup.dob);
                    bydefaultimageurl = checkSignup.photo;
                    Glide.with(this).load(checkSignup.photo).fitCenter().placeholder(getResources().getDrawable(R.mipmap.blank_profile_xxxhdpi)).into(imageView);

                    if (checkSignup.gender.equalsIgnoreCase("Male")) {
                        maleRadio.setChecked(true);
                        maleRadio.setSelected(true);
                    } else {
                        femaleRadio.setChecked(true);
                        femaleRadio.setSelected(true);
                    }
                }
            }
        }
        else if (busModel.serviceName.equalsIgnoreCase("addMediaProfile")) {
            MediaModel mediaModel = MediaModel.getMediaModel(busModel.jsonElement, true);
            photoStr = mediaModel.idImage;
            Glide.with(this).load(photoStr).into(imageView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    private void getLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                Log.d("LocationLat", latitude + "");
                Log.d("LocationLong", longitude + "");
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        });
    }

    private void selectDate() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dobEditText.setText(getExpireMonth(monthOfYear + 1) + " " + dayOfMonth + ", " + year);
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.setTitle("");
        if (!isDestroyed()) {
            datePickerDialog.show();
        }
    }

    private void calluser(GoogleSignInAccount account) {
        if (account == null) {
            return;
        }
        AccountManager am = AccountManager.get(this);
        Account me = account.getAccount();

        if (me != null) {
            user = me.name;
            am.getAuthToken(me, "oauth2:https://mail.google.com/", null, this, new OnTokenAcquired(), null);
        }
    }

    private class OnTokenAcquired implements AccountManagerCallback<Bundle> {
        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            try {
                Bundle bundle = result.getResult();
                String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);

                Intent intent = new Intent(SignUpScreen.this, IntroScreens.class);
                intent.putExtra("isGoogle", true);
                intent.putExtra("token", token);
                intent.putExtra("user", user);
                startActivity(intent);
                finish();
            } catch (Exception e) {
                Log.d("test", e.getMessage());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GOOGLE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                calluser(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }


        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath;
            if (data != null) {
                if (data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH) != null) {
                    filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);

                    Glide.with(this).load(filePath).into(imageView);
                    MultipartBody.Part body = prepareFilePart(filePath);
                    RequestBody type = RequestBody.create(MultipartBody.FORM, "profile");
                    addMedia(this, body, type, "addMediaProfile");
                }
            }
        }

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE);
    }

    private void strictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


    private void selectProfilePic() {
        Intent intent = new Intent(this, ImageSelectActivity.class);
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
        startActivityForResult(intent, 1213);
    }
}