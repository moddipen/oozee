package com.app.oozee.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.SelectContactsAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.blockednumber.BlockedNumber;
import com.app.oozee.blockednumber.BlockedNumberDao;
import com.app.oozee.blockednumber.BlockedNumberDatabase;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.AsyncExecutorUtil;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.multipleBlock;
import static com.app.oozee.blockednumber.BlockedNumberType.EXACT_MATCH;
import static com.app.oozee.database.AllContactsModel.getAllContacs;

public class SelectContactsScreen extends BaseActivity implements SelectContactsAdapter.SendSelectedContacts,
        Config.VCardInterface {

    private ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
    private ArrayList<LocalContactsModel> tempContactsModelsAdapter = new ArrayList<>();
    private ArrayList<LocalContactsModel> tempContactsModels = new ArrayList<>();
    private ACProgressFlower mProgressDialog;
    private boolean isShare;
    private boolean isBlock;
    private BlockedNumberDao blockedNumberDao;
    private EditText searchEditText;
    private SelectContactsAdapter selectContactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_contacts_screen);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        searchEditText = findViewById(R.id.searchEditText);
        TextView addTextView = findViewById(R.id.addTextView);
        ImageView backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(v -> finish());

        isShare = getIntent().getBooleanExtra("isShare", false);
        isBlock = getIntent().getBooleanExtra("isBlock", false);

        if (isShare) {
            addTextView.setText("Share");
        } else if (isBlock) {
            addTextView.setText("Block");
        }

        addTextView.setOnClickListener(v -> {
            if (isShare) {
                if (tempContactsModels.size() > 0) {
                    Config.generateVCard(tempContactsModels, SelectContactsScreen.this);
                } else {
                    showToast("Please select atleast one contact to share");
                }

            } else if (isBlock) {
                blockedNumberDao = BlockedNumberDatabase.getInstance(SelectContactsScreen.this).blockedNumberDao();
                if (tempContactsModels.size() > 0) {
                    ArrayList<String> strings = new ArrayList<>();
                    int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                    for (int i = 0; i < tempContactsModels.size(); i++) {
                        strings.add(tempContactsModels.get(i).number);

                        BlockedNumber blockedNumber = new BlockedNumber(EXACT_MATCH, "+91",
                                getNumber(tempContactsModels.get(i).number));
                        addNumber(blockedNumber);
                    }

                    BlockedContactModel blockedContactModel = new BlockedContactModel(userId, 99, strings);
                    addContactsByType(SelectContactsScreen.this, blockedContactModel, multipleBlock.ordinal(), "blockedContacts");
                } else {
                    showToast("Please select atleast one contact to block");
                }
            } else {
                Intent intent = new Intent();
                intent.putExtra("tempContactsModels", tempContactsModels);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterContacts(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                filterContacts(searchEditText.getText().toString().trim());
                hideKeyboard(this);
                return true;
            }
            return false;
        });

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        ArrayList<LocalContactsModel> contactsModelss = getAllContacs(databaseHelper);

        Collections.sort(contactsModelss, (o1, o2) -> o1.name.compareToIgnoreCase(o2.name));

        String initial = "";
        for (int i = 0; i < contactsModelss.size(); i++) {
            String name = contactsModelss.get(i).name;
            String newInitial = getStringInitial(name);
            if (!initial.equalsIgnoreCase(newInitial)) {
                initial = newInitial;
            }
            contactsModels.add(new LocalContactsModel(name, contactsModelss.get(i).date,
                    contactsModelss.get(i).dir, contactsModelss.get(i).number));
        }

        dismissProgressDialog(mProgressDialog);

        boolean trueFalse = false;
        if (isShare || isBlock) {
            trueFalse = true;
        }

        Iterator<LocalContactsModel> i = contactsModels.iterator();
        while (i.hasNext()) {
            LocalContactsModel localContactsModel = i.next();
            if (localContactsModel.name == null || localContactsModel.name.isEmpty() ||
                    localContactsModel.number == null || localContactsModel.number.isEmpty()) {
                i.remove();
            }
        }
        tempContactsModelsAdapter.addAll(contactsModels);

        selectContactsAdapter = new SelectContactsAdapter(tempContactsModelsAdapter, this, trueFalse);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(selectContactsAdapter);
    }

    @Override
    public void created(File file) {
        /*Intent i = new Intent();
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        i.setAction(android.content.Intent.ACTION_VIEW);
        i.setDataAndType(Uri.fromFile(file), "text/x-vcard");
        startActivity(i);*/

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(ContactsContract.Contacts.CONTENT_VCARD_TYPE);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share Oozee Contacts");
        startActivity(intent);
    }

    /*@SuppressLint("StaticFieldLeak")
    private class getContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = MyProgressDialog.show(SelectContactsScreen.this, "", "");
        }

        ArrayList<String> stringss = new ArrayList<>();
        ArrayList<LocalContactsModel> contactsModelss = new ArrayList<>();

        @Override
        protected Void doInBackground(Void... voids) {

            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.Data.CONTACT_ID};

            Cursor people = getContentResolver().query(uri, projection, null, null, null);

            if (people != null) {
                int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int rrawContactId = people.getColumnIndex(ContactsContract.Data.CONTACT_ID);
                if (people.moveToFirst()) {
                    do {
                        String name = people.getString(indexName);
                        String number = people.getString(indexNumber);
                        String rawContactId = people.getString(rrawContactId);

                        ArrayList<String> strings = getContactDetails(rawContactId);
                        String email = "";
                        String image = "";
                        if (strings.size() > 0) {
                            for (int i = 0; i < strings.size(); i++) {
                                if (strings.get(i).contains("@")) {
                                    email = strings.get(i);
                                } else {
                                    image = strings.get(i);
                                }
                            }
                        }

                        if (!stringss.contains(name)) {
                            stringss.add(name);
                            number = number.replaceAll(" ", "");
                            contactsModelss.add(new LocalContactsModel(name, email, image, number));
                        }
                    } while (people.moveToNext());
                }

                people.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //ArrayList<String> strings = stringss;
            Collections.sort(contactsModelss, new Comparator<LocalContactsModel>() {
                @Override
                public int compare(LocalContactsModel o1, LocalContactsModel o2) {
                    return o1.name.compareToIgnoreCase(o2.name);
                }
            });

            String initial = "";
            for (int i = 0; i < contactsModelss.size(); i++) {
                String name = contactsModelss.get(i).name;
                String newInitial = getStringInitial(name);
                if (!initial.equalsIgnoreCase(newInitial)) {
                    initial = newInitial;
                }
                contactsModels.add(new LocalContactsModel(name, contactsModelss.get(i).date,
                        contactsModelss.get(i).dir, contactsModelss.get(i).number));
            }

            dismissProgressDialog(mProgressDialog);

            boolean trueFalse = false;
            if (isShare || isBlock) {
                trueFalse = true;
            }

            SelectContactsAdapter selectContactsAdapter = new SelectContactsAdapter(contactsModels,
                    SelectContactsScreen.this, trueFalse);
            recyclerView.setLayoutManager(new LinearLayoutManager(SelectContactsScreen.this));
            recyclerView.setAdapter(selectContactsAdapter);
        }
    }*/

    @Override
    public void sendContacts(ArrayList<LocalContactsModel> contactsModel) {
        this.tempContactsModels = contactsModel;
    }

    @Subscribe
    public void Contacts(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("blockedContacts")) {
            showToast("Contacts blocked susccessfully");
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    public ArrayList<String> getContactDetails(String contactId) {
        ArrayList<String> strings = new ArrayList<>();

        try (Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.PHOTO_URI,},
                ContactsContract.Data.CONTACT_ID + "=?",
                new String[]{contactId}, null)) {
            if (phoneCursor != null) {
                int idxAvatarUri = phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);

                while (phoneCursor.moveToNext()) {
                    String avatarUri = phoneCursor.getString(idxAvatarUri);

                    if (avatarUri != null && !strings.contains(avatarUri)) {
                        strings.add(avatarUri);
                    }
                }
            }
        }

        try (Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS,},
                ContactsContract.Data.CONTACT_ID + "=?", new String[]{String.valueOf(contactId)}, null)) {
            if (emailCursor != null) {
                int idxAddress = emailCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Email.ADDRESS);
                while (emailCursor.moveToNext()) {
                    String address = emailCursor.getString(idxAddress);
                    if (address != null && !strings.contains(address)) {
                        strings.add(address);
                    }
                }
            }
        }
        return strings;
    }

    public void addNumber(final BlockedNumber number) {
        AsyncExecutorUtil.getInstance().getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                blockedNumberDao.insert(number);
            }
        });
    }

    private String getNumber(String number) {
        number = number.replaceAll("-", "");

        if (number.startsWith("+91")) {
            number = number.replaceAll("\\+91", "");
        }
        if (number.startsWith("91") && number.length() > 10) {
            number = number.replaceFirst("91", "");
        }
        return number;
    }

    private void filterContacts(String string) {
        tempContactsModelsAdapter.clear();
        if (selectContactsAdapter != null) {
            for (int i = 0; i < contactsModels.size(); i++) {
                if (contactsModels.get(i).number.contains(string)) {
                    tempContactsModelsAdapter.add(contactsModels.get(i));
                } else if (contactsModels.get(i).name.toLowerCase().contains(string.toLowerCase())) {
                    tempContactsModelsAdapter.add(contactsModels.get(i));
                }
            }
            selectContactsAdapter.notifyDataSetChanged();
        }
    }
}