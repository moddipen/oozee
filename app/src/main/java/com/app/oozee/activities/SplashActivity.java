package com.app.oozee.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.utils.OozeePreferences;

import static com.app.oozee.utils.Utility.PERMISSIONSSIGNUP;
import static com.app.oozee.utils.Utility.PERMISSION_ALL;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.getTypeFace1;
import static com.app.oozee.utils.Utility.hasPermissions;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        OozeePreferences.getInstance().getEditor().putBoolean("isIdle", false).apply();
        OozeePreferences.getInstance().getEditor().putBoolean("isOutGoing", false).apply();
        OozeePreferences.getInstance().getEditor().putBoolean("isRinging", false).apply();
        OozeePreferences.getInstance().getEditor().putBoolean("isRefreshed", false).apply();

        Button letsStartButton = findViewById(R.id.letsStartButton);
        TextView organizeCallsTextView = findViewById(R.id.organizeCallsTextView);
        TextView socialTextview = findViewById(R.id.socialTextview);
        TextView copyrightTextView = findViewById(R.id.copyrightTextView);

        Typeface typeface = getTypeFace(this);
        Typeface typeface1 = getTypeFace1(this);
        letsStartButton.setTypeface(typeface);
        organizeCallsTextView.setTypeface(typeface1);
        socialTextview.setTypeface(typeface);
        copyrightTextView.setTypeface(typeface);

        letsStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionsDialog();
            }
        });

        boolean isFromNotificaiton = getIntent().getBooleanExtra("isFromNotificaiton", false);
        String accessToken = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
        if (accessToken != null && !accessToken.equalsIgnoreCase("")) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra("isFromNotificaiton", isFromNotificaiton);
            startActivity(intent);
            finish();
        }
    }

    private void permissionsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_permissions);

        TextView continueTextView = dialog.findViewById(R.id.continueTextView);
        TextView textView = dialog.findViewById(R.id.textView);

        Typeface typeface = getTypeFace(this);
        continueTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        continueTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isDestroyed()) {
                    dialog.dismiss();
                }
                if (!hasPermissions(SplashActivity.this, PERMISSIONSSIGNUP)) {
                    ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONSSIGNUP, PERMISSION_ALL);
                } else {
                    Intent intent = new Intent(SplashActivity.this, EnterPhoneNumber.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        if (!isDestroyed() && dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Intent intent = new Intent(SplashActivity.this, EnterPhoneNumber.class);
        startActivity(intent);
        finish();
        /*if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (grantResults.length > 1 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            } else {
                showToast("Please grant all permissions");
            }
        } else {
            showToast("Please grant all permissions");
        }*/
    }
}
