
package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.BlogModels;
import com.app.oozee.utils.Utility;

public class BlogsDetailsScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogs_details_screen);

        ImageView imageView = findViewById(R.id.imageView);
        TextView textView = findViewById(R.id.textView);
        TextView slugTextView = findViewById(R.id.slugTextView);
        WebView webview = findViewById(R.id.webview);

        Typeface typeface = Utility.getTypeFace(this);
        textView.setTypeface(typeface);
        slugTextView.setTypeface(typeface);

        BlogModels blogModels = getIntent().getParcelableExtra("blogdetails");

        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(blogModels.content, "text/html", "UTF-8");

        textView.setText(blogModels.title);
        slugTextView.setText(blogModels.slug);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
