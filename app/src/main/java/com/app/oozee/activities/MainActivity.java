package com.app.oozee.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.adapters.NavigationAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.fragments.ContactsFragment;
import com.app.oozee.fragments.DialPadFragment;
import com.app.oozee.fragments.RecentFragment;
import com.app.oozee.fragments.ReminderFragment;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.NavigationItem;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.Sms;
import com.app.oozee.services.ContactsBackupService;
import com.app.oozee.services.ReadIncomingMessageService;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.NonSwipeableViewPager;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.utils.Utility.PERMISSIONS;
import static com.app.oozee.utils.Utility.PERMISSION_ALL;
import static com.app.oozee.utils.Utility.PHONENUMBER;
import static com.app.oozee.utils.Utility.USERNAME;
import static com.app.oozee.utils.Utility.USERSUBSCRIPTION;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.hasPermissions;

public class MainActivity extends BaseActivity {

    public View recentView;
    private View viewLine1;
    private View viewLine2;
    private View viewLine3;
    private View viewLine4;
    //private View viewLine5;
    private CircleImageView imageView;
    private ImageView close_icon;
    private TextView initialTextView, txt_edit_profile;
    private DrawerLayout drawer;
    public NonSwipeableViewPager viewPager;
    public ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
    public ArrayList<LocalContactsModel> dialPadContacts = new ArrayList<>();
    public ArrayList<LocalContactsModel> recentModels = new ArrayList<>();
    public ArrayList<LocalContactsModel> allCallLogs = new ArrayList<>();
    public ArrayList<Sms> messages = new ArrayList<>();
    private boolean isFromNotificaiton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        startService(new Intent(this, ReadIncomingMessageService.class));
//        BusManager.getInstance().register(this);

        getLocation();

        ImageView menuImageView = findViewById(R.id.menuImageView);
        final ImageView notificationImageView = findViewById(R.id.notificationImageView);
        final ImageView searchImageView = findViewById(R.id.searchImageView);
        final TextView searchEditText = findViewById(R.id.searchEditText);
        final TextView searchEditText1 = findViewById(R.id.searchEditText1);

        isFromNotificaiton = getIntent().getBooleanExtra("isFromNotificaiton", false);

        NavigationView navigationView = findViewById(R.id.nav_view);

        Typeface typeface = getTypeFace(this);
        searchEditText.setTypeface(typeface);
        searchEditText1.setTypeface(typeface);

        View contactsView = findViewById(R.id.contactsView);
        recentView = findViewById(R.id.recentView);
        View dialpadView = findViewById(R.id.dialpadView);
        View remindersView = findViewById(R.id.remindersView);
        //View messagesView = findViewById(R.id.messagesView);

        ImageView imageView1 = contactsView.findViewById(R.id.imageView);
        viewLine1 = contactsView.findViewById(R.id.viewLine);

        ImageView imageView2 = recentView.findViewById(R.id.imageView);
        viewLine2 = recentView.findViewById(R.id.viewLine);

        ImageView imageView3 = dialpadView.findViewById(R.id.imageView);
        viewLine3 = dialpadView.findViewById(R.id.viewLine);

        ImageView imageView4 = remindersView.findViewById(R.id.imageView);
        viewLine4 = remindersView.findViewById(R.id.viewLine);

        //ImageView imageView5 = messagesView.findViewById(R.id.imageView);
        //viewLine5 = messagesView.findViewById(R.id.viewLine);

        final ImageView showOptionsImageView = findViewById(R.id.showOptionsImageView);

        imageView1.setImageDrawable(getResources().getDrawable(R.mipmap.contacts));
        imageView2.setImageDrawable(getResources().getDrawable(R.mipmap.recent));
        imageView3.setImageDrawable(getResources().getDrawable(R.mipmap.dialpad));
        imageView4.setImageDrawable(getResources().getDrawable(R.mipmap.reminder));
        //imageView5.setImageDrawable(getResources().getDrawable(R.mipmap.messages));

        drawer = findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                showOptionsImageView.setVisibility(View.GONE);
                searchImageView.setVisibility(View.GONE);
                notificationImageView.setVisibility(View.GONE);
                searchEditText.setVisibility(View.GONE);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                showOptionsImageView.setVisibility(View.VISIBLE);
                searchImageView.setVisibility(View.VISIBLE);
                notificationImageView.setVisibility(View.VISIBLE);
                searchEditText.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                showOptionsImageView.setVisibility(View.GONE);
                searchImageView.setVisibility(View.GONE);
                notificationImageView.setVisibility(View.GONE);
                searchEditText.setVisibility(View.GONE);
            }
        };

        notificationImageView.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, NotificationsScreen.class);
            startActivity(intent);
        });

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        viewPager = findViewById(R.id.viewPager);

        contactsView.setOnClickListener(v -> {
            setVisibleView(1);
            viewPager.setCurrentItem(0);
        });

        recentView.setOnClickListener(v -> {
            setVisibleView(2);
            viewPager.setCurrentItem(1);
        });

        dialpadView.setOnClickListener(v -> {
            setVisibleView(3);
            viewPager.setCurrentItem(2);
        });

        remindersView.setOnClickListener(v -> {
            setVisibleView(4);
            viewPager.setCurrentItem(3);
        });

        /*messagesView.setOnClickListener(v -> {
            setVisibleView(5);
            viewPager.setCurrentItem(4);
        });*/

        showOptionsImageView.setOnClickListener(this::showPopUpWindow);

        menuImageView.setOnClickListener(v -> {
            showOptionsImageView.setVisibility(View.GONE);
            searchImageView.setVisibility(View.GONE);
            notificationImageView.setVisibility(View.GONE);
            searchEditText.setVisibility(View.GONE);
            drawer.openDrawer(GravityCompat.START);
        });

        setVisibleView(1);

        RecyclerView recyclerView = navigationView.getHeaderView(0).findViewById(R.id.navRecyclerView);
        imageView = navigationView.getHeaderView(0).findViewById(R.id.imageView);
        close_icon = navigationView.getHeaderView(0).findViewById(R.id.close_icon);
        txt_edit_profile = navigationView.getHeaderView(0).findViewById(R.id.txt_edit_profile);
        TextView textView = navigationView.getHeaderView(0).findViewById(R.id.textView);
        TextView nameTextView = navigationView.getHeaderView(0).findViewById(R.id.nameTextView);
        initialTextView = navigationView.getHeaderView(0).findViewById(R.id.initialTextView);

        close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(Gravity.START);
            }
        });

        imageView.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, UserProfileScreen.class);
            startActivity(intent);
        });

        txt_edit_profile.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, UserProfileScreen.class);
            startActivity(intent);
        });

        String phoneNumber = OozeePreferences.getInstance().getPref().getString(PHONENUMBER, "");
        String userName = OozeePreferences.getInstance().getPref().getString(USERNAME, "");
        textView.setText(phoneNumber);
        nameTextView.setText(userName);
        initialTextView.setText(getStringInitial(userName));

        ArrayList<NavigationItem> navigationItems = new ArrayList<>();

        String subscription = OozeePreferences.getInstance().getPref().getString(USERSUBSCRIPTION, "Free");
        if (subscription == null || subscription.equalsIgnoreCase("Free")) {
            navigationItems.add(new NavigationItem(getResources().getDrawable(R.mipmap.premium), "Oozee Premium"));
        }
        navigationItems.add(new NavigationItem(getResources().getDrawable(R.mipmap.eedback), "Feedback"));
        navigationItems.add(new NavigationItem(getResources().getDrawable(R.mipmap.invite), "Invite"));
        navigationItems.add(new NavigationItem(getResources().getDrawable(R.mipmap.faq), "FAQ"));
        navigationItems.add(new NavigationItem(getResources().getDrawable(R.mipmap.setting), "Settings"));

        NavigationAdapter navigationAdapter = new NavigationAdapter(navigationItems, this, drawer);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(navigationAdapter);

        searchEditText.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SearchScreenActivity.class);
            startActivity(intent);
        });


        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            setupViewPager(viewPager);
            viewPager.setOffscreenPageLimit(4);
            viewPager.setCurrentItem(0);

            new Handler().postDelayed(() -> {
                if (isFromNotificaiton) {
                    viewPager.setCurrentItem(3);
                }
            }, 1000);
        }
    }

    private void setVisibleView(int i) {
        viewLine1.setVisibility(View.INVISIBLE);
        viewLine2.setVisibility(View.INVISIBLE);
        viewLine3.setVisibility(View.INVISIBLE);
        viewLine4.setVisibility(View.INVISIBLE);
        //viewLine5.setVisibility(View.INVISIBLE);

        if (i == 1) {
            viewLine1.setVisibility(View.VISIBLE);
        } else if (i == 2) {
            viewLine2.setVisibility(View.VISIBLE);
        } else if (i == 3) {
            viewLine3.setVisibility(View.VISIBLE);
        } else if (i == 4) {
            viewLine4.setVisibility(View.VISIBLE);
        } else if (i == 5) {
            //viewLine5.setVisibility(View.VISIBLE);
        }
    }

    private class MainAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        private MainAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        MainAdapter adapter = new MainAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContactsFragment());
        adapter.addFragment(new RecentFragment());
        adapter.addFragment(new DialPadFragment());
        adapter.addFragment(new ReminderFragment());
        //adapter.addFragment(new MessagesFragment());
        viewPager.setAdapter(adapter);
    }

    public void showPopUpWindow(View view) {
        Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_right);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView declineTextView = dialog.findViewById(R.id.declineTextView);
        TextView blockTextView = dialog.findViewById(R.id.blockTextView);
        TextView shareTextView = dialog.findViewById(R.id.shareTextView);
        TextView recordTextView = dialog.findViewById(R.id.recordTextView);

        Typeface typeface = getTypeFace(this);
        declineTextView.setTypeface(typeface);
        blockTextView.setTypeface(typeface);
        recordTextView.setTypeface(typeface);
        shareTextView.setTypeface(typeface);

        recordTextView.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(this, RecordCallsActivity.class);
            startActivity(intent);
        });

        blockTextView.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(this, BlockedContacts.class);
            intent.putExtra("isBlock", true);
            startActivity(intent);
        });

        shareTextView.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(this, SelectContactsScreen.class);
            intent.putExtra("isShare", true);
            startActivity(intent);
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.TOP | Gravity.END);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Added Contact", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Cancelled Added Contact", Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_OK && requestCode == 121) {
            new Handler().postDelayed(() -> {
                BusModel busModel = new BusModel();
                busModel.serviceName = "refreshContacts";
                BusManager.getInstance().post(busModel);
            }, 500);
        }
    }

    public static String getStringInitial(String name) {
        if (name != null) {
            if (name.length() > 1) {
                return name.substring(0, 1).toUpperCase();
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    private void getLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                Log.d("LocationLat", latitude + "");
                Log.d("LocationLong", longitude + "");

                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                ProfileModel profileModel = new ProfileModel(userId, latitude, longitude);
                updateLocationCall(MainActivity.this, profileModel, "ignore");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("grantpermissions", grantResults.length + "");
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (grantResults.length > 1 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (grantResults.length > 2 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    if (grantResults.length > 3 && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                        if (grantResults.length > 4 && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                            if (grantResults.length > 5 && grantResults[5] == PackageManager.PERMISSION_GRANTED) {
                                if (grantResults.length > 6 && grantResults[6] == PackageManager.PERMISSION_GRANTED) {
                                    if (grantResults.length > 7 && grantResults[7] == PackageManager.PERMISSION_GRANTED) {
                                        if (grantResults.length > 8 && grantResults[8] == PackageManager.PERMISSION_GRANTED) {
                                            if (grantResults.length > 9 && grantResults[9] == PackageManager.PERMISSION_GRANTED) {
                                                //if (grantResults.length > 10 && grantResults[10] == PackageManager.PERMISSION_GRANTED) {
                                                   // if (grantResults.length > 11 && grantResults[11] == PackageManager.PERMISSION_GRANTED) {
                                                        setupViewPager(viewPager);
                                                        viewPager.setOffscreenPageLimit(4);
                                                        viewPager.setCurrentItem(0);
                                                    /*} else {
                                                        showToast("Please grant all permissions");
                                                        finish();
                                                    }
                                                } else {
                                                    showToast("Please grant all permissions");
                                                    finish();
                                                }*/
                                            } else {
                                                showToast("Please grant all permissions");
                                                finish();
                                            }
                                        } else {
                                            showToast("Please grant all permissions");
                                            finish();
                                        }
                                    } else {
                                        showToast("Please grant all permissions");
                                        finish();
                                    }
                                } else {
                                    showToast("Please grant all permissions");
                                    finish();
                                }
                            } else {
                                showToast("Please grant all permissions");
                                finish();
                            }
                        } else {
                            showToast("Please grant all permissions");
                            finish();
                        }
                    } else {
                        showToast("Please grant all permissions");
                        finish();
                    }
                } else {
                    showToast("Please grant all permissions");
                    finish();
                }
            } else {
                showToast("Please grant all permissions");
                finish();
            }
        } else {
            showToast("Please grant all permissions");
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        drawer.closeDrawers();
        String phto = OozeePreferences.getInstance().getPref().getString("photoStr", "");
        if (imageView != null && initialTextView != null) {
            if (phto != null && !phto.equalsIgnoreCase("")) {
                initialTextView.setVisibility(View.GONE);
                Glide.with(this).load(phto).into(imageView);
            }
        }

        boolean oozeeContactsToDB = OozeePreferences.getInstance().getPref().getBoolean("oozeeContactstodb", false);
        if (oozeeContactsToDB) {
            Intent intent = new Intent(this, ContactsBackupService.class);
            stopService(intent);
            startService(intent);
        }
    }
}
