package com.app.oozee.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.app.oozee.R;
import com.app.oozee.adapters.RecordingAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.RecordingModel;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.models.RecordingModel.getRecordingList;

public class RecordingsScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordings_screen);

        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        RecordingModel recordingModel = new RecordingModel(userId);
        getRecordings(this, recordingModel, "recordings");

    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("recordings")) {
            ArrayList<RecordingModel> recordingModels = getRecordingList(busModel.jsonElement);
            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            RecordingAdapter recordingAdapter = new RecordingAdapter(recordingModels, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(recordingAdapter);
        }
    }
}
