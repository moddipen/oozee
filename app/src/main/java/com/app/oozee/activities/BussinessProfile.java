package com.app.oozee.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.UpdateProfileModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

public class BussinessProfile extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bussiness_profile);

        final ProfileModel profileModel = getIntent().getParcelableExtra("profileModel");

        ImageView imageView = findViewById(R.id.imageView);
        Button buttonBussiness = findViewById(R.id.buttonBussiness);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final TextInputEditText companyNameEditText = findViewById(R.id.companyNameEditText);
        final TextInputEditText companyAddressEditText = findViewById(R.id.companyAddressEditText);
        final TextInputEditText websiteEditText = findViewById(R.id.websiteEditText);
        final TextInputEditText industryEditText = findViewById(R.id.industryEditText);

        companyNameEditText.setText(profileModel.company_name);
        companyAddressEditText.setText(profileModel.company_address);
        websiteEditText.setText(profileModel.website);
        industryEditText.setText(profileModel.industry);

        buttonBussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String companyNameEditTextStr = companyNameEditText.getEditableText().toString().trim();
                String companyAddressEditTextStr = companyAddressEditText.getEditableText().toString().trim();
                String websiteEditTextStr = websiteEditText.getEditableText().toString().trim();
                String industryEditTextStr = industryEditText.getEditableText().toString().trim();

                if (companyNameEditTextStr.equalsIgnoreCase("")) {
                    showToast("Please enter company name");
                    return;
                }

                if (companyAddressEditTextStr.equalsIgnoreCase("")) {
                    showToast("Please enter company address");
                    return;
                }

                profileModel.company_name = companyNameEditTextStr;
                profileModel.company_address = companyAddressEditTextStr;
                profileModel.website = websiteEditTextStr;
                profileModel.industry = industryEditTextStr;

                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                UpdateProfileModel profileModels = new UpdateProfileModel(userId, profileModel.first_name,
                        profileModel.last_name, profileModel.email, profileModel.nick_name, profileModel.gender,
                        profileModel.about, profileModel.address, profileModel.website,
                        profileModel.industry, profileModel.company_name, profileModel.company_address,
                        profileModel.photo);
                updateProfileCall(BussinessProfile.this, profileModels, "profileupdatedBussiness");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("profileupdatedBussiness")) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
