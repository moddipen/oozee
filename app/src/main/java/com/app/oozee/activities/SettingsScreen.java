package com.app.oozee.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.SettingsAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BlogModels;
import com.app.oozee.models.BusModel;
import com.app.oozee.utils.BusManager;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.models.BlogModels.getBlogsList;
import static com.app.oozee.models.BlogModels.getcmsList;

public class SettingsScreen extends BaseActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_screen);

        recyclerView = findViewById(R.id.recyclerView);
        TextView titleTextView = findViewById(R.id.titleTextView);
        ImageView backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(v -> finish());

        if (getIntent().getBooleanExtra("blogs", false)) {
            titleTextView.setText("Blogs");
            getBlogs(this, "blogs");
        } else {
            titleTextView.setText("Settings");
            getCmsPages(this, "cmsPages");
        }
    }

    @Subscribe
    public void GetCmsPages(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("cmsPages")) {
            ArrayList<BlogModels> blogModels = getcmsList(busModel.jsonElement);

            //blogModels.add(0, new BlogModels("Blocked Contacts"));
            //blogModels.add(1, new BlogModels("Dead Contacts"));
            blogModels.add(new BlogModels("Backup Contacts"));
            blogModels.add(new BlogModels("Security"));
            SettingsAdapter settingsAdapter = new SettingsAdapter(blogModels, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(settingsAdapter);
        } else if (busModel.serviceName.equalsIgnoreCase("blogs")) {
            ArrayList<BlogModels> blogModels = getBlogsList(busModel.jsonElement);
            SettingsAdapter settingsAdapter = new SettingsAdapter(blogModels, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(settingsAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }
}
