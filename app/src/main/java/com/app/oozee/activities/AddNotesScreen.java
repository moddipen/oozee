package com.app.oozee.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.NotesModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.app.oozee.utils.Utility;
import com.squareup.otto.Subscribe;

import java.util.Objects;

import static com.app.oozee.utils.Utility.getTypeFace;

public class AddNotesScreen extends BaseActivity {

    private boolean isFromEdit;
    private int noteId;
    private EditText descriptionEditText;
    private EditText titleEdittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes_screen);

        Button submitButton = findViewById(R.id.submitButton);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        titleEdittext = findViewById(R.id.titleEdittext);
        ImageView backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Typeface typeface = Utility.getTypeFace(this);
        submitButton.setTypeface(typeface);
        titleEdittext.setTypeface(typeface);
        descriptionEditText.setTypeface(typeface);

        final String phoneNumber = getIntent().getStringExtra("phoneNumber");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleEdittext.getText().toString().trim();
                String note = descriptionEditText.getText().toString().trim();

                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                NotesModel notesModel = new NotesModel(userId, title, note, phoneNumber, "99");

                if (isFromEdit) {
                    editNote(AddNotesScreen.this, noteId, notesModel, "updateNotes");
                } else {
                    addNote(AddNotesScreen.this, notesModel, "addedNotese");
                }
            }
        });

        isFromEdit = getIntent().getBooleanExtra("isFromEdit", false);
        if (isFromEdit) {
            NotesModel notesModel = getIntent().getParcelableExtra("notesModel");
            noteId = notesModel.id;
            titleEdittext.setText(notesModel.title);
            descriptionEditText.setText(notesModel.note);

            submitButton.setText("Update Note");
        }
    }

    @Subscribe
    public void AddNote(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("addedNotese")) {
            showToast("Note added");
            setResult(RESULT_OK);
            finish();
        } else if (busModel.serviceName.equalsIgnoreCase("updateNotes")) {
            showToast("Note updated");
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (!titleEdittext.getText().toString().trim().equalsIgnoreCase("") ||
                !descriptionEditText.getText().toString().trim().equalsIgnoreCase("")) {
            super.onBackPressed();
            showPopUpWindow();
        } else {
            finish();
        }
    }

    public void showPopUpWindow() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        imageView.setText("Notes");
        textView.setText("You have not saved note yet. If you do not save it, it would not be visible further. Do you want to continue?");
        cancelTextView.setText("Cancel");
        performTextView.setText("Yes");

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }
}