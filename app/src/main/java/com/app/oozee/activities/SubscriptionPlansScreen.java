package com.app.oozee.activities;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.FeaturesAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.PlanModel;
import com.app.oozee.util.IabHelper;
import com.app.oozee.util.IabResult;
import com.app.oozee.util.Inventory;
import com.app.oozee.util.Purchase;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.CenterScrollListener;
import com.app.oozee.utils.OozeePreferences;
import com.app.oozee.utils.ScrollZoomLayoutManager;
import com.app.oozee.utils.Utility;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.util.IabHelper.IABHELPER_USER_CANCELLED;
import static com.app.oozee.utils.Utility.USERSUBSCRIPTION;

public class SubscriptionPlansScreen extends BaseActivity {

    private ScrollZoomLayoutManager scrollZoomLayoutManager;
    private RecyclerView recyclerView;
    private TextView txtView;
    private ArrayList<PlanModel> planModels = new ArrayList<>();
    private IabHelper mHelper;
    private String ITEM_SKU = "sku_123.premium";  /*android.test.purchased*/
    private String PURCHASEFAILED = "Failed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_plans_screen);

        recyclerView = findViewById(R.id.recyclerView);
        txtView = findViewById(R.id.txtView);
        ImageView imageView = findViewById(R.id.imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (planModels != null && planModels.size() > 0) {
                        txtView.setText(planModels.get(scrollZoomLayoutManager.getCurrentPosition()).name);
                    }
                }
            });
        }

        /*asfafaf*/

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAph9msdMHx7QPvf+spED9f+ed20u7JI9KU0aUDPI19dpIByv7BuUJAiaVWUmIfjssCwZQnvC5hkEZisnnPn9wU4w/AqO19OX+h2ehJDEfqri2quMkHw4Nc7u5Psj6cZWmZBbOAvC52AzoRMxy5YRUiEB3lqk+foM7gp3PZrKo8oqms9Y2zxYBAyDLa8rpPKV2RkYDNw/uVcRNUFiTVhvXnGeEQ+7I4iUl5dGOKi/92SCDo6EFU3NAOjuvNyvlTSCdIwFIIoSABmPEFFS6KJ6TfdPzTQpwzwFpT6l2JHCG8U+GHNOu6RowlqAIuwtWUVV5q5tnLtN91rzr3rMEreEnBwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    showToast("In-app Billing setup failed: " + result);
                } else {
                    showToast("In-app Billing is set up OK");
                }
            }
        });

        getPlans(this, "plans");
    }

    @Subscribe
    public void Subscription(BusModel busModel) {

        if (busModel.serviceName.equalsIgnoreCase("plans")) {
            planModels = PlanModel.getPlansList(busModel.jsonElement);

            String planString = OozeePreferences.getInstance().getPref().getString(USERSUBSCRIPTION, "Free");
            for (int i = 0 ; i < planModels.size(); i ++){
                if (planString != null && planString.equalsIgnoreCase(planModels.get(i).name)){
                    planModels.remove(i);
                    break;
                }
            }
            scrollZoomLayoutManager = new ScrollZoomLayoutManager(this, Dp2px(10));
            recyclerView.addOnScrollListener(new CenterScrollListener());
            recyclerView.setLayoutManager(scrollZoomLayoutManager);
            recyclerView.setAdapter(new Adapter(planModels));
        }
    }

    class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<PlanModel> planModels;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(SubscriptionPlansScreen.this).
                    inflate(R.layout.conceirge_layout_swipe, parent, false));
        }

        public Adapter(ArrayList<PlanModel> planModels) {
            this.planModels = planModels;
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

            ((MyViewHolder) holder).titleTextView.setText(planModels.get(position).name);

            if (planModels.get(position).price == 0) {
                ((MyViewHolder) holder).buyNowButton.setText("FREE");
            } else {
                ((MyViewHolder) holder).buyNowButton.setText("INR " + planModels.get(position).price);
            }


            FeaturesAdapter featuresAdapter = new FeaturesAdapter(planModels, SubscriptionPlansScreen.this,
                    position);
            ((MyViewHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(SubscriptionPlansScreen.this));
            ((MyViewHolder) holder).recyclerView.setAdapter(featuresAdapter);

            ((MyViewHolder) holder).buyNowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!((MyViewHolder) holder).buyNowButton.getText().toString().trim().equalsIgnoreCase("FREE")) {
                        buyClick();
                    } else {
                        showToast("You have the same plan enabled currently, please choose a different plan");
                    }

                }
            });

            /*if (position == 0) {
                ((MyViewHolder) holder).scrollView.setBackgroundColor(getResources().getColor(R.color.orangeColor));
            } else {
                ((MyViewHolder) holder).scrollView.setBackgroundColor(getResources().getColor(R.color.purpleColor));
            }*/

        }

        @Override
        public int getItemCount() {
            return planModels.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView titleTextView;
            private ScrollView scrollView;
            private TextView features;
            private Button buyNowButton;
            private RecyclerView recyclerView;

            MyViewHolder(View itemView) {
                super(itemView);
                titleTextView = itemView.findViewById(R.id.titleTextView);
                features = itemView.findViewById(R.id.features);
                buyNowButton = itemView.findViewById(R.id.buyNowButton);
                recyclerView = itemView.findViewById(R.id.recyclerView);
                scrollView = itemView.findViewById(R.id.scrollView);

                Typeface typeface = Utility.getTypeFace(SubscriptionPlansScreen.this);
                titleTextView.setTypeface(typeface);
                features.setTypeface(typeface);
                buyNowButton.setTypeface(typeface);
            }
        }
    }

    public int Dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    public void buyClick() {
        try {
            mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001, mPurchaseFinishedListener,
                    "mypurchasetoken");
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                if (result.getResponse() != IABHELPER_USER_CANCELLED) {
                    if (!isDestroyed()) {
                        showToast(PURCHASEFAILED);
                    }
                }

            } else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
            }
        }
    };

    public void consumeItem() {
        try {
            mHelper.queryInventoryAsync(mReceivedInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (result.isFailure()) {
                if (result.getResponse() != IABHELPER_USER_CANCELLED) {
                    if (!isDestroyed()) {
                        showToast(PURCHASEFAILED);
                    }
                }
            } else {
                try {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                if (!isDestroyed()) {
                    showToast("Success");
                }
            } else {
                if (result.getResponse() != IABHELPER_USER_CANCELLED) {
                    if (!isDestroyed()) {
                        showToast(PURCHASEFAILED);
                    }
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) {
            try {
                mHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                e.printStackTrace();
            }
        }
        mHelper = null;
    }
}
