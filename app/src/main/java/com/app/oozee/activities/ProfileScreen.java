package com.app.oozee.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.adapters.ChipsAdapter;
import com.app.oozee.adapters.ContactsAdapter;
import com.app.oozee.adapters.NotesAdapter;
import com.app.oozee.adapters.TagAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.blockednumber.BlockedNumber;
import com.app.oozee.blockednumber.BlockedNumberDao;
import com.app.oozee.blockednumber.BlockedNumberDatabase;
import com.app.oozee.chatmodule.ChatActivity;
import com.app.oozee.database.AllContactsModel;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.NotesModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.TagModels;
import com.app.oozee.utils.AsyncExecutorUtil;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;
import com.squareup.otto.Subscribe;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;
import com.xiaofeng.flowlayoutmanager.cache.Line;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.quicklist;
import static com.app.oozee.blockednumber.BlockedNumberType.EXACT_MATCH;
import static com.app.oozee.database.TagsDb.getAllTags;
import static com.app.oozee.database.TagsDb.insertAllTags;
import static com.app.oozee.models.TagModels.getSubTagList;
import static com.app.oozee.models.TagModels.getTagList;
import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.Config.checkSubscription;
import static com.app.oozee.utils.Config.deleteContact;
import static com.app.oozee.utils.Config.generateVCard;
import static com.app.oozee.utils.Config.modifyContact;
import static com.app.oozee.utils.Config.openDialog;
import static com.app.oozee.utils.Config.openWhatsapp;
import static com.app.oozee.utils.Config.sendEmail;
import static com.app.oozee.utils.Config.voiceCall;
import static com.app.oozee.utils.Utility.COUNTRYID;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.getTypeFace1;

public class ProfileScreen extends BaseActivity implements Config.DeleteConfirmed, Config.VCardInterface,
        NotesAdapter.DeleteNote, ChipsAdapter.TagSubTag, TagAdapter.AddTagInterface {

    private LocalContactsModel contactsModel;
    private ArrayList<TagModels> tagModels = new ArrayList<>();
    private RecyclerView chipsRecyclerView;
    private TextView textView;
    private BottomSheetBehavior bottomSheetBehavior;
    private TextView mutualTextView;
    private RecyclerView recyclerView;
    private RecyclerView tagLinear;
    private ArrayList<TagModels> profileTagsModel = new ArrayList<>();
    private TextView emailActual;
    private TextView locationActual;
    private TextView location;
    private TextView email;
    private View emailLocationView;
    private RelativeLayout locationRelative;
    private DatabaseHelper databaseHelper;
    private int contactUserId = 0;
    private int isBlockId = 0;
    private int tagId = 0;
    private boolean isCallLog;
    private BlockedNumberDao blockedNumberDao;
    private TextView historyButton;
    private TextView addToContactsButton;
    private LinearLayout emailLinear;
    private TextView notesButton;
    private TextView initialTextView;
    private CircleImageView profileImageView;
    private TextView genderTextview;
    private ScrollView scrollView;
    private String imagelinkornot ;
    private boolean isquicklist = false;
    private List<BlockedNumber> blockedNumberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details_screen);

        Typeface typeface = getTypeFace(this);
        Typeface typeface1 = getTypeFace1(this);
        TextView nameTextView = findViewById(R.id.nameTextView);
        TextView numberTextView = findViewById(R.id.numberTextView);
        mutualTextView = findViewById(R.id.mutualTextView);
        recyclerView = findViewById(R.id.recyclerView);
        tagLinear = findViewById(R.id.tagLinear);
        genderTextview = findViewById(R.id.genderTextview);
        location = findViewById(R.id.location);
        locationActual = findViewById(R.id.locationActual);
        emailLocationView = findViewById(R.id.emailLocationView);
        locationRelative = findViewById(R.id.locationRelative);
        email = findViewById(R.id.email);
        emailActual = findViewById(R.id.emailActual);
        emailLinear = findViewById(R.id.emailLinear);
        notesButton = findViewById(R.id.notesButton);
        TextView share = findViewById(R.id.share);
        TextView searchweb = findViewById(R.id.searchweb);
        TextView seemap = findViewById(R.id.seemap);
        initialTextView = findViewById(R.id.initialTextView);
        ImageView imageView = findViewById(R.id.imageView);
        ImageView settings = findViewById(R.id.settings);
        ImageView one = findViewById(R.id.one);
        ImageView two = findViewById(R.id.two);
        ImageView three = findViewById(R.id.three);
        ImageView four = findViewById(R.id.four);
        profileImageView = findViewById(R.id.profileImageView);

        LinearLayout shareLayout = findViewById(R.id.shareLayout);
        LinearLayout searchWebLayout = findViewById(R.id.searchWebLayout);
        LinearLayout seeOnMapLayout = findViewById(R.id.seeOnMapLayout);

        historyButton = findViewById(R.id.historyButton);
        addToContactsButton = findViewById(R.id.addToContactsButton);
        scrollView = findViewById(R.id.scrollView);
        addToContactsButton.setVisibility(View.GONE);

        nameTextView.setTypeface(typeface);
        numberTextView.setTypeface(typeface);
        mutualTextView.setTypeface(typeface);
        location.setTypeface(typeface);
        locationActual.setTypeface(typeface);
        email.setTypeface(typeface);
        emailActual.setTypeface(typeface);
        share.setTypeface(typeface);
        searchweb.setTypeface(typeface);
        seemap.setTypeface(typeface);
        historyButton.setTypeface(typeface);
        initialTextView.setTypeface(typeface);

        databaseHelper = new DatabaseHelper(this);
        contactsModel = getIntent().getParcelableExtra("contactsModel");
        String number = contactsModel.number;
        number = number.replaceAll("-", "");

        if (number.startsWith("+91")) {
            number = number.replaceAll("\\+91", "");
        }
        if (number.startsWith("91") && number.length() > 10) {
            number = number.replaceFirst("91", "");
        }
        contactsModel.number = number;

        isCallLog = getIntent().getBooleanExtra("isCallLog", false);
        if (isCallLog) {
            String name = AllContactsModel.getNameFromNumber(databaseHelper, contactsModel.number);
            if (name.equalsIgnoreCase("")) {
                isquicklist = true;
                addToContactsButton.setVisibility(View.VISIBLE);
                //historyButton.setText("+ Add to contacts");
            } else {
                addToContactsButton.setVisibility(View.GONE);
                historyButton.setText("History");
            }
        }

        nameTextView.setText(contactsModel.name);

        if (contactsModel.number.length() != 10) {
            numberTextView.setText(contactsModel.number);
        } else {
            numberTextView.setText("+91" + contactsModel.number);
        }

        initialTextView.setText(contactsModel.initial);

        imagelinkornot = contactsModel.image;

        if (contactsModel.image != null && !contactsModel.image.equalsIgnoreCase("")) {
            Uri uri = Uri.parse(contactsModel.image);
            /*profileImageView.setImageURI(uri);*/
            Glide.with(this).load(uri).into(profileImageView);
            initialTextView.setVisibility(View.GONE);
        } else {
            Glide.with(three).load(getResources().getDrawable(R.drawable.circle)).fitCenter().into(profileImageView);
            initialTextView.setVisibility(View.VISIBLE);
        }

        RelativeLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        textView = findViewById(R.id.textView);
        chipsRecyclerView = findViewById(R.id.chipsRecyclerView);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    scrollView.setAlpha(1f);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        imageView.setOnClickListener(v -> finish());

        settings.setOnClickListener(this::showPopUpWindow);

        addToContactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, "")
                        .putExtra(ContactsContract.Intents.Insert.PHONE, contactsModel.number);
                startActivityForResult(contactIntent, 1);
            }
        });

        historyButton.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileScreen.this, CallLogActivity.class);
            intent.putExtra("contacts", contactsModel);
            startActivity(intent);
        });

        shareLayout.setOnClickListener(v -> {
            ArrayList<LocalContactsModel> contactsModels1 = new ArrayList<>();
            contactsModels1.add(contactsModel);
            generateVCard(contactsModels1, ProfileScreen.this);
        });

        searchWebLayout.setOnClickListener(v -> {
            String escapedQuery = "";
            try {
                escapedQuery = URLEncoder.encode(contactsModel.number, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Uri uri = Uri.parse("http://www.google.com/#q=" + escapedQuery);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });

        seeOnMapLayout.setOnClickListener(v -> {
            String map = "http://maps.google.co.in/maps?q=" + locationActual.getText().toString().trim();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
            startActivity(intent);
        });

        one.setOnClickListener(v -> callPhone(ProfileScreen.this, contactsModel.number));

        two.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileScreen.this, MessagesScreen.class);
            intent.putExtra("address", contactsModel.number);
            intent.putExtra("smsid", "0");
            intent.putExtra("image", contactsModel.image);
            startActivity(intent);
        });

        emailLinear.setOnClickListener(v -> sendEmail(ProfileScreen.this, emailActual.getText().toString().trim()));

        notesButton.setOnClickListener(v -> {
            Intent intent = new Intent(ProfileScreen.this, NotesScreen.class);
            intent.putExtra("number", contactsModel.number);
            startActivity(intent);
        });

        blockedNumberDao = BlockedNumberDatabase.getInstance(this).blockedNumberDao();
        AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberList = blockedNumberDao.getAll());

        getWhatsAppNumbers(contactsModel.name, three, four);

        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);

        BlockedContactModel blockedContacts = new BlockedContactModel(userId,
                contactsModel.number, countryid);
        getcontactDetails(this, blockedContacts, "contactDetails");

    }

    public void showPopUpWindow(View view) {
        Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_contacts_details);
        dialog.setCancelable(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView blockTextView = dialog.findViewById(R.id.blockTextView);
        TextView addToQuickList = dialog.findViewById(R.id.addToQuickList);
        TextView recordTextView = dialog.findViewById(R.id.recordTextView);
        TextView blockNumber = dialog.findViewById(R.id.blockNumber);
        TextView addNotes = dialog.findViewById(R.id.addNotes);
      //  TextView chatTextView = dialog.findViewById(R.id.chatTextView);
        TextView markSpam = dialog.findViewById(R.id.markSpam);

        if (isquicklist == true) {
            addToQuickList.setVisibility(View.GONE);
        }

        Typeface typeface = getTypeFace(this);
        blockTextView.setTypeface(typeface);
        recordTextView.setTypeface(typeface);
        blockNumber.setTypeface(typeface);
        addToQuickList.setTypeface(typeface);
        addNotes.setTypeface(typeface);
      //  chatTextView.setTypeface(typeface);
        markSpam.setTypeface(typeface);

        if (contactUserId == 0) {
        //    chatTextView.setVisibility(View.GONE);
        }

        if (isBlockId != 0) {
            blockNumber.setText("Unblock Contact");
        }

        recordTextView.setOnClickListener(v -> {
            dialog.dismiss();
            modifyContact(contactsModel.number, this);
        });

        blockTextView.setOnClickListener(v -> {
            dialog.dismiss();
            openDialog(this, "Are you sure you want to delete this contact?");
        });

        addToQuickList.setOnClickListener(v -> {
            dialog.dismiss();
            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            BlockedContactModel blockedContacts = new BlockedContactModel(userId,
                    contactsModel.number, 99,contactsModel.image);
            addContactsByType(this, blockedContacts, quicklist.ordinal(),
                    "quicklist");
        });

        blockNumber.setOnClickListener(v -> {
            dialog.dismiss();

            if (blockNumber.getText().toString().toLowerCase().contains("unblock".toLowerCase())) {
                unblockContact();
            } else {
                showPopUpWindow(contactsModel);
            }
        });

        addNotes.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(this, AddNotesScreen.class);
            intent.putExtra("phoneNumber", contactsModel.number);
            startActivityForResult(intent, 221);
        });
/*
        chatTextView.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("username", contactsModel.name);
            intent.putExtra("contactUserId", contactUserId);
            startActivity(intent);
        });*/

        markSpam.setOnClickListener(v -> {
            dialog.dismiss();
            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            TagModels tagModel = new TagModels(userId, contactsModel.number, 99, 0, 0);
            profileTagsModel.add(new TagModels("Spam"));
           // setTagAdapter(profileTagsModel);
            addTag(ProfileScreen.this, tagModel, "");
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.TOP | Gravity.END);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    @Override
    public void delete() {
        deleteContact(contactsModel.number, contactsModel.name, ProfileScreen.this);
        onActivityResult(122, RESULT_OK, new Intent());
    }

    public void getWhatsAppNumbers(String contactName, ImageView three, ImageView four) {
        Cursor cursor1 = getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI,
                new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.ACCOUNT_TYPE + "= ? AND " +
                        ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME_PRIMARY + " = ?",
                new String[]{"com.whatsapp", contactName},
                null);

        if (cursor1 != null) {
            while (cursor1.moveToNext()) {
                String rawContactId = cursor1.getString(cursor1.getColumnIndex(ContactsContract.RawContacts._ID));

                Cursor cursor2 = getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI, new String[]{ContactsContract.Data.DATA3},
                        ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.RAW_CONTACT_ID + " = ? ",
                        new String[]{"vnd.android.cursor.item/vnd.com.whatsapp.profile", rawContactId}, null);

                if (cursor2 != null) {
                    while (cursor2.moveToNext()) {
                        String phoneNumber = cursor2.getString(0);

                        if (TextUtils.isEmpty(phoneNumber))
                            continue;

                        if (phoneNumber.startsWith("Message "))
                            phoneNumber = phoneNumber.replace("Message ", "");
                        whatsAppText(phoneNumber, three, four);
                    }

                    if (!cursor2.isClosed()) {
                        cursor2.close();
                    }
                }
            }
            if (!cursor1.isClosed()) {
                cursor1.close();
            }
        }
    }

    private void whatsAppText(final String number, ImageView three, ImageView four) {
        three.setVisibility(View.VISIBLE);
        four.setVisibility(View.VISIBLE);

        three.setOnClickListener(v -> openWhatsapp(ProfileScreen.this, number));

        four.setOnClickListener(v -> voiceCall(number, ProfileScreen.this, true));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 122) {
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == RESULT_OK && requestCode == 111) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == 1 && resultCode == -1) {
            isCallLog = false;
            addToContactsButton.setVisibility(View.GONE);
            historyButton.setText("History");
        } else if (requestCode == 221 && resultCode == RESULT_OK) {
            getNote();
        }
    }

    @Subscribe
    public void Tags(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("tagCall")) {
            tagModels.clear();
            tagModels = getTagList(busModel.jsonElement);
            insertAllTags(databaseHelper, tagModels);
        } else if (busModel.serviceName.equalsIgnoreCase("subtagCall")) {
            tagModels.clear();
            tagModels = getSubTagList(busModel.jsonElement);
            subChipView();
        } else if (busModel.serviceName.equalsIgnoreCase("addTag")) {
            showToast("Tag added");
        } else if (busModel.serviceName.equalsIgnoreCase("quicklist")) {
            showToast("Contact added to quicklist");
        } else if (busModel.serviceName.equalsIgnoreCase("blockContact")) {
            BlockedNumber blockedNumber = new BlockedNumber(EXACT_MATCH, "+91",
                    contactsModel.number);
            addNumber(blockedNumber);

            showToast("Contact added to block list");
        } else if (busModel.serviceName.equalsIgnoreCase("contactDetails")) {
            ProfileModel profileModel = ProfileModel.getProfileModel(busModel.jsonElement);

            boolean isMutual = OozeePreferences.getInstance().getPref().getBoolean("isMutual", false);
            if (!isMutual) {
                profileModel.mutualContacts.clear();
            }

            if (profileModel != null && profileModel.mutualContacts != null &&
                    profileModel.mutualContacts.size() > 0) {
                mutualTextView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                ContactsAdapter contactsAdapter = new ContactsAdapter(profileModel.mutualContacts, this);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(contactsAdapter);
            } else {
                mutualTextView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }

            if (profileModel != null) {

                isBlockId = profileModel.isblock;
                if ((profileModel.email.equalsIgnoreCase("")
                        || profileModel.email.equalsIgnoreCase("-"))
                        && (profileModel.address.equalsIgnoreCase("") ||
                        profileModel.address.equalsIgnoreCase("-"))) {
                    checkAndSetVisibility(locationRelative, 0);
                } else {
                    checkAndSetVisibility(locationRelative, 1);

                    if (profileModel.email.equalsIgnoreCase("")
                            || profileModel.email.equalsIgnoreCase("-")) {
                        checkAndSetVisibility(email, 0);
                        checkAndSetVisibility(emailLinear, 0);
                        checkAndSetVisibility(emailLocationView, 0);
                    } else {
                        checkAndSetVisibility(email, 1);
                        checkAndSetVisibility(emailLinear, 1);
                        emailActual.setText(profileModel.email);
                    }

                    if (profileModel.address.equalsIgnoreCase("") ||
                            profileModel.address.equalsIgnoreCase("-")) {

                        checkAndSetVisibility(locationActual, 0);
                        checkAndSetVisibility(location, 0);
                        checkAndSetVisibility(emailLocationView, 0);
                    } else {
                        checkAndSetVisibility(locationActual, 1);
                        checkAndSetVisibility(location, 1);
                        locationActual.setText(profileModel.address);
                    }
                }

                setTagAdapter(profileModel.tag);

                if (profileModel.gender != null && !profileModel.gender.equalsIgnoreCase("")) {
                    if (checkSubscription()) {
                        genderTextview.setVisibility(View.VISIBLE);

                        if (profileModel.gender.equalsIgnoreCase("M") || profileModel.gender.equalsIgnoreCase("Male")) {
                            genderTextview.setText("M");
                            genderTextview.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupmale)));
                            profileImageView.setBorderColor(getResources().getColor(R.color.callpopupmale));
                        } else {
                            profileImageView.setBorderColor(getResources().getColor(R.color.callpopupgirl));
                            genderTextview.setText("F");
                            genderTextview.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.callpopupgirl)));
                        }
                    }
                } else {
                    genderTextview.setVisibility(View.GONE);
                }

                if (imagelinkornot == null || imagelinkornot.equalsIgnoreCase("")){
                    if (profileModel.photo != null && !profileModel.photo.equalsIgnoreCase("")) {
                        Glide.with(this).load(profileModel.photo).fitCenter()
                                .placeholder(getResources().getDrawable(R.drawable.circle)).into(profileImageView);
                        if (checkSubscription()) {
                            if (profileModel.gender.equalsIgnoreCase("M") || profileModel.gender.equalsIgnoreCase("Male")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupmale));
                            } else if (profileModel.gender.equalsIgnoreCase("F") || profileModel.gender.equalsIgnoreCase("Female")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupgirl));
                            }
                        }
                        initialTextView.setVisibility(View.GONE);
                    } else {
                        Glide.with(this).load(getResources().getDrawable(R.drawable.circle)).fitCenter()
                                .into(profileImageView);
                        if (checkSubscription()) {
                            if (profileModel.gender.equalsIgnoreCase("M") || profileModel.gender.equalsIgnoreCase("Male")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupmale));
                            } else if (profileModel.gender.equalsIgnoreCase("F") || profileModel.gender.equalsIgnoreCase("Female")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupgirl));
                            }
                        }
                        initialTextView.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    if (profileModel.photo != null && !profileModel.photo.equalsIgnoreCase("")) {
                        /*Glide.with(this).load(profileModel.photo).fitCenter()
                                .placeholder(getResources().getDrawable(R.drawable.circle)).into(profileImageView);*/
                        if (checkSubscription()) {
                            if (profileModel.gender.equalsIgnoreCase("M") || profileModel.gender.equalsIgnoreCase("Male")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupmale));
                            } else if (profileModel.gender.equalsIgnoreCase("F") || profileModel.gender.equalsIgnoreCase("Female")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupgirl));
                            }
                        }
                        initialTextView.setVisibility(View.GONE);
                    } else {
                        /*Glide.with(this).load(getResources().getDrawable(R.drawable.circle)).fitCenter()
                                .into(profileImageView);*/
                        if (checkSubscription()) {
                            if (profileModel.gender.equalsIgnoreCase("M") || profileModel.gender.equalsIgnoreCase("Male")) {
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupmale));
                            } else if (profileModel.gender.equalsIgnoreCase("F") || profileModel.gender.equalsIgnoreCase("Female")) {
                                Toast.makeText(getApplicationContext(), "" + profileModel.gender, Toast.LENGTH_SHORT).show();
                                profileImageView.setBorderColor(getResources().getColor(R.color.callpopupgirl));
                            }
                        }
                       // initialTextView.setVisibility(View.VISIBLE);
                    }
                }

                contactUserId = profileModel.contact_user_id;
            }
            getNote();
        } else if (busModel.serviceName.equalsIgnoreCase("notesProfile2")) {
            ArrayList<NotesModel> notesModels = NotesModel.getNotesList(busModel.jsonElement);

            if (notesModels.size() == 0) {
                notesButton.setVisibility(View.GONE);
            } else {
                notesButton.setVisibility(View.VISIBLE);
            }

            tagModels = getAllTags(databaseHelper);
            if (tagModels.size() == 0) {
                getTags(this, "tagCall");
            }
        } else if (busModel.serviceName.equalsIgnoreCase("delete")) {
            showToast("Note deleted");
        } else if (busModel.serviceName.equalsIgnoreCase("removeBlockedContact")) {
            showToast("Contact removed from block list");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    public void created(File file) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(ContactsContract.Contacts.CONTENT_VCARD_TYPE);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Share Oozee Contact");
        startActivity(intent);
    }

    public void showPopUpWindow(final LocalContactsModel contactsModel) {
        Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        Typeface typeface = getTypeFace(this);
        Typeface typefacebold = getTypeFace1(this);
        imageView.setTypeface(typefacebold);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);


        performTextView.setOnClickListener(v -> {
            dialog.dismiss();
            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
            BlockedContactModel blockedContacts = new BlockedContactModel(userId,
                    contactsModel.number, countryid);
            addContactsByType(ProfileScreen.this, blockedContacts, blocked.ordinal(),
                    "blockContact");
        });

        cancelTextView.setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }

    @Override
    public void deleteNote(NotesModel notesModel) {
        deleteNote(ProfileScreen.this, notesModel.id, "delete");
    }

    private void chipView() {
        scrollView.setAlpha(0.4f);

        textView.setText("Select Category");
        ChipsAdapter chipsAdapter = new ChipsAdapter(tagModels, this, false);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.removeItemPerLineLimit();
        chipsRecyclerView.setLayoutManager(flowLayoutManager);
        chipsRecyclerView.setAdapter(chipsAdapter);
    }

    private void subChipView() {
        scrollView.setAlpha(0.4f);

        textView.setText("Select Sub Category");
        ChipsAdapter chipsAdapter = new ChipsAdapter(tagModels, this, true);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.removeItemPerLineLimit();
        chipsRecyclerView.setLayoutManager(flowLayoutManager);
        chipsRecyclerView.setAdapter(chipsAdapter);
    }

    public void addNumber(final BlockedNumber number) {
        blockedNumberDao = BlockedNumberDatabase.getInstance(this).blockedNumberDao();
        AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberDao.insert(number));
    }

    private void getNote() {
        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
        BlockedContactModel blockedContactModel = new BlockedContactModel(userId, contactsModel.number, countryid);
        getNotes(this, blockedContactModel, "notesProfile2");
    }

    @Override
    public void tagSubtag(boolean isSubtag, int id, String name) {
        if (isSubtag) {
            scrollView.setAlpha(1f);

            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            TagModels tagModel = new TagModels(userId, contactsModel.number, 99, tagId, id);
            addTag(ProfileScreen.this, tagModel, "addTag");

            tagModel.name = name;
            profileTagsModel.add(tagModel);
            setTagAdapter(profileTagsModel);

            tagId = 0;
            tagModels.clear();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            scrollView.setAlpha(1f);

            tagId = id;
            getSubTags(ProfileScreen.this, id, "subtagCall");
        }
    }

    private void unblockContact() {
        String number = contactsModel.number;

        number = number.replaceAll("-", "");

        if (number.startsWith("+91")) {
            number = number.replaceAll("\\+91", "");
        }
        if (number.startsWith("91") && number.length() > 10) {
            number = number.replaceFirst("91", "");
        }

        final BlockedNumber blockedNumber;
        for (int i = 0; i < blockedNumberList.size(); i++) {
            if (blockedNumberList.get(i).getPhoneNumber().equalsIgnoreCase(number)) {
                blockedNumber = blockedNumberList.get(i);
                AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberDao.delete(blockedNumber));
                break;
            }
        }

        blockedNumberDao = BlockedNumberDatabase.getInstance(this).blockedNumberDao();
        AsyncExecutorUtil.getInstance().getExecutor().execute(() -> blockedNumberList = blockedNumberDao.getAll());

        deleteContactsByType(ProfileScreen.this, isBlockId, blocked.ordinal(), "removeBlockedContact");
    }

    private void setTagAdapter(ArrayList<TagModels> tagModels) {
        for (int i = 0; i < tagModels.size(); i++) {
            if (tagModels.get(i).name != null &&
                    tagModels.get(i).name.equalsIgnoreCase("addtag")) {
                tagModels.remove(i);
                break;
            }
        }

        TagModels tagModel = new TagModels(0, "", 99, 0, 0);
        tagModel.name = "addtag";
        tagModels.add(tagModel);
        profileTagsModel = tagModels;
        TagAdapter tagAdapter = new TagAdapter(tagModels, this);

       /* if (tagModels.size() == 1) {
            tagLinear.setLayoutManager(new GridLayoutManager(this, 1));
        } else if (tagModels.size() == 2) {
            tagLinear.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            tagLinear.setLayoutManager(new GridLayoutManager(this, 3));
        }
        */

        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        flowLayoutManager.removeItemPerLineLimit();
        tagLinear.setLayoutManager(flowLayoutManager);
        tagLinear.setAdapter(tagAdapter);
    }

    @Override
    public void addTag() {
        if (tagModels.size() == 0) {
            tagModels = getAllTags(new DatabaseHelper(ProfileScreen.this));
        }
        chipView();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}