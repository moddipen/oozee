package com.app.oozee.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.adapters.DialogListingAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.database.AllContactsModel;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.receivers.AlarmReceiver;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.MyProgressDialog;
import com.app.oozee.utils.OozeePreferences;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.suke.widget.SwitchButton;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.utils.Utility.BACKUPDATA;

public class BackupActivity extends BaseActivity implements DialogListingAdapter.DialogListing
        , Config.VCardInterface, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Dialog dialog;
    private TextView frequencyTextView;
    private static final String TAG = "DriveAPi";
    private GoogleApiClient mGoogleApiClient;
    private File file;
    private static final int REQUEST_CODE_SIGN_IN = 0;
    private ACProgressFlower myProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);

        Button backUpButton = findViewById(R.id.backUpButton);
        final RelativeLayout frequencyRelative = findViewById(R.id.frequencyRelative);
        frequencyTextView = findViewById(R.id.frequencyTextView);
        SwitchButton switchView = findViewById(R.id.switchView);
        ImageView imageView = findViewById(R.id.imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        switchView.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (isChecked) {
                    frequencyRelative.setVisibility(View.VISIBLE);
                } else {
                    frequencyRelative.setVisibility(View.GONE);
                }
                OozeePreferences.getInstance().getEditor().putBoolean(BACKUPDATA, isChecked).apply();
            }
        });

        backUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        frequencyRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showModelForFrequency();
            }
        });
    }

    public void showModelForFrequency() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_listing);
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);

        ArrayList<String> strings = new ArrayList<>();

        strings.add("Daily");
        strings.add("Weekly");
        strings.add("Monthly");

        DialogListingAdapter dialogListingAdapter = new DialogListingAdapter(this, strings);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(dialogListingAdapter);

        if (!(isFinishing())) {
            dialog.show();
        }
    }

    @Override
    public void sendData(int position) {
        if (dialog != null) {
            dialog.dismiss();
        }

        if (position == 0) {
            frequencyTextView.setText("Daily");
        } else if (position == 1) {
            frequencyTextView.setText("Weekly");
        } else {
            frequencyTextView.setText("Monthly");
        }
        scheduleAlarmforDay(position);
    }

    private void scheduleAlarmforDay(int position) {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = getPendingIntent(position);
        if (alarmMgr != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeZone(TimeZone.getDefault());
            if (position == 0) {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 5);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 1);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            } else if (position == 1) {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 7);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            } else {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 30);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            }
        }
    }

    private PendingIntent getPendingIntent(int position) {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("isBackup", true);
        intent.putExtra("frequency", position);
        return PendingIntent.getBroadcast(this, 888, intent, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    /*** Create a new file and save it to Drive.*/
    private void saveFileToDrive() {
        Log.d(TAG, "Creating new contents.");
        ArrayList<LocalContactsModel> allContactsModel = AllContactsModel.getAllContacs(new DatabaseHelper(this));
        Config.generateVCard(allContactsModel, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SIGN_IN) {
            if (mGoogleApiClient == null) {
                Log.d(TAG,"mGoogleAPiClient :: "+mGoogleApiClient);
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Drive.API).addScope(Drive.SCOPE_FILE)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
            }
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void created(final File file) {
        this.file = file;
        Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
            @Override
            public void onResult(@NonNull DriveApi.DriveContentsResult result) {
                if (!result.getStatus().isSuccess()) {
                    dismissProgressDialog(myProgressDialog);
                    Toast.makeText(BackupActivity.this, "Error while trying to create new file contents", Toast.LENGTH_LONG).show();
                    return;
                }

                String mimeType = "text/x-vcard";
                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                        .setTitle("Contacts" + Calendar.getInstance().getTimeInMillis()) // Google Drive File name
                        .setMimeType(mimeType)
                        .setStarred(true).build();
                // create a file on root folder

                Drive.DriveApi.getRootFolder(mGoogleApiClient).createFile(mGoogleApiClient, changeSet, result.getDriveContents()).
                        setResultCallback(backupFileCallback);
            }
        });
    }

    private ResultCallback<DriveFolder.DriveFileResult> backupFileCallback = new
            ResultCallback<DriveFolder.DriveFileResult>() {
                @Override
                public void onResult(DriveFolder.DriveFileResult result) {
                    if (!result.getStatus().isSuccess()) {
                        dismissProgressDialog(myProgressDialog);
                        return;
                    }
                    DriveFile mfile = result.getDriveFile();
                    mfile.open(mGoogleApiClient, DriveFile.MODE_WRITE_ONLY, new DriveFile.DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long bytesExpected) {
                        }
                    }).setResultCallback(backupContentsOpenedCallback);
                }
            };

    private ResultCallback<DriveApi.DriveContentsResult> backupContentsOpenedCallback = new
            ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }
                    DriveContents contents = result.getDriveContents();
                    BufferedOutputStream bos = new BufferedOutputStream(contents.getOutputStream());
                    byte[] buffer = new byte[1024];
                    int n;

                    try {
                        FileInputStream is = new FileInputStream(file);
                        BufferedInputStream bis = new BufferedInputStream(is);

                        while ((n = bis.read(buffer)) > 0) {
                            bos.write(buffer, 0, n);
                        }
                        bos.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    contents.commit(mGoogleApiClient, null).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Log.d(TAG, "Completed");
                            dismissProgressDialog(myProgressDialog);
                            Toast.makeText(BackupActivity.this, "Successfully completed backup"
                                    , Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            };

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        myProgressDialog = MyProgressDialog.show(BackupActivity.this, "", "");
        saveFileToDrive();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"Error OnConnectionSusoended ::"+i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    /*** Start sign in activity.*/
    private void signIn() {
        Log.i(TAG, "Start sign in");
        GoogleSignInClient GoogleSignInClient = buildGoogleSignInClient();
        startActivityForResult(GoogleSignInClient.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }

    /*** Build a Google SignIn cli.*/
    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestScopes(Drive.SCOPE_FILE)
                        .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }
}