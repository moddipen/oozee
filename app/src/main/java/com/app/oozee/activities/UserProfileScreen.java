package com.app.oozee.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.MediaModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.UpdateProfileModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.app.oozee.utils.Utility;
import com.bumptech.glide.Glide;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Objects;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.utils.Config.checkSubscription;
import static com.app.oozee.utils.Utility.getTypeFace;

public class UserProfileScreen extends BaseActivity {

    private EditText aboutUserTextView;
    private EditText firstName;
    private EditText lastName;
    private EditText nickName;
    private EditText address;
    private EditText email;
    private TextView initialTextView;
    private Button buttonBussiness;
    private ImageView profileImageView;
    private RelativeLayout relativeLayout;
    private String websiteStr, industryStr, companyNameStr, companyaddressStr, aboutStr, genderStr,
            photoStr;
    private ArrayList<ProfileModel> profileModel = new ArrayList<>();
    private boolean isUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_screen);

        ImageView imageView = findViewById(R.id.imageView);
        TextView saveTextView = findViewById(R.id.saveTextView);
        final TextView contactsTextView = findViewById(R.id.contactsTextView);
        final TextView aboutSTextView = findViewById(R.id.aboutSTextView);
        relativeLayout = findViewById(R.id.relativeLayout);
        aboutUserTextView = findViewById(R.id.aboutTextView);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        nickName = findViewById(R.id.nickName);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email);
        initialTextView = findViewById(R.id.initialTextView);
        buttonBussiness = findViewById(R.id.buttonBussiness);

        ImageView editFirstName = findViewById(R.id.editFirstName);
        final ImageView editAbout = findViewById(R.id.editAbout);
        final ImageView editAddress = findViewById(R.id.editAddress);
        final ImageView editEmail = findViewById(R.id.editEmail);
        final ImageView editLastName = findViewById(R.id.editLastName);
        final ImageView editNickName = findViewById(R.id.editNickName);
        profileImageView = findViewById(R.id.profileImageView);

        Typeface typeface = Utility.getTypeFace(this);
        contactsTextView.setTypeface(typeface);
        aboutSTextView.setTypeface(typeface);
        aboutUserTextView.setTypeface(typeface);
        firstName.setTypeface(typeface);
        lastName.setTypeface(typeface);
        nickName.setTypeface(typeface);
        address.setTypeface(typeface);
        email.setTypeface(typeface);

        final View contactView = findViewById(R.id.contactView);
        final View aboutView = findViewById(R.id.aboutView);

        final ScrollView aboutScrollView = findViewById(R.id.aboutScrollView);
        final ScrollView contactScrollView = findViewById(R.id.contactScrollView);

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProfilePic();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editFirstName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter first Name", 1, firstName);
            }
        });

        editAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter about", 6, aboutUserTextView);
            }
        });

        editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter address", 4, address);
            }
        });

        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter email", 5, email);
            }
        });

        editLastName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter last name", 2, lastName);
            }
        });

        editNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogForUpdate("Enter nick name", 3, nickName);
            }
        });

        contactsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutScrollView.setVisibility(View.GONE);
                contactScrollView.setVisibility(View.VISIBLE);

                contactsTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
                aboutSTextView.setTextColor(getResources().getColor(R.color.darkGrayColor2));

                aboutView.setVisibility(View.INVISIBLE);
                contactView.setVisibility(View.VISIBLE);
            }
        });

        aboutSTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutScrollView.setVisibility(View.VISIBLE);
                contactScrollView.setVisibility(View.GONE);

                aboutSTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
                contactsTextView.setTextColor(getResources().getColor(R.color.darkGrayColor2));
                aboutView.setVisibility(View.VISIBLE);
                contactView.setVisibility(View.INVISIBLE);
            }
        });

        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstNameString = firstName.getText().toString().trim();
                String lastNameString = lastName.getText().toString().trim();
                String nickNameString = nickName.getText().toString().trim();
                String emailAddressString = email.getText().toString().trim();
                String addressString = address.getText().toString().trim();
                aboutStr = aboutUserTextView.getText().toString().trim();

                if (firstNameString.equalsIgnoreCase("-") ||
                        firstNameString.equalsIgnoreCase("")) {
                    showToast("Please enter first name");
                    return;
                }

                if (lastNameString.equalsIgnoreCase("-")
                        || lastNameString.equalsIgnoreCase("")) {
                    showToast("Please enter last name");
                    return;
                }

                if (emailAddressString.equalsIgnoreCase("-") ||
                        emailAddressString.equalsIgnoreCase("")) {
                    showToast("Please enter email address");
                    return;
                }

                aboutStr = aboutStr.replace("-", "");

                OozeePreferences.getInstance().getEditor().putString("photoStr", photoStr).apply();
                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                UpdateProfileModel profileModel = new UpdateProfileModel(userId, firstNameString,
                        lastNameString, emailAddressString, nickNameString, genderStr, aboutStr,
                        addressString, websiteStr, industryStr, companyNameStr, companyaddressStr,
                        photoStr);
                updateProfileCall(UserProfileScreen.this, profileModel, "profileupdated");
            }
        });

        buttonBussiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileScreen.this, BussinessProfile.class);
                intent.putExtra("profileModel", profileModel.get(0));
                startActivityForResult(intent, 121);
            }
        });

        if (checkSubscription()) {
            buttonBussiness.setVisibility(View.VISIBLE);
        } else {
            buttonBussiness.setVisibility(View.GONE);
        }

        getProfileUser(this, "profileDetailsAuth");
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("profileDetailsAuth")) {

            profileModel = ProfileModel.getProfile(busModel.jsonElement);

            if (profileModel.size() == 0) {
                finish();
                return;
            }

            ProfileModel profile = profileModel.get(0);
            aboutUserTextView.setText(profile.about);

            if (profile.first_name == null || profile.first_name.equalsIgnoreCase("")) {
                firstName.setText("-");
                initialTextView.setText("-");
            } else {
                firstName.setText(profile.first_name);
                initialTextView.setText(getStringInitial(profile.first_name));
            }

            if (profile.last_name == null || profile.last_name.equalsIgnoreCase("")) {
                lastName.setText("-");
            } else {
                lastName.setText(profile.last_name);
            }

            if (profile.nick_name == null || profile.nick_name.equalsIgnoreCase("")) {
                nickName.setText("-");
            } else {
                nickName.setText(profile.nick_name);
            }

            if (profile.address == null || profile.address.equalsIgnoreCase("")) {
                address.setText("-");
            } else {
                address.setText(profile.address);
            }

            if (profile.email == null || profile.email.equalsIgnoreCase("")) {
                email.setText("-");
            } else {
                email.setText(profile.email);
            }

            websiteStr = profile.website;
            industryStr = profile.industry;
            companyNameStr = profile.company_name;
            companyaddressStr = profile.company_address;
            aboutStr = profile.about;
            genderStr = profile.gender;
            photoStr = profile.photo;

            if ((websiteStr != null && !websiteStr.equalsIgnoreCase("")) ||
                    (companyNameStr != null && !companyNameStr.equalsIgnoreCase("")) ||
                    (companyaddressStr != null && !companyaddressStr.equalsIgnoreCase("")) ||
                    (industryStr != null && !industryStr.equalsIgnoreCase(""))) {
                buttonBussiness.setText("Update Bussiness Profile");
            }

            OozeePreferences.getInstance().getEditor().putString("photoStr", photoStr).apply();
            if (photoStr != null && !photoStr.equalsIgnoreCase("")) {
                Glide.with(this).load(photoStr).into(profileImageView);
                initialTextView.setVisibility(View.GONE);
            }
            relativeLayout.setVisibility(View.VISIBLE);
        } else if (busModel.serviceName.equalsIgnoreCase("profileupdated")) {
            showToast("Profile Updated");
            finish();
        } else if (busModel.serviceName.equalsIgnoreCase("addMediaProfile")) {
            MediaModel mediaModel = MediaModel.getMediaModel(busModel.jsonElement, true);
            photoStr = mediaModel.idImage;
            Glide.with(this).load(photoStr).into(profileImageView);
            initialTextView.setVisibility(View.GONE);
        }
    }

    public void showDialogForUpdate(String hint, final int updateText, EditText enterTextView) {
        /*Dialog dialog = new Dialog(UserProfileScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_username_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }*/

        /*final EditText enterTextView = dialog.findViewById(R.id.enterTextView);
        Button btnUpdate = dialog.findViewById(R.id.btnUpdate);*/

        enterTextView.setFocusable(true);
        enterTextView.setFocusableInTouchMode(true);

        if (!enterTextView.getText().toString().trim().equalsIgnoreCase("-")) {
            enterTextView.setText(enterTextView.getText().toString().trim());
            enterTextView.setSelection(enterTextView.getText().toString().length());
        }

        enterTextView.requestFocus();
        showKeyboard(this);
        /*btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = enterTextView.getText().toString().trim();
                if (value.equalsIgnoreCase("")) {
                    showToast("Please enter some value");
                    return;
                }

                if (updateText == 5 && !value.matches(emailPattern)) {
                    showToast("Please enter valid email address");
                    return;
                }

                if (updateText == 1) {
                    firstName.setText(value);
                } else if (updateText == 2) {
                    lastName.setText(value);
                } else if (updateText == 3) {
                    nickName.setText(value);
                } else if (updateText == 4) {
                    address.setText(value);
                } else if (updateText == 5) {
                    email.setText(value);
                } else if (updateText == 6) {
                    aboutUserTextView.setText(value);
                }
                isUpdated = true;
                dialog.dismiss();
            }
        });

        if (!isDestroyed()) {
            dialog.show();
            enterTextView.performClick();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 121 && resultCode == RESULT_OK) {
            buttonBussiness.setText("Update Bussiness Profile");
        }/* else if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            // do your logic here...

            if (images != null && images.size() > 0) {

            }
        }*/ else if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath;
            if (data != null) {
                if (data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH) != null) {
                    filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);

                    Glide.with(this).load(filePath).into(profileImageView);
                    initialTextView.setVisibility(View.GONE);
                    MultipartBody.Part body = prepareFilePart(filePath);
                    RequestBody type = RequestBody.create(MultipartBody.FORM, "profile");
                    addMedia(this, body, type, "addMediaProfile");
                }
            }
        }
    }

    private void selectProfilePic() {
        Intent intent = new Intent(this, ImageSelectActivity.class);
        intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, false);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_CAMERA, true);//default is true
        intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);//default is true
        startActivityForResult(intent, 1213);
    }

    @Override
    public void onBackPressed() {
        if (isUpdated) {
            super.onBackPressed();
            showPopUpWindowALert();
        } else {
            finish();
        }
    }

    private void showPopUpWindowALert() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        imageView.setText("Profile");
        textView.setText("You have not saved profile yet. If you do not save it, it would not be updated. Do you want to continue?");
        cancelTextView.setText("Cancel");
        performTextView.setText("Yes");

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }
}