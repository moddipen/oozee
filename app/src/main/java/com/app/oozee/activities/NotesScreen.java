package com.app.oozee.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.NotesAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.NotesModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.COUNTRYID;

public class NotesScreen extends BaseActivity implements NotesAdapter.DeleteNote {

    private RecyclerView recyclerView;
    private TextView noNotesTextView;
    private String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_screen);

        Button addNotes = findViewById(R.id.addNotes);
        TextView titleTextView = findViewById(R.id.titleTextView);
        recyclerView = findViewById(R.id.recyclerView);
        noNotesTextView = findViewById(R.id.noNotesTextView);

        titleTextView.setText("Notes");

        ImageView backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NotesScreen.this, AddNotesScreen.class);
                startActivityForResult(intent, 221);
            }
        });

        number = getIntent().getStringExtra("number");
        getNote();
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("delete")) {
            showToast("Note deleted");
        } else if (busModel.serviceName.equalsIgnoreCase("notesProfile")) {
            ArrayList<NotesModel> notesModels = NotesModel.getNotesList(busModel.jsonElement);

            if (notesModels.size() == 0) {
                noNotesTextView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                noNotesTextView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                NotesAdapter recordingAdapter = new NotesAdapter(notesModels, this, number);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(recordingAdapter);
            }
        }
    }

    @Override
    public void deleteNote(NotesModel notesModel) {
        deleteNote(NotesScreen.this, notesModel.id, "delete");
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    private void getNote() {
        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
        BlockedContactModel blockedContactModel = new BlockedContactModel(userId, number, countryid);
        getNotes(this, blockedContactModel, "notesProfile");
    }
}