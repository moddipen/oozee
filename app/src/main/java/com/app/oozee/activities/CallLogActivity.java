package com.app.oozee.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.CallLogAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.MyProgressDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.utils.Config.getDateFromTimeStampHistory;

public class CallLogActivity extends BaseActivity {

    private ACProgressFlower myProgressDialog;
    public ArrayList<LocalContactsModel> allCallLogs = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView noDataTextView;
    private String phoneNumbers;
    public ImageView clear_history_btn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);

        recyclerView = findViewById(R.id.recyclerView);
        noDataTextView = findViewById(R.id.noDataTextView);
        ImageView backImageView = findViewById(R.id.backImageView);
        clear_history_btn = findViewById(R.id.clear_history_btn);
        clear_history_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allCallLogs.clear();
                clear_history_btn.setVisibility(View.GONE);
                DeleteCallById(phoneNumbers);
                DeleteCallById("+91"+phoneNumbers);
                ArrayList<LocalContactsModel> strings = new ArrayList<>();
                Collections.reverse(strings);

                for (int i = 0; i < strings.size(); i++) {
                    String name = strings.get(i).name;
                    String newInitial = getDateFromTimeStampHistory(strings.get(i).date);
                    allCallLogs.add(new LocalContactsModel(name, newInitial, strings.get(i).dir,
                            strings.get(i).number, strings.get(i).duration, strings.get(i).callDayTime));
                }
                // dismissProgressDialog(myProgressDialog);
                CallLogAdapter callLogAdapter = new CallLogAdapter(allCallLogs, CallLogActivity.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(CallLogActivity.this));
                recyclerView.setAdapter(callLogAdapter);
                callLogAdapter.notifyDataSetChanged();
                if (allCallLogs.size() > 0) {
                    noDataTextView.setVisibility(View.GONE);
                } else {
                    noDataTextView.setVisibility(View.VISIBLE);
                }
            }
        });

        backImageView.setOnClickListener(v -> finish());

        LocalContactsModel contacts = getIntent().getParcelableExtra("contacts");

        phoneNumbers = contacts.number;
        phoneNumbers = phoneNumbers.replaceAll(" ", "");
        new getCalls().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class getCalls extends AsyncTask<Void, Void, Void> {
        ArrayList<LocalContactsModel> stringss = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //myProgressDialog = MyProgressDialog.show(CallLogActivity.this, "", "");
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
            int number;
            if (managedCursor != null) {
                number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
                while (managedCursor.moveToNext()) {
                    String phNumber = managedCursor.getString(number);
                    String nameStr = managedCursor.getString(name);
                    String callType = managedCursor.getString(type);
                    String callDate = managedCursor.getString(date);
                    Date callDayTime = new Date(Long.valueOf(callDate));
                    String callDuration = managedCursor.getString(duration);
                    int dircode = Integer.parseInt(callType);
                    String dir = "";

                    switch (dircode) {
                        case CallLog.Calls.OUTGOING_TYPE:
                            dir = "OUTGOING";
                            break;

                        case CallLog.Calls.INCOMING_TYPE:
                            dir = "INCOMING";
                            break;

                        case CallLog.Calls.MISSED_TYPE:
                            dir = "MISSED";
                            break;
                    }

                    if (phNumber.trim().contains(phoneNumbers.trim())) {
                        if (nameStr != null) {
                            stringss.add(new LocalContactsModel(nameStr, callDate, dir, phNumber, callDuration,
                                    callDayTime));
                        } else {
                            stringss.add(new LocalContactsModel(phNumber, callDate, dir, phNumber, callDuration,
                                    callDayTime));
                        }
                    }
                }
                managedCursor.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ArrayList<LocalContactsModel> strings = stringss;
            Collections.reverse(strings);

            for (int i = 0; i < strings.size(); i++) {
                String name = strings.get(i).name;
                String newInitial = getDateFromTimeStampHistory(strings.get(i).date);
                allCallLogs.add(new LocalContactsModel(name, newInitial, strings.get(i).dir,
                        strings.get(i).number, strings.get(i).duration, strings.get(i).callDayTime));
            }
            // dismissProgressDialog(myProgressDialog);

            CallLogAdapter callLogAdapter = new CallLogAdapter(allCallLogs, CallLogActivity.this);
            recyclerView.setLayoutManager(new LinearLayoutManager(CallLogActivity.this));
            recyclerView.setAdapter(callLogAdapter);
            if (allCallLogs.size() > 0) {
                noDataTextView.setVisibility(View.GONE);
                clear_history_btn.setVisibility(View.VISIBLE);
            } else {
                noDataTextView.setVisibility(View.VISIBLE);
                clear_history_btn.setVisibility(View.GONE);
            }
        }
    }

    public void DeleteCallById(String idd) {
        String queryString = "NUMBER='" + idd + "'";
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getContentResolver().delete(CallLog.Calls.CONTENT_URI, queryString, null);
    }
}