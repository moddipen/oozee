package com.app.oozee.activities;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.RemindersModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.receivers.AlarmReceiver;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import static com.app.oozee.database.RemindersModel.insertReminder;
import static com.app.oozee.database.RemindersModel.updateReminder;
import static com.app.oozee.utils.Config.getDateFormat2;
import static com.app.oozee.utils.Config.getDayDateYear;
import static com.app.oozee.utils.Config.getExpireMonth;
import static com.app.oozee.utils.Utility.getTypeFace;

public class AddReminderScreen extends BaseActivity {

    private EditText selectNumberEditText;
    private EditText selectDateEdittext;
    private ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
    private DatabaseHelper databaseHelper;
    private String startTimeString;
    private int reminderId = 0;
    private boolean isFromEdit;
    private EditText descriptionEditText;
    private EditText titleEdittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder_screen);

        databaseHelper = new DatabaseHelper(this);

        Button submitButton = findViewById(R.id.submitButton);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        titleEdittext = findViewById(R.id.titleEdittext);
        selectDateEdittext = findViewById(R.id.selectDateEdittext);
        selectNumberEditText = findViewById(R.id.selectNumberEditText);
        ImageView backImageView = findViewById(R.id.backImageView);
        submitButton.setText("Add Reminder");

        final TextView one = findViewById(R.id.one);
        final TextView two = findViewById(R.id.two);
        final TextView three = findViewById(R.id.three);
        final TextView four = findViewById(R.id.four);
        final TextView five = findViewById(R.id.five);
        final TextView six = findViewById(R.id.six);
        final TextView seven = findViewById(R.id.seven);
        final Switch yesNoSwitch = findViewById(R.id.yesNoSwitch);

        Typeface typeface = Utility.getTypeFace(this);

        submitButton.setTypeface(typeface);
        titleEdittext.setTypeface(typeface);
        descriptionEditText.setTypeface(typeface);
        selectNumberEditText.setTypeface(typeface);

        selectNumberEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(AddReminderScreen.this);
                Intent intent = new Intent(AddReminderScreen.this, SelectContactsScreen.class);
                startActivityForResult(intent, 111);
            }
        });

        selectDateEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(AddReminderScreen.this);
                selectDate();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(AddReminderScreen.this);

                String title = titleEdittext.getText().toString().trim();
                String note = descriptionEditText.getText().toString().trim();
                String dateString = selectDateEdittext.getText().toString().trim();

                if (title.equalsIgnoreCase("")) {
                    showToast("Please add title");
                    return;
                }

                if (note.equalsIgnoreCase("")) {
                    showToast("Please add note");
                    return;
                }

                String contactName = "";
                String contactNumber = "";

                if (contactsModels.size() != 0) {
                    contactName = contactsModels.get(0).name;
                    contactNumber = contactsModels.get(0).number;
                }

                if (dateString.equalsIgnoreCase("")) {
                    showToast("Please select date");
                    return;
                }

                String isRegular = "0";
                if (yesNoSwitch.isChecked()) {
                    isRegular = "1";
                }

                long mid_trigger = scheduleAlarm(dateString, title);
                dateString = Config.getDateFormat(dateString);

                RemindersModel remindersModel = new RemindersModel(0, contactName,
                        contactNumber, dateString, isRegular, title, note, mid_trigger);
                if (isFromEdit) {
                    updateReminder(remindersModel, databaseHelper, reminderId);
                } else {
                    insertReminder(remindersModel, databaseHelper);
                }

                finish();
            }
        });

        yesNoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setSelected(one, isChecked);
                setSelected(two, isChecked);
                setSelected(three, isChecked);
                setSelected(four, isChecked);
                setSelected(five, isChecked);
                setSelected(six, isChecked);
                setSelected(seven, isChecked);
            }
        });

        setListeners(one);
        setListeners(two);
        setListeners(three);
        setListeners(four);
        setListeners(five);
        setListeners(six);
        setListeners(seven);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        isFromEdit = getIntent().getBooleanExtra("isFromEdit", false);
        if (isFromEdit) {
            RemindersModel remindersModel = getIntent().getParcelableExtra("remindersModel");
            reminderId = remindersModel.id;
            titleEdittext.setText(remindersModel.title);
            descriptionEditText.setText(remindersModel.notes);
            if (remindersModel.isRegular.equalsIgnoreCase("1")) {
                yesNoSwitch.setChecked(true);
            }
            contactsModels.clear();
            if (!remindersModel.name.equalsIgnoreCase("") &&
                    !remindersModel.number.equalsIgnoreCase("")) {
                contactsModels.add(new LocalContactsModel(remindersModel.name, remindersModel.number));
                selectNumberEditText.setText("Contact Selected - " + remindersModel.name);
            } else {
                selectNumberEditText.setText("");
            }

            selectDateEdittext.setText(getDateFormat2(remindersModel.date));

            submitButton.setText("Update Reminder");
        }
    }

    private void setSelected(TextView textView, boolean trueFalse) {
        if (trueFalse) {
            textView.setSelected(true);
            textView.setBackground(getResources().getDrawable(R.drawable.circledays));
        } else {
            textView.setSelected(false);
            textView.setBackground(getResources().getDrawable(R.drawable.circle));
        }
    }

    private void setListeners(final TextView textView) {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textView.isSelected()) {
                    setSelected(textView, false);
                } else {
                    setSelected(textView, true);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 111) {
            if (data != null) {
                contactsModels = data.getParcelableArrayListExtra("tempContactsModels");

                if (contactsModels.size() > 0) {
                    selectNumberEditText.setText("Contact Selected - " + contactsModels.get(0).name);
                } else {
                    selectNumberEditText.setText("Select contact");
                }
            }
        }
    }

    private void selectDate() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (checkIfBeforeDate(getExpireMonth(monthOfYear + 1) + " " + dayOfMonth + ", " + year + " 00:00")) {
                    selectDateEdittext.setText(getExpireMonth(monthOfYear + 1) + " " + dayOfMonth + ", " + year);
                    selectTime();
                } else {
                    selectDateEdittext.setText("");
                }
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.setTitle("");
        if (!isDestroyed()) {
            datePickerDialog.show();
        }
    }

    private void selectTime() {
        Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                startTimeString = selectDateEdittext.getText().toString().trim();
                selectDateEdittext.setText(startTimeString + " " + getDate(hourOfDay, minute));
                startTimeString = startTimeString + " " + hourOfDay + ":" + minute;
            }
        }, mHour, mMinute, false);

        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                selectDateEdittext.setText("");
            }
        });

        timePickerDialog.setTitle("");

        if (!isDestroyed()) {
            timePickerDialog.show();
        }
    }

    private String getDate(int hour, int minute) {
        String min;
        if (minute < 10) {
            min = "0" + String.valueOf(minute);
        } else {
            min = String.valueOf(minute);
        }

        if (hour >= 12) {
            hour = hour - 12;
            String h;

            if (hour < 10) {
                h = "0" + hour;
            } else {
                h = String.valueOf(hour);
            }

            if (h.equalsIgnoreCase("00")) {
                h = "12";
            }
            int hhh = Integer.parseInt(h);
            return hhh + ":" + min + " PM";
        } else if (hour < 10) {
            String h = "0" + hour;
            if (h.equalsIgnoreCase("00")) {
                h = "12";
            }
            int hhh = Integer.parseInt(h);
            return hhh + ":" + min + " AM";
        }
        return hour + ":" + min + " AM";
    }

    private boolean checkIfBeforeDate(String end) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.CANADA);
        Date startDate = null;
        Date c = Calendar.getInstance().getTime();
        Date endDate = null;
        try {
            endDate = sdf.parse(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            String startate = sdf.format(c);
            startDate = sdf.parse(startate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return endDate != null && startDate != null && (endDate.after(startDate) || !endDate.before(startDate));
    }

    public long scheduleAlarm(String dateString, String title) {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);

        ArrayList<String> strings = getDayDateYear(dateString);
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.set(Calendar.YEAR, Integer.parseInt(strings.get(0)));
        cal.set(Calendar.MONTH, Integer.parseInt(strings.get(1)) - 1);
        cal.set(Calendar.DATE, Integer.parseInt(strings.get(2)));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strings.get(3)));
        cal.set(Calendar.MINUTE, Integer.parseInt(strings.get(4)));
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long mid_trigger = cal.getTimeInMillis();
        PendingIntent alarmIntent = getPendingIntent((int) (mid_trigger - 1000), title);

        if (alarmMgr != null) {
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, mid_trigger, alarmIntent);
        }

        return mid_trigger;
    }

    private PendingIntent getPendingIntent(int trigger, String title) {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("title", title);
        intent.putExtra("requestcode", trigger);
        return PendingIntent.getBroadcast(this, trigger, intent, 0);
    }

    @Override
    public void onBackPressed() {
        if (!titleEdittext.getText().toString().trim().equalsIgnoreCase("") ||
                !descriptionEditText.getText().toString().trim().equalsIgnoreCase("")) {
            showPopUpWindow();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    public void showPopUpWindow() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        imageView.setText("Reminder");
        textView.setText("You have not saved reminder yet. If you do not save it, it would not be visible further. Do you want to continue?");
        cancelTextView.setText("Cancel");
        performTextView.setText("Yes");

        Typeface typeface = getTypeFace(this);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!isDestroyed()) {
            dialog.show();
        }
    }
}