package com.app.oozee.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.design.widget.BottomSheetBehavior;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.TagModels;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.Config.DeleteConfirmed;
import com.app.oozee.utils.OozeePreferences;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.quicklist;
import static com.app.oozee.models.TagModels.getSubTagList;
import static com.app.oozee.models.TagModels.getTagList;
import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.Config.deleteContact;
import static com.app.oozee.utils.Config.modifyContact;
import static com.app.oozee.utils.Config.openDialog;
import static com.app.oozee.utils.Config.openWhatsapp;
import static com.app.oozee.utils.Config.voiceCall;
import static com.app.oozee.utils.Utility.getTypeFace;

public class ContactDetailsScreen extends BaseActivity implements DeleteConfirmed {

    private TextView videoTextView;
    private TextView messageTextView;
    private TextView voiceTextView;
    private View view6;
    private View view7;
    private View view8;
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private LocalContactsModel contactsModel;
    private LinearLayout llBottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;
    private ArrayList<TagModels> tagModels = new ArrayList<>();
    private int tagId = 0;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details_screen);

        ImageView settings = findViewById(R.id.settings);

        contactsModel = getIntent().getParcelableExtra("contactsModel");

        TextView titleTextView = findViewById(R.id.titleTextView);
        TextView addTagTextView = findViewById(R.id.addTagTextView);
        TextView initialTextView = findViewById(R.id.initialTextView);
        TextView numberTextView = findViewById(R.id.numberTextView);
        TextView viewCallHistoryTextView = findViewById(R.id.viewCallHistoryTextView);
        RelativeLayout relativeLayout2 = findViewById(R.id.relativeLayout2);
        RelativeLayout relativeLayout1 = findViewById(R.id.relativeLayout1);
        messageTextView = findViewById(R.id.messageTextView);
        voiceTextView = findViewById(R.id.voiceTextView);
        videoTextView = findViewById(R.id.videoTextView);
        view6 = findViewById(R.id.view6);
        view7 = findViewById(R.id.view7);
        view8 = findViewById(R.id.view8);
        imageView1 = findViewById(R.id.imageView1);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        ImageView backImageView = findViewById(R.id.backImageView);

        //llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        addTagTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (tagModels.size() == 0) {
                    getTags(ContactDetailsScreen.this, "tagCall");
                }
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);*/
            }
        });

        titleTextView.setText(contactsModel.name);
        numberTextView.setText(contactsModel.number);
        initialTextView.setText(contactsModel.initial);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpWindow(v);
            }
        });

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        viewCallHistoryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactDetailsScreen.this, CallLogActivity.class);
                intent.putExtra("contacts", contactsModel);
                startActivity(intent);
            }
        });

        numberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPhone(ContactDetailsScreen.this, contactsModel.number);
            }
        });

        relativeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPhone(ContactDetailsScreen.this, contactsModel.number);
            }
        });

        relativeLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactDetailsScreen.this, MessagesScreen.class);
                intent.putExtra("address", contactsModel.number);
                intent.putExtra("smsid", "0");
                intent.putExtra("image",contactsModel.image);
                startActivity(intent);
            }
        });

        getWhatsAppNumbers(contactsModel.name);
        //getAddress(contactsModel.number);

//        getTags(this, "tagCall");
    }

    public void getWhatsAppNumbers(String contactName) {
        Cursor cursor1 = getContentResolver().query(
                ContactsContract.RawContacts.CONTENT_URI,
                new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.ACCOUNT_TYPE + "= ? AND " + ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME_PRIMARY + " = ?",
                new String[]{"com.whatsapp", contactName},
                null);

        if (cursor1 != null) {
            while (cursor1.moveToNext()) {
                String rawContactId = cursor1.getString(cursor1.getColumnIndex(ContactsContract.RawContacts._ID));

                Cursor cursor2 = getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI, new String[]{ContactsContract.Data.DATA3},
                        ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.RAW_CONTACT_ID + " = ? ",
                        new String[]{"vnd.android.cursor.item/vnd.com.whatsapp.profile", rawContactId}, null);

                if (cursor2 != null) {
                    while (cursor2.moveToNext()) {
                        String phoneNumber = cursor2.getString(0);

                        if (TextUtils.isEmpty(phoneNumber))
                            continue;

                        if (phoneNumber.startsWith("Message "))
                            phoneNumber = phoneNumber.replace("Message ", "");
                        whatsAppText(phoneNumber);
                    }

                    if (!cursor2.isClosed()) {
                        cursor2.close();
                    }
                }
            }
            if (!cursor1.isClosed()) {
                cursor1.close();
            }
        }
    }

    public void showPopUpWindow(View view) {
        View popView = LayoutInflater.from(this).inflate(R.layout.popup_contacts_details, null);
        final PopupWindow popupWindowHelper4 = new PopupWindow(popView, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        int he = view.getHeight();
        int wid = view.getWidth() / 3;

        popupWindowHelper4.setOutsideTouchable(true);
        popupWindowHelper4.setBackgroundDrawable(new ColorDrawable());

        TextView blockTextView = popView.findViewById(R.id.blockTextView);
        TextView addToQuickList = popView.findViewById(R.id.addToQuickList);
        TextView recordTextView = popView.findViewById(R.id.recordTextView);

        Typeface typeface = getTypeFace(this);
        blockTextView.setTypeface(typeface);
        recordTextView.setTypeface(typeface);

        recordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindowHelper4.dismiss();
                modifyContact(contactsModel.number, ContactDetailsScreen.this);
            }
        });

        blockTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindowHelper4.dismiss();
                openDialog(ContactDetailsScreen.this, "Are you sure you want to delete this contact?");
            }
        });

        addToQuickList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindowHelper4.dismiss();
                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                BlockedContactModel blockedContacts = new BlockedContactModel(userId,
                        contactsModel.number, 99);
                addContactsByType(ContactDetailsScreen.this, blockedContacts, quicklist.ordinal(),
                        "quicklist");
            }
        });

        if (!isDestroyed()) {
            popupWindowHelper4.showAtLocation(view, Gravity.TOP | Gravity.END, wid, he);
        }
    }

    private void whatsAppText(final String number) {
        videoTextView.setVisibility(View.VISIBLE);
        messageTextView.setVisibility(View.VISIBLE);
        voiceTextView.setVisibility(View.VISIBLE);
        view6.setVisibility(View.VISIBLE);
        view7.setVisibility(View.VISIBLE);
        view8.setVisibility(View.VISIBLE);
        imageView1.setVisibility(View.VISIBLE);
        imageView2.setVisibility(View.VISIBLE);
        imageView3.setVisibility(View.VISIBLE);

        videoTextView.setText("Video call " + number);
        voiceTextView.setText("Voice call " + number);
        messageTextView.setText("Message " + number);

        messageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsapp(ContactDetailsScreen.this, number);
            }
        });

        voiceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voiceCall(number, ContactDetailsScreen.this, false);
            }
        });

        videoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voiceCall(number, ContactDetailsScreen.this, true);
            }
        });
    }

    @Override
    public void delete() {
        deleteContact(contactsModel.number, contactsModel.name, ContactDetailsScreen.this);
        onActivityResult(122, RESULT_OK, new Intent());
    }

    private void getAddress(String number) {
        String contactNumber = number;
        contactNumber = contactNumber.replace("+", "").replace(" ", "");
        if (!contactNumber.startsWith("91")) {
            contactNumber = "91" + contactNumber;
        }

        Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ContactsContract.Data._ID},
                ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + contactNumber + "%'",
                null,
                ContactsContract.Contacts.DISPLAY_NAME);

        String id = "";
        if (cursor != null && cursor.moveToFirst()) {
            id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }

        Uri postal_uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI;
        Cursor postal_cursor = getContentResolver().query(postal_uri, null, ContactsContract.Data.CONTACT_ID + "=" +
                id, null, null);
        if (postal_cursor != null) {
            while (postal_cursor.moveToNext()) {
                String Strt = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                String Cty = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                String cntry = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
            }
            postal_cursor.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 122) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == 111) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Subscribe
    public void Tags(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("tagCall")) {
            tagModels.clear();
            count = 0;
            tagModels = getTagList(busModel.jsonElement);
            count = tagModels.size();/*
            ChipGroup chipGroup = llBottomSheet.findViewById(R.id.entry_chip_group);
            chipGroup.removeAllViews();
            for (int i = 0; i < tagModels.size(); i++) {
                Chip chips = new Chip(this);
                chips.setTextColor(getResources().getColor(R.color.darkGrayColor2));
                chips.setText(tagModels.get(i).name);
                chips.setChipStartPadding(5);
                chips.setCheckable(true);
                chipGroup.addView(chips, i);
            }

            chipGroup.invalidate();

            chipGroup.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(ChipGroup chipGroup, int i) {
                    tagId = tagModels.get(i - 1).id;
                    getSubTags(ContactDetailsScreen.this, tagId, "subtagCall");
                }
            });*/
        } else if (busModel.serviceName.equalsIgnoreCase("subtagCall")) {

            tagModels.clear();
            tagModels = getSubTagList(busModel.jsonElement);

           /* ChipGroup chipGroup = llBottomSheet.findViewById(R.id.entry_chip_group);
            chipGroup.removeAllViews();
            chipGroup.invalidate();
            for (int i = 0; i < tagModels.size(); i++) {
                Chip chips = new Chip(this);
                chips.setTextColor(getResources().getColor(R.color.darkGrayColor2));
                chips.setText(tagModels.get(i).name);
                chips.setChipStartPadding(5);
                chips.setCheckable(true);
                chipGroup.addView(chips, i);
            }

            chipGroup.invalidate();

            chipGroup.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(ChipGroup chipGroup, int i) {
                    int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                    TagModels tagModel = new TagModels(userId, contactsModel.number, 99
                            ,tagId, tagModels.get(i - count).id);
                    addTag(ContactDetailsScreen.this, tagModel, "addTag");
                    tagModels.clear();
                    count = 0;
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            });*/
        } else if (busModel.serviceName.equalsIgnoreCase("addTag")) {
            showToast("Tag added");
        } else if (busModel.serviceName.equalsIgnoreCase("quicklist")) {
            showToast("Contact added to quicklist");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }
}
