package com.app.oozee.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.adapters.NotificationsAdapter;
import com.app.oozee.baseclass.BaseActivity;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.NotificationsModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import static com.app.oozee.models.NotificationsModel.getNotifications;

public class NotificationsScreen extends BaseActivity {

    private RecyclerView recyclerView;
    private TextView noDataTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        ImageView backImageView = findViewById(R.id.backImageView);
        recyclerView = findViewById(R.id.recyclerView);
        noDataTextView = findViewById(R.id.noDataTextView);

        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);

        BlockedContactModel blockedContactModel = new BlockedContactModel(userId);
        getNotification(this, blockedContactModel, "notifications");
    }

    @Subscribe
    public void Response(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("notifications")) {
            ArrayList<NotificationsModel> notificationsModels = getNotifications(busModel.jsonElement);

            if (notificationsModels.size() == 0) {
                noDataTextView.setVisibility(View.VISIBLE);
            } else {
                noDataTextView.setVisibility(View.GONE);
            }

            NotificationsAdapter notificationsAdapter = new NotificationsAdapter(notificationsModels, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(notificationsAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }
}
