package com.app.oozee.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app.oozee.R;
import com.app.oozee.activities.SplashActivity;
import com.app.oozee.models.BusModel;
import com.app.oozee.utils.BusManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private CharSequence name = "OozeeApp";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (remoteMessage.getData().containsKey("status")
                    && remoteMessage.getData().get("status") != null) {
                if (remoteMessage.getData().containsKey("phone_number")
                        && remoteMessage.getData().get("phone_number") != null) {
                    String status = remoteMessage.getData().get("status");
                    String phone_number = remoteMessage.getData().get("phone_number");
                    if (phone_number != null) {
                        if (phone_number.length() > 10) {
                            phone_number = phone_number.substring(phone_number.length() - 10);
                        }
                    }
                    //sendNotification(status, phone_number);
                } else {
                    //  sendNotification(remoteMessage);
                }
            } else {
                //sendNotification(remoteMessage);
            }
        } else {
        //   sendNotification(remoteMessage);
        }
    }

   /* private void sendNotification(RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int importance = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0*//* Request code *//*, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }
        NotificationChannel mChannel = null;
        String CHANNEL_ID = "oozee01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        notification.setSmallIcon(getNotificationIcon(notification))
                .setContentTitle("Oozee")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID).build();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        if (mNotificationManager != null) {
            int notifyID = 1;
            mNotificationManager.notify(notifyID, notification.build());
        }
    }

    private void sendNotification(String messageBody, String phone_number) {

        final BusModel busModel = new BusModel();
        busModel.serviceName = "updateUi";
        busModel.message = messageBody;
        busModel.phoneNumber = phone_number;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                BusManager.getInstance().post(busModel);
            }
        });
    }*/

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(Color.parseColor("#ffffff"));
            return R.drawable.notificationicon;
        }
        return R.mipmap.appiconroundf;
    }
}