package com.app.oozee.models;

public class VoiceMessageModel {

    public int media_id;
    public int sender_user_id;
    public int receiver_user_id;

    public VoiceMessageModel(int media_id, int sender_user_id, int receiver_user_id) {
        this.media_id = media_id;
        this.sender_user_id = sender_user_id;
        this.receiver_user_id = receiver_user_id;
    }
}
