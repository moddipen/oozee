package com.app.oozee.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.app.oozee.utils.Config.getChatDate;
import static com.app.oozee.utils.Config.getChatLong;

public class MessageModel {

    public int viewType;
    public String message;
    public String date;
    public String id;
    public boolean isSMS;
    public String room;
    public long timeStamp;
    public ArrayList<MessageModel> messages = new ArrayList<>();
    public ArrayList<MessageModel
            > reads = new ArrayList<>();

    public MessageModel(String id, int viewType, String message, String date,
                        boolean isSMS, long timeStamp) {
        this.id = id;
        this.viewType = viewType;
        this.message = message;
        this.date = date;
        this.isSMS = isSMS;
        this.timeStamp = timeStamp;
    }

    private MessageModel() {
    }


    public static MessageModel getMessagesModel(JSONObject jsonObject, int senderId,
                                                List<MessageModel> messageList) {
        MessageModel message = new MessageModel();
        if (jsonObject != null) {
            try {
                ArrayList<String> integers = new ArrayList<>();
                for (int i = 0; i < messageList.size(); i++) {
                    integers.add(messageList.get(i).id);
                }

                message.decodeJsons(jsonObject);

                JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("messages");
                for (int i = 0; i < jsonArray.length(); i++) {
                    MessageModel messageModel = new MessageModel();
                    boolean isSuccess = messageModel.decodeJson(jsonArray.getJSONObject(i),
                            senderId, integers);
                    if (isSuccess) {
                        message.messages.add(messageModel);
                    }
                }

                JSONArray read = jsonObject.getJSONObject("data").getJSONArray("reads");
                for (int i = 0; i < read.length(); i++) {
                    MessageModel messageModel = new MessageModel();
                    boolean isSuccess = messageModel.decodeJson(jsonArray.getJSONObject(i),
                            senderId, integers);
                    if (isSuccess) {
                        message.reads.add(messageModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return message;
    }

    private void decodeJsons(JSONObject jsonObject) {
        try {
            room = jsonObject.getJSONObject("data").getString("room");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean decodeJson(JSONObject jsonObject, int senderId, ArrayList<String> integers) {
        try {
            message = jsonObject.getString("message");
            id = jsonObject.getString("id");
            date = jsonObject.getString("created_at");
            timeStamp = getChatLong(date);
            date = getChatDate(date);
            if (integers.contains(id)) {
                return false;
            }

            if (jsonObject.getInt("sender_id") == senderId) {
                viewType = 0;
            } else {
                viewType = 1;
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }
}
