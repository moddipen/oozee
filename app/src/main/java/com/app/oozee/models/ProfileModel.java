package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.activities.MainActivity.getStringInitial;

public class ProfileModel implements Parcelable {

    public int user_id;
    public String first_name;
    public String last_name;
    public String email;
    public String nick_name;
    public String gender;
    public String about;
    public String address;
    public String website;
    public String business;
    public String industry;
    public String company_name;
    public String company_address;
    public String number;
    public double latitude;
    public double longitude;
    public double loginlatitude;
    public double loginlongitude;
    public String photo;
    public String service_provider;
    public int spam;
    public int isblock;
    public int contact_user_id;

    public int id;
    public int phone_number_id;
    public String type;
    public int status;

    public String phoneNumber;
    public boolean premium;
    public ArrayList<LocalContactsModel> mutualContacts = new ArrayList<>();
    public ArrayList<TagModels> tag = new ArrayList<>();

    public ProfileModel(int user_id, String first_name, String last_name, String email,
                        String nick_name, String gender, String about,
                        String address, String website, String industry,
                        String company_name, String company_address, String photo) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.nick_name = nick_name;
        this.gender = gender;
        this.about = about;
        this.address = address;
        this.website = website;
        this.industry = industry;
        this.company_name = company_name;
        this.company_address = company_address;
        this.photo = photo;
    }

    public ProfileModel(int user_id, double latitude, double longitude) {
        this.user_id = user_id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public ProfileModel(String first_name, String last_name, String number) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.number = number;
    }

    public ProfileModel() {
    }

    public static ArrayList<ProfileModel> getProfile(JsonElement jsonElement) {
        ArrayList<ProfileModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonObject jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("user").getAsJsonObject();

                ProfileModel countryModel = new ProfileModel();
                countryModel.decodeJson(jsonArray);
                countryModels.add(countryModel);
            }
        }
        return countryModels;
    }

    public static ProfileModel getProfileModel(JsonElement jsonElement) {
        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonObject jsonObject = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                ProfileModel profileModel = new ProfileModel();
                profileModel.decodeData(jsonObject);
                JsonArray jsonArray = jsonObject.get("mutual").getAsJsonArray();
                profileModel.decodeMutual(jsonArray);
                return profileModel;
            }
        }
        return null;
    }

    public static ProfileModel getPopUpDetails(JsonElement jsonElement) {
        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonObject jsonObject = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                ProfileModel profileModel = new ProfileModel();
                profileModel.decodePopUpData(jsonObject);
                return profileModel;
            }
        }
        return null;
    }


    private void decodeData(JsonObject jsonObject) {
        email = Config.getJsonObjectString(jsonObject, "email");
        address = Config.getJsonObjectString(jsonObject, "address");
        first_name = Config.getJsonObjectString(jsonObject, "first_name");
        last_name = Config.getJsonObjectString(jsonObject, "last_name");
        photo = Config.getJsonObjectString(jsonObject, "photo");
        contact_user_id = Config.getJsonObjectInt(jsonObject, "contact_user_id");
        gender = Config.getJsonObjectString(jsonObject, "gender");
        isblock = Config.getJsonObjectInt(jsonObject, "isblock");

        if (email == null || email.equalsIgnoreCase("")) {
            email = "-";
        }
        if (address == null || address.equalsIgnoreCase("")) {
            address = "-";
        }

        JsonArray jsonArray = Config.getJsonArray(jsonObject, "tag");
        if (jsonArray != null) {
            tag = TagModels.getTag(jsonArray);
        }
    }

    private void decodePopUpData(JsonObject jsonObject) {
        number = Config.getJsonObjectString(jsonObject, "phone_number");
        email = Config.getJsonObjectString(jsonObject, "email");
        address = Config.getJsonObjectString(jsonObject, "address");
        first_name = Config.getJsonObjectString(jsonObject, "first_name");
        last_name = Config.getJsonObjectString(jsonObject, "last_name");
        photo = Config.getJsonObjectString(jsonObject, "photo");
        contact_user_id = Config.getJsonObjectInt(jsonObject, "contact_user_id");
        gender = Config.getJsonObjectString(jsonObject, "gender");
        address = Config.getJsonObjectString(jsonObject, "location");
        service_provider = Config.getJsonObjectString(jsonObject, "service_provider");
        spam = Config.getJsonObjectInt(jsonObject, "spam");
        premium = Config.getJsonObjectBool(jsonObject, "premium");
        website = Config.getJsonObjectString(jsonObject, "website");
        business = Config.getJsonObjectString(jsonObject, "business");

        if (email == null || email.equalsIgnoreCase("")) {
            email = "-";
        }
        if (address == null || address.equalsIgnoreCase("")) {
            address = "-";
        }

        JsonArray jsonArray = Config.getJsonArray(jsonObject, "tag");
        if (jsonArray != null) {
            tag = TagModels.getTag(jsonArray);
        }
    }

    private void decodeMutual(JsonArray mutual) {
        for (int i = 0; i < mutual.size(); i++) {
            String fName = Config.getJsonObjectString(mutual.get(i).getAsJsonObject(), "first_name");
            String last_name = Config.getJsonObjectString(mutual.get(i).getAsJsonObject(), "last_name");
            String number = Config.getJsonObjectString(mutual.get(i).getAsJsonObject(), "number");
            mutualContacts.add(new LocalContactsModel(1, fName + last_name, getStringInitial(fName), number));
        }
    }

    private void decodeJson(JsonObject profile) {
        id = Config.getJsonObjectInt(profile, "id");
        phone_number_id = Config.getJsonObjectInt(profile, "phone_number_id");
        type = Config.getJsonObjectString(profile, "type");
        status = Config.getJsonObjectInt(profile, "status");

        JsonObject jsonObject = Config.getJsonObject(profile, "profile");
        user_id = Config.getJsonObjectInt(jsonObject, "id");
        first_name = Config.getJsonObjectString(jsonObject, "first_name");
        last_name = Config.getJsonObjectString(jsonObject, "last_name");
        email = Config.getJsonObjectString(jsonObject, "email");
        nick_name = Config.getJsonObjectString(jsonObject, "nick_name");
        photo = Config.getJsonObjectString(jsonObject, "photo");
        gender = Config.getJsonObjectString(jsonObject, "gender");
        about = Config.getJsonObjectString(jsonObject, "about");
        address = Config.getJsonObjectString(jsonObject, "address");
        website = Config.getJsonObjectString(jsonObject, "website");
        industry = Config.getJsonObjectString(jsonObject, "industry");
        company_name = Config.getJsonObjectString(jsonObject, "company_name");
        company_address = Config.getJsonObjectString(jsonObject, "company_address");

        JsonObject number = Config.getJsonObject(profile, "number");
        phoneNumber = Config.getJsonObjectString(number, "number");


        JsonObject location = Config.getJsonObject(profile, "location");
        latitude = Config.getJsonObjectDouble(location, "latitude");
        longitude = Config.getJsonObjectDouble(location, "longitude");
        loginlatitude = Config.getJsonObjectDouble(location, "login_lat");
        loginlongitude = Config.getJsonObjectDouble(location, "login_long");
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.user_id);
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.email);
        dest.writeString(this.nick_name);
        dest.writeString(this.gender);
        dest.writeString(this.about);
        dest.writeString(this.address);
        dest.writeString(this.website);
        dest.writeString(this.business);
        dest.writeString(this.industry);
        dest.writeString(this.company_name);
        dest.writeString(this.company_address);
        dest.writeString(this.number);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.loginlatitude);
        dest.writeDouble(this.loginlongitude);
        dest.writeString(this.photo);
        dest.writeString(this.service_provider);
        dest.writeInt(this.spam);
        dest.writeInt(this.isblock);
        dest.writeInt(this.contact_user_id);
        dest.writeInt(this.id);
        dest.writeInt(this.phone_number_id);
        dest.writeString(this.type);
        dest.writeInt(this.status);
        dest.writeString(this.phoneNumber);
        dest.writeByte(this.premium ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.mutualContacts);
        dest.writeTypedList(this.tag);
    }

    protected ProfileModel(Parcel in) {
        this.user_id = in.readInt();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.nick_name = in.readString();
        this.gender = in.readString();
        this.about = in.readString();
        this.address = in.readString();
        this.website = in.readString();
        this.business = in.readString();
        this.industry = in.readString();
        this.company_name = in.readString();
        this.company_address = in.readString();
        this.number = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.loginlatitude = in.readDouble();
        this.loginlongitude = in.readDouble();
        this.photo = in.readString();
        this.service_provider = in.readString();
        this.spam = in.readInt();
        this.isblock = in.readInt();
        this.contact_user_id = in.readInt();
        this.id = in.readInt();
        this.phone_number_id = in.readInt();
        this.type = in.readString();
        this.status = in.readInt();
        this.phoneNumber = in.readString();
        this.premium = in.readByte() != 0;
        this.mutualContacts = in.createTypedArrayList(LocalContactsModel.CREATOR);
        this.tag = in.createTypedArrayList(TagModels.CREATOR);
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel source) {
            return new ProfileModel(source);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };
}
