package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class CountryModel {

    public int id;
    public String name;
    public int code;

    public CountryModel(int id, String name, int code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public CountryModel() {

    }

    public static ArrayList<CountryModel> getCountryList(JsonElement jsonElement) {
        ArrayList<CountryModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("countries").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    CountryModel countryModel = new CountryModel();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = Config.getJsonObjectInt(jsonObject, "id");
        name = Config.getJsonObjectString(jsonObject, "name");
        code = Config.getJsonObjectInt(jsonObject, "code");
    }
}
