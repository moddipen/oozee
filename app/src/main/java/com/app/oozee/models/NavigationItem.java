package com.app.oozee.models;

import android.graphics.drawable.Drawable;

public class NavigationItem {

    public Drawable drawable;
    public String title;

    public NavigationItem(Drawable drawable, String title) {
        this.drawable = drawable;
        this.title = title;
    }
}
