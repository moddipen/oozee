package com.app.oozee.models;

public class SettingsModel {

    public int user_id;
    public String name;
    public int value;

    public SettingsModel(int user_id, String name, int value) {
        this.user_id = user_id;
        this.name = name;
        this.value = value;
    }
}
