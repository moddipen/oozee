package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getJsonObject;
import static com.app.oozee.utils.Config.getJsonObjectInt;
import static com.app.oozee.utils.Config.getJsonObjectString;

public class SearchContactModel implements Parcelable {

    public int user_id;
    public int country_id;
    public String phone_number;
    public String photo;
    public String address;
    public String first_name;
    public String last_name;
    public String email;
    public String tags;
    public String gender;
    public int phone_number_id;

    public SearchContactModel(String first_name, String phone_number) {
        this.first_name = first_name;
        this.phone_number = phone_number;
    }

    public SearchContactModel(String first_name, String phone_number, String photo) {
        this.first_name = first_name;
        this.phone_number = phone_number;
        this.photo = photo;
    }

    public SearchContactModel(String first_name, String gender, int phone_number_id) {
        this.first_name = first_name;
        this.gender = gender;
        this.phone_number_id = phone_number_id;
    }

    public SearchContactModel(int user_id, int country_id, String phone_number) {
        this.user_id = user_id;
        this.country_id = country_id;
        this.phone_number = phone_number;
    }

    public SearchContactModel() {
    }


    public static ArrayList<SearchContactModel> getList(JsonElement jsonElement) {
        ArrayList<SearchContactModel> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonObject jsonObject = getJsonObject(jsonElement.getAsJsonObject(), "data");
                if (jsonObject != null) {
                    SearchContactModel planModel = new SearchContactModel();
                    boolean add = planModel.decodeJson(jsonObject);
                    if (add) {
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    private boolean decodeJson(JsonObject jsonObject) {
        first_name = getJsonObjectString(jsonObject, "first_name");
        last_name = getJsonObjectString(jsonObject, "last_name");
        email = getJsonObjectString(jsonObject, "email");
        phone_number_id = getJsonObjectInt(jsonObject, "phone_number_id");
        tags = getJsonObjectString(jsonObject, "tags");
        phone_number = getJsonObjectString(jsonObject, "phone_number");
        address = getJsonObjectString(jsonObject, "address");
        gender = getJsonObjectString(jsonObject,"gender");
        photo = getJsonObjectString(jsonObject,"photo");
        return !first_name.equalsIgnoreCase("") && !phone_number.equalsIgnoreCase("");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.user_id);
        dest.writeInt(this.country_id);
        dest.writeString(this.phone_number);
        dest.writeString(this.photo);
        dest.writeString(this.address);
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.email);
        dest.writeString(this.tags);
        dest.writeInt(this.phone_number_id);
        dest.writeString(this.gender);
    }

    protected SearchContactModel(Parcel in) {
        this.user_id = in.readInt();
        this.country_id = in.readInt();
        this.phone_number = in.readString();
        this.photo = in.readString();
        this.address = in.readString();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.email = in.readString();
        this.tags = in.readString();
        this.phone_number_id = in.readInt();
        this.gender = in.readString();
    }

    public static final Creator<SearchContactModel> CREATOR = new Creator<SearchContactModel>() {
        @Override
        public SearchContactModel createFromParcel(Parcel source) {
            return new SearchContactModel(source);
        }

        @Override
        public SearchContactModel[] newArray(int size) {
            return new SearchContactModel[size];
        }
    };
}
