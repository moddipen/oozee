package com.app.oozee.models;

import com.google.gson.JsonElement;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class BusModel {
    public String serviceName;
    public String message;
    public String phoneNumber;
    public JsonElement jsonElement;
    public ArrayList<String> contactsList = new ArrayList<>();

    public LocalContactsModel contactsModel;
}
