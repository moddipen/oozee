package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;

public class  LocalContactsModel implements Parcelable {

    public int viewType;
    public int id;
    public String name;
    public String initial;
    public String date;
    public String dir;
    public String number;
    public String rawContactId;
    public String duration;
    public String image;
    public String gender;
    public Date callDayTime;
    public boolean isCalled;
    public int count;

    public LocalContactsModel(int viewType, String name, String initial) {
        this.viewType = viewType;
        this.name = name;
        this.initial = initial;
    }

    public LocalContactsModel(int id, int viewType, String name, String initial, String number,
                              String image, String gender) {
        this.id = id;
        this.viewType = viewType;
        this.name = name;
        this.initial = initial;
        this.number = number;
        this.image = image;
        this.gender = gender;
    }

    public LocalContactsModel(int viewType, String name, String initial, String number) {
        this.viewType = viewType;
        this.name = name;
        this.initial = initial;
        this.number = number;
    }

    public LocalContactsModel(int viewType, String name, String initial, String number,
                              String image) {
        this.viewType = viewType;
        this.name = name;
        this.initial = initial;
        this.number = number;
        this.image = image;
    }

    public LocalContactsModel(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public LocalContactsModel(int viewType, String name, String initial, String date, String dir,
                              String number, String image, int count) {
        this.viewType = viewType;
        this.name = name;
        this.initial = initial;
        this.date = date;
        this.dir = dir;
        this.number = number;
        this.image = image;
        this.count = count;
    }

    public LocalContactsModel(String name, String date, String dir) {
        this.name = name;
        this.date = date;
        this.dir = dir;
    }

    public LocalContactsModel(String name, String date, String dir, String number) {
        this.name = name;
        this.date = date;
        this.dir = dir;
        this.number = number;
    }

    public LocalContactsModel(String name, String date, String dir, String number,
                              String rawContactId) {
        this.name = name;
        this.date = date;
        this.dir = dir;
        this.number = number;
        this.rawContactId = rawContactId;
    }

    public LocalContactsModel(String name, String date, String dir, String number, String duration,
                              Date callDayTime) {
        this.name = name;
        this.date = date;
        this.dir = dir;
        this.number = number;
        this.duration = duration;
        this.callDayTime = callDayTime;
    }

    public LocalContactsModel() {

    }

    public static ArrayList<LocalContactsModel> getLocalContacts(JsonElement jsonElement) {
        ArrayList<LocalContactsModel> localContactsModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("contacts").getAsJsonArray();
                for (int i = 0; i < jsonArray.size(); i++) {
                    LocalContactsModel localContactsModel = new LocalContactsModel();
                    localContactsModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    localContactsModels.add(localContactsModel);
                }
            }
        }

        return localContactsModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        number = Config.getJsonObjectString(jsonObject, "number");
        name = Config.getJsonObjectString(jsonObject, "status");
        if (name.contains(" ")){
            name = Config.getDateForString2(Config.getJsonObjectString(jsonObject, "status"));
        }
        //name = Config.getJsonObjectString(jsonObject, "status");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.viewType);
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.initial);
        dest.writeString(this.date);
        dest.writeString(this.dir);
        dest.writeString(this.number);
        dest.writeString(this.rawContactId);
        dest.writeString(this.duration);
        dest.writeString(this.image);
        dest.writeString(this.gender);
        dest.writeLong(this.callDayTime != null ? this.callDayTime.getTime() : -1);
        dest.writeByte(this.isCalled ? (byte) 1 : (byte) 0);
        dest.writeInt(this.count);
    }

    protected LocalContactsModel(Parcel in) {
        this.viewType = in.readInt();
        this.id = in.readInt();
        this.name = in.readString();
        this.initial = in.readString();
        this.date = in.readString();
        this.dir = in.readString();
        this.number = in.readString();
        this.rawContactId = in.readString();
        this.duration = in.readString();
        this.image = in.readString();
        this.gender = in.readString();
        long tmpCallDayTime = in.readLong();
        this.callDayTime = tmpCallDayTime == -1 ? null : new Date(tmpCallDayTime);
        this.isCalled = in.readByte() != 0;
        this.count = in.readInt();
    }

    public static final Creator<LocalContactsModel> CREATOR = new Creator<LocalContactsModel>() {
        @Override
        public LocalContactsModel createFromParcel(Parcel source) {
            return new LocalContactsModel(source);
        }

        @Override
        public LocalContactsModel[] newArray(int size) {
            return new LocalContactsModel[size];
        }
    };
}
