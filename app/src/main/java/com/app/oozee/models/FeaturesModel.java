package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class FeaturesModel {

    public int id;
    public String title;

    public static ArrayList<FeaturesModel> getFeatures(JsonArray jsonArray) {
        ArrayList<FeaturesModel> featuresModels = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                FeaturesModel featuresModel = new FeaturesModel();
                featuresModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                featuresModels.add(featuresModel);
            }
        }
        return featuresModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = Config.getJsonObjectInt(jsonObject, "id");
        title = Config.getJsonObjectString(jsonObject, "title");
    }
}