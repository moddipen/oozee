package com.app.oozee.models;

public class UpdateProfileModel {

    public int user_id;
    public String first_name;
    public String last_name;
    public String email;
    public String nick_name;
    public String gender;
    public String about;
    public String address;
    public String website;
    public String industry;
    public String company_name;
    public String company_address;
    public String photo;

    public UpdateProfileModel(int user_id, String first_name, String last_name, String email,
                              String nick_name, String gender, String about,
                              String address, String website, String industry,
                              String company_name, String company_address, String photo) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.nick_name = nick_name;
        this.gender = gender;
        this.about = about;
        this.address = address;
        this.website = website;
        this.industry = industry;
        this.company_name = company_name;
        this.company_address = company_address;
        this.photo = photo;
    }
}
