package com.app.oozee.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getJsonArray;
import static com.app.oozee.utils.Config.getJsonObjectInt;
import static com.app.oozee.utils.Config.getJsonObjectString;

public class PlanModel {

    public int id;
    public String name;
    public int price;
    public String type;
    public String features;
    public ArrayList<FeaturesModel> defaultFeatures = new ArrayList<>();
    public ArrayList<FeaturesModel> extraFeatures = new ArrayList<>();
    public int user_id;
    public int plan_id;

    public PlanModel() {

    }

    public PlanModel(int user_id, int plan_id) {
        this.user_id = user_id;
        this.plan_id = plan_id;
    }

    public PlanModel(int user_id) {
        this.user_id = user_id;
    }

    public static ArrayList<PlanModel> getPlansList(JsonElement jsonElement) {
        ArrayList<PlanModel> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "plans");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        PlanModel planModel = new PlanModel();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    public static PlanModel getPlansModel(JsonObject jsonObject) {
        if (jsonObject != null) {
            PlanModel planModel = new PlanModel();
            planModel.decodeJson(jsonObject);
            return planModel;
        }
        return null;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = getJsonObjectInt(jsonObject, "id");
        name = getJsonObjectString(jsonObject, "name");
        price = getJsonObjectInt(jsonObject, "price");
        type = getJsonObjectString(jsonObject, "type");
        features = getJsonObjectString(jsonObject, "features");

        defaultFeatures = FeaturesModel.getFeatures(getJsonArray(jsonObject, "defaultFeatures"));
        extraFeatures = FeaturesModel.getFeatures(getJsonArray(jsonObject, "extraFeatures"));
    }
}