package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getJsonArray;
import static com.app.oozee.utils.Config.getJsonObjectInt;
import static com.app.oozee.utils.Config.getJsonObjectString;

public class NotesModel implements Parcelable {

    public int id;
    public int user_id;
    public String title;
    public String note;
    public String phone_number;
    public String country_id;

    public NotesModel(int user_id, String title, String note,
                      String phone_number, String country_id) {
        this.user_id = user_id;
        this.title = title;
        this.note = note;
        this.phone_number = phone_number;
        this.country_id = country_id;
    }

    public NotesModel() {

    }

    public static ArrayList<NotesModel> getNotesList(JsonElement jsonElement) {
        ArrayList<NotesModel> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "notes");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        NotesModel planModel = new NotesModel();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = getJsonObjectInt(jsonObject, "id");
        user_id = getJsonObjectInt(jsonObject, "user_id");
        title = getJsonObjectString(jsonObject, "title");
        note = getJsonObjectString(jsonObject, "note");
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeString(this.title);
        dest.writeString(this.note);
        dest.writeString(this.phone_number);
        dest.writeString(this.country_id);
    }

    protected NotesModel(Parcel in) {
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.title = in.readString();
        this.note = in.readString();
        this.phone_number = in.readString();
        this.country_id = in.readString();
    }

    public static final Creator<NotesModel> CREATOR = new Creator<NotesModel>() {
        @Override
        public NotesModel createFromParcel(Parcel source) {
            return new NotesModel(source);
        }

        @Override
        public NotesModel[] newArray(int size) {
            return new NotesModel[size];
        }
    };
}