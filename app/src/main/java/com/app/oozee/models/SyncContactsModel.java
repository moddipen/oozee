package com.app.oozee.models;

public class SyncContactsModel {

    private int user_id;
    private int media_id;
    private int country_id;

    public SyncContactsModel(int media_id) {
        this.media_id = media_id;
    }

    public SyncContactsModel(int media_id, int user_id, int country_id) {
        this.media_id = media_id;
        this.user_id = user_id;
        this.country_id = country_id;
    }
}
