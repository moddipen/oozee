package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class BlockedContactModel {

    public int user_id;
    public int contact_id;

    public int id;
    public String phone_number;
    public String status;
    public String last_name;
    public String first_name;
    public String gender;
    public String image;
    public int country_id;
    private ArrayList<String> contacts;

    public BlockedContactModel(int user_id) {
        this.user_id = user_id;
    }

    public BlockedContactModel(int user_id, int contact_id) {
        this.user_id = user_id;
        this.contact_id = contact_id;
    }

    public BlockedContactModel(int user_id, String phone_number, int country_id) {
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.country_id = country_id;
    }

    public BlockedContactModel(int user_id, String phone_number, String status) {
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.status = status;
    }

    public BlockedContactModel(int user_id, int country_id, ArrayList<String> contacts) {
        this.user_id = user_id;
        this.contacts = contacts;
        this.country_id = country_id;
    }

    public BlockedContactModel(int user_id, String phone_number, int country_id,String image) {
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.country_id = country_id;
        this.image = image;
    }

    public BlockedContactModel() {
    }

    public static ArrayList<BlockedContactModel> getBlockedList(JsonElement jsonElement) {
        ArrayList<BlockedContactModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("contacts").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    BlockedContactModel countryModel = new BlockedContactModel();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    public static ArrayList<BlockedContactModel> getDeadList(JsonElement jsonElement) {
        ArrayList<BlockedContactModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("contacts").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    BlockedContactModel countryModel = new BlockedContactModel();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    public static ArrayList<BlockedContactModel> getQuickList(JsonElement jsonElement) {
        ArrayList<BlockedContactModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("contacts").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    BlockedContactModel countryModel = new BlockedContactModel();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }


    private void decodeJson(JsonObject jsonObject) {

        id = Config.getJsonObjectInt(jsonObject, "id");
        user_id = Config.getJsonObjectInt(jsonObject, "user_id");
        phone_number = Config.getJsonObjectString(jsonObject, "phone_number");
        last_name = Config.getJsonObjectString(jsonObject, "last_name");
        first_name = Config.getJsonObjectString(jsonObject, "first_name");
        country_id = Config.getJsonObjectInt(jsonObject, "country_id");
        gender = Config.getJsonObjectString(jsonObject, "gender");
    }
}
