package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class MediaModel {

    public int id;
    public String idImage;

    public static MediaModel getMediaModel(JsonElement jsonElement, boolean isProfile) {
        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonObject jsonObject = jsonElement.getAsJsonObject().get("data").getAsJsonObject();
                MediaModel mediaModel = new MediaModel();
                mediaModel.decodeJson(jsonObject, isProfile);
                return mediaModel;
            }
        }
        return null;
    }

    private void decodeJson(JsonObject jsonObject, boolean isProfile) {
        if (isProfile) {
            idImage = Config.getJsonObjectString(jsonObject, "id");
        }else{
            id = Config.getJsonObjectInt(jsonObject, "id");
        }
    }
}
