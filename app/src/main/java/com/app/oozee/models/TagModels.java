package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class TagModels implements Parcelable {

    public int id;
    public String name;
    public int user_id;
    public String phone_number;
    public int tag_id;
    public int sub_tag_id;
    public int country_id;
    public int isMain;

    public TagModels(int user_id, String phone_number, int country_id,
                     int tag_id, int sub_tag_id) {
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.country_id = country_id;
        this.tag_id = tag_id;
        this.sub_tag_id = sub_tag_id;
    }

    public TagModels() {
    }

    public TagModels(String name) {
    }

    public static ArrayList<TagModels> getTagList(JsonElement jsonElement) {
        ArrayList<TagModels> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("tags").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    TagModels countryModel = new TagModels();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject(), 1);
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    public static ArrayList<TagModels> getSubTagList(JsonElement jsonElement) {
        ArrayList<TagModels> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("sub_tags").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    TagModels countryModel = new TagModels();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject(), 0);
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    public static ArrayList<TagModels> getTag(JsonArray jsonObject){
        ArrayList<TagModels> tagModels = new ArrayList<>();
        for (int i = 0; i < jsonObject.size(); i++){
            TagModels tagModel = new TagModels();
            tagModel.decodeJson2(jsonObject.get(i).getAsJsonObject(), 0);
            tagModels.add(tagModel);
        }

        return tagModels;
    }

    private void decodeJson(JsonObject jsonObject, int isMainInt) {
        id = Config.getJsonObjectInt(jsonObject, "id");
        name = Config.getJsonObjectString(jsonObject, "name");
        isMain = isMainInt;
    }

    private void decodeJson2(JsonObject jsonObject, int isMainInt) {
        id = Config.getJsonObjectInt(jsonObject, "id");
        name = Config.getJsonObjectString(jsonObject, "sub_tag_name");
        isMain = isMainInt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.user_id);
        dest.writeString(this.phone_number);
        dest.writeInt(this.tag_id);
        dest.writeInt(this.sub_tag_id);
        dest.writeInt(this.country_id);
        dest.writeInt(this.isMain);
    }

    protected TagModels(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.user_id = in.readInt();
        this.phone_number = in.readString();
        this.tag_id = in.readInt();
        this.sub_tag_id = in.readInt();
        this.country_id = in.readInt();
        this.isMain = in.readInt();
    }

    public static final Creator<TagModels> CREATOR = new Creator<TagModels>() {
        @Override
        public TagModels createFromParcel(Parcel source) {
            return new TagModels(source);
        }

        @Override
        public TagModels[] newArray(int size) {
            return new TagModels[size];
        }
    };
}
