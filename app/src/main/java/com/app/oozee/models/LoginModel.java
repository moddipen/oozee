package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class LoginModel {

    public String phone_number;
    public String first_name;
    public String last_name;
    public String email;
    public int country_id;
    public String gender;
    public String type;
    public double latitude;
    public double longitude;
    public String device_imei;
    public String device_token;
    public String device_type;
    public String access_token;
    public String refresh_token;
    public String birthdate;
    public String photo;
    public int user_id;
    public PlanModel planModel;

    public LoginModel(String phone_number, String first_name, String last_name,
                      String email, int country_id, String gender,
                      String type, double latitude, double longitude, String device_imei,
                      String device_token, String device_type, String birthdate , String photo) {
        this.phone_number = phone_number;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.country_id = country_id;
        this.gender = gender;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.device_imei = device_imei;
        this.device_token = device_token;
        this.device_type = device_type;
        this.birthdate = birthdate;
        this.photo = photo;
    }

    private LoginModel() {

    }

    public static LoginModel getLogin(JsonElement jsonElement) {
        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                LoginModel loginModel = new LoginModel();
                loginModel.decodeJson(jsonElement.getAsJsonObject().get("data").getAsJsonObject());
                return loginModel;
            }
        }
        return null;
    }

    private void decodeJson(JsonObject jsonObject) {
        access_token = Config.getJsonObjectString(jsonObject, "access_token");
        refresh_token = Config.getJsonObjectString(jsonObject, "refresh_token");
        user_id = Config.getJsonObjectInt(jsonObject, "user_id");

        JsonObject object = Config.getJsonObject(jsonObject, "current_plan");
        if (object != null) {
            planModel = PlanModel.getPlansModel(object);
        }
    }
}
