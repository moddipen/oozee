package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getJsonArray;
import static com.app.oozee.utils.Config.getJsonObjectInt;
import static com.app.oozee.utils.Config.getJsonObjectString;

public class BlogModels implements Parcelable {

    public int id;
    public String title;
    public String slug;
    public String content;
    public int status;

    public BlogModels() {

    }

    public BlogModels(String title) {
        this.title = title;
    }

    public static ArrayList<BlogModels> getBlogsList(JsonElement jsonElement) {
        ArrayList<BlogModels> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "blogs");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        BlogModels planModel = new BlogModels();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    public static ArrayList<BlogModels> getNewsList(JsonElement jsonElement) {
        ArrayList<BlogModels> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "news");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        BlogModels planModel = new BlogModels();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    public static ArrayList<BlogModels> getcmsList(JsonElement jsonElement) {
        ArrayList<BlogModels> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "cms_pages");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        BlogModels planModel = new BlogModels();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = getJsonObjectInt(jsonObject, "id");
        status = getJsonObjectInt(jsonObject, "status");
        title = getJsonObjectString(jsonObject, "title");
        slug = getJsonObjectString(jsonObject, "slug");
        content = getJsonObjectString(jsonObject, "content");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.slug);
        dest.writeString(this.content);
        dest.writeInt(this.status);
    }

    protected BlogModels(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.slug = in.readString();
        this.content = in.readString();
        this.status = in.readInt();
    }

    public static final Creator<BlogModels> CREATOR = new Creator<BlogModels>() {
        @Override
        public BlogModels createFromParcel(Parcel source) {
            return new BlogModels(source);
        }

        @Override
        public BlogModels[] newArray(int size) {
            return new BlogModels[size];
        }
    };
}
