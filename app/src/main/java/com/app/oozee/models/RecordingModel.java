package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class RecordingModel {

    public int user_id;
    public int plan_id;
    public int country_id;
    public String phone_number;

    public RecordingModel(int user_id, int plan_id, int country_id, String phone_number) {
        this.user_id = user_id;
        this.plan_id = plan_id;
        this.country_id = country_id;
        this.phone_number = phone_number;
    }

    public RecordingModel(int user_id) {
        this.user_id = user_id;
    }

    public RecordingModel() {
    }

    public static ArrayList<RecordingModel> getRecordingList(JsonElement jsonElement) {
        ArrayList<RecordingModel> countryModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = jsonElement.getAsJsonObject().get("data").getAsJsonObject().get("recordings").getAsJsonArray();

                for (int i = 0; i < jsonArray.size(); i++) {
                    RecordingModel countryModel = new RecordingModel();
                    countryModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                    countryModels.add(countryModel);
                }
            }
        }
        return countryModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        user_id = Config.getJsonObjectInt(jsonObject, "user_id");
        plan_id = Config.getJsonObjectInt(jsonObject, "plan_id");
        country_id = Config.getJsonObjectInt(jsonObject, "country_id");
        phone_number = Config.getJsonObjectString(jsonObject, "phone_number");
    }
}
