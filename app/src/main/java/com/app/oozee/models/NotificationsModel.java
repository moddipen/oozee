package com.app.oozee.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getJsonArray;
import static com.app.oozee.utils.Config.getJsonObjectInt;
import static com.app.oozee.utils.Config.getJsonObjectString;

public class NotificationsModel {

    public int id;
    public int user_id;
    public String type;
    public String title;

    public NotificationsModel() {
    }

    public static ArrayList<NotificationsModel> getNotifications(JsonElement jsonElement) {
        ArrayList<NotificationsModel> planModels = new ArrayList<>();

        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                JsonArray jsonArray = getJsonArray(jsonElement.getAsJsonObject().get("data").getAsJsonObject(), "notifications");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        NotificationsModel planModel = new NotificationsModel();
                        planModel.decodeJson(jsonArray.get(i).getAsJsonObject());
                        planModels.add(planModel);
                    }
                }
            }
        }
        return planModels;
    }

    private void decodeJson(JsonObject jsonObject) {
        id = getJsonObjectInt(jsonObject, "id");
        user_id = getJsonObjectInt(jsonObject, "user_id");
        title = getJsonObjectString(jsonObject, "title");
        type = getJsonObjectString(jsonObject, "type");
    }
}