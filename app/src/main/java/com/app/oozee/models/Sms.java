package com.app.oozee.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Sms implements Parcelable {

    public ArrayList<Sms> sms = new ArrayList<>();
    private String _id;
    private String _address;
    private String _msg;
    private String _readState; //"0" for have not read sms and "1" for have read sms
    private String _time;
    private String _folderName;
    public String isStarred;

    public String getId(){
        return _id;
    }
    public String getAddress(){
        return _address;
    }
    public String getMsg(){
        return _msg;
    }
    public String getReadState(){
        return _readState;
    }
    public String getTime(){
        return _time;
    }
    public String getFolderName(){
        return _folderName;
    }


    public void setId(String id){
        _id = id;
    }
    public void setAddress(String address){
        _address = address;
    }
    public void setMsg(String msg){
        _msg = msg;
    }
    public void setReadState(String readState){
        _readState = readState;
    }
    public void setTime(String time){
        _time = time;
    }
    public void setFolderName(String folderName){
        _folderName = folderName;
    }

    public Sms() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.sms);
        dest.writeString(this._id);
        dest.writeString(this._address);
        dest.writeString(this._msg);
        dest.writeString(this._readState);
        dest.writeString(this._time);
        dest.writeString(this._folderName);
    }

    protected Sms(Parcel in) {
        this.sms = in.createTypedArrayList(Sms.CREATOR);
        this._id = in.readString();
        this._address = in.readString();
        this._msg = in.readString();
        this._readState = in.readString();
        this._time = in.readString();
        this._folderName = in.readString();
    }

    public static final Creator<Sms> CREATOR = new Creator<Sms>() {
        @Override
        public Sms createFromParcel(Parcel source) {
            return new Sms(source);
        }

        @Override
        public Sms[] newArray(int size) {
            return new Sms[size];
        }
    };
}
