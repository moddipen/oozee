package com.app.oozee.models;

import com.app.oozee.utils.Config;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CheckSignup {

    public String phone_number;
    public int country_id;
    public int user_id;
    public String first_name;
    public String last_name;
    public String email;
    public String gender;
    public String dob;
    public String photo;

    public CheckSignup(String phone_number, int country_id) {
        this.phone_number = phone_number;
        this.country_id = country_id;
    }

    private CheckSignup() {

    }

    public static CheckSignup getLogin(JsonElement jsonElement) {
        if (jsonElement instanceof JsonObject) {
            if (jsonElement.getAsJsonObject().get("success").getAsBoolean()) {
                CheckSignup loginModel = new CheckSignup();
                loginModel.decodeJson(jsonElement.getAsJsonObject().get("data").getAsJsonObject());
                return loginModel;
            }
        }
        return null;
    }

    private void decodeJson(JsonObject jsonObject) {
        user_id = Config.getJsonObjectInt(jsonObject, "user_id");
        first_name = Config.getJsonObjectString(jsonObject, "first_name");
        last_name = Config.getJsonObjectString(jsonObject, "last_name");
        email = Config.getJsonObjectString(jsonObject, "email");
        gender = Config.getJsonObjectString(jsonObject, "gender");
        dob = Config.getJsonObjectString(jsonObject, "dob");
        photo = Config.getJsonObjectString(jsonObject,"photo");
    }
}
