package com.app.oozee.interfaces;

import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.CheckSignup;
import com.app.oozee.models.LoginModel;
import com.app.oozee.models.NotesModel;
import com.app.oozee.models.PlanModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.RecordingModel;
import com.app.oozee.models.SearchContactModel;
import com.app.oozee.models.SettingsModel;
import com.app.oozee.models.SyncContactsModel;
import com.app.oozee.models.TagModels;
import com.app.oozee.models.UpdateProfileModel;
import com.app.oozee.models.VoiceMessageModel;
import com.google.gson.JsonElement;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface OozeeInterface {

    @POST
    Call<JsonElement> loginCall(@Url String url, @Body LoginModel loginModel);

    @POST
    Call<JsonElement> checkSignUp(@Url String url, @Body CheckSignup checkSignup);

    @POST
    Call<JsonElement> updateCall(@Url String url, @Body ProfileModel profileModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> updateCall(@Url String url, @Body UpdateProfileModel profileModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> updateSettings(@Url String url, @Body SettingsModel settingsModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> searchContact(@Url String url, @Body SearchContactModel searchContactModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> planModel(@Url String url, @Body PlanModel planModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> notesModel(@Url String url, @Body NotesModel notesModel, @Header("Authorization") String bearer);

    @PUT
    Call<JsonElement> editnotesModel(@Url String url, @Body NotesModel notesModel, @Header("Authorization") String bearer);

    @GET
    Call<JsonElement> allServiceCall(@Url String url, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> addContactToList(@Url String url, @Body ArrayList<BlockedContactModel> blockedContactModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> addSingleContactToList(@Url String url, @Body BlockedContactModel blockedContactModel,
                                             @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> addTagModel(@Url String url, @Body TagModels tagModels, @Header("Authorization") String bearer);

    @DELETE
    Call<JsonElement> deleteApiCall(@Url String url, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> addVoiceMessage(@Url String url, @Body VoiceMessageModel blockedContactModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> addRecording(@Url String url, @Body RecordingModel blockedContactModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> syncContacts(@Url String url, @Body SyncContactsModel syncContactsModel, @Header("Authorization") String bearer);

    @POST
    Call<JsonElement> getNotes(@Url String url, @Body BlockedContactModel blockedContactModel,
                               @Header("Authorization") String bearer);

    @Multipart
    @POST("add-media")
    Call<JsonElement> addMedia(@Part MultipartBody.Part file, @Part("type") RequestBody type);
}
