package com.app.oozee.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.app.oozee.activities.BackupActivity;
import com.app.oozee.activities.ProfileScreen;
import com.app.oozee.activities.SelectContactsScreen;
import com.app.oozee.models.LocalContactsModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.app.oozee.utils.Utility.USERSUBSCRIPTION;

public class Config {

    public static Integer checkIntForNull(String convertToInt) {
        if (convertToInt != null && !convertToInt.equalsIgnoreCase("")) {
            return Integer.valueOf(convertToInt);
        }
        return 0;
    }

    public static Integer checkIntegerForNullComment(String checkForNull) {

        if (checkForNull != null && !checkForNull.equalsIgnoreCase("")) {
            return Integer.parseInt(checkForNull) + 1;
        }
        return 1;
    }

    public static String getJsonObjectString(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return "";
            }
            if (jsonObject.get(key) != null) {
                if (jsonObject.get(key) != null && !jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsString();
                }
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Boolean getJsonObjectBool(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return false;
            }
            if (jsonObject.get(key) != null) {
                if (!jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsBoolean();
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int getIntFromString(String string) {
        if (string != null && !string.equalsIgnoreCase("")) {
            return Integer.parseInt(string);
        } else {
            return 0;
        }
    }

    public static Date getDateFromTimeStamp(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.CANADA);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getChatDateString() {
        long time = Calendar.getInstance().getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CANADA);
        return sdf.format(new Date(time));
    }

    public static String getDateFromTimeStampMeet(long time) {
        String deate = "";
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);//get local date
            SimpleDateFormat spf = new SimpleDateFormat("EEE MMM dd, yyyy, ", Locale.US);
            deate = spf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return deate;
    }

    public static String getDateFromTimeStampStartEnd(long starttime, long endTime, int i) {
        String deate = "";
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(starttime * 1000));
        String localTime2 = sdf.format(new Date(endTime * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);
            SimpleDateFormat spf = new SimpleDateFormat("hh:mma", Locale.US);
            String deate1 = spf.format(date);
            deate1 = removeZero(deate1);

            if (i == 0) {
                return deate1;
            }

            date = sdf.parse(localTime2);
            deate = spf.format(date);
            deate = removeZero(deate);

            deate = deate1 + " - " + deate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return deate;
    }

    private static String removeZero(String time) {
        if (time.startsWith("0")) {
            time = time.substring(1);
        }
        return time;
    }

    public static String getDateFromTimeStampForLocalNoti(long time) {
        String deate;
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.CANADA);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);//get local date
            SimpleDateFormat spf = new SimpleDateFormat("dd MM yyyy HH mm ss", Locale.CANADA);
            deate = spf.format(date);
            return deate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getDateFromTimeStampEdit(long time) {
        String deate;
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);//get local date
            SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
            deate = spf.format(date);
            String arrp[] = deate.split(" ");
            String i4 = arrp[3];
            String[] aa = i4.split(":");
            String i5 = removeZero(aa[0]);
            deate = deate.replace(aa[0] + ":", i5 + ":");
            return deate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static ArrayList<String> getDayDateYear(String time) {

        ArrayList<String> strings = new ArrayList<>();
        String deate;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        try {
            Date d = sdf.parse(time);
            SimpleDateFormat spf = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US);

            deate = spf.format(d);
            String arrp[] = deate.split(" ");
            String dateDay = arrp[0];
            String month = arrp[1];
            String year = arrp[2];
            String hour = arrp[3];

            String h[] = hour.split(":");
            String hours = h[0];
            String mins = h[1];

            strings.add(year);
            strings.add(month);
            strings.add(dateDay);
            strings.add(hours);
            strings.add(mins);
            return strings;
        } catch (ParseException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static String getHrSecDate(long time) {
        String deate;
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);//get local date
            SimpleDateFormat spf = new SimpleDateFormat("MMM dd, yyyy HH mm", Locale.US);
            deate = spf.format(date);
            return deate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getJsonObjectInt(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return 0;
            }
            if (!jsonObject.get(key).isJsonNull() && jsonObject.get(key) != null && jsonObject.get(key).getAsString() != null
                    && !jsonObject.get(key).getAsString().equalsIgnoreCase("")) {
                return Integer.parseInt(jsonObject.get(key).getAsString());
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static double getJsonObjectDouble(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return 0;
            }
            if (jsonObject.get(key) != null) {
                if (!jsonObject.get(key).isJsonNull()) {
                    return Double.parseDouble(jsonObject.get(key).getAsString());
                }
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long getJsonObjectLong(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return 0;
            }
            if (!jsonObject.get(key).isJsonNull()) {
                return Long.parseLong(jsonObject.get(key).getAsString());
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getTimeFromTimeStamp(long time) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("hh:mm aa");
        return formatter.format(new Date(time));

       /* String deate = "";
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.CANADA);
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time * 1000));
        Date date;
        try {
            date = sdf.parse(localTime);
            SimpleDateFormat spf = new SimpleDateFormat("hh:mm a", Locale.CANADA);
            deate = spf.format(date);
            deate = removeZero(deate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return deate;*/
    }

    public static int getJsonObjectInteger(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return 0;
            }
            if (!jsonObject.get(key).isJsonNull() && jsonObject.get(key).getAsString() != null
                    && !jsonObject.get(key).getAsString().equalsIgnoreCase("")) {
                return jsonObject.get(key).getAsInt();
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public static JsonArray getJsonArray(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return null;
            }
            if (!jsonObject.get(key).isJsonNull()) {
                return jsonObject.get(key).getAsJsonArray();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static JsonObject getJsonObject(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null) {
                return null;
            }
            if (!jsonObject.has(key)) {
                return null;
            }
            if (jsonObject.get(key) == null) {
                return null;
            }

            if (jsonObject.get(key).isJsonObject()) {
                return jsonObject.get(key).getAsJsonObject();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateFromTimeStamp(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.US);
        return formatter.format(new Date(Long.parseLong(name)));
    }

    public static String getDateFromTimeStampDate(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return formatter.format(new Date(Long.parseLong(name)));
    }

    public static String getDateFromTimeStampHistory(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return formatter.format(new Date(Long.parseLong(name)));
    }

    public static String getDateFromStamp(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        return formatter.format(new Date(Long.parseLong(name)));
    }

    public static String getDateFromStampMMM(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return formatter.format(new Date(Long.parseLong(name)));
    }

    public static String getDateFormat(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US);
        try {
            Date date = formatter.parse(name);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeForAgo(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        try {
            Date date = formatter.parse(name);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateFormat2(String name) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
        try {
            Date date = formatter.parse(name);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getChatDate(String name) {
        Log.d("DateString", name);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        try {
            Date date = formatter.parse(name);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getChatLong(String name) {
        Log.d("DateString", name);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        try {
            Date date = formatter.parse(name);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getDateForString(String dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        try {
            Date date = formatter.parse(dateTime);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDateForString2(String dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            Date date = formatter.parse(dateTime);
            return String.valueOf(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeFromString(String dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        try {
            Date date = formatter.parse(dateTime);
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static File generateVCard(ArrayList<LocalContactsModel> contactsModels, Context context) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();

        File vcfFile = new File(context.getExternalFilesDir(null), "oozee111.vcf");
        if (vcfFile.exists()) {
            vcfFile.delete();
        }

        if (!vcfFile.exists()) {
            try {
                vcfFile.createNewFile();
                try {
                    FileOutputStream fileinput = new FileOutputStream(vcfFile, true);
                    fileinput.close();
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        for (int i = 0; i < contactsModels.size(); i++) {
            LocalContactsModel p = contactsModels.get(i);
            try {
                OutputStreamWriter file_writer = new OutputStreamWriter(new FileOutputStream(vcfFile, true));
                BufferedWriter buffered_writer = new BufferedWriter(file_writer);
                buffered_writer.write("BEGIN:VCARD\r\n");
                buffered_writer.write("VERSION:3.0\r\n");
                try {
                    buffered_writer.write("N:" + p.name + ";" + p.name + "\r\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    buffered_writer.write("FN:" + p.name /*+ " " + p.date*/ + "\r\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    buffered_writer.write("TEL;TYPE=WORK,VOICE:" + p.number + "\r\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    buffered_writer.write("EMAIL;TYPE=PREF,INTERNET:" + p.date + "\r\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (p.dir != null && !p.dir.equalsIgnoreCase("")) {
                    Uri uri = Uri.withAppendedPath(Uri.parse(p.dir), ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
                    Bitmap bitmap = getContactImage(uri.getPath(), context);
                    //Bitmap bitmap = null;
                    if (bitmap != null) {
                        try {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                            byte[] b = baos.toByteArray();
                            String image_encoded = Base64.encodeToString(b, Base64.DEFAULT);
                            /*Write the encoded version of image to vCard 2.1, NOTICE that no determining whether the image is GIF or JPEG is needed*/
                            buffered_writer.write("PHOTO;ENCODING=BASE64:" + image_encoded + "\r\n");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        buffered_writer.write("PHOTO;ENCODING=BASE64:" + "" + "\r\n");
                    }
                }
                buffered_writer.write("END:VCARD\r\n");
                buffered_writer.close();
                //buffered_writer.write("ORG:" + p.name + "\r\n");
                //fw.write("TITLE:" + p.getTitle() + "\r\n");
                //buffered_writer.write("TEL;TYPE=HOME,VOICE:" + p.dir + "\r\n");
                //fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (context instanceof SelectContactsScreen || context instanceof ProfileScreen ||
                context instanceof BackupActivity) {
            if (!((Activity) context).isDestroyed()) {
                VCardInterface vCardInterface = (VCardInterface) context;
                vCardInterface.created(vcfFile);
            }
        }
        return vcfFile;
        /*Intent i = new Intent();
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        i.setAction(android.content.Intent.ACTION_VIEW);
        i.setDataAndType(Uri.fromFile(file), "text/x-vcard");
        startActivity(i);*/
    }

    public static void openWhatsapp(Context context, String phone) {
        phone = phone.replace("+", "").replace(" ", "");
        if (!phone.startsWith("91")) {
            phone = "91" + phone;
        }
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
        sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(phone) + "@s.whatsapp.net");
        context.startActivity(sendIntent);
    }

    public static void voiceCall(String number, Context context, boolean isVideo) {
        String contactNumber = number;
        contactNumber = contactNumber.replace("+", "").replace(" ", "");
        if (!contactNumber.startsWith("91")) {
            contactNumber = "91" + contactNumber;
        }

        Cursor cursor;
        if (isVideo) {
            cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                    new String[]{ContactsContract.Data._ID},
                    ContactsContract.RawContacts.ACCOUNT_TYPE + " = 'com.whatsapp' " +
                            "AND " + ContactsContract.Data.MIMETYPE + " = 'vnd.android.cursor.item/vnd.com.whatsapp.video.call' " +
                            "AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + contactNumber + "%'", null,
                    ContactsContract.Contacts.DISPLAY_NAME);
        } else {
            cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                    new String[]{ContactsContract.Data._ID},
                    ContactsContract.RawContacts.ACCOUNT_TYPE + " = 'com.whatsapp' " +
                            "AND " + ContactsContract.Data.MIMETYPE + " = 'vnd.android.cursor.item/vnd.com.whatsapp.voip.call' " +
                            "AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + contactNumber + "%'", null,
                    ContactsContract.Contacts.DISPLAY_NAME);
        }

        if (cursor != null) {
            long id = -1;
            while (cursor.moveToNext()) {
                id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data._ID));
            }

            if (!cursor.isClosed()) {
                cursor.close();
            }

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("content://com.android.contacts/data/" + id), "vnd.android.cursor.item/vnd.com.whatsapp.voip.call");
            intent.setPackage("com.whatsapp");
            context.startActivity(intent);
        }
    }

    public static void callPhone(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static void shareIntent(Context context) {
        String shareBody = "Click on the link and get the latest Oozee app : " + "https://oozee.page.link/u9DC";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Invite Friends"));
    }

    public static void showKeyboard(Activity activity) {
        if (activity != null && !activity.isDestroyed()) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(),
                            InputMethodManager.SHOW_FORCED, 0);
                }
            }
        }
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && !activity.isDestroyed()) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static String getExpireMonth(int month) {

        String str = "";
        if (month == 1) {
            str = "Jan";
        } else if (month == 2) {
            str = "Feb";
        } else if (month == 3) {
            str = "Mar";
        } else if (month == 4) {
            str = "Apr";
        } else if (month == 5) {
            str = "May";
        } else if (month == 6) {
            str = "Jun";
        } else if (month == 7) {
            str = "Jul";
        } else if (month == 8) {
            str = "Aug";
        } else if (month == 9) {
            str = "Sep";
        } else if (month == 10) {
            str = "Oct";
        } else if (month == 11) {
            str = "Nov";
        } else if (month == 12) {
            str = "Dec";
        }
        return str;
    }

    public static String getDurationString(int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        return " (" + twoDigitString(hours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s)";
    }

    private static String twoDigitString(int number) {
        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }

    public static void modifyContact(String phonenumber, Context context) {
        phonenumber = phonenumber.replace("+", "").replace(" ", "");

        String s = ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + phonenumber + "%'";
        Log.d("PrintDatabasequery", s);

        Cursor mCursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null, s, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            int mLookupKeyIndex = mCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
            String mCurrentLookupKey = mCursor.getString(mLookupKeyIndex);
            int mIdIndex = mCursor.getColumnIndex(ContactsContract.Contacts._ID);
            long mCurrentId = mCursor.getLong(mIdIndex);

            mCursor.close();

            Uri mSelectedContactUri = ContactsContract.Contacts.getLookupUri(mCurrentId, mCurrentLookupKey);
            Intent editIntent = new Intent(Intent.ACTION_EDIT);
            editIntent.setDataAndType(mSelectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            ((Activity) context).startActivityForResult(editIntent, 111);
            return;
        }

        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }

        if (phonenumber.length() == 10) {
            phonenumber = "91" + phonenumber;
        }

        s = ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + phonenumber + "%'";
        Log.d("PrintDatabasequery", s);

        mCursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null, s, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            int mLookupKeyIndex = mCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
            String mCurrentLookupKey = mCursor.getString(mLookupKeyIndex);
            int mIdIndex = mCursor.getColumnIndex(ContactsContract.Contacts._ID);
            long mCurrentId = mCursor.getLong(mIdIndex);
            Log.d("PrintDatabasequery", mCurrentId + "");
            mCursor.close();

            Uri mSelectedContactUri = ContactsContract.Contacts.getLookupUri(mCurrentId, mCurrentLookupKey);
            Intent editIntent = new Intent(Intent.ACTION_EDIT);
            editIntent.setDataAndType(mSelectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            ((Activity) context).startActivityForResult(editIntent, 111);
            return;
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }

        phonenumber = "+" + phonenumber;
        s = ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + phonenumber + "%'";
        Log.d("PrintDatabasequery", s);

        mCursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                null, s, null, null);
        if (mCursor != null && mCursor.moveToFirst()) {
            int mLookupKeyIndex = mCursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY);
            String mCurrentLookupKey = mCursor.getString(mLookupKeyIndex);
            int mIdIndex = mCursor.getColumnIndex(ContactsContract.Contacts._ID);
            long mCurrentId = mCursor.getLong(mIdIndex);

            mCursor.close();

            Uri mSelectedContactUri = ContactsContract.Contacts.getLookupUri(mCurrentId, mCurrentLookupKey);
            Intent editIntent = new Intent(Intent.ACTION_EDIT);
            editIntent.setDataAndType(mSelectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
            ((Activity) context).startActivityForResult(editIntent, 111);
            return;
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
    }

    public static void deleteContact(String phonenumber, String name, Context context) {
        // First select raw contact id by given name and family name.
        phonenumber = phonenumber.replace("+", "").replace(" ", "");
        if (!phonenumber.startsWith("91")) {
            phonenumber = "91" + phonenumber;
        }

        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phonenumber));
        Cursor cur = context.getContentResolver().query(contactUri, null, null, null, null);
        try {
            if (cur.moveToFirst()) {
                do {
                    if (cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)).equalsIgnoreCase(name)) {
                        String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                        context.getContentResolver().delete(uri, null, null);
                        return;
                    }

                } while (cur.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openDialog(final Context context, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Yes", (arg0, arg1) -> {
            DeleteConfirmed deleteConfirmed = (DeleteConfirmed) context;
            deleteConfirmed.delete();
        });

        alertDialogBuilder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public interface DeleteConfirmed {
        void delete();
    }

    public interface VCardInterface {
        void created(File file);
    }

    private static Bitmap getContactImage(String file_name, Context context) {
        Bitmap thumbnail = null;
        if (file_name != null && !file_name.equalsIgnoreCase("")) {
            try {
                File filePath = context.getFileStreamPath(file_name);
                FileInputStream fis = new FileInputStream(filePath);
                thumbnail = BitmapFactory.decodeStream(fis);
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return thumbnail;
    }

    public static void sendEmail(Context context, String toEmail) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{toEmail});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        context.startActivity(Intent.createChooser(intent, "Select App"));
    }

    public static boolean checkSubscription() {
        String subscriptionStr = OozeePreferences.getInstance().getPref().getString(USERSUBSCRIPTION, "");
        if (subscriptionStr != null && subscriptionStr.equalsIgnoreCase("free")) {
            return false;
        }
        return true;
    }

    /*public static byte[] getContactImage(Uri url, Context context) {
        String file = getRealPathFromURI(url, context);
        Bitmap bm = BitmapFactory.decodeFile(file);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return b;
    }

    public static String getRealPathFromURI (Uri contentUri, Context context) {
        String path = null;
        String[] proj = { ContactsContract.CommonDataKinds.Phone.PHOTO_URI };
        Cursor cursor = context.getContentResolver().query(contentUri, proj,
                null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
            cursor.close();
        }
        return path;
    }*/

    public static String getRealPathFromURI(Uri contentUri, Context context) {
        String path = null;
        String[] proj = {ContactsContract.CommonDataKinds.Phone.PHOTO_URI};
        Cursor cursor = context.getContentResolver().query(contentUri, proj,
                null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
            cursor.close();
        }
        return path;
    }
}
