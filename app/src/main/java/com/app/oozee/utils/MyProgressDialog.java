package com.app.oozee.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class MyProgressDialog extends Dialog {

    @NonNull
    public static ACProgressFlower show(@NonNull Context context, CharSequence title, CharSequence message) {
        return show(context, title, message, false);
    }

    @NonNull
    public static ACProgressFlower show(@NonNull Context context, CharSequence title, CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    @NonNull
    public static ACProgressFlower show(@NonNull Context context, CharSequence title, CharSequence message, boolean indeterminate,
                                        boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    @NonNull
    public static ACProgressFlower show(@NonNull Context context, CharSequence title, CharSequence message, boolean indeterminate,
                                        boolean cancelable, OnCancelListener cancelListener) {
        /*MyProgressDialog dialog = new MyProgressDialog(context);
        dialog.setTitle(title);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);

        View view = View.inflate(context, R.layout.dialog_progressbar, null);
        TextView textView = view.findViewById(R.id.textView);
        textView.setText(message.toString());
        dialog.addContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));*/

        ACProgressFlower dialog = new ACProgressFlower.Builder(context)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();

        if (!((Activity) context).isDestroyed() && dialog.getWindow() != null) {
            dialog.show();
        }

        return dialog;
    }

    public MyProgressDialog(@NonNull Context context) {
        super(context, R.style.NewDialog);
    }
}