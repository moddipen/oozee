package com.app.oozee.utils;

import android.content.SharedPreferences;

import com.app.oozee.application.OozeeApplication;

public class OozeePreferences {
    private static final String PREFERENCE_NAME = "oozee";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static final String IS_FIRST_TIME = "isFirstTime";

    /* Singleton Class */
    private static OozeePreferences instance = new OozeePreferences();

    private OozeePreferences() {

    }

    public static OozeePreferences getInstance() {
        return instance;
    }

    public void init() {
        boolean isFirstTime = getIsFirstTime();
        if (isFirstTime) {
            setIsFirstTime(false);
        }
    }

    private boolean getIsFirstTime() {
        boolean value = true;
        if (getPref() != null && getPref().contains(IS_FIRST_TIME)) {
            value = getPref().getBoolean(IS_FIRST_TIME, true);
        }
        return value;
    }

    private void setIsFirstTime(boolean value) {
        getEditor().putBoolean(IS_FIRST_TIME, value);
        getEditor();
    }

    public SharedPreferences getPref() {
        if (pref == null) {
            int PRIVATE_MODE = 0;
            pref = OozeeApplication.getAppContext().getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        }
        return pref;
    }

    public SharedPreferences.Editor getEditor() {
        if (editor == null) {
            editor = getPref().edit();
        }
        return editor;
    }
}