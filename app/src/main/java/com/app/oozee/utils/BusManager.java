package com.app.oozee.utils;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class BusManager {
    private static final Bus fmBus = new Bus(ThreadEnforcer.MAIN);

    public static Bus getInstance() {
        return fmBus;
    }

    public BusManager(){}
}
