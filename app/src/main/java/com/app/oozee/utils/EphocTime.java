package com.app.oozee.utils;

import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class EphocTime {

    /*Singleton Class*/
    private static EphocTime instance = new EphocTime();

    private EphocTime(){}

    public static EphocTime getInstance(){
        return instance;
    }
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static long min;

    public static String getTimeAgoFeed(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }
       // long now = cal.getTimeInMillis();
        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return "";
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            min = diff / MINUTE_MILLIS;
            //return diff / MINUTE_MILLIS + " minutes ago";
            return diff / MINUTE_MILLIS + " mins ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hrs ago";
            //return "hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "1 day ago";
        } else {
            if (diff / DAY_MILLIS < 7) {
                return diff / DAY_MILLIS + " days ago";
            } else {
                return diff / DAY_MILLIS + " days ago";
                //return getTimeDateFeed(time);
            }
            //return "days ago";
        }
    }
}
