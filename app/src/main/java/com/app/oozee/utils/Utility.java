package com.app.oozee.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;

public class Utility {

    public static final String BACKUPDATA = "backupdata";
    public static final String COUNTRIESLOADED = "countriesloaded";
    public static final String DeviceType = "Android";
    public static final String USERNAME = "username";
    public static final String PHONENUMBER = "phonenumber";
    public static final String USERSUBSCRIPTION = "usersubscription";
    public static final String COUNTRYID = "countryid";
    public static final String[] Transactional = {"booked", "otp", "pin", "secure", "password",
            "credited", "debited", "blocked", "secure code", "tracking"};
    public static final int PERMISSION_ALL = 1;

    public static final String[] PERMISSIONS = {
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.WRITE_CONTACTS,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            //android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
            //Manifest.permission.SEND_SMS,
            //Manifest.permission.MODIFY_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG
    };

    public static final String[] PERMISSIONSSIGNUP = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ANSWER_PHONE_CALLS,
            Manifest.permission.MODIFY_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    public static final String onCall = "On Call";

    public static String getJsonObjectString(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return "";
            }
            if (jsonObject.get(key) != null) {
                if (jsonObject.get(key) != null && !jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsString();
                }
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static JsonObject getJsonObject(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return null;
            }
            if (jsonObject.get(key) != null) {
                if (jsonObject.get(key) != null && !jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsJsonObject();
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JsonArray getJsonArray(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return null;
            }
            if (jsonObject.get(key) != null) {
                if (jsonObject.get(key) != null && !jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsJsonArray();
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean getJsonObjectboolean(JsonObject jsonObject, String key) {
        try {
            if (jsonObject == null || !jsonObject.has(key)) {
                return false;
            }
            if (jsonObject.get(key) != null) {
                if (jsonObject.get(key) != null && !jsonObject.get(key).isJsonNull()) {
                    return jsonObject.get(key).getAsBoolean();
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static Typeface getTypeFace(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "segoeui.ttf");
    }

    public static Typeface getTypeFacesemibold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "seguisemibold.ttf");
    }

    public static Typeface getTypeFace1(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "segoeuibold.ttf");
    }

    public static final CircularProgressIndicator.ProgressTextAdapter textAdapter =
            new CircularProgressIndicator.ProgressTextAdapter() {
                @Override
                public String formatText(double progress) {
                    int p = (int) progress;
                    return p + "%";
                }
            };
}
