package com.app.oozee.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.activities.MainActivity;
import com.app.oozee.adapters.ContactsAdapter;
import com.app.oozee.baseclass.BaseFragment;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.services.VCardService;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.database.AllContactsModel.insertAllContacts;
import static com.app.oozee.utils.Utility.textAdapter;

public class ContactsFragment extends BaseFragment {

    private IndexFastScrollRecyclerView recyclerView;
    private DatabaseHelper databaseHelper;
    private CircularProgressIndicator circularProgressIndicator;
    private ArrayList<LocalContactsModel> statuses = new ArrayList<>();
    private ContactsAdapter contactsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contacts_fragment, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        circularProgressIndicator = view.findViewById(R.id.progress);
        circularProgressIndicator.setProgressTextAdapter(textAdapter);

        if (getActivity() != null) {
            databaseHelper = new DatabaseHelper(getActivity());
            if (((MainActivity) getActivity()).contactsModels == null ||
                    ((MainActivity) getActivity()).contactsModels.size() == 0) {
                boolean oozeeContactsToDB = OozeePreferences.getInstance().getPref().getBoolean("oozeeContactstodb", false);
                if (oozeeContactsToDB) {
                    allContactsGet();
                } else {
                    new getContacts().execute();
                }
            }
        }
        return view;
    }

   /* private void loadContacts() {
        if (getActivity() != null) {

            *//*for (int i = 0;i<((MainActivity) getActivity()).contactsModels.size();i++){
                if (((MainActivity) getActivity()).contactsModels.get(i).name == null || ((MainActivity) getActivity()).contactsModels.get(i).name == " "){
                    ((MainActivity) getActivity()).contactsModels.remove(i);
                }
            }*//*

            ArrayList<LocalContactsModel> marraylist = new ArrayList<>(((MainActivity) getActivity()).contactsModels);
            for (int i = 0;i<marraylist.size();i++){
                if (marraylist.get(i).name == null || marraylist.get(i).name == " " ||  marraylist.get(i).number == null || marraylist.get(i).number == " "){
                    marraylist.remove(i);
                }
            }
            contactsAdapter = new ContactsAdapter(marraylist, getActivity());
            contactsAdapter.setContacts(statuses);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(contactsAdapter);
            int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
            BlockedContactModel blockedContactModel = new BlockedContactModel(userId);
            getUsersStatuses(getActivity(), blockedContactModel, "userStatus");
        }
    }*/

   private void loadContacts() {
       if (getActivity() != null) {

           Iterator<LocalContactsModel> i = ((MainActivity) getActivity()).contactsModels.iterator();
           while (i.hasNext()) {
               LocalContactsModel localContactsModel = i.next();
               if (localContactsModel.name == null || localContactsModel.name.isEmpty() ||
                       localContactsModel.number == null || localContactsModel.number.isEmpty()) {
                   i.remove();
               }
           }

           contactsAdapter = new ContactsAdapter(((MainActivity) getActivity()).contactsModels, getActivity());
           contactsAdapter.setContacts(statuses);
           recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
           recyclerView.setIndexBarColor(R.color.darkGrayColor);
           recyclerView.setIndexBarTextColor(R.color.darkGrayColor2);
           recyclerView.setIndexBarCornerRadius(10);
           recyclerView.setIndexbarWidth(30);
           recyclerView.setIndexTextSize(8);
           recyclerView.setAdapter(contactsAdapter);
           int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
           BlockedContactModel blockedContactModel = new BlockedContactModel(userId);
           getUsersStatuses(getActivity(), blockedContactModel, "userStatus");
       }
   }

    @SuppressLint("StaticFieldLeak")
    private class getContacts extends AsyncTask<Void, Void, Void> {
        ArrayList<String> stringss = new ArrayList<>();
        ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
        boolean oozeeContacts;
        boolean oozeeContactsToDB;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (getActivity() != null) {
                Toast.makeText(getActivity(), "We are configuring oozee so it may take upto couple of minutes, please wait", Toast.LENGTH_LONG).show();
                oozeeContacts = OozeePreferences.getInstance().getPref().getBoolean("syncContacts", false);
                oozeeContactsToDB = OozeePreferences.getInstance().getPref().getBoolean("oozeeContactstodb", false);
                circularProgressIndicator.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Email.DATA,
                        ContactsContract.Data.CONTACT_ID, ContactsContract.Contacts._ID};
                if (getActivity() != null) {
                    // start from 0 and fetch 10 contacts
                /*Cursor people = getActivity().getContentResolver().query(uri, projection, null
                        , null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " limit 0, 10");*/
                    Cursor people = getActivity().getContentResolver().query(uri, projection, null
                            , null, null);
                    if (people != null) {
                        int totalcount = people.getCount();
                        int count = 0;

                        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        int rrawContactId = people.getColumnIndex(ContactsContract.Data.CONTACT_ID);

                        if (people.moveToFirst()) {
                            do {
                                try {
                                    count = count + 1;
                                    final int finalCount = count;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            int ct = (100 * finalCount) / totalcount;
                                            circularProgressIndicator.setProgress(ct, 100);
                                        }
                                    });

                                    String name = people.getString(indexName);
                                    String number = people.getString(indexNumber);

                                    String rawContactId = people.getString(rrawContactId);
                                    ArrayList<String> strings = getContactDetails(rawContactId);
                                    String email = "";
                                    String image = "";
                                    if (strings.size() > 0) {
                                        for (int i = 0; i < strings.size(); i++) {
                                            if (strings.get(i).contains("@")) {
                                                email = strings.get(i);
                                            } else {
                                                image = strings.get(i);
                                            }
                                        }
                                    }
                                    if (!stringss.contains(name)) {
                                        stringss.add(name);
                                        number = number.replaceAll(" ", "");
                                        number = number.replaceAll("-", "");
                                        contactsModels.add(new LocalContactsModel(name, email, image, number, rawContactId));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } while (people.moveToNext());
                        }

                        if (!oozeeContactsToDB) {
                            OozeePreferences.getInstance().getEditor().putBoolean("oozeeContactstodb", true).commit();
                            insertAllContacts(contactsModels, databaseHelper);
                        }
                        people.close();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                Log.d("contactsModels", contactsModels.size() + "");
                Collections.sort(contactsModels, new Comparator<LocalContactsModel>() {
                    @Override
                    public int compare(LocalContactsModel o1, LocalContactsModel o2) {
                        return o1.name.toLowerCase().compareTo(o2.name.toLowerCase());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).contactsModels.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (getActivity() != null) {
                    String initial = "";
                    for (int i = 0; i < contactsModels.size(); i++) {
                        String name = contactsModels.get(i).name;
                        String newInitial = getStringInitial(name);
                        if (!initial.equalsIgnoreCase(newInitial)) {
                            initial = newInitial;
                            ((MainActivity) getActivity()).contactsModels.add(new LocalContactsModel(0, name, newInitial, contactsModels.get(i).number,
                                    contactsModels.get(i).dir));
                        }
                        ((MainActivity) getActivity()).contactsModels.add(new LocalContactsModel(1, name, newInitial, contactsModels.get(i).number
                                , contactsModels.get(i).dir));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (getActivity() != null) {
                try {
                    circularProgressIndicator.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                    if (!oozeeContacts) {
                        Intent intent = new Intent(getActivity(), VCardService.class);
                        intent.putExtra("contactsModels", contactsModels);
                        getActivity().startService(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                loadContacts();
            }
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isVisible()) {
            if (getActivity() != null) {
                if (((MainActivity) getActivity()).contactsModels == null ||
                        ((MainActivity) getActivity()).contactsModels.size() == 0) {
                    boolean oozeeContactsToDB = OozeePreferences.getInstance().getPref().getBoolean("oozeeContactstodb", false);
                    if (oozeeContactsToDB) {
                        allContactsGet();
                    } else {
                        new getContacts().execute();
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }

    @Subscribe
    public void GetContacts(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("updateUi")) {
            if (statuses == null) {
                statuses = new ArrayList<>();
            }

            for (int i = 0; i < statuses.size(); i++) {
                if (statuses.get(i).number.contains(busModel.phoneNumber)) {
                    statuses.remove(i);
                    break;
                } else if (busModel.phoneNumber.contains(statuses.get(i).number)) {
                    statuses.remove(i);
                    break;
                }
            }

            statuses.add(new LocalContactsModel(busModel.message, busModel.phoneNumber));
            if (contactsAdapter != null) {
                contactsAdapter.setContacts(statuses);
                contactsAdapter.notifyDataSetChanged();
            }
        } else if (busModel.serviceName.equalsIgnoreCase("userStatus")) {
            ArrayList<LocalContactsModel> localContactsModels = LocalContactsModel.getLocalContacts(busModel.jsonElement);

            for (int i = 0; i < localContactsModels.size(); i++) {
                statuses.add(new LocalContactsModel(localContactsModels.get(i).name,
                        localContactsModels.get(i).number));
            }
            if (contactsAdapter != null) {
                contactsAdapter.setContacts(statuses);
                contactsAdapter.notifyDataSetChanged();
            }
        } else if (busModel.serviceName.equalsIgnoreCase("DeletedContacts")) {
            allContactsGet();
        }
    }

    public ArrayList<String> getContactDetails(String contactId) {
        ArrayList<String> strings = new ArrayList<>();
        if (getActivity() != null) {
            try (Cursor phoneCursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Phone.PHOTO_URI,},
                    ContactsContract.Data.CONTACT_ID + "=?",
                    new String[]{contactId}, null)) {
                if (phoneCursor != null) {
                    int idxAvatarUri = phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                    while (phoneCursor.moveToNext()) {
                        String avatarUri = phoneCursor.getString(idxAvatarUri);

                        if (avatarUri != null && !strings.contains(avatarUri)) {
                            strings.add(avatarUri);
                        }
                    }

                    if (!phoneCursor.isClosed()) {
                        phoneCursor.close();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try (Cursor emailCursor = getActivity().getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS,},
                    ContactsContract.Data.CONTACT_ID + "=?",
                    new String[]{String.valueOf(contactId)}, null)) {
                if (emailCursor != null) {
                    int idxAddress = emailCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Email.ADDRESS);
                    while (emailCursor.moveToNext()) {
                        String address = emailCursor.getString(idxAddress);
                        if (address != null && !strings.contains(address)) {
                            strings.add(address);
                        }
                    }
                    if (!emailCursor.isClosed()) {
                        emailCursor.close();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return strings;
    }

    private void allContactsGet() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        ArrayList<LocalContactsModel> contactsModels = getAllContacs(databaseHelper);

        Collections.sort(contactsModels, (o1, o2) -> o1.name.toLowerCase().compareTo(o2.name.toLowerCase()));
        if (getActivity() != null) {
            ((MainActivity) getActivity()).contactsModels.clear();
        }
        String initial = "";
        for (int i = 0; i < contactsModels.size(); i++) {
            String name = contactsModels.get(i).name;
            String newInitial = getStringInitial(name);
            if (!initial.equalsIgnoreCase(newInitial)) {
                initial = newInitial;
                ((MainActivity) getActivity()).contactsModels.add(new LocalContactsModel(0, name, newInitial, contactsModels.get(i).number,
                        contactsModels.get(i).dir));
            }
            ((MainActivity) getActivity()).contactsModels.add(new LocalContactsModel(1, name, newInitial, contactsModels.get(i).number
                    , contactsModels.get(i).dir));
        }

        boolean oozeeContacts = OozeePreferences.getInstance().getPref().getBoolean("syncContacts", false);
        if (!oozeeContacts) {
            Intent intent = new Intent(getActivity(), VCardService.class);
            intent.putExtra("contactsModels", contactsModels);
            getActivity().startService(intent);
        }

        loadContacts();
    }
}