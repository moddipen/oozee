package com.app.oozee.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.activities.MainActivity;
import com.app.oozee.adapters.SearchDialAdapter;
import com.app.oozee.baseclass.BaseFragment;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.OozeePreferences;

import java.util.ArrayList;
import java.util.Collections;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.utils.Utility.getTypeFace;

public class DialPadFragment extends BaseFragment {

    private EditText editText;
    private RecyclerView recyclerView;
    private ImageView addToContacts;
    private RelativeLayout relativeLayout;
    private ImageView deleteButton;
    private SearchDialAdapter searchDialAdapter;
    public ArrayList<LocalContactsModel> tempDialPadContacts = new ArrayList<>();
    private MediaPlayer mp;
    private boolean dialTone;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialpad_fragment, container, false);

        mp = MediaPlayer.create(getActivity(), R.raw.beep);

        editText = view.findViewById(R.id.editText);
        ImageView one = view.findViewById(R.id.one);
        ImageView two = view.findViewById(R.id.two);
        ImageView three = view.findViewById(R.id.three);
        ImageView four = view.findViewById(R.id.four);
        ImageView five = view.findViewById(R.id.five);
        ImageView six = view.findViewById(R.id.six);
        ImageView seven = view.findViewById(R.id.seven);
        ImageView eight = view.findViewById(R.id.eight);
        ImageView nine = view.findViewById(R.id.nine);
        ImageView star = view.findViewById(R.id.star);
        ImageView zero = view.findViewById(R.id.zero);
        ImageView hash = view.findViewById(R.id.hash);
        addToContacts = view.findViewById(R.id.addToContacts);
        ImageView callButton = view.findViewById(R.id.callButton);
        deleteButton = view.findViewById(R.id.deleteButton);
        relativeLayout = view.findViewById(R.id.relativeLayout);
        recyclerView = view.findViewById(R.id.recyclerView);

        searchDialAdapter = new SearchDialAdapter(getActivity(), tempDialPadContacts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(searchDialAdapter);

        Typeface typeface = getTypeFace(getActivity());
        editText.setTypeface(typeface);

        one.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "1");
        });

        two.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "2");
        });

        three.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "3");
        });

        four.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "4");
        });

        five.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "5");
        });

        six.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "6");
        });

        seven.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "7");
        });

        eight.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "8");
        });

        nine.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "9");
        });

        zero.setOnClickListener(v -> {
            startSound();
            editText.setText(editText.getText().toString().trim() + "0");
        });

        deleteButton.setOnClickListener(v -> {
            startSound();
            String edit = editText.getText().toString().trim();
            edit = edit.substring(0, edit.length() - 1);
            editText.setText(edit);
        });

        deleteButton.setOnLongClickListener(v -> {
            startSound();
            editText.setText("");
            return true;
        });

        editText.setInputType(InputType.TYPE_NULL);

        editText.addTextChangedListener(mTextWatcher);

        addToContacts.setVisibility(View.INVISIBLE);
        deleteButton.setVisibility(View.INVISIBLE);

        callButton.setOnClickListener(v -> {
            String phoneNumber = editText.getText().toString().trim();
            phoneNumber = phoneNumber.replace("#", "%23");

            if (!phoneNumber.equalsIgnoreCase("") && phoneNumber.length() > 2) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + phoneNumber));
                startActivity(intent);
            } else {
                new getCalls().execute();
            }
        });

        addToContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editText.getText().toString().trim();
                if (!phoneNumber.equalsIgnoreCase("")) {
                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    contactIntent.putExtra(ContactsContract.Intents.Insert.NAME, "")
                            .putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);

                    getActivity().startActivityForResult(contactIntent, 1);
                } else {
                    Toast.makeText(getActivity(), "Please enter valid number", Toast.LENGTH_SHORT).show();
                }
            }
        });

        star.setOnClickListener(v -> editText.setText(editText.getText().toString().trim() + "*"));

        hash.setOnClickListener(v -> editText.setText(editText.getText().toString().trim() + "#"));

        zero.setOnLongClickListener(v -> {
            editText.setText(editText.getText().toString().trim() + "+");
            return true;
        });

        return view;
    }

    private void allContactsGet() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        ArrayList<LocalContactsModel> contactsModels = getAllContacs(databaseHelper);

        Collections.sort(contactsModels, (o1, o2) -> o1.name.compareTo(o2.name));
        if (getActivity() != null) {
            ((MainActivity) getActivity()).dialPadContacts.clear();
        }
        String initial = "";
        for (int i = 0; i < contactsModels.size(); i++) {
            String name = contactsModels.get(i).name;
            String newInitial = getStringInitial(name);
            if (!initial.equalsIgnoreCase(newInitial)) {
                initial = newInitial;
            }
            ((MainActivity) getActivity()).dialPadContacts.add(new LocalContactsModel(1, name, newInitial, contactsModels.get(i).number
                    , contactsModels.get(i).dir));
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isVisible()) {
            if (getActivity() != null) {
                if (((MainActivity) getActivity()).dialPadContacts == null ||
                        ((MainActivity) getActivity()).dialPadContacts.size() == 0) {
                    allContactsGet();
                }
            }
            dialTone = OozeePreferences.getInstance().getPref().getBoolean("dialTone", false);
        }
    }

    private void setRecyclerAdapter(String number) {
        if (number.length() < 3) {
            return;
        }

        tempDialPadContacts.clear();
        for (int i = 0; i < ((MainActivity) getActivity()).dialPadContacts.size(); i++) {
            if (((MainActivity) getActivity()).dialPadContacts.get(i).number.contains(number)) {
                tempDialPadContacts.add(((MainActivity) getActivity()).dialPadContacts.get(i));
            }
        }

        for (int i = 0; i < ((MainActivity) getActivity()).dialPadContacts.size(); i++) {
            if (((MainActivity) getActivity()).dialPadContacts.get(i).number.equalsIgnoreCase(number)) {
                addToContacts.setVisibility(View.INVISIBLE);
                break;
            }
        }

        if (searchDialAdapter != null) {
            searchDialAdapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getCalls extends AsyncTask<Void, Void, Void> {
        ArrayList<LocalContactsModel> stringss = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (getActivity() != null) {
                Cursor managedCursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI,
                        null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");
                if (managedCursor != null) {
                    int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                    int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                    int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                    int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                    //int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
                    while (managedCursor.moveToNext()) {
                        String phNumber = managedCursor.getString(number);
                        String nameStr = managedCursor.getString(name);
                        String callType = managedCursor.getString(type);
                        String callDate = managedCursor.getString(date);
                        //Date callDayTime = new Date(Long.valueOf(callDate));
                        //String callDuration = managedCursor.getString(duration);
                        String dir = "";
                        int dircode = Integer.parseInt(callType);

                        switch (dircode) {
                            case CallLog.Calls.OUTGOING_TYPE:
                                dir = "OUTGOING";
                                break;

                            case CallLog.Calls.INCOMING_TYPE:
                                dir = "INCOMING";
                                break;

                            case CallLog.Calls.MISSED_TYPE:
                                dir = "MISSED";
                                break;
                        }

                        if (nameStr != null) {
                            stringss.add(new LocalContactsModel(nameStr, callDate, dir, phNumber));
                        } else {
                            stringss.add(new LocalContactsModel(phNumber, callDate, dir, phNumber));
                        }
                    }
                    managedCursor.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (stringss.size() > 0) {
                editText.setText(stringss.get(0).number);
            }
        }
    }

    private void startSound() {
        if (!dialTone) {
            return;
        }
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.start();
        }
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0) {
                tempDialPadContacts.clear();

                addToContacts.setVisibility(View.INVISIBLE);
                deleteButton.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.grayColor));

                editText.setText(" ");
            } else {
                if (!editText.getText().toString().trim().equalsIgnoreCase("")) {
                    addToContacts.setVisibility(View.VISIBLE);
                    deleteButton.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.whiteColor));
                }
            }

            setRecyclerAdapter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
