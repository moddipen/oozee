package com.app.oozee.fragments;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.oozee.R;
import com.app.oozee.activities.MainActivity;
import com.app.oozee.adapters.RecentAdapter;
import com.app.oozee.adapters.RecentTopAdapter;
import com.app.oozee.baseclass.BaseFragment;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.MyProgressDialog;
import com.app.oozee.utils.OozeePreferences;
import com.squareup.otto.Subscribe;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.Calendar;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.quicklist;
import static com.app.oozee.database.AllContactsModel.getNameFromNumber;
import static com.app.oozee.fragments.MessagesFragment.getAddress;
import static com.app.oozee.utils.Config.getDateFromStamp;
import static com.app.oozee.utils.Config.getDateFromStampMMM;

public class RecentFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private RecyclerView horizontalRecyclerView;
    private ACProgressFlower myProgressDialog;
    private DatabaseHelper databaseHelper;
    private ArrayList<LocalContactsModel> statuses = new ArrayList<>();
    private RecentAdapter recentAdapter;
    private RecentTopAdapter recentTopAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recent_fragment, container, false);
        horizontalRecyclerView = view.findViewById(R.id.horizontalRecyclerView);
        recyclerView = view.findViewById(R.id.recyclerView);
        databaseHelper = new DatabaseHelper(getActivity());
        return view;
    }

    private void loadCallLogs() {
        if (getActivity() != null) {
            //Collections.reverse(((MainActivity) getActivity()).allCallLogs);
            recentAdapter = new RecentAdapter(((MainActivity) getActivity()).allCallLogs, getActivity(),
                    databaseHelper);
            recentAdapter.setContacts(statuses);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(recentAdapter);
        }
    }

    private void loadContacts() {
        if (getActivity() != null) {
            recentTopAdapter = new RecentTopAdapter(((MainActivity) getActivity()).recentModels, getActivity());
            recentTopAdapter.setContacts(statuses);
            horizontalRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            horizontalRecyclerView.setItemAnimator(null);
            horizontalRecyclerView.setAdapter(recentTopAdapter);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getCalls extends AsyncTask<Void, Void, Void> {
        ArrayList<LocalContactsModel> stringss = new ArrayList<>();
        ArrayList<LocalContactsModel> finalstringss = new ArrayList<>();
        ArrayList<String> numberss = new ArrayList<>();
        String initialsss = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (getActivity() != null) {
                //myProgressDialog = MyProgressDialog.show(getActivity(), "", "");
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (getActivity() != null) {
                /*android.provider.CallLog.Calls.DATE + " DESC limit 200;"*/
                Cursor managedCursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI,
                        null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 150;");
                //null, null, null, android.provider.CallLog.Calls.DATE + " DESC");
                if (managedCursor != null) {
                    int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
                    int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
                    int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                    int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
                    //int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
                    while (managedCursor.moveToNext()) {
                        String phNumber = managedCursor.getString(number);
                        String nameStr = managedCursor.getString(name);
                        String callType = managedCursor.getString(type);
                        String callDate = managedCursor.getString(date);
                        //Date callDayTime = new Date(Long.valueOf(callDate));
                        //String callDuration = managedCursor.getString(duration);
                        String dir = "";
                        int dircode = Integer.parseInt(callType);

                        switch (dircode) {
                            case CallLog.Calls.OUTGOING_TYPE:
                                dir = "OUTGOING";
                                break;

                            case CallLog.Calls.INCOMING_TYPE:
                                dir = "INCOMING";
                                break;

                            case CallLog.Calls.MISSED_TYPE:
                                dir = "MISSED";
                                break;
                        }

                        String pnum = phNumber;
                        pnum = getAddress(pnum);

                        String newDate = getDateFromStamp(callDate);

                        if (initialsss.equalsIgnoreCase(newDate)) {
                            if (!numberss.contains(pnum)) {
                                numberss.add(pnum);

                                if (nameStr != null) {
                                    stringss.add(new LocalContactsModel(nameStr, callDate, dir, phNumber));
                                } else {
                                    String names = getNameFromNumber(databaseHelper, phNumber);
                                    if (names.equalsIgnoreCase("")) {
                                        stringss.add(new LocalContactsModel(phNumber, callDate, dir, phNumber));
                                    } else {
                                        stringss.add(new LocalContactsModel(names, callDate, dir, phNumber));
                                    }
                                }
                            } else {
                                int count = stringss.get(numberss.indexOf(pnum)).count;
                                if (count == 0) {
                                    count = 1;
                                }
                                stringss.get(numberss.indexOf(pnum)).count = count + 1;
                            }
                        } else {
                            finalstringss.addAll(stringss);
                            stringss.clear();
                            numberss.clear();

                            initialsss = newDate;
                            if (!numberss.contains(pnum)) {
                                numberss.add(pnum);

                                if (nameStr != null) {
                                    stringss.add(new LocalContactsModel(nameStr, callDate, dir, phNumber));
                                } else {
                                    String names = getNameFromNumber(databaseHelper, phNumber);
                                    if (names.equalsIgnoreCase("")) {
                                        stringss.add(new LocalContactsModel(phNumber, callDate, dir, phNumber));
                                    } else {
                                        stringss.add(new LocalContactsModel(names, callDate, dir, phNumber));
                                    }
                                }
                            } else {
                                int count = stringss.get(numberss.indexOf(pnum)).count;
                                if (count == 0) {
                                    count = 1;
                                }
                                stringss.get(numberss.indexOf(pnum)).count = count + 1;
                            }
                        }
                    }
                    managedCursor.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getActivity() != null) {
                ((MainActivity) getActivity()).allCallLogs.clear();
                String initial = "";
                ArrayList<String> arrayList = new ArrayList<>();
                final ArrayList<LocalContactsModel> strings = finalstringss;
                for (int i = 0; i < strings.size(); i++) {
                    String name = strings.get(i).name;
                    String newDate = getDateFromStamp(strings.get(i).date);
                    if (!newDate.equalsIgnoreCase(initial)) {
                        initial = newDate;
                        String text;
                        if (arrayList.size() == 0) {
                            if (getDateFromStamp(strings.get(i).date).equalsIgnoreCase(getDateFromStamp(String.valueOf(Calendar.getInstance().getTimeInMillis())))) {
                                text = "TODAY";
                            } else {
                                text = getDateFromStampMMM(strings.get(i).date);
                            }
                        } else if (arrayList.size() == 1) {
                            Calendar c = Calendar.getInstance();
                            String ss = getDateFromStamp(strings.get(i).date);
                            String ss1 = getDateFromStamp(String.valueOf(c.getTimeInMillis() - (1000 * 60 * 60 * 24)));
                            if (ss.equalsIgnoreCase(ss1)) {
                                text = "YESTERDAY";
                            } else {
                                text = getDateFromStampMMM(strings.get(i).date);
                            }
                        } else {
                            text = getDateFromStampMMM(strings.get(i).date);
                        }
                        arrayList.add(initial);
                        ((MainActivity) getActivity()).allCallLogs.add(new LocalContactsModel(0, text, text));
                    }

                    ((MainActivity) getActivity()).allCallLogs.add(new LocalContactsModel(1, name, getStringInitial(name), strings.get(i).date,
                            strings.get(i).dir, strings.get(i).number, "", strings.get(i).count));
                }
                loadCallLogs();
                //myProgressDialog.dismiss();

                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                BlockedContactModel blockedContactModel = new BlockedContactModel(userId);
                getUsersStatuses(getActivity(), blockedContactModel, "userStatus");
            }
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (menuVisible && isVisible() && getActivity() != null) {
            if (((MainActivity) getActivity()).allCallLogs == null ||
                    ((MainActivity) getActivity()).allCallLogs.size() == 0) {
                new getCalls().execute();
            } else {

                boolean refresh = OozeePreferences.getInstance().getPref().getBoolean("isRefreshed", true);

                if (refresh) {
                    OozeePreferences.getInstance().getEditor().putBoolean("isRefreshed", false).apply();
                    new getCalls().execute();
                } else {
                    getContactsByType(getActivity(), quicklist.ordinal(), "quickListRecent");
                }
            }
        }
    }

    @Subscribe
    public void GetResponse(BusModel busModel) {
        if (busModel.serviceName.equalsIgnoreCase("quickListRecent")) {
            ArrayList<BlockedContactModel> blockedContactModels = BlockedContactModel.getQuickList(busModel.jsonElement);
            ((MainActivity) getActivity()).recentModels.clear();
            for (int i = 0; i < blockedContactModels.size(); i++) {
                if (getActivity() != null) {

                    //String image = getImageFromNumber(databaseHelper, blockedContactModels.get(i).phone_number);
                    String image = "";
                    blockedContactModels.get(i).first_name = getNameFromNumber(databaseHelper, blockedContactModels.get(i).phone_number);
                    ((MainActivity) getActivity()).recentModels.add(new LocalContactsModel(blockedContactModels.get(i).id,
                            0, blockedContactModels.get(i).first_name /*+
                            " " + blockedContactModels.get(i).last_name*/,
                            getStringInitial(blockedContactModels.get(i).first_name),
                            blockedContactModels.get(i).phone_number,
                            image, blockedContactModels.get(i).gender));
                }
            }
            loadContacts();
        } else if (busModel.serviceName.equalsIgnoreCase("removeQuickList")) {
            deleteContactsByType(getActivity(), busModel.contactsModel.id,
                    quicklist.ordinal(), "deleted");
        } else if (busModel.serviceName.equalsIgnoreCase("updateUi")) {
            if (statuses == null) {
                statuses = new ArrayList<>();
            }

            for (int i = 0; i < statuses.size(); i++) {
                if (statuses.get(i).number.contains(busModel.phoneNumber)) {
                    statuses.remove(i);
                    break;
                } else if (busModel.phoneNumber.contains(statuses.get(i).number)) {
                    statuses.remove(i);
                    break;
                }
            }

            statuses.add(new LocalContactsModel(busModel.message, busModel.phoneNumber));
            if (recentAdapter != null) {
                recentAdapter.setContacts(statuses);
                recentAdapter.notifyDataSetChanged();
            }
            if (recentTopAdapter != null) {
                recentTopAdapter.setContacts(statuses);
                recentTopAdapter.notifyDataSetChanged();
            }
        } else if (busModel.serviceName.equalsIgnoreCase("userStatus")) {
            ArrayList<LocalContactsModel> localContactsModels = LocalContactsModel.getLocalContacts(busModel.jsonElement);

            for (int i = 0; i < localContactsModels.size(); i++) {
                statuses.add(new LocalContactsModel(localContactsModels.get(i).name,
                        localContactsModels.get(i).number));
            }
            if (recentAdapter != null) {
                recentAdapter.setContacts(statuses);
                recentAdapter.notifyDataSetChanged();
            }

            if (recentTopAdapter != null) {
                recentTopAdapter.setContacts(statuses);
                recentTopAdapter.notifyDataSetChanged();
            }
            getContactsByType(getActivity(), quicklist.ordinal(), "quickListRecent");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusManager.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusManager.getInstance().unregister(this);
    }
}