package com.app.oozee.fragments;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.activities.MainActivity;
import com.app.oozee.adapters.MessagesAdapter;
import com.app.oozee.baseclass.BaseFragment;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.Sms;
import com.app.oozee.utils.MyProgressDialog;
import com.app.oozee.utils.OozeePreferences;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressFlower;

import static com.app.oozee.database.AllContactsModel.getMultipleImagesNumbers;
import static com.app.oozee.database.MessagesDb.getAllSms;
import static com.app.oozee.database.MessagesDb.getStarredMessages;
import static com.app.oozee.database.MessagesDb.insertAllmessages;
import static com.app.oozee.utils.Utility.Transactional;
import static com.app.oozee.utils.Utility.getTypeFace;

public class MessagesFragment extends BaseFragment {

    private List<Sms> tempcontactsModels1 = new ArrayList<>();
    private RecyclerView recyclerView;
    private MessagesAdapter messagesAdapter;
    private ACProgressFlower myProgressDialog;
    private LinearLayout linearone;
    private ImageView personalImage;
    private ImageView transactionalImage;
    private ImageView promotionalImage;
    private ImageView starredImage;
    private int whichOption = 1;
    private DatabaseHelper databaseHelper;
    private ArrayList<LocalContactsModel> localContactsModelsImagesNames = new ArrayList<>();
    private boolean optionone , optiontwo , optionthree , optionfour ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.messages_fragment, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        SwitchButton unreadSwitch = view.findViewById(R.id.unreadSwitch);



        databaseHelper = new DatabaseHelper(getActivity());

        unreadSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            tempcontactsModels1.clear();
            if (whichOption == 1) {
                calculate(1, isChecked);
            } else if (whichOption == 2) {
                calculate(2, isChecked);
            } else if (whichOption == 3) {
                calculate(3, isChecked);
            } else {
                calculate(4, isChecked);
            }
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        });


        TextView unreadTextView = view.findViewById(R.id.unreadTextView);
        TextView one = view.findViewById(R.id.one);
        TextView two = view.findViewById(R.id.two);
        TextView three = view.findViewById(R.id.three);
        TextView four = view.findViewById(R.id.four);
        linearone = view.findViewById(R.id.linearone);
        LinearLayout lineartwo = view.findViewById(R.id.lineartwo);
        LinearLayout linearthree = view.findViewById(R.id.linearthree);
        LinearLayout linearfour = view.findViewById(R.id.linearfour);

        personalImage = view.findViewById(R.id.personalImage);
        transactionalImage = view.findViewById(R.id.transactionalImage);
        promotionalImage = view.findViewById(R.id.promotionalImage);
        starredImage = view.findViewById(R.id.starredImage);

        if (getActivity() != null) {
            Typeface typeface = getTypeFace(getActivity());
            unreadTextView.setTypeface(typeface);
            one.setTypeface(typeface);
            two.setTypeface(typeface);
            three.setTypeface(typeface);
            four.setTypeface(typeface);
        }

        linearone.setOnClickListener(v -> {
            whichOption = 1;
            personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personalselected));
            promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselected));
            transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselected));
            starredImage.setImageDrawable(getResources().getDrawable(R.mipmap.starreddeselected));

            if (optiontwo){
                transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselectednotification));
            }
            if (optionthree){
                promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselectednotification));
            }
            calculate(1, unreadSwitch.isChecked());
            messagesAdapter.setDate(localContactsModelsImagesNames);
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        });

        lineartwo.setOnClickListener(v -> {
            whichOption = 2;
            personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personaldeselected));
            promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselected));
            transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactionselected));
            starredImage.setImageDrawable(getResources().getDrawable(R.mipmap.starreddeselected));

            if (optionone){
                personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personaldeselectednotification));
            }

            if (optionthree){
                promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselectednotification));
            }

            calculate(2, unreadSwitch.isChecked());
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        });

        linearthree.setOnClickListener(v -> {

            whichOption = 3;
            personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personaldeselected));
            promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotionselected));
            transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselected));
            starredImage.setImageDrawable(getResources().getDrawable(R.mipmap.starreddeselected));

            if (optiontwo){
                transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselectednotification));
            }

            if (optionone){
                personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personaldeselectednotification));
            }

            calculate(3, unreadSwitch.isChecked());
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        });

        linearfour.setOnClickListener(v -> {

            whichOption = 4;
            personalImage.setImageDrawable(getResources().getDrawable(R.mipmap.personaldeselected));
            promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselected));
            transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselected));
            starredImage.setImageDrawable(getResources().getDrawable(R.mipmap.starredselected));

            if (optiontwo){
                transactionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.transactiondeselectednotification));
            }

            if (optionthree){
                promotionalImage.setImageDrawable(getResources().getDrawable(R.mipmap.promotiondeselectednotification));
            }

            calculate(4, unreadSwitch.isChecked());
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        });

        return view;
    }

    private void loadSms(int i) {
        messagesAdapter = new MessagesAdapter(tempcontactsModels1, getActivity(), whichOption, localContactsModelsImagesNames);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(messagesAdapter);

        if (i == 1) {
            linearone.performClick();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class getSms extends AsyncTask<Void, Void, Void> {
        ArrayList<Sms> lstSms = new ArrayList<>();
        ArrayList<String> address = new ArrayList<>();
        long timeStamp = OozeePreferences.getInstance().getPref().getLong("timeStamp", 0);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgressDialog = MyProgressDialog.show(getActivity(), "", "");
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Uri message = Uri.parse("content://sms/");
            ContentResolver cr = getActivity().getContentResolver();
            Cursor c;
            if (timeStamp != 0) {
                c = cr.query(message, null, "date>=" + timeStamp, null
                        , "date ASC");
            } else {
                c = cr.query(message, null, null, null, null);
            }

            int totalSMS;
            if (c != null) {
                totalSMS = c.getCount();
                if (c.moveToFirst()) {
                    for (int i = 0; i < totalSMS; i++) {
                        Sms objSms = new Sms();
                        String addr = c.getString(c.getColumnIndexOrThrow("address"));
                        objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                        objSms.setAddress(addr);
                        objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                        objSms.setReadState(c.getString(c.getColumnIndex("read")));
                        objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                        if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                            objSms.setFolderName("Inbox");
                        } else {
                            objSms.setFolderName("Sent");
                        }

                        if (!address.contains(addr)) {
                            lstSms.add(objSms);

                            addr = getAddress(addr);
                            address.add(addr);
                            lstSms.get(lstSms.size() - 1).sms.add(objSms);
                        } else {
                            addr = getAddress(addr);

                            int pos = address.indexOf(addr);
                            lstSms.get(pos).sms.add(objSms);
                        }
                        c.moveToNext();
                    }
                }
                OozeePreferences.getInstance().getEditor().putLong("timeStamp", Calendar.getInstance().getTimeInMillis()).apply();
                insertAllmessages(databaseHelper, lstSms);
                c.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (timeStamp != 0) {
                List<Sms> lstSms1 = getAllSms(databaseHelper);
                lstSms.addAll(lstSms1);
            }

            Collections.sort(lstSms, (o1, o2) -> Long.compare(Long.parseLong(o1.getTime()), Long.parseLong(o2.getTime())));

            Collections.reverse(lstSms);
            ((MainActivity) getActivity()).messages = lstSms;
            loadSms(1);
            myProgressDialog.dismiss();
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isVisible()) {
/*            if (((MainActivity) getActivity()).messages == null ||
                    ((MainActivity) getActivity()).messages.size() == 0) {*/
            new getSms().execute();
            //          }
        }
    }

    private void calculate(int whichOption, boolean isNotRead) {
        if (whichOption == 1) {
            tempcontactsModels1.clear();

            ArrayList<String> phoneNumbers = new ArrayList<>();
            for (int i = 0; i < ((MainActivity) getActivity()).messages.size(); i++) {
                String address = (((MainActivity) getActivity()).messages).get(i).getAddress();
                if (address != null && address.length() >= 10) {
                    address = getAddress(address);
                    if (!phoneNumbers.contains(address)) {
                        phoneNumbers.add(address);
                    }
                }
            }

            localContactsModelsImagesNames = getMultipleImagesNumbers(databaseHelper, phoneNumbers);
            phoneNumbers.clear();
            for (int i = 0; i < ((MainActivity) getActivity()).messages.size(); i++) {
                String address = (((MainActivity) getActivity()).messages).get(i).getAddress();

                address = getAddress(address);
                if (address != null && !phoneNumbers.contains(address)) {
                    phoneNumbers.add(address);
                    if (isNotRead) {
                        if (address.length() >= 10 && ((MainActivity) getActivity()).messages.get(i)
                                .getReadState().equalsIgnoreCase("0")) {
                            Sms sms = (((MainActivity) getActivity()).messages.get(i));
                            tempcontactsModels1.add(sms);
                            optionone = true;
                        }
                    } else {
                        if (address.length() >= 10) {
                            Sms sms = (((MainActivity) getActivity()).messages.get(i));
                            tempcontactsModels1.add(sms);
                        }
                    }
                }
            }
        } else if (whichOption == 2) {
            tempcontactsModels1.clear();
            for (int i = 0; i < ((MainActivity) getActivity()).messages.size(); i++) {
                String address = (((MainActivity) getActivity()).messages).get(i).getMsg();
                boolean istrue = false;
                for (String str : Transactional) {
                    if (address.toLowerCase().contains(str)) {
                        istrue = true;
                        break;
                    }
                }

                if (isNotRead) {
                    if (istrue && (((MainActivity) getActivity()).messages).get(i)
                            .getReadState().equalsIgnoreCase("0")) {
                        Sms sms = (((MainActivity) getActivity()).messages.get(i));
                        tempcontactsModels1.add(sms);
                        optiontwo = true;
                    }
                } else {
                    if (istrue) {
                        Sms sms = (((MainActivity) getActivity()).messages.get(i));
                        tempcontactsModels1.add(sms);
                    }
                }

            }
        } else if (whichOption == 3) {
            tempcontactsModels1.clear();
            for (int i = 0; i < ((MainActivity) getActivity()).messages.size(); i++) {
                String address = (((MainActivity) getActivity()).messages).get(i).getAddress();

                String msg = (((MainActivity) getActivity()).messages).get(i).getMsg();
                boolean istrue = false;
                for (String str : Transactional) {
                    if (msg.toLowerCase().contains(str)) {
                        istrue = true;
                        break;
                    }
                }

                if (isNotRead) {
                    if (!istrue && address != null && address.length() < 10 && (((MainActivity) getActivity()).messages).get(i)
                            .getReadState().equalsIgnoreCase("0")) {
                        Sms sms = (((MainActivity) getActivity()).messages.get(i));
                        tempcontactsModels1.add(sms);
                        optionthree = true;
                    }
                } else {
                    if (!istrue && address != null && address.length() < 10) {
                        Sms sms = (((MainActivity) getActivity()).messages.get(i));
                        tempcontactsModels1.add(sms);
                    }
                }
            }
        } else {
            tempcontactsModels1.clear();
            tempcontactsModels1.addAll(getStarredMessages(new DatabaseHelper(getActivity())));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (messagesAdapter != null) {
            loadSms(0);
            messagesAdapter.setWhichOption(whichOption);
        }

        boolean isSMSSent = OozeePreferences.getInstance().getPref().getBoolean("isSMSSent", false);
        if (isSMSSent) {
            OozeePreferences.getInstance().getEditor().putBoolean("isSMSSent", false).apply();
            new getSms().execute();
        }
    }

    public static String getAddress(String address) {
        if (address != null && address.startsWith("+91") && address.length() > 10) {
            address = address.replace("+91", "");
        }
        if (address != null && address.startsWith("91") && address.length() > 10) {
            address = address.replace("91", "");
        }
        return address;
    }
}
