package com.app.oozee.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.AddReminderScreen;
import com.app.oozee.adapters.ReminderAdapter;
import com.app.oozee.baseclass.BaseFragment;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.RemindersModel;
import com.app.oozee.utils.Utility;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.app.oozee.database.RemindersModel.getAllReminders;

public class ReminderFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private TextView noRemindersTextView;
    private DatabaseHelper databaseHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reminders, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        noRemindersTextView = view.findViewById(R.id.noRemindersTextView);
        TextView addReminder = view.findViewById(R.id.addReminder);
        TextView upcoming  = view.findViewById(R.id.upcoming);

        Typeface typeface1 = Utility.getTypeFace1(getContext());
        Typeface typeface = Utility.getTypeFace(getContext());
        addReminder.setTypeface(typeface1);
        upcoming.setTypeface(typeface);

        addReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddReminderScreen.class);
                startActivity(intent);
            }
        });

        databaseHelper = new DatabaseHelper(getActivity());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    public void refreshData(){
        ArrayList<RemindersModel> remindersModels = getAllReminders(databaseHelper);

        if (remindersModels.size() > 0){
            noRemindersTextView.setVisibility(View.GONE);
        }else{
            noRemindersTextView.setVisibility(View.VISIBLE);
        }

        ReminderAdapter reminderAdapter = new ReminderAdapter(remindersModels, getActivity(), databaseHelper);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(reminderAdapter);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (menuVisible && isVisible()){
            refreshData();
        }
    }
}
