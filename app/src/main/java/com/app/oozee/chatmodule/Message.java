package com.app.oozee.chatmodule;

import com.app.oozee.utils.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Message {


    private int mType;
    private String mUsername;
    private String mMessage;
    private String dateTime;
    private int id;

    public int user_id;
    public int message_id;
    /*: '2019-06-08 11:16:10'*/

    public Message(int type, String username, String message, String dateTime) {
        this.mType = type;
        this.mMessage = message;
        this.mUsername = username;
        this.dateTime = dateTime;
    }

    public int getmType() {
        return mType;
    }

    public String getmMessage() {
        return mMessage;
    }

    public String getmUsername() {
        return mUsername;
    }





    public String getDateTime() {
        return dateTime;
    }
}
