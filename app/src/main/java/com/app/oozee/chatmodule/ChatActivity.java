package com.app.oozee.chatmodule;

import android.os.Bundle;

import com.app.oozee.R;
import com.app.oozee.baseclass.BaseActivity;

public class ChatActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
    }
}