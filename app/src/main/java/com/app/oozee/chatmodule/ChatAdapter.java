package com.app.oozee.chatmodule;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<Message> mMessageList;
    private Context context;
    private MediaPlayer mp;

    ChatAdapter(List<Message> messageList, Context context) {
        this.mMessageList = messageList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            /*case Message.TYPE_MESSAGE_RECEIVED:
                layout = R.layout.message_item_received;
                break;
            case Message.TYPE_LOG:
                layout = R.layout.log_item;
                break;
            case Message.TYPE_MESSAGE_SENT:
                layout = R.layout.message_tem_sent;
                break;*/
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatAdapter.MyViewHolder holder, int position) {
        final Message currentMessage = mMessageList.get(position);
        if (currentMessage.getmUsername() != null) {
            if (position != 0 && currentMessage.getmUsername().equals(mMessageList.get(position - 1).getmUsername())) {
                LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                textParams.setMargins(0, 0, 0, 0);
                holder.mView.setLayoutParams(textParams);
                if (holder.mUsername != null) {
                    holder.mUsername.setVisibility(View.GONE);
                }
            } else {
                LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                textParams.setMargins(0, 0, 0, 0);
                holder.mView.setLayoutParams(textParams);
                holder.setmUsername(currentMessage.getmUsername());
            }
        }

        if (currentMessage.getmMessage().contains(".3gp")) {
            holder.audioRelative.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);
            holder.relativeLayout.setVisibility(View.GONE);
        } else if (currentMessage.getmMessage().contains(".jpg") ||
                currentMessage.getmMessage().contains(".png")) {
            holder.imageView.setVisibility(View.VISIBLE);
            holder.relativeLayout.setVisibility(View.GONE);
            holder.audioRelative.setVisibility(View.GONE);
            Glide.with(context).load(currentMessage.getmMessage()).into(holder.imageView);
        } else {
            holder.relativeLayout.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);
            holder.audioRelative.setVisibility(View.GONE);
            holder.setmMessage(currentMessage.getmMessage());
        }

        holder.audioRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.pauseButton.getVisibility() == View.VISIBLE) {
                    holder.pauseButton.setVisibility(View.GONE);
                    holder.playButton.setVisibility(View.VISIBLE);

                    if (mp != null && mp.isPlaying()) {
                        mp.release();
                    }
                } else {
                    holder.pauseButton.setVisibility(View.VISIBLE);
                    holder.playButton.setVisibility(View.GONE);

                    mp = new MediaPlayer();
                    try {
                        mp.setDataSource(currentMessage.getmMessage());
                        mp.prepare();
                        mp.start();

                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                                holder.pauseButton.setVisibility(View.GONE);
                                holder.playButton.setVisibility(View.VISIBLE);
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        holder.setTime(currentMessage.getDateTime());
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessageList.get(position).getmType();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mMessage, mUsername, time;
        View mView;
        ImageView imageView;
        ImageView pauseButton;
        ImageView tickImageView;
        ImageView playButton;
        RelativeLayout relativeLayout;
        RelativeLayout audioRelative;

        MyViewHolder(View itemView) {
            super(itemView);
            mMessage = itemView.findViewById(R.id.message);
            mUsername = itemView.findViewById(R.id.username);
            time = itemView.findViewById(R.id.time);
            imageView = itemView.findViewById(R.id.imageView);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            audioRelative = itemView.findViewById(R.id.audioRelative);
            playButton = itemView.findViewById(R.id.playButton);
            pauseButton = itemView.findViewById(R.id.pauseButton);
            tickImageView = itemView.findViewById(R.id.tickImageView);
            mView = itemView;
        }

        void setmUsername(String username) {
            if (mUsername == null)
                return;
            mUsername.setText(username);
        }

        void setmMessage(String message) {
            if (mMessage == null)
                return;
            mMessage.setText(message);
        }

        void setTime(String message) {
            if (time == null)
                return;
            time.setText(message);
        }
    }
}
