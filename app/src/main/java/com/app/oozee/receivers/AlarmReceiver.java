package com.app.oozee.receivers;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.app.oozee.R;
import com.app.oozee.activities.SplashActivity;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.services.VCardService;
import com.app.oozee.utils.OozeePreferences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.utils.Utility.BACKUPDATA;

public class AlarmReceiver extends BroadcastReceiver {

    private CharSequence name = "OozeeApp";

    public AlarmReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intents) {

        if (intents.getBooleanExtra("isBackup", false)) {

            boolean backUpData = OozeePreferences.getInstance().getPref().getBoolean(BACKUPDATA, false);
            if (backUpData) {
                int frequency = intents.getIntExtra("frequency", 0);
                scheduleAlarmforDay(context, frequency);

                ArrayList<LocalContactsModel> contactsModels = getAllContacs(new DatabaseHelper(context));
                Intent intent = new Intent(context, VCardService.class);
                intent.putExtra("contactsModels", contactsModels);
                context.startService(intent);
            }
        } else {
            sendNotification(intents.getStringExtra("title"), context);
        }
    }

    private void sendNotification(String messageBody, Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra("isFromNotificaiton", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int importance = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0/* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }
        NotificationChannel mChannel = null;
        String CHANNEL_ID = "oozee01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
        notification.setSmallIcon(getNotificationIcon(notification))
                .setContentTitle("Oozee")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID).build();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        if (mNotificationManager != null) {
            int notifyID = 1;
            mNotificationManager.notify(notifyID, notification.build());
        }
    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(Color.parseColor("#ffffff"));
            return R.drawable.notificationicon;

        }
        return R.mipmap.appiconroundf;
    }

    private void scheduleAlarmforDay(Context context, int position) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent alarmIntent = getPendingIntent(context, position);
        if (alarmMgr != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeZone(TimeZone.getDefault());
            if (position == 0) {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 5);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 1);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            } else if (position == 1) {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 7);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            } else {
                cal.set(Calendar.HOUR_OF_DAY, 2);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.DATE, 30);
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), alarmIntent);
            }
        }
    }

    private PendingIntent getPendingIntent(Context context, int position) {
        Intent intent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
        intent.putExtra("isBackup", true);
        intent.putExtra("frequency", position);
        return PendingIntent.getBroadcast(context.getApplicationContext(), 888, intent, 0);
    }
}