package com.app.oozee.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.app.oozee.activities.PopupActivity;
import com.app.oozee.interfaces.OozeeInterface;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.utils.ApiClient;
import com.app.oozee.utils.OozeePreferences;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.telephony.TelephonyManager.CALL_STATE_OFFHOOK;
import static android.telephony.TelephonyManager.CALL_STATE_RINGING;
import static com.app.oozee.utils.Utility.COUNTRYID;
import static com.app.oozee.utils.Utility.PHONENUMBER;
import static com.app.oozee.utils.Utility.onCall;

public class MyPhoneReceiver extends BroadcastReceiver {

    public static final String Tag = "MyPhoneReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Bundle extras = intent.getExtras();
        strictMode();
        if (extras != null) {
            String state = extras.getString(TelephonyManager.EXTRA_STATE);
            String phoneNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

            if (getPrefBoolean("isOutGoing") && phoneNumber != null) {
                if (!getPrefBoolean("isOutGoingShow")) {
                    String number = phoneNumber;
                    if (number.startsWith("+91")) {
                        number = number.replaceAll("\\+91", "");
                    }
                    if (number.startsWith("91") && number.length() > 10) {
                        number = number.replaceFirst("91", "");
                    }
                    //if (!numberExists(new DatabaseHelper(context), number)) {
                    //Log.d(Tag, "Number Not in contacts");
                    setPrefBoolean("isOutGoingShow", true);
                    OozeePreferences.getInstance().getEditor().putString("unknowntempPhone", number).apply();
                    apiCallForDetails(context, false, number);
                    /*} else {
                        Log.d(Tag, "Number in contacts");
                    }*/
                }
            }

            if (state != null && state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                if (phoneNumber != null) {
                    OozeePreferences.getInstance().getEditor().putString("tempPhone", phoneNumber).apply();
                    apiCallForDetails(context, false, phoneNumber);
                }
            } else if (state != null && state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                if (!getPrefBoolean("isIdle")) {
                    setPrefBoolean("isIdle", true);

                    if (getPrefBoolean("isOutGoing")) {
                        Log.d(Tag, "Outgoing call ended");
                        setPrefBoolean("isOutGoing", false);
                        setPrefBoolean("isOutGoingShow", false);
                        String phne = OozeePreferences.getInstance().getPref().getString("unknowntempPhone", "");
                        if (phne != null) {
                            OozeePreferences.getInstance().getEditor().putString("unknowntempPhone", "").apply();
                            apiCallForDetails(context, true, phne);
                        }
                    }

                    if (getPrefBoolean("isRinging")) {
                        Log.d(Tag, "Incoming call ended");
                        setPrefBoolean("isRinging", false);
                        String phne = OozeePreferences.getInstance().getPref().getString("tempPhone", "");
                        if (phne != null && !phne.equalsIgnoreCase("")) {
                            OozeePreferences.getInstance().getEditor().putString("tempPhone", "").apply();
                            apiCallForDetails(context, true, phne);
                        }
                    }
                    Log.d(Tag, state + "Ended Any One");
                } else {
                    setPrefBoolean("isIdle", false);
                }
            }

            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            telephony.listen(new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    super.onCallStateChanged(state, incomingNumber);
                    if (state == CALL_STATE_RINGING) {
                        Log.d(Tag, "incomingNumber : Triggered");
                        if (!getPrefBoolean("isRinging")) {
                            setPrefBoolean("isRinging", true);
                            Log.d(Tag, "incomingNumber : " + incomingNumber);
                            callStatusApi(onCall);
                        }
                    } else if (state == CALL_STATE_OFFHOOK) {
                        Log.d(Tag, "outGoing : Triggered " + incomingNumber);
                        String mPhoneNumber = OozeePreferences.getInstance().getPref().getString(PHONENUMBER, "");
                        if (incomingNumber == null || incomingNumber.equalsIgnoreCase("") ||
                                incomingNumber.equalsIgnoreCase(mPhoneNumber)) {
                            if (!getPrefBoolean("isOutGoing") && !getPrefBoolean("isRinging")) {
                                setPrefBoolean("isOutGoing", true);
                                Log.d(Tag, "outgoingNumber : " + mPhoneNumber);
                                callStatusApi(onCall);
                            }
                        }
                    }
                }
            }, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    private void strictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void callStatusApi(String status) {
        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
        String mPhoneNumber = OozeePreferences.getInstance().getPref().getString(PHONENUMBER, "");

        BlockedContactModel blockedContacts = new BlockedContactModel(userId, countryid + mPhoneNumber, status);

        String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
        OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
        Call<JsonElement> call = apiService.addSingleContactToList("update-user-status",
                blockedContacts, header);
        try {
            Response execute = call.execute();
            if (execute.isSuccessful()) {
                Log.d(Tag, "Sucess");
            } else {
                Log.d(Tag, "Failure");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(Tag, "Failure " + e.getMessage());
        }
    }

    private void apiCallForDetails(final Context context, final boolean incoming, String phoneNumber) {

        boolean callPopUp = OozeePreferences.getInstance().getPref().getBoolean("callPopUp", false);
        if (!callPopUp) {
            return;
        }

        int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
        int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);

        if (phoneNumber.startsWith("+91")) {
            phoneNumber = phoneNumber.replaceAll("\\+91", "");
        }

        if (phoneNumber.startsWith("91") && phoneNumber.length() > 10) {
            phoneNumber = phoneNumber.replaceFirst("91", "");
        }

        if (incoming) {
            callStatusApi(String.valueOf(Calendar.getInstance().getTimeInMillis()));
            Log.d(Tag, "Incoming call ended true");
        }

        BlockedContactModel blockedContacts = new BlockedContactModel(userId, phoneNumber, countryid);
        String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
        OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
        Call<JsonElement> call = apiService.addSingleContactToList("call-details", blockedContacts, header);
        try {
            JsonElement loginResponse = call.execute().body();
            if (loginResponse != null) {
                Log.d(Tag, loginResponse.toString());
            }
            ProfileModel profileModel = ProfileModel.getPopUpDetails(loginResponse);
            if (profileModel != null) {
                Intent intent = new Intent(context.getApplicationContext(), PopupActivity.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("callEnded", incoming);
                intent.putExtra("profileModel", profileModel);
                context.startActivity(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(Tag, e.getMessage());
        }
    }

    private boolean getPrefBoolean(String key) {
        return OozeePreferences.getInstance().getPref().getBoolean(key, false);
    }

    private void setPrefBoolean(String key, boolean value) {
        OozeePreferences.getInstance().getEditor().putBoolean(key, value).apply();
    }
}