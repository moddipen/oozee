package com.app.oozee.services;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;


public class ReadIncomingMessageService  extends Service
{

    private final String TAG = this.getClass().getSimpleName();

    private SmsBroadcastReceiver mSMSreceiver;
    private IntentFilter mIntentFilter;

    @Override
    public void onCreate()
    {
        super.onCreate();


        Log.i("OOZEE", "Reading messages service started");
        //SMS event receiver
        mSMSreceiver = new SmsBroadcastReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.setPriority(2147483647);
        registerReceiver(mSMSreceiver, mIntentFilter);

//        Intent intent = new Intent("android.provider.Telephony.SMS_RECEIVED");
//        List<ResolveInfo> infos = getPackageManager().queryBroadcastReceivers(intent, 0);
//        for (ResolveInfo info : infos) {
//            Log.i(TAG, "Receiver name:" + info.activityInfo.name + "; priority=" + info.priority);
//        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("OOZEE","Service Stopped.");
        // Unregister the SMS receiver
//        unregisterReceiver(mSMSreceiver);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


}