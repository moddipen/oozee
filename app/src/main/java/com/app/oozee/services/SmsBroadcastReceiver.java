package com.app.oozee.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.RemindersModel;
import com.app.oozee.receivers.AlarmReceiver;
import com.app.oozee.utils.Config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.ALARM_SERVICE;
import static com.app.oozee.database.RemindersModel.insertReminder;
import static com.app.oozee.utils.Config.getDayDateYear;

public class SmsBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        Log.d("Message Received", "true");
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            if (bundle != null) {
                //---retrieve the SMS message received---
                try {

                    String smsMessageStr = "";

                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        String smsBody = smsMessage.getMessageBody().toString();
                        String address = smsMessage.getOriginatingAddress();
                        smsMessageStr += smsBody + "\n";
                    }
                    Log.d("Message Body", smsMessageStr);
                    createEvent(context, smsMessageStr);

                } catch (Exception e) {
                    Log.d("Exception caught", e.getMessage());
                }
            }
        }
    }

    HashMap<String, String> titleMap = new HashMap<String, String>() {
        {
            put("Flight Booking", "flight");
            put("Flight Booking", "Flight ");
            put("Flight Booking", "GoAir");
            put("Bill payment due", "payment due");
            put("Bill payment due", "due");
            put("Train Booking", "DOJ");
            put("Train Booking","TRN");
            put("Bus Booking", "bus");
            put("Movie Booking", "movie");
            put("Train Booking", "irctc");
            put("OLA Ride Booking", "ola");
            put("Uber Ride Booking", "uber");


        }
    };
    String[] eventKeyword = {"booked", "booking", "book at", "book", "book my show", "ticket", "tickets", "booking id", "bookingid", "ticketid","Flight","flight","bus","Dear Customer","dear customer","creditcard","statement","payment due","movie","movie tickets","movie ticket","departure","trn","TRN","DOJ","doj"};
    String[] regExpression = {"[0-9]{1,2}/[a-zA-Z]{3}/[0-9]{4}","[0-9]{1,2}:[0-9]{1,2}[ ][a-zA-Z]{3},[ ][0-9]{1,2}[ ][a-zA-Z]{3}[ ][0-9]{4}","[a-zA-Z]{3},[ ][0-9]{1,2}[ ][a-zA-Z]{3}[ ][0-9]{2},[ ][0-9]{1,2}:[0-9]{1,2}[ ][a-zA-Z]{2}","[a-zA-Z]{0,3}[ ][0-9]{1,2},[ ][0-9]{4},[ ][0-9]{1,2}:[0-9]{1,2}[ ][a-zA-Z]{2}","[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2},[0-9][a-zA-Z]"};
    String[] dateFormat = {"dd/MMM/yyyy","hh:mm EEE, dd MMM yyyy","EEE, dd MMM yy, hh:mm aa","MMM dd, yyyy, hh:mm aa","dd-MM-yy haa"};


    private DatabaseHelper databaseHelper;

    void createEvent(Context context, String messageBody) {
        String title = "New Event";
        String note = messageBody;
        String dateString = "";


        boolean keyword_exist = false;

        String lower_case_message = messageBody.toLowerCase();

        for (int i = 0; i < eventKeyword.length; i++) {
            if (lower_case_message.contains(eventKeyword[i].toLowerCase())) {
                keyword_exist = true;
                break;
            }
        }

        Iterator it = titleMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String keyName = pair.getKey().toString();
            String value = pair.getValue().toString();

            if (lower_case_message.contains(value.toLowerCase())) {
                title = keyName;
                break;
            }
        }

        if (keyword_exist) {
            databaseHelper = new DatabaseHelper(context);
            String tempMesage = messageBody.toUpperCase();

            long timestampVal = 0;


            for(int k=0;k<regExpression.length;k++){
                String regex = regExpression[k];
                Matcher m = Pattern.compile(regex).matcher(tempMesage);
                if (m.find()) {
                    try {
                        String groupName = m.group(0);
                        if(groupName.endsWith("A") || groupName.endsWith("P")){
                            groupName = groupName + "M";
                        }
                        Date date = new SimpleDateFormat(dateFormat[k]).parse(groupName);
                        timestampVal = date.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(timestampVal  != 0){
                SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
                try {
                    Date convertedDate = new Date(timestampVal);
                    dateString = outputFormat.format(convertedDate).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Log.d("Extracted Date is ", dateString + "");

            String contactName = "-";
            String contactNumber = "-";
            String isRegular = "0";

            if (dateString == null || dateString.equals("")) {
                Toast.makeText(context, "Date and time not found in event", Toast.LENGTH_LONG).show();
            } else {
                long mid_trigger = scheduleAlarm(context, dateString, title);
                dateString = Config.getDateFormat(dateString);

                RemindersModel remindersModel = new RemindersModel(0, contactName,
                        contactNumber, dateString, isRegular, title, note, mid_trigger);
                insertReminder(remindersModel, databaseHelper);
            }
        }


    }

    public long scheduleAlarm(Context context, String dateString, String title) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        ArrayList<String> strings = getDayDateYear(dateString);
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.set(Calendar.YEAR, Integer.parseInt(strings.get(0)));
        cal.set(Calendar.MONTH, Integer.parseInt(strings.get(1)) - 1);
        cal.set(Calendar.DATE, Integer.parseInt(strings.get(2)));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strings.get(3)));
        cal.set(Calendar.MINUTE, Integer.parseInt(strings.get(4)));
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long mid_trigger = cal.getTimeInMillis();
        PendingIntent alarmIntent = getPendingIntent(context, (int) (mid_trigger - 1000), title);

        if (alarmMgr != null) {
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, mid_trigger, alarmIntent);
        }

        return mid_trigger;
    }

    private PendingIntent getPendingIntent(Context context, int trigger, String title) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("title", title);
        intent.putExtra("requestcode", trigger);
        return PendingIntent.getBroadcast(context, trigger, intent, 0);
    }
}
