package com.app.oozee.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.util.Log;

import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;

import java.util.ArrayList;

import static com.app.oozee.database.AllContactsModel.deleteContact;
import static com.app.oozee.database.AllContactsModel.getAllContacs;
import static com.app.oozee.database.AllContactsModel.insertAllContacts;

public class ContactsBackupService extends IntentService {

    private static final String TAG = "ContactsBackup";

    public ContactsBackupService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        performTask(intent);
    }

    private void performTask(Intent intent) {
        strictMode();
        getContactList();
    }

    private void getContactList() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        ArrayList<String> strings = new ArrayList<>();
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                strings.add(id);
                Log.d("Contacts", id);
            }
        }
        if (cur != null) {
            cur.close();
        }

        ArrayList<String> deletedContacts = new ArrayList<>();
        ArrayList<String> addedToContacts = new ArrayList<>();
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        ArrayList<LocalContactsModel> contactsModels = getAllContacs(databaseHelper);

        if (contactsModels.size() > strings.size()) {
            for (int i = 0; i < contactsModels.size(); i++) {
                boolean isInDatabase = false;
                for (int j = 0; j < strings.size(); j++) {
                    if (strings.get(j).equalsIgnoreCase(contactsModels.get(i).rawContactId)) {
                        isInDatabase = true;
                    }
                }
                if (!isInDatabase) {
                    deletedContacts.add(contactsModels.get(i).rawContactId);
                    break;
                }
            }

            for (int k = 0; k < deletedContacts.size(); k++) {
                deleteContact(databaseHelper, deletedContacts.get(k));
            }

            BusModel busModel = new BusModel();
            busModel.serviceName = "DeletedContacts";
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    //BusManager.getInstance().post(busModel);
                }
            });
            OozeePreferences.getInstance().getEditor().putBoolean("isRefreshed", true).apply();
        } else if (contactsModels.size() < strings.size()) {
            for (int i = 0; i < strings.size(); i++) {
                boolean isInDatabase = false;
                for (int j = 0; j < contactsModels.size(); j++) {
                    if (strings.get(i).equalsIgnoreCase(contactsModels.get(j).rawContactId)) {
                        isInDatabase = true;
                        break;
                    }
                }
                if (!isInDatabase) {
                    addedToContacts.add(strings.get(i));
                }
            }

            for (int k = 0; k < addedToContacts.size(); k++) {
                addContact(addedToContacts.get(k));
            }

            BusModel busModel = new BusModel();
            busModel.serviceName = "DeletedContacts";
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    //BusManager.getInstance().post(busModel);
                }
            });

            OozeePreferences.getInstance().getEditor().putBoolean("isRefreshed", true).apply();
        }
    }

    private void addContact(String id) {
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{id}, null);

        String number = "";
        String name = "";

        ArrayList<String> strings = getContactDetails(id);
        String email = "";
        String image = "";
        if (strings.size() > 0) {
            for (int i = 0; i < strings.size(); i++) {
                if (strings.get(i).contains("@")) {
                    email = strings.get(i);
                } else {
                    image = strings.get(i);
                }
            }
        }

        if (cursor != null) {
            while (cursor.moveToNext()) {
                number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            }
            cursor.close();
        }
        ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
        contactsModels.add(new LocalContactsModel(name, email, image, number, id));
        insertAllContacts(contactsModels, new DatabaseHelper(getApplicationContext()));
    }

    public ArrayList<String> getContactDetails(String contactId) {
        ArrayList<String> strings = new ArrayList<>();
        try (Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.PHOTO_URI,},
                ContactsContract.Data.CONTACT_ID + "=?",
                new String[]{contactId}, null)) {
            if (phoneCursor != null) {
                int idxAvatarUri = phoneCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                while (phoneCursor.moveToNext()) {
                    String avatarUri = phoneCursor.getString(idxAvatarUri);

                    if (avatarUri != null && !strings.contains(avatarUri)) {
                        strings.add(avatarUri);
                    }
                }

                if (!phoneCursor.isClosed()) {
                    phoneCursor.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Cursor emailCursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS,},
                ContactsContract.Data.CONTACT_ID + "=?",
                new String[]{String.valueOf(contactId)}, null)) {
            if (emailCursor != null) {
                int idxAddress = emailCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Email.ADDRESS);
                while (emailCursor.moveToNext()) {
                    String address = emailCursor.getString(idxAddress);
                    if (address != null && !strings.contains(address)) {
                        strings.add(address);
                    }
                }
                if (!emailCursor.isClosed()) {
                    emailCursor.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strings;
    }

    private void strictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}
