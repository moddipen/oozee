package com.app.oozee.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;

import com.app.oozee.interfaces.OozeeInterface;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.MediaModel;
import com.app.oozee.models.SyncContactsModel;
import com.app.oozee.utils.ApiClient;
import com.app.oozee.utils.OozeePreferences;
import com.google.gson.JsonElement;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.app.oozee.baseclass.BaseActivity.MULTIPART_FORM_DATA;
import static com.app.oozee.models.MediaModel.getMediaModel;
import static com.app.oozee.utils.Config.generateVCard;
import static com.app.oozee.utils.Utility.COUNTRYID;

public class VCardService extends IntentService {

    private static final String TAG = "VCardService";

    public VCardService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        performTask(intent);
    }

    private void performTask(Intent intent) {
        strictMode();
        ArrayList<LocalContactsModel> contactsModels = intent.getParcelableArrayListExtra("contactsModels");
        File vcfFile = generateVCard(contactsModels, getApplicationContext());

        MultipartBody.Part body = prepareFilePart(vcfFile.getAbsolutePath());
        RequestBody type = RequestBody.create(MultipartBody.FORM, "vcard");

        OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
        Call<JsonElement> call = apiService.addMedia(body, type);

        try {
            JsonElement loginResponse = call.execute().body();
            MediaModel mediaModel = getMediaModel(loginResponse, false);
            if (mediaModel != null) {

                Log.d(TAG, "Media Uploaded");
                Log.d(TAG, mediaModel.id + "");

                int userId = OozeePreferences.getInstance().getPref().getInt("userId", 0);
                int countryid = OozeePreferences.getInstance().getPref().getInt(COUNTRYID, 99);
                SyncContactsModel syncContactsModel = new SyncContactsModel(mediaModel.id,
                        userId, countryid);
                String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
                OozeeInterface apiService2 = ApiClient.getClient().create(OozeeInterface.class);
                Call<JsonElement> call2 = apiService2.syncContacts("sync-contacts", syncContactsModel, header);

                JsonElement syncContactsResponse = call2.execute().body();
                if (syncContactsResponse != null) {
                    OozeePreferences.getInstance().getEditor().putBoolean("syncContacts", true).commit();
                    Log.d(TAG, "Successfully synced");
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @NonNull
    public MultipartBody.Part prepareFilePart(String fileUri) {
        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);
        return MultipartBody.Part.createFormData("media", file.getName(), requestFile);
    }

    private void strictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}