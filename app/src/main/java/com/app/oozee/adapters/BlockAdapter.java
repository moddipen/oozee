package com.app.oozee.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.utils.Config;
import com.app.oozee.utils.Utility;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.utils.Utility.getTypeFace;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.MyCallLog> {

    private ArrayList<BlockedContactModel> blockedContactModels;
    private Context context;
    private boolean isBlock;

    class MyCallLog extends RecyclerView.ViewHolder {

        private TextView numberTextView;
        private TextView nameTextView;
        private TextView initialTextView;
        private CircleImageView imageView;
        private ImageView removeFromList;

        private MyCallLog(View view) {
            super(view);

            initialTextView = view.findViewById(R.id.initialTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            removeFromList = view.findViewById(R.id.removeFromList);
            nameTextView = view.findViewById(R.id.nameTextView);
            imageView = view.findViewById(R.id.imageView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            numberTextView.setTypeface(typeface);
            initialTextView.setTypeface(typeface);

            if (!isBlock) {
                removeFromList.setVisibility(View.GONE);
            }
        }
    }

    public BlockAdapter(ArrayList<BlockedContactModel> blockedContactModels, Context context,
                        boolean isBlock) {
        this.blockedContactModels = blockedContactModels;
        this.context = context;
        this.isBlock = isBlock;
    }

    @NonNull
    @Override
    public MyCallLog onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_blocked, parent, false);
        return new MyCallLog(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyCallLog holder, final int position) {

        final BlockedContactModel blockedContactModel = blockedContactModels.get(position);

        holder.initialTextView.setText(getStringInitial(blockedContactModel.first_name));
        holder.nameTextView.setText(blockedContactModel.first_name);
        holder.numberTextView.setText(blockedContactModel.phone_number);

        if (blockedContactModel.image != null && !blockedContactModel.image.equalsIgnoreCase("")) {
            Glide.with(context).load(blockedContactModel.image).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.GONE);
        } else {
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.VISIBLE);
        }

        holder.removeFromList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFromlist("Are you sure you want to remove this contact from block list?", blockedContactModel,
                        holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return blockedContactModels.size();
    }

    private void removeFromlist(String message, final BlockedContactModel blockedContactModel,
                                final int pos) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        BlockContactInterface blockContactInterface = (BlockContactInterface) context;
                        blockContactInterface.blockContact(blockedContactModel);
                        blockedContactModels.remove(pos);
                        notifyDataSetChanged();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public interface BlockContactInterface {
        void blockContact(BlockedContactModel blockedContactModel);
    }
}