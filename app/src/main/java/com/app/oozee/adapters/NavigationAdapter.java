package com.app.oozee.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.AppSettingsActivity;
import com.app.oozee.activities.FeedbackScreen;
import com.app.oozee.activities.SettingsScreen;
import com.app.oozee.activities.SubscriptionPlansScreen;
import com.app.oozee.models.NavigationItem;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.shareIntent;
import static com.app.oozee.utils.Utility.getTypeFace;

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.ContactsHolder> {

    private ArrayList<NavigationItem> navigationItems;
    private Context context;
    private DrawerLayout drawerLayout;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView nameTextView;

        private ContactsHolder(View view) {
            super(view);

            imageView = view.findViewById(R.id.imageView);
            nameTextView = view.findViewById(R.id.nameTextView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
        }
    }

    public NavigationAdapter(ArrayList<NavigationItem> navigationItems, Context context,
                             DrawerLayout drawerLayout) {
        this.navigationItems = navigationItems;
        this.context = context;
        this.drawerLayout = drawerLayout;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_navigation, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsHolder holder, final int position) {
        final NavigationItem contactsModel = navigationItems.get(position);
        holder.nameTextView.setText(contactsModel.title);
        Glide.with(context).load(contactsModel.drawable).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout != null) {
                    drawerLayout.closeDrawers();
                }
                if (contactsModel.title.equalsIgnoreCase("Oozee Premium")) {
                    Intent intent = new Intent(context, SubscriptionPlansScreen.class);
                    context.startActivity(intent);
                } else if (contactsModel.title.equalsIgnoreCase("Feedback")) {
                    Intent intent = new Intent(context, FeedbackScreen.class);
                    context.startActivity(intent);
                } else if (contactsModel.title.equalsIgnoreCase("Invite")) {
                    shareIntent(context);
                } else if (contactsModel.title.equalsIgnoreCase("FAQ")) {
                    Intent intent = new Intent(context, SettingsScreen.class);
                    intent.putExtra("blogs", true);
                    context.startActivity(intent);
                } else if (contactsModel.title.equalsIgnoreCase("Settings")) {
                    Intent intent = new Intent(context, AppSettingsActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return navigationItems.size();
    }
}