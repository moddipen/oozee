package com.app.oozee.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.EphocTime.getTimeAgoFeed;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.getTypeFacesemibold;
import static com.app.oozee.utils.Utility.onCall;

public class RecentTopAdapter extends RecyclerView.Adapter<RecentTopAdapter.ContactsHolder> {

    private ArrayList<LocalContactsModel> contactsModels;
    private Context context;
    private ArrayList<LocalContactsModel> statuses;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relativeLayout;
        private TextView initialTextView;
        private TextView nameTextView;
        private CircleImageView imageView;
        private ImageView dotimageView;
        private TextView ephocTextView;
        private ImageView ephocImageView;

        private ContactsHolder(View view) {
            super(view);

            relativeLayout = view.findViewById(R.id.relativeLayout);
            initialTextView = view.findViewById(R.id.initialTextView);
            ephocImageView = view.findViewById(R.id.ephocImageView);
            ephocTextView = view.findViewById(R.id.ephocTextView);
            nameTextView = view.findViewById(R.id.nameTextView);
            imageView = view.findViewById(R.id.imageView);
            dotimageView = view.findViewById(R.id.dotimageView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(getTypeFacesemibold(context));
            initialTextView.setTypeface(typeface);
        }
    }

    public RecentTopAdapter(ArrayList<LocalContactsModel> contactsModels, Context context) {
        this.contactsModels = contactsModels;
        this.context = context;
    }

    public void setContacts(ArrayList<LocalContactsModel> statuses) {
        this.statuses = statuses;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recent_top, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsHolder holder, final int position) {
        final LocalContactsModel contactsModel = contactsModels.get(position);

        if (contactsModel.name == null || contactsModel.name.equalsIgnoreCase("")) {
            holder.initialTextView.setText(getStringInitial(contactsModel.number));
            holder.nameTextView.setText(contactsModel.number);
        } else {
            holder.initialTextView.setText(contactsModel.initial);
            holder.nameTextView.setText(contactsModel.name);
        }

        if (contactsModel.image != null && !contactsModel.image.equalsIgnoreCase("")) {
            Glide.with(context).load(contactsModel.image).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.GONE);
        } else {
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.VISIBLE);
        }

        if (contactsModel.gender != null && !contactsModel.gender.equalsIgnoreCase("")) {
            holder.dotimageView.setVisibility(View.VISIBLE);
            if (contactsModel.gender.equalsIgnoreCase("M") ||
                    contactsModel.gender.equalsIgnoreCase("Male")) {
                // holder.dotimageView.setBackgroundColor(context.getResources().getColor(R.color.callpopupmale));
                holder.dotimageView.setImageResource(R.mipmap.male_badge_xxxhdpi);
            } else {
                //  holder.dotimageView.setBackgroundColor(context.getResources().getColor(R.color.callpopupgirl));
                holder.dotimageView.setImageResource(R.mipmap.female_badge_xxxhdpi);
            }

        } else {
            holder.dotimageView.setVisibility(View.GONE);
        }

        String status = getStringFromUserStatus(contactsModel.number);
        boolean isStatus = OozeePreferences.getInstance().getPref().getBoolean("isStatus", false);
        if (!isStatus) {
            status = "";
        }
        if (status.equalsIgnoreCase("")) {
            holder.relativeLayout.setVisibility(View.INVISIBLE);
            holder.ephocTextView.setVisibility(View.GONE);
            holder.ephocImageView.setVisibility(View.GONE);
        } else {
            holder.relativeLayout.setVisibility(View.VISIBLE);
            holder.ephocTextView.setVisibility(View.VISIBLE);
            holder.ephocImageView.setVisibility(View.VISIBLE);
            updateText(holder, status);
        }


        holder.itemView.setOnClickListener(v -> callPhone(context, contactsModel.number));

        holder.itemView.setOnLongClickListener(v -> {
            showPopUpWindow(contactsModel);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return contactsModels.size();
    }

    private void showPopUpWindow(final LocalContactsModel contactsModel) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        Typeface typeface = getTypeFace(context);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        imageView.setText("Remove from Quick List");
        textView.setText("This contact will be removed from quick list. You can again add it to quick lists anytime.");
        performTextView.setText("Remove");

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                BusModel busModel = new BusModel();
                busModel.serviceName = "removeQuickList";
                busModel.contactsModel = contactsModel;
                BusManager.getInstance().post(busModel);

                contactsModels.remove(contactsModel);
                notifyDataSetChanged();
            }
        });

        cancelTextView.setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!((Activity) context).isDestroyed()) {
            dialog.show();
        }
    }

    private String getStringFromUserStatus(String number) {
        if (statuses != null) {
            for (int i = 0; i < statuses.size(); i++) {
                if (number.toLowerCase().contains(statuses.get(i).number.toLowerCase())) {
                    return statuses.get(i).name;
                } else if (statuses.get(i).number.toLowerCase().contains(number.toLowerCase())) {
                    return statuses.get(i).name;
                }
            }
        }
        return "";
    }

    private void updateText(final ContactsHolder contactsHolder, final String time) {

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                handler.postDelayed(this, 60000);
                if (time.equalsIgnoreCase(onCall)) {
                    contactsHolder.ephocTextView.setText(onCall);
                    contactsHolder.ephocTextView.setTextColor(context.getResources().getColor(R.color.darkGrayColor2));
                    contactsHolder.ephocImageView.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greenColor)));
                } else {
                    if (!time.contains(" ")) {
                        contactsHolder.ephocTextView.setText("active " + getTimeAgoFeed(Long.parseLong(time)));
                        contactsHolder.ephocTextView.setTextColor(context.getResources().getColor(R.color.darkGrayColor2));
                        contactsHolder.ephocImageView.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.orangeColor)));
                    }
                }
            }
        };
        handler.postDelayed(r, 0);
    }
}