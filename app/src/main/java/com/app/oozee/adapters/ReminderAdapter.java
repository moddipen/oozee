package com.app.oozee.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.AddReminderScreen;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.RemindersModel;
import com.app.oozee.utils.Config;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;
import java.util.Calendar;

import static com.app.oozee.database.RemindersModel.deleteReminder;
import static com.app.oozee.utils.Utility.getTypeFace;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.MyViewHolder> {

    private ArrayList<RemindersModel> remindersModels;
    private Context context;
    private DatabaseHelper databaseHelper;
    private ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView mobileTextView;
        private TextView timeTextView;
        private TextView dateTextView;
        private TextView deleteTextView;
        private TextView editTextView;
        private ImageView imageView;
        private View viewLine;
        private RelativeLayout relativeLayout;
        private SwipeRevealLayout swipeRevealLayout;

        private MyViewHolder(View view) {
            super(view);


            deleteTextView = view.findViewById(R.id.deleteTextView);
            editTextView = view.findViewById(R.id.editTextView);
            nameTextView = view.findViewById(R.id.nameTextView);
            mobileTextView = view.findViewById(R.id.mobileTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            dateTextView = view.findViewById(R.id.dateTextView);
            imageView = view.findViewById(R.id.imageView);
            swipeRevealLayout = view.findViewById(R.id.swipeRevealLayout);
            /*viewLine = view.findViewById(R.id.viewLine);*/
            relativeLayout = view.findViewById(R.id.relativeLayout);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            mobileTextView.setTypeface(typeface);
            timeTextView.setTypeface(typeface);
            dateTextView.setTypeface(typeface);
        }
    }

    public ReminderAdapter(ArrayList<RemindersModel> remindersModels, Context context,
                           DatabaseHelper databaseHelper) {
        this.remindersModels = remindersModels;
        this.context = context;
        this.databaseHelper = databaseHelper;

        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_reminder, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final RemindersModel remindersModel = remindersModels.get(position);

        viewBinderHelper.bind(holder.swipeRevealLayout, remindersModel.title);

        holder.nameTextView.setText(remindersModel.title);
        holder.mobileTextView.setText(remindersModel.notes);
        holder.timeTextView.setText(Config.getTimeFromString(remindersModel.date));
        holder.dateTextView.setText(Config.getDateForString(remindersModel.date));

        if (Calendar.getInstance().getTimeInMillis() > remindersModel.mid_trigger) {
            holder.imageView.setImageDrawable(context.getResources().getDrawable(R.mipmap.completed));
        } else {
            holder.imageView.setImageDrawable(context.getResources().getDrawable(R.mipmap.toto));
        }

        holder.deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteReminder(databaseHelper, remindersModel.id);
                remindersModels.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });

        holder.editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddReminderScreen.class);
                intent.putExtra("isFromEdit", true);
                intent.putExtra("remindersModel", remindersModel);
                context.startActivity(intent);
            }
        });

/*
        if (remindersModels.size() == (position + 1)) {
            holder.viewLine.setVisibility(View.GONE);
        } else {
            holder.viewLine.setVisibility(View.VISIBLE);
        }
*/

        if (holder.getAdapterPosition() + 1 == remindersModels.size()) {
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                    holder.relativeLayout.getLayoutParams();
            params.setMargins(0, 0, 0, 80);
            holder.relativeLayout.setLayoutParams(params);
        } else {

        }
    }

    @Override
    public int getItemCount() {
        return remindersModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}