package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.LocalContactsModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.utils.Utility.getTypeFace;

public class SelectContactsAdapter extends RecyclerView.Adapter<SelectContactsAdapter.ContactsHolder> {

    private ArrayList<LocalContactsModel> contactsModels;
    private ArrayList<LocalContactsModel> tempContactsModel = new ArrayList<>();
    private Context context;
    private boolean isFromHome;
    private CheckBox checkBox;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView initialTextView;
        private TextView nameTextView;
        private CheckBox checkbox;
        private CircleImageView imageView;

        private ContactsHolder(View view) {
            super(view);

            initialTextView = view.findViewById(R.id.initialTextView);
            nameTextView = view.findViewById(R.id.nameTextView);
            checkbox = view.findViewById(R.id.checkbox);
            imageView = view.findViewById(R.id.imageView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            initialTextView.setTypeface(typeface);
        }
    }

    public SelectContactsAdapter(ArrayList<LocalContactsModel> contactsModels, Context context,
                                 boolean isFromHome) {
        this.contactsModels = contactsModels;
        this.context = context;
        this.isFromHome = isFromHome;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_select_contacts, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsHolder holder, final int position) {
        final LocalContactsModel contactsModel = contactsModels.get(position);
        holder.initialTextView.setText(getStringInitial(contactsModel.name));
        holder.nameTextView.setText(contactsModel.name);

        if (contactsModel.dir != null && !contactsModel.dir.equalsIgnoreCase("")) {
            Glide.with(context).load(contactsModel.dir).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.GONE);
        } else {
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.VISIBLE);
        }

        if (contactsModel.isCalled) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        holder.nameTextView.setOnClickListener(v -> {

            if (checkBox != null) {
                if (!isFromHome) {
                    if (checkBox.isChecked()) {
                        checkBox.setChecked(false);
                    }
                }
            }

            if (holder.checkbox.isChecked()) {
                if (isFromHome) {
                    contactsModel.isCalled = false;
                    tempContactsModel.remove(contactsModel);
                }
                holder.checkbox.setChecked(false);
            } else {
                if (checkBox != null) {
                    if (!(checkBox == holder.checkbox && !checkBox.isChecked())) {
                        if (isFromHome) {
                            tempContactsModel.remove(contactsModel);
                            contactsModel.isCalled = true;
                            tempContactsModel.add(contactsModel);
                        } else {
                            tempContactsModel.clear();
                            contactsModel.isCalled = true;
                            tempContactsModel.add(contactsModel);
                        }
                    }
                } else {
                    tempContactsModel.clear();
                    contactsModel.isCalled = true;
                    tempContactsModel.add(contactsModel);
                }

                holder.checkbox.setChecked(true);
            }
            checkBox = holder.checkbox;

            SendSelectedContacts sendSelectedContacts = (SendSelectedContacts) context;
            sendSelectedContacts.sendContacts(tempContactsModel);
        });
    }

    @Override
    public int getItemCount() {
        return contactsModels.size();
    }

    public interface SendSelectedContacts {
        void sendContacts(ArrayList<LocalContactsModel> contactsModel);
    }
}