package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.FeaturesModel;
import com.app.oozee.models.PlanModel;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class FeaturesAdapter extends RecyclerView.Adapter<FeaturesAdapter.MyCallLog> {

    private Context context;
    private ArrayList<FeaturesModel> featuresModels = new ArrayList<>();

    class MyCallLog extends RecyclerView.ViewHolder {

        private TextView titleTextView;

        private MyCallLog(View view) {
            super(view);

            titleTextView = view.findViewById(R.id.titleTextView);

            Typeface typeface = getTypeFace(context);
            titleTextView.setTypeface(typeface);
        }
    }

    public FeaturesAdapter(ArrayList<PlanModel> planModels, Context context, int whichPosition) {
        this.context = context;

        featuresModels.addAll(planModels.get(whichPosition).defaultFeatures);
        featuresModels.addAll(planModels.get(whichPosition).extraFeatures);
    }

    @NonNull
    @Override
    public MyCallLog onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_feartures, parent, false);
        return new MyCallLog(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyCallLog holder, final int position) {
        FeaturesModel featuresModel = featuresModels.get(position);
        holder.titleTextView.setText("\u2022 " + featuresModel.title);
    }

    @Override
    public int getItemCount() {
        return featuresModels.size();
    }
}