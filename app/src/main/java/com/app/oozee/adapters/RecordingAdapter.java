package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.RecordingModel;

import java.io.File;
import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class RecordingAdapter extends RecyclerView.Adapter<RecordingAdapter.MyCallLog> {

    private ArrayList<RecordingModel> recordingModels;
    private Context context;

    class MyCallLog extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView numberTextView;

        private MyCallLog(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);
            numberTextView = view.findViewById(R.id.numberTextView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);

        }
    }

    public RecordingAdapter(ArrayList<RecordingModel> recordingModels, Context context) {
        this.recordingModels = recordingModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyCallLog onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recordings, parent, false);
        return new MyCallLog(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyCallLog holder, final int position) {
        RecordingModel recordingModel = recordingModels.get(position);
        holder.nameTextView.setText(recordingModel.phone_number);
        holder.numberTextView.setText(recordingModel.phone_number);
    }

    @Override
    public int getItemCount() {
        return recordingModels.size();
    }

    public void audioPlayer(String path, String fileName) {
        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource(path + File.separator + fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}