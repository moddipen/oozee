package com.app.oozee.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.AddNotesScreen;
import com.app.oozee.models.NotesModel;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private ArrayList<NotesModel> notesModels;
    private Context context;
    private String phoneNumber;
    private ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView mobileTextView;
        private TextView timeTextView;
        private TextView dateTextView;
        private TextView deleteTextView;
        private TextView editTextView;
        private ImageView imageView;
        private View viewLine;
        private SwipeRevealLayout swipeRevealLayout;

        private MyViewHolder(View view) {
            super(view);

            deleteTextView = view.findViewById(R.id.deleteTextView);
            editTextView = view.findViewById(R.id.editTextView);
            nameTextView = view.findViewById(R.id.nameTextView);
            mobileTextView = view.findViewById(R.id.mobileTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            dateTextView = view.findViewById(R.id.dateTextView);
            imageView = view.findViewById(R.id.imageView);
            swipeRevealLayout = view.findViewById(R.id.swipeRevealLayout);
            viewLine = view.findViewById(R.id.viewLine);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            mobileTextView.setTypeface(typeface);
            timeTextView.setTypeface(typeface);
            dateTextView.setTypeface(typeface);

            imageView.setVisibility(View.GONE);
            timeTextView.setVisibility(View.GONE);
            dateTextView.setVisibility(View.GONE);
        }

    }

    public NotesAdapter(ArrayList<NotesModel> notesModels, Context context, String phoneNumber) {
        this.notesModels = notesModels;
        this.context = context;
        this.phoneNumber = phoneNumber;
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_note, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final NotesModel notesModel = notesModels.get(position);

        viewBinderHelper.bind(holder.swipeRevealLayout, notesModel.title);

        holder.nameTextView.setText(notesModel.title);
        holder.mobileTextView.setText(notesModel.note);

        holder.deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteNote deleteNote = (DeleteNote) context;
                deleteNote.deleteNote(notesModel);
                notesModels.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });

        holder.editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddNotesScreen.class);
                intent.putExtra("isFromEdit", true);
                intent.putExtra("notesModel", notesModel);
                intent.putExtra("phoneNumber", phoneNumber);
                ((Activity) context).startActivityForResult(intent, 221);
            }
        });

        if (notesModels.size() == (position + 1)) {
            holder.viewLine.setVisibility(View.GONE);
        } else {
            holder.viewLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return notesModels.size();
    }

    public interface DeleteNote {
        void deleteNote(NotesModel notesModel);
    }
}