package com.app.oozee.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.MessagesScreen;
import com.app.oozee.activities.ProfileScreen;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.utils.OozeePreferences;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.sarproj.com.layout.SwipeLayout;

import static com.app.oozee.utils.Config.callPhone;
import static com.app.oozee.utils.Config.getDateFromTimeStamp;
import static com.app.oozee.utils.Config.openWhatsapp;
import static com.app.oozee.utils.EphocTime.getTimeAgoFeed;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.getTypeFacesemibold;
import static com.app.oozee.utils.Utility.onCall;

public class RecentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<LocalContactsModel> contactsModels;
    private Context context;
    private DatabaseHelper databaseHelper;
    private Animation animationUp, animationDown;
    private int showPosition = -1;
    private LinearLayout oldView = null;
    private SwipeLayout swipeLayoutOld = null;
    private RelativeLayout viewForegroundOld = null;
    private ArrayList<LocalContactsModel> statuses;

    public class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView initialTextView;
        private TextView ephocTextView;
        private ImageView ephocImageView;
        private LinearLayout linearLayout;
        private ImageView callPhoneImageView;
        private ImageView messgeImageView;
        private ImageView whatsappImageView;
        private ImageView info;
        private TextView nameTextView;
        private TextView mobileNumberTextView;
        private TextView mobileTextView;
        private TextView timeTextView;
        private ImageView callImageView;
        private View viewLine;
        private CircleImageView imageView;
        public RelativeLayout viewForeground;
        public ImageView messageImageView;
        public ImageView callImageViews;
        public RelativeLayout rel;
        public SwipeLayout swipeLayout;
        private AdView adView;


        private ContactsHolder(View view) {
            super(view);

            linearLayout = view.findViewById(R.id.linearLayout);
            initialTextView = view.findViewById(R.id.initialTextView);
            ephocTextView = view.findViewById(R.id.ephocTextView);
            ephocImageView = view.findViewById(R.id.ephocImageView);
            nameTextView = view.findViewById(R.id.nameTextView);
            mobileTextView = view.findViewById(R.id.mobileTextView);
            mobileNumberTextView = view.findViewById(R.id.mobileNumberTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            info  = view.findViewById(R.id.info);
            viewLine = view.findViewById(R.id.viewLine);
            callImageView = view.findViewById(R.id.callImageView);
            callPhoneImageView = view.findViewById(R.id.callPhoneImageView);
            messgeImageView = view.findViewById(R.id.messgeImageView);
            whatsappImageView = view.findViewById(R.id.whatsappImageView);
            imageView = view.findViewById(R.id.imageView);
            viewForeground = view.findViewById(R.id.view_foreground);
            messageImageView = view.findViewById(R.id.messageImageView);
            swipeLayout = view.findViewById(R.id.swipe_layout);
            rel = view.findViewById(R.id.rel);
            callImageViews = view.findViewById(R.id.callImageViews);

            adView = view.findViewById(R.id.adView);
            startAds(adView);

            Typeface typeface = getTypeFace(context);
            initialTextView.setTypeface(typeface);
            nameTextView.setTypeface(getTypeFacesemibold(context));
            mobileTextView.setTypeface(typeface);
            timeTextView.setTypeface(typeface);
        }
    }

    private class DateHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        private DateHolder(View view) {
            super(view);

            textView = view.findViewById(R.id.textView);

            Typeface typeface = getTypeFace(context);
            textView.setTypeface(typeface);
        }
    }

    public RecentAdapter(ArrayList<LocalContactsModel> contactsModels, Context context, DatabaseHelper databaseHelper) {
        this.contactsModels = contactsModels;
        this.context = context;
        this.databaseHelper = databaseHelper;

        animationUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        animationDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
    }

    public void setContacts(ArrayList<LocalContactsModel> statuses) {
        this.statuses = statuses;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 0) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_date, parent, false);
            return new DateHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recent, parent, false);
            return new ContactsHolder(itemView);
        }
    }

    private void startAds(AdView mAdView) {
        //AdView mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        final LocalContactsModel contactsModel = contactsModels.get(position);

        if (contactsModel.viewType == 0) {
            ((DateHolder) holder).textView.setText(contactsModel.name);
        } else {

            ((ContactsHolder) holder).initialTextView.setText(contactsModel.initial);
            ((ContactsHolder) holder).nameTextView.setText(contactsModel.name);
            ((ContactsHolder) holder).mobileNumberTextView.setText("Mobile: " + contactsModel.number);
            ((ContactsHolder) holder).timeTextView.setText(getDateFromTimeStamp(contactsModel.date));

            if(4==0) {
                ((RecentAdapter.ContactsHolder) holder).adView.setVisibility(View.VISIBLE);
            }else{
                ((RecentAdapter.ContactsHolder) holder).adView.setVisibility(View.GONE);
            }

            if (!contactsModel.isCalled) {
                //contactsModels.get(holder.getAdapterPosition()).image = getImageFromNumber(databaseHelper, contactsModel.number);
                //contactsModel.isCalled = true;
            } else {

            }

            if (contactsModel.count == 0) {
                ((ContactsHolder) holder).mobileTextView.setText("Mobile");
            } else {
                ((ContactsHolder) holder).mobileTextView.setText("Mobile [" + contactsModel.count + "]");
            }

            String status = getStringFromUserStatus(contactsModel.number);
            boolean isStatus = OozeePreferences.getInstance().getPref().getBoolean("isStatus", false);
            if (!isStatus) {
                status = "";
            }
            if (status.equalsIgnoreCase("")) {
                ((ContactsHolder) holder).ephocTextView.setVisibility(View.GONE);
                ((ContactsHolder) holder).ephocImageView.setVisibility(View.GONE);
            } else {
                ((ContactsHolder) holder).ephocTextView.setVisibility(View.VISIBLE);
                ((ContactsHolder) holder).ephocImageView.setVisibility(View.VISIBLE);
                updateText(((ContactsHolder) holder), status);
            }

            if (contactsModel.image != null && !contactsModel.image.equalsIgnoreCase("")) {
                Glide.with(context).load(contactsModel.image).fitCenter().into(((ContactsHolder) holder).imageView);
                ((ContactsHolder) holder).initialTextView.setVisibility(View.GONE);
            } else {
                Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(((ContactsHolder) holder).imageView);
                ((ContactsHolder) holder).initialTextView.setVisibility(View.VISIBLE);
            }


            if (contactsModel.dir.equalsIgnoreCase("INCOMING")) {
                ((ContactsHolder) holder).callImageView.setImageDrawable(context.getDrawable(R.mipmap.incoming));
            } else if (contactsModel.dir.equalsIgnoreCase("MISSED")) {
                ((ContactsHolder) holder).callImageView.setImageDrawable(context.getDrawable(R.mipmap.missedcall));
            } else {
                ((ContactsHolder) holder).callImageView.setImageDrawable(context.getDrawable(R.mipmap.outgoing));
            }

            if (contactsModels.get(holder.getAdapterPosition() - 1).viewType == 0
                    && contactsModels.size() > (holder.getAdapterPosition() + 1) &&
                    contactsModels.get(holder.getAdapterPosition() + 1).viewType == 0) {
                ((ContactsHolder) holder).viewLine.setVisibility(View.INVISIBLE);
                ((ContactsHolder) holder).itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less));
            } else if (contactsModels.get(holder.getAdapterPosition() - 1).viewType == 0) {
                ((ContactsHolder) holder).itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_top));
                ((ContactsHolder) holder).viewLine.setVisibility(View.VISIBLE);
            } else if (contactsModels.size() > (holder.getAdapterPosition() + 1) &&
                    contactsModels.get(holder.getAdapterPosition() + 1).viewType == 1) {
                ((ContactsHolder) holder).itemView.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                ((ContactsHolder) holder).viewLine.setVisibility(View.VISIBLE);
            } else {
                ((ContactsHolder) holder).viewLine.setVisibility(View.INVISIBLE);
                ((ContactsHolder) holder).itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_bottom));
            }

            ((ContactsHolder) holder).rel.setOnClickListener(v -> {

                if (oldView != null && oldView != ((ContactsHolder) holder).linearLayout) {
                    oldView.setVisibility(View.GONE);
                    oldView.startAnimation(animationUp);
                }

                if (swipeLayoutOld != null && swipeLayoutOld != ((ContactsHolder) holder).swipeLayout) {
                    if (swipeLayoutOld.isLeftOpen()) {
                        swipeLayoutOld.close(true);
                    } else if (swipeLayoutOld.isRightOpen()) {
                        swipeLayoutOld.close(true);
                    }

                    if (viewForegroundOld != null && viewForegroundOld != ((ContactsHolder) holder).viewForeground) {
                        viewForegroundOld.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                    }
                }

                oldView = ((ContactsHolder) holder).linearLayout;

                ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));

                if (((ContactsHolder) holder).linearLayout.isShown()) {
                    showPosition = -1;
                    ((ContactsHolder) holder).linearLayout.setVisibility(View.GONE);
                    ((ContactsHolder) holder).linearLayout.startAnimation(animationUp);
                } else {
                    showPosition = holder.getAdapterPosition();
                    ((ContactsHolder) holder).linearLayout.setVisibility(View.VISIBLE);
                    ((ContactsHolder) holder).linearLayout.startAnimation(animationDown);
                }
            });

            ((ContactsHolder) holder).whatsappImageView.setOnClickListener(v -> {
                String number = contactsModel.number;
                openWhatsapp(context, number);
            });

            ((ContactsHolder) holder).swipeLayout.setOnActionsListener(new SwipeLayout.SwipeActionsListener() {
                @Override
                public void onOpen(int direction, boolean isContinuous) {

                    if (swipeLayoutOld != null && swipeLayoutOld != ((ContactsHolder) holder).swipeLayout) {
                        if (swipeLayoutOld.isLeftOpen()) {
                            swipeLayoutOld.close(true);
                        } else if (swipeLayoutOld.isRightOpen()) {
                            swipeLayoutOld.close(true);
                        }

                        if (viewForegroundOld != null && viewForegroundOld != ((ContactsHolder) holder).viewForeground) {
                            viewForegroundOld.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                        }
                    }
                    swipeLayoutOld = ((ContactsHolder) holder).swipeLayout;
                    viewForegroundOld = ((ContactsHolder) holder).viewForeground;

                    if (direction == SwipeLayout.LEFT) {

                        ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.verylighColorPrimary));

                        new Handler().postDelayed(() -> {
                            ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));

                            ((ContactsHolder) holder).swipeLayout.close(true);
                            String number = contactsModel.number;

                            Intent intent = new Intent(context, MessagesScreen.class);
                            intent.putExtra("address", number);
                            intent.putExtra("smsid", "0");
                            intent.putExtra("image",contactsModel.image);
                            context.startActivity(intent);
                        }, 500);

                    } else if (direction == SwipeLayout.RIGHT) {

                        ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.lightlightgreencolor));

                        new Handler().postDelayed(() -> {
                            ((ContactsHolder) holder).swipeLayout.close(true);
                            callPhone(context, contactsModel.number);
                            ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                        }, 500);
                    }
                }

                @Override
                public void onClose() {
                    ((ContactsHolder) holder).viewForeground.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                }
            });

            ((ContactsHolder) holder).messgeImageView.setOnClickListener(v -> {
                Intent intent = new Intent(context, MessagesScreen.class);
                intent.putExtra("address", contactsModel.number);
                intent.putExtra("smsid", "0");
                intent.putExtra("image",contactsModel.image);
                context.startActivity(intent);
            });

            ((ContactsHolder) holder).callPhoneImageView.setOnClickListener(v -> callPhone(context, contactsModel.number));

            ((ContactsHolder) holder).initialTextView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ProfileScreen.class);
                intent.putExtra("contactsModel", contactsModel);
                intent.putExtra("isCallLog", true);
                ((Activity) context).startActivityForResult(intent, 121);
            });

            ((ContactsHolder) holder).imageView.setOnClickListener(v -> {
                Intent intent = new Intent(context, ProfileScreen.class);
                intent.putExtra("contactsModel", contactsModel);
                intent.putExtra("isCallLog", true);
                ((Activity) context).startActivityForResult(intent, 121);
            });

            ((ContactsHolder) holder).info.setOnClickListener(v -> {
                Intent intent = new Intent(context, ProfileScreen.class);
                intent.putExtra("contactsModel", contactsModel);
                intent.putExtra("isCallLog", true);
                ((Activity) context).startActivityForResult(intent, 121);
            });


            if (showPosition == holder.getAdapterPosition()) {
                ((ContactsHolder) holder).linearLayout.setVisibility(View.VISIBLE);
            } else {
                ((ContactsHolder) holder).linearLayout.setVisibility(View.GONE);
            }

           /* if (holder.getAdapterPosition() + 1 == contactsModels.size()) {
                SwipeLayout.LayoutParams params = (SwipeLayout.LayoutParams) ((RecentAdapter.ContactsHolder) holder).swipeLayout.getLayoutParams();
                params.setMargins(0, 0, 0, 80);
                ((ContactsHolder) holder).swipeLayout.setLayoutParams(params);
            } else {

            }*/
        }
    }

    @Override
    public int getItemCount() {
        return contactsModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return contactsModels.get(position).viewType;
    }

    private void updateText(final ContactsHolder contactsHolder, final String time) {

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                handler.postDelayed(this, 60000);
                if (time.equalsIgnoreCase(onCall)) {
                    contactsHolder.ephocTextView.setText(onCall);
                    contactsHolder.ephocTextView.setTextColor(context.getResources().getColor(R.color.darkGrayColor2));
                    contactsHolder.ephocImageView.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greenColor)));
                } else {
                    if (!time.contains(" ")) {
                        contactsHolder.ephocTextView.setText("active " + getTimeAgoFeed(Long.parseLong(time)));
                        contactsHolder.ephocTextView.setTextColor(context.getResources().getColor(R.color.darkGrayColor2));
                        contactsHolder.ephocImageView.setImageTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.orangeColor)));
                    }
                }
            }
        };
        handler.postDelayed(r, 0);
    }

    private String getStringFromUserStatus(String number) {
        if (statuses != null) {
            for (int i = 0; i < statuses.size(); i++) {
                if (number.toLowerCase().contains(statuses.get(i).number.toLowerCase())) {
                    return statuses.get(i).name;
                } else if (statuses.get(i).number.toLowerCase().contains(number.toLowerCase())) {
                    return statuses.get(i).name;
                }
            }
        }
        return "";
    }
}