package com.app.oozee.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oozee.R;
import com.app.oozee.activities.MessagesScreen;
import com.app.oozee.activities.ProfileScreen;
import com.app.oozee.activities.SearchScreenActivity;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.database.SearchContactsDb;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.SearchContactModel;
import com.bumptech.glide.Glide;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TooManyListenersException;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.SearchContactsDb.insertHistorySearch;
import static com.app.oozee.utils.Utility.getTypeFace;
import static com.app.oozee.utils.Utility.textAdapter;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ContactsHolder> {

    private ArrayList<SearchContactModel> searchContactModels;
    private Context context;
    private boolean isOozee;
    private boolean isMessage;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView locationTextView;
        private CircleImageView imageView;
        private ImageView gendericon;

        private ContactsHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);
            locationTextView = view.findViewById(R.id.locationTextView);
            imageView = view.findViewById(R.id.imageView);
            gendericon = view.findViewById(R.id.gendericon);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            locationTextView.setTypeface(typeface);
        }
    }

    public SearchAdapter(Context context, ArrayList<SearchContactModel> searchContactModels,
                         boolean isOozee, boolean isMessage) {
        this.searchContactModels = searchContactModels;
        this.context = context;
        this.isOozee = isOozee;
        this.isMessage = isMessage;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_contacts, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsHolder holder, int position) {
        final SearchContactModel searchContactModel = searchContactModels.get(position);
        if (isOozee) {

            holder.nameTextView.setText(searchContactModel.first_name + " " + searchContactModel.last_name);
            holder.locationTextView.setText(searchContactModel.address);

            Glide.with(context).load(searchContactModel.photo).placeholder(context.getResources().getDrawable(R.mipmap.personalselected))
                    .fitCenter().into(holder.imageView);

            if (searchContactModels.get(position).gender == "Male") {
                holder.gendericon.setVisibility(View.VISIBLE);
                holder.gendericon.setImageResource(R.mipmap.male_badge_xxxhdpi);
            } else if (searchContactModels.get(position).gender == "Female") {
                holder.gendericon.setVisibility(View.VISIBLE);
                holder.gendericon.setImageResource(R.mipmap.female_badge_xxxhdpi);
            } else {
                holder.gendericon.setVisibility(View.GONE);
            }

        } else {
            holder.nameTextView.setText(searchContactModel.first_name);
            holder.locationTextView.setText(searchContactModel.phone_number);

            if (searchContactModel.photo != null && !searchContactModel.photo.equalsIgnoreCase("")) {
                Glide.with(context).load(searchContactModel.photo).fitCenter().into(holder.imageView);
            } else {
                Glide.with(context).load(context.getResources().getDrawable(R.mipmap.personalselected)).
                        fitCenter().into(holder.imageView);
            }
        }

        if (holder.getAdapterPosition() == 0 && !(searchContactModels.size() > (holder.getAdapterPosition() + 1))) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less));
        } else if (holder.getAdapterPosition() == 0 && searchContactModels.size() > (holder.getAdapterPosition() + 1)) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_top));
        } else if (searchContactModels.size() > (holder.getAdapterPosition() + 1)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
        } else {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_bottom));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SearchContactsDb searchContactsDb = new SearchContactsDb(searchContactModel.first_name,
                        searchContactModel.phone_number, searchContactModel.photo);
                insertHistorySearch(searchContactsDb, new DatabaseHelper(context));

                String fName;
                if (isOozee) {
                    fName = searchContactModel.first_name + " " + searchContactModel.last_name;
                } else {
                    fName = searchContactModel.first_name;
                }

                if (!isMessage) {
                    LocalContactsModel localContactsModel = new LocalContactsModel(0, fName,
                            getStringInitial(fName),
                            searchContactModel.phone_number, searchContactModel.photo);

                    Intent intent = new Intent(context, ProfileScreen.class);
                    intent.putExtra("contactsModel", localContactsModel);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, MessagesScreen.class);
                    intent.putExtra("address", searchContactModel.phone_number);
                    intent.putExtra("smsid", "0");
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchContactModels.size();
    }
}