package com.app.oozee.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.MessageModel;
import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class SmsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MessageModel> messageModels;
    private Context context;
    private MediaPlayer mp;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mMessage, mUsername, time;
        View mView;
        ImageView imageView;
        ImageView pauseButton;
        ImageView tickImageView;
        ImageView playButton;
        LinearLayout relativeLayout;
        RelativeLayout audioRelative;

        MyViewHolder(View itemView) {
            super(itemView);
            mMessage = itemView.findViewById(R.id.message);
            mUsername = itemView.findViewById(R.id.username);
            time = itemView.findViewById(R.id.time);
            imageView = itemView.findViewById(R.id.imageView);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            audioRelative = itemView.findViewById(R.id.audioRelative);
            playButton = itemView.findViewById(R.id.playButton);
            pauseButton = itemView.findViewById(R.id.pauseButton);
            tickImageView = itemView.findViewById(R.id.tickImageView);
            mView = itemView;
        }

        void setmMessage(String message) {
            if (mMessage == null)
                return;
            mMessage.setText(message);
        }

        void setTime(String message) {
            if (time == null)
                return;
            time.setText(message);
        }
    }

    private class DateHolder extends RecyclerView.ViewHolder {

        private TextView dateTextView;

        private DateHolder(View view) {
            super(view);

            dateTextView = view.findViewById(R.id.dateTextView);

            Typeface typeface = getTypeFace(context);
            dateTextView.setTypeface(typeface);
        }
    }

    public SmsAdapter(ArrayList<MessageModel> messageModels, Context context) {
        this.messageModels = messageModels;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == -1) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatsmsdate, parent, false);
            return new DateHolder(itemView);
        } else if (viewType == 0) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_tem_sent, parent, false);
            return new MyViewHolder(itemView);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item_received, parent, false);
            return new MyViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        MessageModel messageModel = messageModels.get(position);

        if (messageModel.viewType == -1) {
            ((DateHolder) holder).dateTextView.setText(messageModel.message);
        } /*else if (messageModel.viewType == 0) {
            ((ReceiverHolder) holder).receiverTextView.setText(messageModel.message);
            ((ReceiverHolder) holder).dateTextView.setText(getDateFromTimeStamp(messageModel.date));
        } else {
            ((SenderHolder) holder).senderTextView.setText(messageModel.message);
            ((SenderHolder) holder).dateTextView.setText(getDateFromTimeStamp(messageModel.date));
        }*/ else {
            if (messageModel.message.contains(".3gp")) {
                ((MyViewHolder) holder).audioRelative.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).imageView.setVisibility(View.GONE);
                ((MyViewHolder) holder).relativeLayout.setVisibility(View.GONE);
            } else if (messageModel.message.contains(".jpg") ||
                    messageModel.message.contains(".png")) {
                ((MyViewHolder) holder).imageView.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).relativeLayout.setVisibility(View.GONE);
                ((MyViewHolder) holder).audioRelative.setVisibility(View.GONE);
                Glide.with(context).load(messageModel.message).into(((MyViewHolder) holder).imageView);
            } else {
                ((MyViewHolder) holder).relativeLayout.setVisibility(View.VISIBLE);
                ((MyViewHolder) holder).imageView.setVisibility(View.GONE);
                ((MyViewHolder) holder).audioRelative.setVisibility(View.GONE);
                ((MyViewHolder) holder).setmMessage(messageModel.message);
            }

            ((MyViewHolder) holder).audioRelative.setOnClickListener(v -> {
                if (((MyViewHolder) holder).pauseButton.getVisibility() == View.VISIBLE) {
                    ((MyViewHolder) holder).pauseButton.setVisibility(View.GONE);
                    ((MyViewHolder) holder).playButton.setVisibility(View.VISIBLE);

                    if (mp != null && mp.isPlaying()) {
                        mp.release();
                    }
                } else {
                    ((MyViewHolder) holder).pauseButton.setVisibility(View.VISIBLE);
                    ((MyViewHolder) holder).playButton.setVisibility(View.GONE);

                    mp = new MediaPlayer();
                    try {
                        mp.setDataSource(messageModel.message);
                        mp.prepare();
                        mp.start();

                        mp.setOnCompletionListener(mp -> {
                            mp.release();
                            ((MyViewHolder) holder).pauseButton.setVisibility(View.GONE);
                            ((MyViewHolder) holder).playButton.setVisibility(View.VISIBLE);
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            ((MyViewHolder) holder).setTime(messageModel.date);

            if (messageModel.isSMS){
                ((MyViewHolder)holder).relativeLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#F18314")));
            }else{
                ((MyViewHolder)holder).relativeLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#2BCDD9")));
            }
        }
    }

    @Override
    public int getItemCount() {
        return messageModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messageModels.get(position).viewType;
    }

    /*private class SenderHolder extends RecyclerView.ViewHolder {

        private TextView senderTextView;
        private TextView dateTextView;

        private SenderHolder(View view) {
            super(view);

            senderTextView = view.findViewById(R.id.senderTextView);
            dateTextView = view.findViewById(R.id.dateTextView);

            Typeface typeface = getTypeFace(context);
            senderTextView.setTypeface(typeface);
            dateTextView.setTypeface(typeface);
        }
    }

    private class ReceiverHolder extends RecyclerView.ViewHolder {

        private TextView receiverTextView;
        private TextView dateTextView;

        private ReceiverHolder(View view) {
            super(view);

            receiverTextView = view.findViewById(R.id.receiverTextView);
            dateTextView = view.findViewById(R.id.dateTextView);

            Typeface typeface = getTypeFace(context);
            receiverTextView.setTypeface(typeface);
            dateTextView.setTypeface(typeface);
        }
    }*/
}