package com.app.oozee.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.MessagesScreen;
import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.LocalContactsModel;
import com.app.oozee.models.Sms;
import com.app.oozee.utils.Utility;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.activities.MainActivity.getStringInitial;
import static com.app.oozee.database.MessagesDb.deleteSMS;
import static com.app.oozee.database.MessagesDb.updateMessage;
import static com.app.oozee.database.MessagesDb.updateMessageRead;
import static com.app.oozee.utils.Config.getDateFromTimeStampDate;
import static com.app.oozee.utils.Utility.getTypeFace;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ContactsHolder> {

    private List<Sms> contactsModels;
    private Context context;
    private int whichOption;
    private ArrayList<LocalContactsModel> localContactsModelsImagesNames;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView initialTextView;
        private TextView nameTextView;
        private TextView messageTextView;
        private TextView timeTextView;
        private TextView countTextView;
        private ImageView starIcon;
        private CircleImageView imageView;
        private RelativeLayout relativeLayout;
        private AdView adView;

        private ContactsHolder(View view) {
            super(view);

            initialTextView = view.findViewById(R.id.initialTextView);
            nameTextView = view.findViewById(R.id.nameTextView);
            messageTextView = view.findViewById(R.id.messageTextView);
            timeTextView = view.findViewById(R.id.timeTextView);
            countTextView = view.findViewById(R.id.countTextView);
            imageView = view.findViewById(R.id.imageView);
            starIcon = view.findViewById(R.id.starIcon);
            relativeLayout = view.findViewById(R.id.relativeLayout);

            adView = view.findViewById(R.id.adView);
            startAds(adView);

            Typeface typeface = getTypeFace(context);
            initialTextView.setTypeface(typeface);
            nameTextView.setTypeface(typeface);
            messageTextView.setTypeface(typeface);
            timeTextView.setTypeface(typeface);
        }
    }

    public MessagesAdapter(List<Sms> contactsModels, Context context, int whichOption,
                           ArrayList<LocalContactsModel> localContactsModelsImagesNames) {
        this.contactsModels = contactsModels;
        this.context = context;
        this.whichOption = whichOption;
        this.localContactsModelsImagesNames = localContactsModelsImagesNames;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_messages, parent, false);
        return new ContactsHolder(itemView);
    }

    public void setWhichOption(int whichOption) {
        this.whichOption = whichOption;
    }

    public void setDate(ArrayList<LocalContactsModel> localContactsModelsImagesNames) {
        this.localContactsModelsImagesNames = localContactsModelsImagesNames;
    }

    private void startAds(AdView mAdView) {
        //AdView mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ContactsHolder holder, int position) {
        final Sms contactsModel = contactsModels.get(position);

        if(position%4==0) {
            holder.adView.setVisibility(View.VISIBLE);
        }else{
            holder.adView.setVisibility(View.GONE);
        }

        if (whichOption == 1) {
            String name = getName(contactsModel.getAddress());
            String image = getImage(contactsModel.getAddress());
            if (name.equalsIgnoreCase("")) {
                holder.nameTextView.setText(contactsModel.getAddress());
                holder.initialTextView.setText(getStringInitial(contactsModel.getAddress()));
            } else {
                holder.nameTextView.setText(name);
                holder.initialTextView.setText(getStringInitial(name));
            }

            if (image != null && !image.equalsIgnoreCase("")) {
                Glide.with(context).load(image).fitCenter().into(holder.imageView);
                holder.initialTextView.setVisibility(View.GONE);
            } else {
                Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(holder.imageView);
                holder.initialTextView.setVisibility(View.VISIBLE);
            }
        } else {
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.circle)).fitCenter().into(holder.imageView);
            holder.initialTextView.setVisibility(View.VISIBLE);

            holder.initialTextView.setText(getStringInitial(contactsModel.getAddress()));
            holder.nameTextView.setText(contactsModel.getAddress());
        }

        if (contactsModel.isStarred == null || contactsModel.isStarred.equalsIgnoreCase("0")) {
            holder.starIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.starreddeselected));
            //not selected
        } else {
            holder.starIcon.setImageDrawable(context.getResources().getDrawable(R.mipmap.starredselected));
        }

        holder.messageTextView.setText(contactsModel.getMsg());
        holder.timeTextView.setText(getDateFromTimeStampDate(contactsModel.getTime()));

        if (contactsModel.getReadState().equalsIgnoreCase("0")) {
            holder.countTextView.setVisibility(View.VISIBLE);
            holder.countTextView.setText(contactsModel.sms.size() + "");
            holder.messageTextView.setTypeface(Utility.getTypeFacesemibold(context));
        } else {
            holder.countTextView.setVisibility(View.INVISIBLE);
            holder.messageTextView.setTypeface(Utility.getTypeFace(context));
        }

        if (contactsModels.size() == 1) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less));
        } else if (holder.getAdapterPosition() == 0) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_top));
        } else if (contactsModels.size() > (holder.getAdapterPosition() + 1)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
        } else {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_bottom));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactsModels.get(holder.getAdapterPosition()).setReadState("1");
                updateMessageRead(new DatabaseHelper(context), contactsModel.getId());
                Intent intent = new Intent(context, MessagesScreen.class);
                intent.putExtra("address", contactsModel.getAddress());
                intent.putExtra("smsid", contactsModel.getId());
                intent.putExtra("whichOption", whichOption);
                intent.putExtra("image", getImage(contactsModel.getAddress()));
                context.startActivity(intent);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                deletePopUpWindow(contactsModel, holder.getAdapterPosition());
                return true;
            }
        });

        holder.starIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contactsModel.isStarred == null || contactsModel.isStarred.equalsIgnoreCase("0")) {
                    showPopUpWindow(contactsModel, false, holder.getAdapterPosition());
                } else {
                    showPopUpWindow(contactsModel, true, holder.getAdapterPosition());
                }
            }
        });

        if (holder.getAdapterPosition() + 1 == contactsModels.size()) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.relativeLayout.getLayoutParams();
            params.setMargins(0, 0, 0, 80);
            holder.relativeLayout.setLayoutParams(params);
        } else {

        }
    }

    @Override
    public int getItemCount() {
        return contactsModels.size();
    }

    private void showPopUpWindow(final Sms contactsModel, final boolean isStarred, final int pos) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        Typeface typeface = getTypeFace(context);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        if (isStarred) {
            imageView.setText("Remove from Starred?");
            textView.setText("Once you click remove, the message will be removed from starred tab.");
            performTextView.setText("Remove");
        } else {
            imageView.setText("Add To Starred?");
            textView.setText("Once you click add, the message will be visible in starred tab as well.");
            performTextView.setText("Add");
        }

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isStarred) {
                    updateMessage(new DatabaseHelper(context), contactsModel.getId(), "0");
                    contactsModels.get(pos).isStarred = "0";
                } else {
                    updateMessage(new DatabaseHelper(context), contactsModel.getId(), "1");
                    contactsModels.get(pos).isStarred = "1";
                }
                notifyDataSetChanged();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!((Activity) context).isDestroyed()) {
            dialog.show();
        }
    }

    private void deletePopUpWindow(final Sms contactsModel, final int pos) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView imageView = dialog.findViewById(R.id.imageView);
        TextView textView = dialog.findViewById(R.id.textView);
        TextView cancelTextView = dialog.findViewById(R.id.cancelTextView);
        TextView performTextView = dialog.findViewById(R.id.performTextView);

        Typeface typeface = getTypeFace(context);
        imageView.setTypeface(typeface);
        cancelTextView.setTypeface(typeface);
        performTextView.setTypeface(typeface);
        textView.setTypeface(typeface);

        imageView.setText("Delete Message");
        textView.setText("Are you sure you want to delete message?");
        performTextView.setText("Delete");

        performTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteSMS(new DatabaseHelper(context), contactsModel.getId());
                deleteSms(contactsModel.getId());
                contactsModels.remove(pos);
                notifyDataSetChanged();
            }
        });

        cancelTextView.setOnClickListener(v -> dialog.dismiss());

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);

        if (!((Activity) context).isDestroyed()) {
            dialog.show();
        }
    }

    private void deleteSms(String smsId) {
        try {
            context.getContentResolver().delete(Uri.parse("content://sms/" + smsId), null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getName(String address) {
        for (int i = 0; i < localContactsModelsImagesNames.size(); i++) {
            if (address != null && address.contains(localContactsModelsImagesNames.get(i).dir)) {
                return localContactsModelsImagesNames.get(i).name;
            } else if (address != null && localContactsModelsImagesNames.get(i).dir.contains(address)) {
                return localContactsModelsImagesNames.get(i).name;
            }
        }
        return "";
    }

    private String getImage(String address) {
        for (int i = 0; i < localContactsModelsImagesNames.size(); i++) {
            if (address != null && address.contains(localContactsModelsImagesNames.get(i).dir)) {
                return localContactsModelsImagesNames.get(i).date;
            } else if (address != null && localContactsModelsImagesNames.get(i).dir.contains(address)) {
                return localContactsModelsImagesNames.get(i).date;
            }
        }
        return "";
    }
}