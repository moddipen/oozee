package com.app.oozee.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.ProfileScreen;
import com.app.oozee.models.LocalContactsModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.app.oozee.utils.Utility.getTypeFace;

public class SearchDialAdapter extends RecyclerView.Adapter<SearchDialAdapter.ContactsHolder> {

    ArrayList<LocalContactsModel> dialPadContacts;
    private Context context;
    private boolean isOozee;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView numberTextView;
        private CircleImageView imageView;

        private ContactsHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);
            numberTextView = view.findViewById(R.id.numberTextView);
            imageView = view.findViewById(R.id.imageView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
            numberTextView.setTypeface(typeface);
        }
    }

    public SearchDialAdapter(Context context, ArrayList<LocalContactsModel> dialPadContacts) {
        this.dialPadContacts = dialPadContacts;
        this.context = context;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_searchdial_contacts, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactsHolder holder, int position) {

        final LocalContactsModel searchContactModel = dialPadContacts.get(position);

        holder.nameTextView.setText(searchContactModel.name);
        holder.numberTextView.setText(searchContactModel.number);

        Glide.with(context).load(searchContactModel.image).
                placeholder(context.getResources().getDrawable(R.mipmap.personalselected)).
                fitCenter().into(holder.imageView);


        if (holder.getAdapterPosition() == 0 && !(dialPadContacts.size() > (holder.getAdapterPosition() + 1))) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less));
        } else if (holder.getAdapterPosition() == 0 && dialPadContacts.size() > (holder.getAdapterPosition() + 1)) {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_top));
        } else if (dialPadContacts.size() > (holder.getAdapterPosition() + 1)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
        } else {
            holder.itemView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_bottom));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileScreen.class);
                intent.putExtra("contactsModel", searchContactModel);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dialPadContacts.size();
    }
}