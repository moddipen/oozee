package com.app.oozee.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;

import java.util.ArrayList;

public class DialogListingAdapter extends RecyclerView.Adapter<DialogListingAdapter.InvoiceViewHolder> {

    private Context context;
    private ArrayList<String> strings;

    public DialogListingAdapter(Context context, ArrayList<String> strings) {
        this.context = context;
        this.strings = strings;
    }

    @NonNull
    @Override
    public InvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dialog_listing, parent, false);
        return new InvoiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InvoiceViewHolder holder, int position) {
        holder.textView.setText(strings.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogListing dialogListing = (DialogListing) context;
                dialogListing.sendData(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    class InvoiceViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        InvoiceViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

    public interface DialogListing {
        void sendData(int position);
    }
}
