package com.app.oozee.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.activities.BackupActivity;
import com.app.oozee.activities.BlockedContacts;
import com.app.oozee.activities.BlogsDetailsScreen;
import com.app.oozee.activities.SecurityActivity;
import com.app.oozee.models.BlogModels;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ContactsHolder> {

    private ArrayList<BlogModels> blogModels;
    private Context context;

    class ContactsHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;

        private ContactsHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
        }
    }

    public SettingsAdapter(ArrayList<BlogModels> blogModels, Context context) {
        this.blogModels = blogModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ContactsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings, parent, false);
        return new ContactsHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsHolder holder, final int position) {
        final BlogModels blogModel = blogModels.get(position);

        holder.nameTextView.setText(blogModel.title);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (blogModel.title.equalsIgnoreCase("Blocked Contacts")) {
                    Intent intent = new Intent(context, BlockedContacts.class);
                    context.startActivity(intent);
                } else if (blogModel.title.equalsIgnoreCase("Dead Contacts")) {
                    Intent intent = new Intent(context, BlockedContacts.class);
                    intent.putExtra("isDead", true);
                    context.startActivity(intent);
                } else if (blogModel.title.equalsIgnoreCase("Backup Contacts")) {
                    Intent intent = new Intent(context, BackupActivity.class);
                    context.startActivity(intent);
                } else if (blogModel.title.equalsIgnoreCase("Security")) {
                    Intent intent = new Intent(context, SecurityActivity.class);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, BlogsDetailsScreen.class);
                    intent.putExtra("blogdetails", blogModel);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogModels.size();
    }
}