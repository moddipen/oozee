package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.LocalContactsModel;

import java.util.ArrayList;

import static com.app.oozee.utils.Config.getDurationString;
import static com.app.oozee.utils.Utility.getTypeFace;

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.MyCallLog> {

    private ArrayList<LocalContactsModel> contactsModels;
    private Context context;

    class MyCallLog extends RecyclerView.ViewHolder {

        private ImageView callImageView;
        private TextView numberTextView;
        private TextView dateTextView;
        private TextView durationTextView;

        private MyCallLog(View view) {
            super(view);

            callImageView = view.findViewById(R.id.imageView);
            numberTextView = view.findViewById(R.id.numberTextView);
            dateTextView = view.findViewById(R.id.dateTextView);
            durationTextView = view.findViewById(R.id.durationTextView);

            Typeface typeface = getTypeFace(context);
            dateTextView.setTypeface(typeface);
            numberTextView.setTypeface(typeface);
            durationTextView.setTypeface(typeface);
        }
    }

    public CallLogAdapter(ArrayList<LocalContactsModel> contactsModels, Context context) {
        this.contactsModels = contactsModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyCallLog onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_calllogs, parent, false);
        return new MyCallLog(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCallLog holder, final int position) {
        LocalContactsModel contactsModel = contactsModels.get(position);

        holder.numberTextView.setText(contactsModel.number);
        holder.durationTextView.setText(getDurationString(Integer.parseInt(contactsModel.duration)));
        holder.dateTextView.setText(contactsModel.date);

        if (contactsModel.dir.equalsIgnoreCase("INCOMING")) {
            holder.callImageView.setImageDrawable(context.getDrawable(R.mipmap.incoming));
        } else if (contactsModel.dir.equalsIgnoreCase("MISSED")) {
            holder.callImageView.setImageDrawable(context.getDrawable(R.mipmap.missedcall));
        } else {
            holder.callImageView.setImageDrawable(context.getDrawable(R.mipmap.outgoing));
        }
    }

    @Override
    public int getItemCount() {
        return contactsModels.size();
    }
}