package com.app.oozee.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.TagModels;

import java.util.ArrayList;

public class ChipsAdapter extends RecyclerView.Adapter<ChipsAdapter.MyChipHolder> {

    private ArrayList<TagModels> tagModels;
    private Context context;
    private boolean isSubTag;
    private TextView textView;

    class MyChipHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        private MyChipHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.textView);
        }
    }

    public ChipsAdapter(ArrayList<TagModels> tagModels, Context context, boolean isSubTag) {
        this.tagModels = tagModels;
        this.context = context;
        this.isSubTag = isSubTag;
    }

    @NonNull
    @Override
    public MyChipHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_chips, parent, false);
        return new MyChipHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyChipHolder holder, int position) {
        TagModels tagModel = tagModels.get(position);

        holder.textView.setText(tagModel.name);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (textView != null) {
                    textView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less_gray));
                    textView.setTextColor(context.getResources().getColor(R.color.darkGrayColor2));
                }
                textView = holder.textView;

                holder.textView.setBackground(context.getResources().getDrawable(R.drawable.rounded_corners_less_primary));
                holder.textView.setTextColor(context.getResources().getColor(R.color.whiteColor));
                int tagId = 0;
                String name = "";
                for (int i = 0; i < tagModels.size(); i++) {
                    if (holder.textView.getText().toString().trim().equalsIgnoreCase(tagModels.get(i).name)) {
                        tagId = tagModels.get(i).id;
                        name = tagModels.get(i).name;
                        break;
                    }
                }
                TagSubTag tagSubTag = (TagSubTag) context;
                tagSubTag.tagSubtag(isSubTag, tagId, name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tagModels.size();
    }

    public interface TagSubTag {
        void tagSubtag(boolean isSubtag, int id, String name);
    }
}