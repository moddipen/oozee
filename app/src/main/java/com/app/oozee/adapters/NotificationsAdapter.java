package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.NotificationsModel;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    private ArrayList<NotificationsModel> notificationsModels;
    private Context context;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private View viewLine;

        private MyViewHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);
            viewLine = view.findViewById(R.id.viewLine);

            Typeface typeface = getTypeFace(context);
            nameTextView.setTypeface(typeface);
        }

    }

    public NotificationsAdapter(ArrayList<NotificationsModel> notificationsModels, Context context) {
        this.notificationsModels = notificationsModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notifications, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final NotificationsModel notificationsModel = notificationsModels.get(position);

        holder.nameTextView.setText(notificationsModel.title);

        if (notificationsModels.size() == (position + 1)) {
            holder.viewLine.setVisibility(View.GONE);
        } else {
            holder.viewLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return notificationsModels.size();
    }
}