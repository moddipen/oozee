package com.app.oozee.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oozee.R;
import com.app.oozee.models.TagModels;

import java.util.ArrayList;

import static com.app.oozee.utils.Utility.getTypeFace;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.MyCallLog> {

    private ArrayList<TagModels> tags;
    private Context context;

    class MyCallLog extends RecyclerView.ViewHolder {

        private TextView addTagTextView;
        private TextView tagTextView;

        private MyCallLog(View view) {
            super(view);

            addTagTextView = view.findViewById(R.id.addTagTextView);
            tagTextView = view.findViewById(R.id.tagTextView);

            Typeface typeface = getTypeFace(context);
            tagTextView.setTypeface(typeface);
            addTagTextView.setTypeface(typeface);
        }
    }

    public TagAdapter(ArrayList<TagModels> tags, Context context) {
        this.tags = tags;
        this.context = context;
    }

    @NonNull
    @Override
    public MyCallLog onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tags, parent, false);
        return new MyCallLog(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyCallLog holder, final int position) {
        TagModels tagModels = tags.get(position);

        if ((holder.getAdapterPosition() + 1) == tags.size()) {
            holder.addTagTextView.setVisibility(View.VISIBLE);
            holder.tagTextView.setVisibility(View.GONE);

            holder.addTagTextView.setOnClickListener(v -> {
                AddTagInterface addTagInterface = (AddTagInterface) context;
                addTagInterface.addTag();
            });
        } else {
            holder.tagTextView.setVisibility(View.VISIBLE);
            holder.addTagTextView.setVisibility(View.GONE);
            holder.tagTextView.setText(tagModels.name);
        }
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }

    public interface AddTagInterface {
        void addTag();
    }
}