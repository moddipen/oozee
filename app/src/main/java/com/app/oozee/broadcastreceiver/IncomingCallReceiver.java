package com.app.oozee.broadcastreceiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.app.oozee.blockednumber.BlockedNumber;
import com.app.oozee.blockednumber.BlockedNumberDao;
import com.app.oozee.blockednumber.BlockedNumberDatabase;
import com.app.oozee.utils.AsyncExecutorUtil;

import java.util.Optional;
import java.util.function.Predicate;

public class IncomingCallReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = IncomingCallReceiver.class.getSimpleName();

    @Override
    @SuppressLint({"MissingPermission"})
    public void onReceive(final Context context, Intent intent) {
        if (!TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(intent.getAction())) {
            Log.e(LOG_TAG, String.format("IncomingCallReceiver called with incorrect intent action: %s", intent.getAction()));
            return;
        }

        final String newState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        Log.d(LOG_TAG, String.format("Call state changed to %s", newState));

        if (TelephonyManager.EXTRA_STATE_RINGING.equals(newState)) {
            final String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            if (phoneNumber == null) {
                Log.d(LOG_TAG, "Ignoring call; for some reason every state change is doubled");
                return;
            }
            Log.i(LOG_TAG, String.format("Incoming call from %s", phoneNumber));

            final BlockedNumberDao blockedNumberDao = BlockedNumberDatabase.getInstance(context).blockedNumberDao();
            AsyncExecutorUtil.getInstance().getExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    Optional<BlockedNumber> match = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        match = blockedNumberDao.getAll().stream().filter(new Predicate<BlockedNumber>() {
                            @Override
                            public boolean test(BlockedNumber blockedNumber) {
                                return blockedNumber.getRegex().matcher(phoneNumber).find();
                            }
                        }).findAny();
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        if (!match.isPresent()) {
                            Log.i(LOG_TAG, "No blocked number matched");
                            return;
                        }
                        Log.i(LOG_TAG, String.format("Blocked number matched: %s", match.get().toFormattedString()));

                        TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            telecomManager.endCall();
                        }
                    }
                }
            });
        }
    }
}
