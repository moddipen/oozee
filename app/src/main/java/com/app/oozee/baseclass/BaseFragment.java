package com.app.oozee.baseclass;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.utils.ApiClient;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.MyProgressDialog;
import com.app.oozee.interfaces.OozeeInterface;
import com.app.oozee.utils.OozeePreferences;
import com.google.gson.JsonElement;

import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.dead;

public class BaseFragment extends Fragment {

    public void getContactsByType(Context context, int i, String tag) {
        if (context != null && !((Activity)context).isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call;
            if (blocked.ordinal() == i) {
                call = apiService.allServiceCall("blocked-contacts", header);
            } else if (dead.ordinal() == i) {
                call = apiService.allServiceCall("dead-contacts", header);
            } else {
                call = apiService.allServiceCall("quick-lists", header);
            }
            servicecall(context, call, tag);
        }
    }

    public void deleteContactsByType(Context context, int id, int i, String tag) {
        if (context != null && !((Activity)context).isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call;

            if (blocked.ordinal() == i) {
                call = apiService.deleteApiCall("blocked-contacts/" + id, header);
            } else if (dead.ordinal() == i) {
                call = apiService.deleteApiCall("dead-contacts/" + id, header);
            } else {
                call = apiService.deleteApiCall("quick-lists/" + id, header);
            }
            servicecall(context, call, tag);
        }
    }

    public void getUsersStatuses(Context context, BlockedContactModel blockedContactModel, String tag) {
        if (context != null && !((Activity)context).isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.getNotes("get-users-status", blockedContactModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    private ACProgressFlower myProgressDialog;

    private void servicecall(final Context context, Call<JsonElement> call, final String tag) {

        if (!tag.equalsIgnoreCase("searchCall") ||
                !tag.equalsIgnoreCase("userStatus")) {
            myProgressDialog = MyProgressDialog.show(context, "", "");
        }

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> jsonElement) {

                dismissProgressDialog(myProgressDialog);

                if (!((Activity) context).isDestroyed()) {
                    if (jsonElement.isSuccessful()) {
                        postData(jsonElement.body(), tag);
                    } else {
                        showToast("No Internet Connection");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                dismissProgressDialog(myProgressDialog);

                if (!((Activity) context).isDestroyed()) {
                    showToast("No Internet Connection");
                }
            }
        });
    }

    public void showToast(String message) {
        if (getActivity() != null && !getActivity().isDestroyed()) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void dismissProgressDialog(ACProgressFlower mProgressDialog) {
        if (getActivity() != null && !getActivity().isDestroyed() && mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void postData(JsonElement jsonElement, String tag) {
        BusModel busModel = new BusModel();
        busModel.jsonElement = jsonElement;
        busModel.serviceName = tag;
        BusManager.getInstance().post(busModel);
    }
}
