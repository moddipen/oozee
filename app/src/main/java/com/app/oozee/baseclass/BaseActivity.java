package com.app.oozee.baseclass;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.app.oozee.interfaces.OozeeInterface;
import com.app.oozee.models.BlockedContactModel;
import com.app.oozee.models.BusModel;
import com.app.oozee.models.CheckSignup;
import com.app.oozee.models.LoginModel;
import com.app.oozee.models.NotesModel;
import com.app.oozee.models.PlanModel;
import com.app.oozee.models.ProfileModel;
import com.app.oozee.models.RecordingModel;
import com.app.oozee.models.SearchContactModel;
import com.app.oozee.models.SettingsModel;
import com.app.oozee.models.SyncContactsModel;
import com.app.oozee.models.TagModels;
import com.app.oozee.models.UpdateProfileModel;
import com.app.oozee.models.VoiceMessageModel;
import com.app.oozee.utils.ApiClient;
import com.app.oozee.utils.BusManager;
import com.app.oozee.utils.MyProgressDialog;
import com.app.oozee.utils.OozeePreferences;
import com.google.gson.JsonElement;

import java.io.File;

import cc.cloudist.acplibrary.ACProgressFlower;
import eltos.simpledialogfragment.form.Check;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.oozee.baseclass.BaseActivity.ContactsList.blocked;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.dead;
import static com.app.oozee.baseclass.BaseActivity.ContactsList.multipleBlock;

public class BaseActivity extends AppCompatActivity {

    public static String MULTIPART_FORM_DATA = "multipart/form-data";

    public enum ContactsList {
        blocked,
        dead,
        quicklist,
        multipleBlock;
    }

    public void loginCall(Context context, LoginModel loginModel, String tag) {
        if (context != null && !isDestroyed()) {
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.loginCall("login", loginModel);
            servicecall(context, call, tag);
        }
    }

    public void checkSignUp(Context context, CheckSignup loginModel, String tag) {
        if (context != null && !isDestroyed()) {
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.checkSignUp("users-details-by-number", loginModel);
            servicecall(context, call, tag);
        }
    }

    public void getProfileUser(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("auth-user", header);
            servicecall(context, call, tag);
        }
    }

    public void updateProfileCall(Context context, UpdateProfileModel profileModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.updateCall("update-user-profile", profileModel, header);
            servicecall(context, call, tag);
        }
    }

    public void updateLocationCall(Context context, ProfileModel profileModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.updateCall("update-user-location", profileModel, header);
            servicecall(context, call, tag);
        }
    }

    public void updateSettingsCall(Context context, SettingsModel settingsModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.updateSettings("update-user-settings", settingsModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    public void getSettingsCall(Context context, SettingsModel settingsModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.updateSettings("get-user-settings", settingsModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    public void searchCall(Context context, SearchContactModel searchContact, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.searchContact("search", searchContact, header);
            servicecall(context, call, tag);
        }
    }

    public void updateUserPlan(Context context, PlanModel planModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.planModel("update-user-plan", planModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    public void getPlanHistory(Context context, PlanModel planModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.planModel("get-plan-history", planModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    public void getPlans(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("plans", header);
            servicecall(context, call, tag);
        }
    }

    public void getNotification(Context context, BlockedContactModel blockedContactModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addSingleContactToList("user-notifications", blockedContactModel,
                    header);
            servicecall(context, call, tag);
        }
    }

    public void addNote(Context context, NotesModel notesModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.notesModel("notes", notesModel, header);
            servicecall(context, call, tag);
        }
    }

    public void editNote(Context context, int id, NotesModel notesModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.editnotesModel("notes/" + id, notesModel, header);
            servicecall(context, call, tag);
        }
    }

    public void getBlogs(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("blogs", header);
            servicecall(context, call, tag);
        }
    }

    public void getNews(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("news", header);
            servicecall(context, call, tag);
        }
    }

    public void getCmsPages(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("cms-pages", header);
            servicecall(context, call, tag);
        }
    }

    public void getCountries(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("countries", header);
            servicecall(context, call, tag);
        }
    }

    public void getcontactDetails(Context context, BlockedContactModel blockedContacts, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addSingleContactToList("contact-details", blockedContacts, header);
            servicecall(context, call, tag);
        }
    }

    public void getNotes(Context context, BlockedContactModel blockedContactModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.getNotes("get-notes", blockedContactModel, header);
            servicecall(context, call, tag);
        }
    }

    public void getContactsByType(Context context, int i, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call;
            if (blocked.ordinal() == i) {
                call = apiService.allServiceCall("blocked-contacts", header);
            } else if (dead.ordinal() == i) {
                call = apiService.allServiceCall("dead-contacts", header);
            } else {
                call = apiService.allServiceCall("quick-lists", header);
            }
            servicecall(context, call, tag);
        }
    }

    public void addContactsByType(Context context, BlockedContactModel blockedContactModel,
                                  int i, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call;
            if (blocked.ordinal() == i) {
                call = apiService.addSingleContactToList("blocked-contacts", blockedContactModel, header);
            } else if (dead.ordinal() == i) {
                call = apiService.addSingleContactToList("dead-contacts", blockedContactModel, header);
            } else if (multipleBlock.ordinal() == i) {
                call = apiService.addSingleContactToList("multiple-blocked-contacts", blockedContactModel, header);
            } else {
                call = apiService.addSingleContactToList("quick-lists", blockedContactModel, header);
            }
            servicecall(context, call, tag);
        }
    }

    public void deleteContactsByType(Context context, int id, int i, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call;

            if (blocked.ordinal() == i) {
                call = apiService.deleteApiCall("blocked-contacts/" + id, header);
            } else if (dead.ordinal() == i) {
                call = apiService.deleteApiCall("dead-contacts/" + id, header);
            } else {
                call = apiService.deleteApiCall("quick-lists/" + id, header);
            }
            servicecall(context, call, tag);
        }
    }

    public void deleteNote(Context context, int id, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.deleteApiCall("notes/" + id, header);
            servicecall(context, call, tag);
        }
    }

    public void getTags(Context context, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("tags", header);
            servicecall(context, call, tag);
        }
    }

    public void getSubTags(Context context, int id, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.allServiceCall("sub-tags/" + id, header);
            servicecall(context, call, tag);
        }
    }

    public void addTag(Context context, TagModels tagModels, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addTagModel("add-number-tags", tagModels, header);
            servicecall(context, call, tag);
        }
    }

    public void addVoiceMessage(Context context, VoiceMessageModel voiceMessageModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addVoiceMessage("voice-messages", voiceMessageModel, header);
            servicecall(context, call, tag);
        }
    }

    public void addRecordings(Context context, RecordingModel voiceMessageModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addRecording("recordings", voiceMessageModel, header);
            servicecall(context, call, tag);
        }
    }

    public void getRecordings(Context context, RecordingModel voiceMessageModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addRecording("get-recordings", voiceMessageModel, header);
            servicecall(context, call, tag);
        }
    }

    public void addMedia(Context context, MultipartBody.Part body, RequestBody type, String tag) {
        if (context != null && !isDestroyed()) {
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.addMedia(body, type);
            servicecall(context, call, tag);
        }
    }

    public void syncContacts(Context context, SyncContactsModel syncContactsModel, String tag) {
        if (context != null && !isDestroyed()) {
            String header = OozeePreferences.getInstance().getPref().getString("bearertoken", "");
            OozeeInterface apiService = ApiClient.getClient().create(OozeeInterface.class);
            Call<JsonElement> call = apiService.syncContacts("sync-contacts", syncContactsModel, header);
            servicecall(context, call, tag);
        }
    }

    private ACProgressFlower myProgressDialog;

    private void servicecall(final Context context, Call<JsonElement> call, final String tag) {

        if (!tag.equalsIgnoreCase("searchCall") && !tag.equalsIgnoreCase("ignore") &&
                !tag.equalsIgnoreCase("addTagPopup") && !tag.equalsIgnoreCase("subtagCallPopUp")) {
            myProgressDialog = MyProgressDialog.show(context, "", "");
        }

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> jsonElement) {

                dismissProgressDialog(myProgressDialog);

                if (!((Activity) context).isDestroyed()) {
                    if (jsonElement.isSuccessful()) {
                        if (!tag.equalsIgnoreCase("ignore")) {
                            postData(jsonElement.body(), tag);
                        }
                    } else {
                        showToast("No Internet Connection");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
                dismissProgressDialog(myProgressDialog);

                if (!((Activity) context).isDestroyed()) {
                    showToast("No Internet Connection");
                }
            }
        });
    }

    private void postData(JsonElement jsonElement, String tag) {
        BusModel busModel = new BusModel();
        busModel.jsonElement = jsonElement;
        busModel.serviceName = tag;
        BusManager.getInstance().post(busModel);
    }

    public void showToast(String message) {
        if (!isDestroyed()) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void dismissProgressDialog(ACProgressFlower mProgressDialog) {
        if (!isDestroyed() && mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public static void showKeyboard(Activity activity) {
        if (activity != null && !activity.isDestroyed()) {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(),
                            InputMethodManager.SHOW_FORCED, 0);
                }
            }
        }
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && !activity.isDestroyed()) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void checkAndSetVisibility(View view, int i) {
        if (i == 1 && view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        } else if (i == 0 && view.getVisibility() != View.GONE) {
            view.setVisibility(View.GONE);
        }
    }

    @NonNull
    public MultipartBody.Part prepareFilePart(String fileUri) {
        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);
        return MultipartBody.Part.createFormData("media", file.getName(), requestFile);
    }
}
