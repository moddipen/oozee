package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.oozee.models.TagModels;

import java.util.ArrayList;

public class TagsDb {

    public static final String TABLE_NAME = "tags";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "tagname";
    public static final String COLUMN_TAGID = "tagId";

    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_TAGID + " INTEGER" + ")";

    public static void insertAllTags(DatabaseHelper databaseHelper, ArrayList<TagModels> tagModels) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for (TagModels ms : tagModels) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, ms.name);
            values.put(COLUMN_TAGID, ms.id);
            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public static ArrayList<TagModels> getAllTags(DatabaseHelper databaseHelper) {
        ArrayList<TagModels> tagModels = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                TagModels objSms = new TagModels();
                objSms.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                objSms.id = (cursor.getInt(cursor.getColumnIndex(COLUMN_TAGID)));
                tagModels.add(objSms);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return tagModels;
    }
}
