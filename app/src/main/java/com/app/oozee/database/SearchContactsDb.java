package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class SearchContactsDb {

    public static final String TABLE_NAME = "searchoozeecontacts";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_NUMBER = "number";

    public int id;
    public String name;
    public String number;
    public String image;

    // Create table SQL query
    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_IMAGE + " TEXT,"
                    + COLUMN_NUMBER + " TEXT" + ")";

    public SearchContactsDb() {
    }

    public SearchContactsDb(String name, String number, String image) {
        this.name = name;
        this.number = number;
        this.image = image;
    }

    public static void insertHistorySearch(SearchContactsDb contactsModels,
                                           DatabaseHelper databaseHelper) {
        if (getCount(databaseHelper, contactsModels.number) == 0) {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SearchContactsDb.COLUMN_NAME, contactsModels.name);
            values.put(SearchContactsDb.COLUMN_NUMBER, contactsModels.number);
            values.put(SearchContactsDb.COLUMN_IMAGE, contactsModels.image);
            db.insert(SearchContactsDb.TABLE_NAME, null, values);
            db.close();
        }
    }

    private static int getCount(DatabaseHelper databaseHelper, String number) {
        String countQuery = "SELECT  * FROM " + TABLE_NAME + " where number = " + number;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public static void deleteSearchHistory(DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.close();
    }

    public static ArrayList<SearchContactsDb> getAllSearchContacts(DatabaseHelper databaseHelper) {
        ArrayList<SearchContactsDb> contactsModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + SearchContactsDb.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SearchContactsDb contactsModel = new SearchContactsDb();
                contactsModel.id = (cursor.getInt(cursor.getColumnIndex(SearchContactsDb.COLUMN_ID)));
                contactsModel.name = (cursor.getString(cursor.getColumnIndex(SearchContactsDb.COLUMN_NAME)));
                if (contactsModel.name == null) {
                    contactsModel.name = "";
                }
                contactsModel.image = (cursor.getString(cursor.getColumnIndex(SearchContactsDb.COLUMN_IMAGE)));
                contactsModel.number = (cursor.getString(cursor.getColumnIndex(SearchContactsDb.COLUMN_NUMBER)));
                if (contactsModel.number == null) {
                    contactsModel.number = "";
                }
                contactsModels.add(contactsModel);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return contactsModels;
    }
}
