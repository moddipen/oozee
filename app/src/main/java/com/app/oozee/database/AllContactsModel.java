package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.oozee.models.LocalContactsModel;

import java.util.ArrayList;

public class AllContactsModel {

    public static final String TABLE_NAME = "oozeeallcontacts";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FNAME = "firstname";
    public static final String COLUMN_LNAME = "lastname";
    public static final String COLUMN_NUMBER = "number";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_RAW = "rawid";

    public int id;
    public String firstName;
    public String lastName;
    public String phoneNumber;
    public String email;
    public String profilePic;

    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_FNAME + " TEXT,"
                    + COLUMN_LNAME + " TEXT,"
                    + COLUMN_NUMBER + " TEXT,"
                    + COLUMN_EMAIL + " TEXT,"
                    + COLUMN_RAW + " TEXT,"
                    + COLUMN_IMAGE + " TEXT" + ")";

    public AllContactsModel() {
    }

    public AllContactsModel(int id, String firstName, String lastName,
                            String phoneNumber, String email, String profilePic) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.profilePic = profilePic;
    }

    public static void deleteContact(DatabaseHelper databaseHelper, String id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_RAW + "=?", new String[]{id});
        db.close();
    }

    public static ArrayList<LocalContactsModel> getAllContacs(DatabaseHelper databaseHelper) {
        ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String name = (cursor.getString(cursor.getColumnIndex(COLUMN_FNAME)));
                String email = (cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
                String image = (cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
                String number = (cursor.getString(cursor.getColumnIndex(COLUMN_NUMBER)));
                String rawId = (cursor.getString(cursor.getColumnIndex(COLUMN_RAW)));

                LocalContactsModel contactsModel = new LocalContactsModel(name, email, image, number, rawId);
                contactsModels.add(contactsModel);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return contactsModels;
    }

    public static void insertAllContacts(ArrayList<LocalContactsModel> contactsModels, DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for (int i = 0; i < contactsModels.size(); i++) {
            ContentValues values = new ContentValues();
            try {
                String number = contactsModels.get(i).number;
                number = number.replaceAll(" ", "");

                if (number.startsWith("+91") && number.length() > 10) {
                    number = number.replace("+91", "");
                }

                if (number.startsWith("91") && number.length() > 10) {
                    number = number.replace("91", "");
                }
                values.put(AllContactsModel.COLUMN_NUMBER, number);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                values.put(AllContactsModel.COLUMN_LNAME, contactsModels.get(i).name);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                values.put(AllContactsModel.COLUMN_FNAME, contactsModels.get(i).name);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                values.put(AllContactsModel.COLUMN_EMAIL, contactsModels.get(i).date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                values.put(AllContactsModel.COLUMN_IMAGE, contactsModels.get(i).dir);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                values.put(AllContactsModel.COLUMN_RAW, contactsModels.get(i).rawContactId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            db.insert(AllContactsModel.TABLE_NAME, null, values);
        }
        if (db.isOpen()) {
            db.close();
        }
    }

    public static String getImageFromNumber(DatabaseHelper databaseHelper, String phoneNumber) {
        String selectQuery = "SELECT  * FROM " + AllContactsModel.TABLE_NAME
                + " where number LIKE " + "'%" + phoneNumber + "%'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String image = "";
        if (cursor != null && !cursor.isClosed()) {
            cursor.moveToFirst();
            image = (cursor.getString(cursor.getColumnIndex(AllContactsModel.COLUMN_IMAGE)));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        if (db.isOpen()) {
            db.close();
        }

        if (image == null) {
            return "";
        }
        return image;
    }

    public static String getNameFromNumber(DatabaseHelper databaseHelper, String phoneNumber) {

        phoneNumber = phoneNumber.replaceAll(" ", "");
        phoneNumber = phoneNumber.replaceAll("-", "");

        if (phoneNumber.contains("+91") && phoneNumber.length() > 10) {
            phoneNumber = phoneNumber.replace("+91", "");
        }

        if (phoneNumber.contains("91") && phoneNumber.length() > 10) {
            phoneNumber = phoneNumber.replace("91", "");
        }

        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where number  LIKE " + "'%" + phoneNumber + "%'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String fName = "";
        String lName = "";
        if (cursor.moveToFirst()) {
            fName = (cursor.getString(cursor.getColumnIndex(COLUMN_FNAME)));
            lName = (cursor.getString(cursor.getColumnIndex(COLUMN_LNAME)));
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        if (db.isOpen()) {
            db.close();
        }

        if (fName == null) {
            fName = "";
        }

        if (lName == null) {
            lName = "";
        }
        return fName /*+ " " + lName*/;
    }

    public static String[] getNameandImageFromNumber(DatabaseHelper databaseHelper, String phoneNumber) {

        String[] values = new String[2];
        phoneNumber = phoneNumber.replaceAll(" ", "");
        phoneNumber = phoneNumber.replaceAll("-", "");

        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where number  LIKE " + "'%" + phoneNumber + "%'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String fName = "";
        String image = "";
        if (cursor.moveToFirst()) {
            fName = (cursor.getString(cursor.getColumnIndex(COLUMN_FNAME)));
            image = (cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        if (db.isOpen()) {
            db.close();
        }

        if (fName == null) {
            fName = "";
        }

        if (image == null) {
            image = "";
        }

        values[0] = fName;
        values[1] = image;
        return values;
    }

    public static ArrayList<LocalContactsModel> getMultipleImagesNumbers(DatabaseHelper databaseHelper, ArrayList<String> phoneNumbers) {
        ArrayList<LocalContactsModel> contactsModels = new ArrayList<>();

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < phoneNumbers.size(); i++) {
            String phoneNumber = phoneNumbers.get(i);

            /*phoneNumber = phoneNumber.replaceAll(" ", "");
            phoneNumber = phoneNumber.replaceAll("-", "");*/

            if (i == phoneNumbers.size() - 1) {
                stringBuilder.append("'").append(phoneNumber).append("'");
            } else {
                stringBuilder.append("'").append(phoneNumber).append("'").append(", ");
            }
        }

        String selectQuery = "SELECT  * FROM " + TABLE_NAME +
                " where number in (" + stringBuilder.toString() + ")";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String fName = (cursor.getString(cursor.getColumnIndex(COLUMN_FNAME)));
                String lName = (cursor.getString(cursor.getColumnIndex(COLUMN_LNAME)));
                String image = (cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
                String number = (cursor.getString(cursor.getColumnIndex(COLUMN_NUMBER)));

                if (fName == null) {
                    fName = "";
                }

                if (lName == null) {
                    lName = "";
                }

                if (image == null) {
                    image = "";
                }

                LocalContactsModel contactsModel = new LocalContactsModel(fName /*+ " " + lName*/, image, number);
                contactsModels.add(contactsModel);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        if (db.isOpen()) {
            db.close();
        }

        return contactsModels;
    }
}
