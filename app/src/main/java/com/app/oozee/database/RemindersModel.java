package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RemindersModel implements Parcelable {

    public static final String TABLE_NAME = "oozeereminders";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_CONTACTNAME = "contactname";
    public static final String COLUMN_CONTACTNUMBER = "contactnumber";
    public static final String COLUMN_DATE = "dates";
    public static final String COLUMN_ISREGULAR = "regular";
    public static final String COLUMN_DATETIME = "datetime";

    public int id;
    public String name;
    public String number;
    public String date;
    public String isRegular;
    public String title;
    public String notes;
    public long mid_trigger;

    public RemindersModel(int id, String name, String number, String date, String isRegular, String title, String notes,
                          long mid_trigger) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.date = date;
        this.isRegular = isRegular;
        this.title = title;
        this.notes = notes;
        this.mid_trigger = mid_trigger;
    }

    // Create table SQL query
    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_TITLE + " TEXT," + COLUMN_NOTES + " TEXT,"
                    + COLUMN_CONTACTNAME + " TEXT," + COLUMN_CONTACTNUMBER + " TEXT,"
                    + COLUMN_DATE + " TEXT,"
                    + COLUMN_DATETIME + " TEXT," + COLUMN_ISREGULAR + " TEXT" + ")";

    public RemindersModel() {
    }


    public static void insertReminder(RemindersModel remindersModel, DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(RemindersModel.COLUMN_TITLE, remindersModel.title);
        values.put(RemindersModel.COLUMN_NOTES, remindersModel.notes);
        values.put(RemindersModel.COLUMN_CONTACTNAME, remindersModel.name);
        values.put(RemindersModel.COLUMN_CONTACTNUMBER, remindersModel.number);
        values.put(RemindersModel.COLUMN_DATE, remindersModel.date);
        values.put(RemindersModel.COLUMN_ISREGULAR, remindersModel.isRegular);
        values.put(RemindersModel.COLUMN_DATETIME, remindersModel.mid_trigger);
        db.insert(RemindersModel.TABLE_NAME, null, values);
        db.close();
    }

    public static void updateReminder(RemindersModel remindersModel, DatabaseHelper databaseHelper,
                                      int id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(RemindersModel.COLUMN_TITLE, remindersModel.title);
        values.put(RemindersModel.COLUMN_NOTES, remindersModel.notes);
        values.put(RemindersModel.COLUMN_CONTACTNAME, remindersModel.name);
        values.put(RemindersModel.COLUMN_CONTACTNUMBER, remindersModel.number);
        values.put(RemindersModel.COLUMN_DATE, remindersModel.date);
        values.put(RemindersModel.COLUMN_ISREGULAR, remindersModel.isRegular);
        values.put(RemindersModel.COLUMN_DATETIME, remindersModel.mid_trigger);
        db.update(TABLE_NAME, values, "id = " + id, null);
        db.close();
    }

    public static void deleteReminder(DatabaseHelper databaseHelper, int id) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public static ArrayList<RemindersModel> getAllReminders(DatabaseHelper databaseHelper) {
        ArrayList<RemindersModel> remindersModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + RemindersModel.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                RemindersModel remindersModel = new RemindersModel();
                remindersModel.id = (cursor.getInt(cursor.getColumnIndex(RemindersModel.COLUMN_ID)));
                remindersModel.name = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_CONTACTNAME)));
                remindersModel.number = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_CONTACTNUMBER)));
                remindersModel.date = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_DATE)));
                remindersModel.isRegular = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_ISREGULAR)));
                remindersModel.title = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_TITLE)));
                remindersModel.notes = (cursor.getString(cursor.getColumnIndex(RemindersModel.COLUMN_NOTES)));
                remindersModel.mid_trigger = (cursor.getLong(cursor.getColumnIndex(RemindersModel.COLUMN_DATETIME)));
                remindersModels.add(remindersModel);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return remindersModels;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.number);
        dest.writeString(this.date);
        dest.writeString(this.isRegular);
        dest.writeString(this.title);
        dest.writeString(this.notes);
        dest.writeLong(this.mid_trigger);
    }

    protected RemindersModel(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.number = in.readString();
        this.date = in.readString();
        this.isRegular = in.readString();
        this.title = in.readString();
        this.notes = in.readString();
        this.mid_trigger = in.readLong();
    }

    public static final Creator<RemindersModel> CREATOR = new Creator<RemindersModel>() {
        @Override
        public RemindersModel createFromParcel(Parcel source) {
            return new RemindersModel(source);
        }

        @Override
        public RemindersModel[] newArray(int size) {
            return new RemindersModel[size];
        }
    };
}
