package com.app.oozee.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "oozeedatabase";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ContactsModel.CREATE_TABLE);
        db.execSQL(RemindersModel.CREATE_TABLE);
        db.execSQL(AllContactsModel.CREATE_TABLE);
        db.execSQL(CountryNameByPrefix.CREATE_TABLE);
        db.execSQL(CountryNameByDatabase.CREATE_TABLE);
        db.execSQL(TagsDb.CREATE_TABLE);
        db.execSQL(MessagesDb.CREATE_TABLE);
        db.execSQL(SearchContactsDb.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ContactsModel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + RemindersModel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + AllContactsModel.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CountryNameByPrefix.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CountryNameByDatabase.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MessagesDb.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TagsDb.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SearchContactsDb.TABLE_NAME);
        onCreate(db);
    }
}
