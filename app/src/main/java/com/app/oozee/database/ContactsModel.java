package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.oozee.models.SearchContactModel;

import java.util.ArrayList;

public class ContactsModel {

    public static final String TABLE_NAME = "oozeecontacts";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_NUMBER = "number";

    public int id;
    public String name;
    public String number;

    // Create table SQL query
    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT," + COLUMN_NUMBER + " TEXT" + ")";

    public ContactsModel() {
    }

    public ContactsModel(int id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    private static void insertAllContacts(ArrayList<ContactsModel> contactsModels, DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for (int i = 0; i < contactsModels.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(ContactsModel.COLUMN_NAME, contactsModels.get(i).name);
            values.put(ContactsModel.COLUMN_NUMBER, contactsModels.get(i).number);
            db.insert(ContactsModel.TABLE_NAME, null, values);
        }
        db.close();
    }

    public static void deleteHistory(DatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.close();
    }

    public static ArrayList<ContactsModel> getAllOozeeContacts(DatabaseHelper databaseHelper) {
        ArrayList<ContactsModel> contactsModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + ContactsModel.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ContactsModel contactsModel = new ContactsModel();
                contactsModel.id = (cursor.getInt(cursor.getColumnIndex(ContactsModel.COLUMN_ID)));
                contactsModel.name = (cursor.getString(cursor.getColumnIndex(ContactsModel.COLUMN_NAME)));
                if (contactsModel.name == null) {
                    contactsModel.name = "";
                }
                contactsModel.number = (cursor.getString(cursor.getColumnIndex(ContactsModel.COLUMN_NUMBER)));
                if (contactsModel.number == null) {
                    contactsModel.number = "";
                }
                contactsModels.add(contactsModel);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return contactsModels;
    }

    public static void checkIfExists(DatabaseHelper databaseHelper,
                                     ArrayList<SearchContactModel> newcontactsModels) {
        String countQuery = "SELECT  * FROM " + ContactsModel.TABLE_NAME;
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        ArrayList<ContactsModel> contactsModels = getAllOozeeContacts(databaseHelper);
        ArrayList<ContactsModel> dummycontactsModels = new ArrayList<>();

        for (int i = 0; i < contactsModels.size(); i++) {
            for (int k = 0; k < newcontactsModels.size(); k++) {

                String name = contactsModels.get(i).name;
                String number = contactsModels.get(i).number;

                if (name == null) {
                    name = "";
                }

                if (number == null) {
                    number = "";
                }
                name = name.toLowerCase();
                number = number.toLowerCase();

                if (!newcontactsModels.get(k).phone_number.equalsIgnoreCase(number) &&
                        !newcontactsModels.get(k).first_name.equalsIgnoreCase(name)) {
                    if (newcontactsModels.get(k).first_name != null &&
                            !newcontactsModels.get(k).first_name.equalsIgnoreCase("")) {
                        dummycontactsModels.add(new ContactsModel(0,
                                newcontactsModels.get(k).first_name, newcontactsModels.get(k).phone_number));
                    }
                }
            }
        }

        if (contactsModels.size() == 0) {
            for (int k = 0; k < newcontactsModels.size(); k++) {
                if (newcontactsModels.get(k).first_name != null &&
                        !newcontactsModels.get(k).first_name.equalsIgnoreCase("")) {
                    dummycontactsModels.add(new ContactsModel(0,
                            newcontactsModels.get(k).first_name, newcontactsModels.get(k).phone_number));
                }
            }
        }
        insertAllContacts(dummycontactsModels, databaseHelper);
        cursor.close();
    }
}
