package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.oozee.models.Sms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MessagesDb {

    public static final String TABLE_NAME = "smsMessages";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_SMSID = "_id";
    public static final String COLUMN_BODY = "body";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_READ = "read";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_COUNT = "count";
    public static final String COLUMN_DATETIME = "lastdatetime";
    public static final String COLUMN_ISSTARRED = "isstarred";

    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_ADDRESS + " TEXT,"
                    + COLUMN_SMSID + " TEXT,"
                    + COLUMN_BODY + " TEXT,"
                    + COLUMN_DATE + " TEXT,"
                    + COLUMN_READ + " TEXT,"
                    + COLUMN_COUNT + " TEXT,"
                    + COLUMN_ISSTARRED + " TEXT,"
                    + COLUMN_DATETIME + " LONG,"
                    + COLUMN_TYPE + " TEXT" + ")";

    public static void insertAllmessages(DatabaseHelper databaseHelper, List<Sms> sms) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for (Sms ms : sms) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ADDRESS, ms.getAddress());
            values.put(COLUMN_SMSID, ms.getId());
            values.put(COLUMN_BODY, ms.getMsg());
            values.put(COLUMN_DATE, ms.getTime());
            values.put(COLUMN_READ, ms.getReadState());
            values.put(COLUMN_TYPE, ms.getFolderName());
            values.put(COLUMN_ISSTARRED, "0");
            values.put(COLUMN_COUNT, ms.sms.size() + "");
            values.put(COLUMN_DATETIME, Calendar.getInstance().getTimeInMillis());
            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public static List<Sms> getAllSms(DatabaseHelper databaseHelper) {
        ArrayList<Sms> contactsModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " ORDER BY _id ASC";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Sms objSms = new Sms();
                objSms.setAddress(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                objSms.setId(cursor.getString(cursor.getColumnIndex(COLUMN_SMSID)));
                objSms.setMsg(cursor.getString(cursor.getColumnIndex(COLUMN_BODY)));
                objSms.setReadState(cursor.getString(cursor.getColumnIndex(COLUMN_READ)));
                objSms.setTime(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                objSms.setFolderName(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
                String count = cursor.getString(cursor.getColumnIndex(COLUMN_COUNT));
                objSms.isStarred = cursor.getString(cursor.getColumnIndex(COLUMN_ISSTARRED));
                int cn = Integer.parseInt(count);
                for (int i = 0; i < cn; i++) {
                    objSms.sms.add(new Sms());
                }
                contactsModels.add(objSms);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return contactsModels;
    }

    public static List<Sms> getStarredMessages(DatabaseHelper databaseHelper) {
        ArrayList<Sms> contactsModels = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where isstarred = '1'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Sms objSms = new Sms();
                objSms.setAddress(cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
                objSms.setId(cursor.getString(cursor.getColumnIndex(COLUMN_SMSID)));
                objSms.setMsg(cursor.getString(cursor.getColumnIndex(COLUMN_BODY)));
                objSms.setReadState(cursor.getString(cursor.getColumnIndex(COLUMN_READ)));
                objSms.setTime(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
                objSms.setFolderName(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
                String count = cursor.getString(cursor.getColumnIndex(COLUMN_COUNT));
                objSms.isStarred = cursor.getString(cursor.getColumnIndex(COLUMN_ISSTARRED));
                int cn = Integer.parseInt(count);
                for (int i = 0; i < cn; i++) {
                    objSms.sms.add(new Sms());
                }
                contactsModels.add(objSms);
            } while (cursor.moveToNext());
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        if (db.isOpen()) {
            db.close();
        }
        return contactsModels;
    }

    public static void updateMessageRead(DatabaseHelper databaseHelper, String id) {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("read", "1");
        sqLiteDatabase.update(TABLE_NAME, cv, "_id = " + id, null);
        sqLiteDatabase.close();
    }

    public static void updateMessage(DatabaseHelper databaseHelper, String id, String input) {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("isstarred", input);
        sqLiteDatabase.update(TABLE_NAME, cv, "_id = " + id, null);
        sqLiteDatabase.close();
    }

    public static void deleteSMS(DatabaseHelper databaseHelper, String name) {
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_NAME, COLUMN_SMSID + "=?", new String[]{name});
        sqLiteDatabase.close();
    }
}
