package com.app.oozee.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.oozee.database.DatabaseHelper;
import com.app.oozee.models.CountryModel;

import java.util.ArrayList;

public class CountryNameByDatabase {

    public static final String TABLE_NAME = "countryserverdatabase";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAMEID = "countryid";

    static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_NAMEID + " INTEGER,"
                    + COLUMN_CODE + " INTEGER" + ")";

    public static void insertAllCountriesDb(DatabaseHelper databaseHelper,
                                            ArrayList<CountryModel> countryModels) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        for (CountryModel countryModel : countryModels) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, countryModel.name.toLowerCase());
            values.put(COLUMN_CODE, countryModel.code);
            values.put(COLUMN_NAMEID, countryModel.id);
            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public static int getCountryId(DatabaseHelper databaseHelper, String countryName) {
        countryName = countryName.toLowerCase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " where name = " + "'" + countryName + "'";
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int countryId = 0;
        if (cursor.moveToFirst()) {
            countryId = cursor.getInt(cursor.getColumnIndex(COLUMN_NAMEID));
        }
        if (db.isOpen()) {
            db.close();
        }
        return countryId;
    }
}
